find_program(
    GLSL_LANG_VALIDATOR
    glslangValidator 
    DOC "glslangValidator for compiling the shaders"
    REQUIRED
)

function(shader_extension_to_name name extension)
  if (extension STREQUAL ".vert")
        set(${name} "vertex" PARENT_SCOPE)
    elseif (extension STREQUAL ".tesc")
        set(${name} "tesselation_control" PARENT_SCOPE)
    elseif (extension STREQUAL ".tese")
        set(${name} "tesselation_evaluation" PARENT_SCOPE)
    elseif (extension STREQUAL ".geom")
        set(${name} "geometry" PARENT_SCOPE)
    elseif (extension STREQUAL ".frag")
        set(${name} "fragment" PARENT_SCOPE)
	elseif (extension STREQUAL ".comp")
        set(${name} "compute" PARENT_SCOPE)
    else ()
        message(SEND_ERROR "Invalid extension: ${extension}")
    endif ()
endfunction()

function(add_shaders)
    set(options)
    set(oneValueArgs TARGET DIRECTORY)
    set(multiValueArgs INCLUDE_DIRECTORIES SOURCE_FILES)
    cmake_parse_arguments(ADD_SHADERS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(include_directories)
    set(potentially_included_files)
    # Do not add the files in the main directory
    # file(
    #   GLOB_RECURSE potentially_included_files 
    #   LIST_DIRECTORIES false
    #   CONFIGURE_DEPENDS
    #   "${ADD_SHADERS_DIRECTORY}/**"
    # )
    foreach (include_directory ${ADD_SHADERS_INCLUDE_DIRECTORIES})
        list(APPEND include_directories "-I${ADD_SHADERS_DIRECTORY}/${include_directory}")

        file(
          GLOB_RECURSE potentially_included_directory_files
          LIST_DIRECTORIES false
          CONFIGURE_DEPENDS
          "${ADD_SHADERS_DIRECTORY}/${include_directory}/**"
        )
        list(APPEND potentially_included_files ${potentially_included_directory_files})
    endforeach ()

    set(target_shaders)
    foreach (shader_file ${ADD_SHADERS_SOURCE_FILES})
        get_filename_component(file_directory ${shader_file} DIRECTORY)
        get_filename_component(file_name ${shader_file} NAME_WE)
        get_filename_component(file_extension ${shader_file} EXT)

        shader_extension_to_name(file_suffix ${file_extension})

        set(input_file "${ADD_SHADERS_DIRECTORY}/${shader_file}")
        set(output_file "${ADD_SHADERS_DIRECTORY}/binary/${file_name}_${file_suffix}.spirv")

        add_custom_command(
            OUTPUT ${output_file}
            COMMAND ${GLSL_LANG_VALIDATOR} -V ${include_directories} -o ${output_file} ${input_file}
            MAIN_DEPENDENCY ${input_file}
            DEPENDS ${potentially_included_files}
        )
        list(APPEND target_shaders ${output_file})
    endforeach ()

    add_custom_target(
      ${ADD_SHADERS_TARGET}-shaders
      DEPENDS ${target_shaders}
    )

    add_dependencies(${ADD_SHADERS_TARGET} ${ADD_SHADERS_TARGET}-shaders)
endfunction()
