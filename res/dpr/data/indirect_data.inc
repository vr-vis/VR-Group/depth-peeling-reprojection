#ifndef SHADER_INCLUDE_INDIRECT_DATA
#define SHADER_INCLUDE_INDIRECT_DATA

struct IndirectDomain
{
    uvec3 indirect_resolution;
    float cell_size;

    uvec3 geometry_resolution;
    uint padding1;

    vec3 min;
    uint padding2;

    vec3 max;
    uint padding3;
};

#endif