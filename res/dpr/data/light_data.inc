#ifndef SHADER_INCLUDE_LIGHT_DATA
#define SHADER_INCLUDE_LIGHT_DATA

#define LIGHT_TYPE_POINT 0
#define LIGHT_TYPE_DIRECTIONAL 1
#define LIGHT_TYPE_SPOT 2

#define MAX_LIGHT_SHADOW_MATRIX_COUNT 6 //At least 6 for point lights

struct LightParameter
{
    vec3 ambient; //where ambient has the unit watt/meter^2 * str
    float exposure;

    uint light_count;
};

struct LightData
{
    vec3 position;
    uint type;

    vec3 color;
    float outer_angle;

    vec3 direction;
    float inner_angle;

    vec2 near_size;
    uint type_index;
    uint padding;

    mat4 shadow_matrix[MAX_LIGHT_SHADOW_MATRIX_COUNT];
    mat4 inv_shadow_matrix[MAX_LIGHT_SHADOW_MATRIX_COUNT];
};

#endif