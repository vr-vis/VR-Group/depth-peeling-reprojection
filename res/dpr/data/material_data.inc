#ifndef SHADER_INCLUDE_MATERIAL_DATA
#define SHADER_INCLUDE_MATERIAL_DATA

//NOTE: Struct needs to be multiple of 256
struct MaterialData
{
    vec3 base_color;
    float opacity;

    vec3 emissive;
    float roughness;

    uvec3 padding1;
    float metallic;

    uvec4 padding2;
    mat4 padding3;
    mat4 padding4;
    mat4 padding5;
};

#endif