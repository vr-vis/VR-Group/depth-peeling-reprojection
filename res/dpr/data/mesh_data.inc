#ifndef SHADER_INCLUDE_MESH_DATA
#define SHADER_INCLUDE_MESH_DATA

//NOTE: Struct needs to be multiple of 256
struct MeshData
{
    mat4 localToWorldSpace;
    mat4 vectorToWorldSpace;
    mat4 padding1;
    mat4 padding2;
};

#endif