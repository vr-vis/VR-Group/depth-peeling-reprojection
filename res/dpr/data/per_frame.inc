#ifndef SHADER_INCLUDE_PER_FRAME_DATA
#define SHADER_INCLUDE_PER_FRAME_DATA

struct PerFrameData
{
    mat4 projection;
    mat4 headToEye;
    mat4 view;
    mat4 viewProjection;
    mat4 invView;
    mat4 invProjection;
    vec3 eyePosition;
};

#endif