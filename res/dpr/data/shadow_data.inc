#ifndef SHADER_INCLUDE_SHADOW_DATA
#define SHADER_INCLUDE_SHADOW_DATA

struct ShadowParameter
{
    uint use_directional;
    uint use_spot;
    uint use_point;
};

#endif