#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "dpr_parameters.inc"
layout(set = 4, binding = 0) uniform Frame {
  DprParameters params;
};

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;
layout(location = 4) in vec4 inClipPos;

layout(set = 1, binding = 0) uniform sampler2D samplerDiffuse;
layout(set = 1, binding = 1) uniform sampler2D samplerNormal;

layout(set = 3, binding = 1) uniform sampler2DArray samplerPreviousDepthBuffer;

layout(location = 0) out vec4 outFragColor;
// layout(location = 1) out vec4 outSecondLayer;

void main() {
    if (gl_Layer == 1) {
      vec4 clipPos = inClipPos / inClipPos.w;
      vec2 depthUV = clipPos.xy * 0.5 + 0.5;
      float depth = texture(samplerPreviousDepthBuffer, vec3(depthUV, 0)).r;
      /* if (clipPos.z <= depth + params.zThreshold) { */
      /*   discard; */
      /* } */
      float zNear = 0.1;
      float zFar = 256.0;
      float linearDepth = zNear * zFar / (zFar + depth * (zNear - zFar)); 
      // outFragColor = vec4(vec3(pow(depth, 64.0)), 1.0);
    }

    vec3 vertexNormal = normalize(inNormal);
    vec3 vertexTangent = normalize(inTangent);
    vec3 vertexBitangent = cross(vertexTangent, vertexNormal);
    mat3 tangentToWorldSpace = mat3(vertexTangent, vertexBitangent, vertexNormal);
    vec3 normalMapNormal = texture(samplerNormal, inUV).xyz * 2.0 - 1.0;
    normalMapNormal.z = 1.0 - sqrt(normalMapNormal.x * normalMapNormal.x + normalMapNormal.y * normalMapNormal.y);
    vec3 normal = tangentToWorldSpace * normalMapNormal;
    vec4 diffuse = texture(samplerDiffuse, inUV);
    float nDotL = dot(normal, normalize(vec3(0.1, 0.14, 1.0)));

    /* if (gl_Layer == 0) { */
    outFragColor = vec4(max(nDotL, 0.23) * diffuse.rgb, diffuse.a);
    /* } */

    // outFragColor = vec4(1.0);

}
