#version 450 core

#extension GL_ARB_viewport_array : enable

layout (triangles, invocations = 2) in;
layout (triangle_strip, max_vertices = 3) out;

layout(location = 0) in vec3 inPos[];
layout(location = 1) in vec3 inNormal[];
layout(location = 2) in vec3 inTangent[];
layout(location = 3) in vec2 inUV[];
layout(location = 4) in vec4 inClipPos[];

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTangent;
layout(location = 3) out vec2 outUV;
layout(location = 4) out vec4 outClipPos;

in gl_PerVertex {
    vec4 gl_Position;
} gl_in[];

void main() {
    for (int i = 0; i < 3; ++i) {
        outPos = inPos[i];
        outNormal = inNormal[i];
        outTangent = inTangent[i];
        outUV = inUV[i];
        outClipPos = inClipPos[i];
        gl_Position = gl_in[i].gl_Position;
        gl_Layer = gl_InvocationID;
        EmitVertex();
    }
    EndPrimitive();
}
