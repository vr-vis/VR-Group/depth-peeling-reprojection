#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "mesh_data.inc"
#include "per_frame.inc"

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(set = 0, binding = 0) uniform Frame {
    PerFrameData perFrame;
};
layout(set = 2, binding = 0) uniform Mesh {
    MeshData meshData;
};

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTangent;
layout(location = 3) out vec2 outUV;
layout(location = 4) out vec4 outClipPos;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    vec4 screenSpacePos = perFrame.viewProjection * (meshData.localToWorldSpace * vec4(inPos, 1.0));
    gl_Position = screenSpacePos;
    outClipPos = screenSpacePos;
    outPos = inPos;
    outNormal = normalize((meshData.localToWorldSpace * vec4(inNormal, 0.0)).xzy);
    outTangent = inTangent;
    outUV = inUV;
}
