#version 450 core

layout(location = 0) in vec2 inUV;

layout(binding = 0) uniform sampler2D samplerLeftEye;
layout(binding = 1) uniform sampler2D samplerRightEye;

layout(location = 0) out vec4 outFragColor;

void main() {
    outFragColor = inUV.x < 1.0 ? texture(samplerLeftEye, inUV) : texture(samplerRightEye, inUV - vec2(1.0, 0.0));
}
