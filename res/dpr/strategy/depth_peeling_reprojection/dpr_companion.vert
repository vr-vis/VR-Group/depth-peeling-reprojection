#version 450 core

layout(location = 0) in vec3 inPos;

layout(location = 0) out vec2 outUV;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    gl_Position = vec4(inPos.xyz, 1.0);
    outUV = inPos.xy * vec2(1.0, 0.5) + vec2(1.0, 0.5);
}
