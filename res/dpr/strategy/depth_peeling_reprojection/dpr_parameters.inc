struct DprParameters {
    mat4 left_to_right_eye;
    float z_min;
    float z_max;
    float zThreshold;
    float discard_threshold;
    int tesselation_level;
};
