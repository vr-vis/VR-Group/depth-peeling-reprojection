#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "dpr_parameters.inc"
layout(set = 0, binding = 0) uniform Frame {
  DprParameters params;
};
layout(location = 0) in vec2 inUV;
layout(location = 1) in flat int inInstanceIndex;

layout(location = 0) out vec4 outFragColor;

layout(binding = 2) uniform sampler2DArray samplerLeftEye;

void main() {
    const vec2 dx = abs(dFdx(inUV));
    if (max(dx.x, dx.y) < params.discard_threshold) {
        discard;
    }
    outFragColor = texture(samplerLeftEye, vec3(inUV, 1.0 - inInstanceIndex));
    /* outFragColor = vec4(dx * 100, 0.0, 1.0); */
}
