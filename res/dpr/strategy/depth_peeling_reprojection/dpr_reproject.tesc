#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "dpr_parameters.inc"
layout(set = 0, binding = 0) uniform Frame {
  DprParameters params;
};

layout (vertices = 4) out;

layout(location = 0) in vec2 inPos[];
layout(location = 0) out vec2 outPos[];
layout(location = 1) in int inInstanceIndex[];
layout(location = 1) out int outInstanceIndex[];

void main() {
  outPos[gl_InvocationID] = inPos[gl_InvocationID];
  outInstanceIndex[gl_InvocationID] = inInstanceIndex[gl_InvocationID];

  const float tessFactor = params.tesselation_level;
  gl_TessLevelOuter[0] = tessFactor;
  gl_TessLevelOuter[1] = tessFactor;
  gl_TessLevelOuter[2] = tessFactor;
  gl_TessLevelOuter[3] = tessFactor;
  gl_TessLevelInner[0] = tessFactor;
  gl_TessLevelInner[1] = tessFactor;
}

