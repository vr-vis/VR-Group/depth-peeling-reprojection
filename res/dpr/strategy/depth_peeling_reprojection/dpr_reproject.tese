#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "dpr_parameters.inc"

layout(set = 0, binding = 0) uniform Frame {
  DprParameters params;
};
layout(binding = 1) uniform sampler2DArray samplerDepthLeftEye;

layout (quads, equal_spacing, ccw) in;

layout(location = 0) in vec2 inPos[];
layout(location = 0) out vec2 outUV;
layout(location = 1) in int inInstanceIndex[];
layout(location = 1) out int outInstanceIndex;

void main() {
  const vec2 oldPosition =
    mix(
      mix(inPos[0], inPos[3], gl_TessCoord.x),
      mix(inPos[1], inPos[2], gl_TessCoord.x),
      gl_TessCoord.y
    );

  outUV = oldPosition;
  const float depth = clamp(texture(samplerDepthLeftEye, vec3(oldPosition, 1.0 - inInstanceIndex[0])).r, params.z_min, params.z_max);
  gl_Position = params.left_to_right_eye * vec4(oldPosition * 2.0 - 1.0, depth, 1.0);
  gl_Position /= gl_Position.w;
  outInstanceIndex = inInstanceIndex[0];
}


