#version 450 core
#extension GL_GOOGLE_include_directive : require

layout(location = 0) in vec2 inPos;
layout(location = 0) out vec2 outPos;
layout(location = 1) out int instanceIndex;

void main() {
    outPos = inPos;
    instanceIndex = gl_InstanceIndex;
}
