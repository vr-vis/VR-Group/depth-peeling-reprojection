#version 450 core
#extension GL_GOOGLE_include_directive : require
#extension GL_EXT_multiview : require

#define MATERIAL_DESCRIPTOR_SET 3
#define LIGHT_DESCRIPTOR_SET 4

#include "multi_view_stereo_base.glsl"