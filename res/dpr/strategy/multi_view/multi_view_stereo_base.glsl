#include "math_library.glsl"
#include "material_library.glsl"
#include "light_library.glsl"

#include "per_frame.inc"

layout(location = 0) out vec4 outFragColor;

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(set = 0, binding = 0) uniform LeftFrame 
{
    PerFrameData left_frame;
};

layout(set = 1, binding = 0) uniform RightFrame
{
    PerFrameData right_frame;
};

void main()
{
    Material material = lookup_material(inUV);
    
    if (material.opacity < EPSILON)
    {
        discard;
    }

    vec3 eye_position = vec3(0.0);

    if (gl_ViewIndex == 0) //Left Eye
    {
        eye_position = left_frame.eyePosition;
    }

    else if (gl_ViewIndex == 1) //Right Eye
    {
        eye_position = right_frame.eyePosition;
    }
    
    vec3 normal = lookup_normal(inUV, inNormal, inTangent);
    vec3 lighting = apply_lighting(eye_position, inPos, normal, material);

    outFragColor = vec4(lighting, 1.0);
}
