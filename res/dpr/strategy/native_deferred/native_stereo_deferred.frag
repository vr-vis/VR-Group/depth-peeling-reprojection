#version 450 core
#extension GL_GOOGLE_include_directive : require

#define MATERIAL_DESCRIPTOR_SET 1

#define COLOR_BUFFER_LOCATION 0
#define NORMAL_BUFFER_LOCATION 1
#define MATERIAL_BUFFER_LOCATION 2

#include "math_library.glsl"
#include "material_library.glsl"
#include "geometry_buffer_library.glsl"

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

void main()
{
    Material material = lookup_material(inUV);
    
    if (material.opacity < EPSILON)
    {
        discard;
    }
    
    vec3 normal = lookup_normal(inUV, inNormal, inTangent);
    
    write_to_buffer(normal, material);
}