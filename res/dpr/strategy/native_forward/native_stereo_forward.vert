#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "mesh_data.inc"
#include "per_frame.inc"
#include "math_library.glsl"

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTangent;
layout(location = 3) out vec2 outUV;

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(set = 0, binding = 0) uniform Frame
{
    PerFrameData frame;
};

layout(set = 2, binding = 0) uniform Mesh
{
    MeshData meshData;
};

void main()
{
    outPos = vec3(meshData.localToWorldSpace * vec4(inPos, 1.0));
    outNormal = normalize((meshData.vectorToWorldSpace * vec4(inNormal, 0.0)).xyz);
    outTangent = normalize((meshData.vectorToWorldSpace * vec4(inTangent, 0.0)).xyz);
    outUV = inUV;

    gl_Position = convert_right_handed(frame.viewProjection * vec4(outPos, 1.0));
}
