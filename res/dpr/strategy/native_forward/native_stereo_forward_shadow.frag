#version 450 core
#extension GL_GOOGLE_include_directive : require

#define MATERIAL_DESCRIPTOR_SET 1
#define LIGHT_DESCRIPTOR_SET 3
#define SHADOW_DESCRIPTOR_SET 4

#include "native_stereo_forward_base.glsl"