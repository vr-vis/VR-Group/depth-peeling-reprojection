#version 450 core
#extension GL_GOOGLE_include_directive : require

layout(location = 0) out vec2 outCoord;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    outCoord = vec2(gl_VertexIndex % 2, gl_VertexIndex / 2);
    gl_Position = vec4(outCoord * 2.0 - 1.0, 0.0, 1.0);
}