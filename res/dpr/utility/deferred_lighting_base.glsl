#include "math_library.glsl"
#include "light_library.glsl"
#include "per_frame.inc"

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 inCoord;

layout(set = 0, binding = 0) uniform sampler2D samplerDepthBuffer;
layout(set = 0, binding = 1) uniform sampler2D samplerColorBuffer;
layout(set = 0, binding = 2) uniform sampler2D samplerNormalBuffer;
layout(set = 0, binding = 3) uniform sampler2D samplerMaterialBuffer;

layout(set = 2, binding = 0) uniform PerFrameBuffer
{
    PerFrameData per_frame;
};

void main()
{
    float buffer_depth = texture(samplerDepthBuffer, inCoord).x;
    vec4 buffer_color = texture(samplerColorBuffer, inCoord);
    vec4 buffer_normal = texture(samplerNormalBuffer, inCoord);
    vec4 buffer_material = texture(samplerMaterialBuffer, inCoord);

    vec4 surface_position = per_frame.invProjection * (vec4(inCoord * 2.0 - 1.0, buffer_depth, 1.0));
    surface_position /= surface_position.w;
    surface_position = per_frame.invView * surface_position;

    vec3 surface_normal = decode_normal(buffer_normal.xy);

    Material material;
    material.base_color = buffer_color.xyz;
    material.occlusion = buffer_color.w;
    material.roughness = buffer_material.x;
    material.metallic = buffer_material.y;
    material.emissive.xy = unpackHalf2x16(packUnorm2x16(buffer_normal.zw));
    material.emissive.z = unpackHalf2x16(packUnorm4x8(buffer_material.zwzw)).x;
    
    vec3 lighting = apply_lighting(per_frame.eyePosition, surface_position.xyz, surface_normal, material);
    outColor = vec4(lighting, 1.0);
}