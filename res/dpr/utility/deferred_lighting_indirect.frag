#version 450 core
#extension GL_GOOGLE_include_directive : require

#define LIGHT_DESCRIPTOR_SET 1
#define INDIRECT_DESCRIPTOR_SET 3

#include "deferred_lighting_base.glsl"