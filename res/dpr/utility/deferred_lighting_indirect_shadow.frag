#version 450 core
#extension GL_GOOGLE_include_directive : require

#define LIGHT_DESCRIPTOR_SET 1
#define SHADOW_DESCRIPTOR_SET 3
#define INDIRECT_DESCRIPTOR_SET 4

#include "deferred_lighting_base.glsl"