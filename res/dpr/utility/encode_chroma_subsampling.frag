#version 450 core

layout(location = 0) out vec2 outChroma;

layout(set = 0, binding = 0) uniform sampler2D samplerInput;

void main()
{
    ivec2 base_coord = 2 * ivec2(gl_FragCoord.xy);

    vec2 color_chroma = vec2(0.0, 0.0);
    color_chroma += texelFetch(samplerInput, base_coord + ivec2(0, 0), 0).xy;
    color_chroma += texelFetch(samplerInput, base_coord + ivec2(1, 0), 0).xy;
    color_chroma += texelFetch(samplerInput, base_coord + ivec2(0, 1), 0).xy;
    color_chroma += texelFetch(samplerInput, base_coord + ivec2(1, 1), 0).xy;
    color_chroma /= 4.0;

    outChroma = color_chroma.xy;
}