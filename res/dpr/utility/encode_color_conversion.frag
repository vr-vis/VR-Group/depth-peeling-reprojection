#version 450 core

layout(location = 0) out float outLuma;
layout(location = 1) out vec2 outChroma;

layout(set = 0, binding = 0) uniform sampler2D samplerInput;

void main()
{
    //Conversion taken form https://en.wikipedia.org/wiki/YUV#SDTV_with_BT.470
    mat3 convert_matrix = mat3
    (
        vec3(0.299, -0.14713, 0.615), //first column
        vec3(0.587, -0.28886, -0.51499), //second column
        vec3(0.114, 0.436, -0.10001) //third column
    );

    float gamma = 2.2;
    float u_max = 0.436;
    float v_max = 0.615;

    ivec2 coord = ivec2(gl_FragCoord.xy);

    vec3 color_rgb_linear = texelFetch(samplerInput, coord, 0).xyz;
    vec3 color_rgb_gamma = pow(color_rgb_linear, vec3(1.0 / gamma)); //Slight change in brighness when compared to emulated headset. Maybe due to different gamma compression when using sRGB.
    vec3 color_yuv = convert_matrix * color_rgb_gamma;

    outLuma = color_yuv.x;
    outChroma.x = ((color_yuv.y / u_max) + 1.0) / 2.0;
    outChroma.y = ((color_yuv.z / v_max) + 1.0) / 2.0;
}
