/*
  Before including the geometry-buffer-library it is neccessary to set the locations of the individual color buffers of the geometry.
  After that, the function write_to_buffer(...) can be used to write a normal and a material to the geometry-buffer.
  Example:
    
    #define COLOR_BUFFER_LOCATION 0
    #define NORMAL_BUFFER_LOCATION 1
    #define MATERIAL_BUFFER_LOCATION 2
    #include "geometry_buffer_library.glsl"

    void main()
    {        
        //... lookup material and lookup normal from normal map

        write_to_buffer(normal, material);
    }
*/

#ifndef SHADER_INCLUDE_GEOMETRY_BUFFER_LIBRARY
#define SHADER_INCLUDE_GEOMETRY_BUFFER_LIBRARY

#ifndef COLOR_BUFFER_LOCATION
    #error "The color buffer location is not specified"
#endif

#ifndef NORMAL_BUFFER_LOCATION
    #error "The normal buffer location is not specified"
#endif

#ifndef MATERIAL_BUFFER_LOCATION
    #error "The material buffer location is not specified"
#endif

#include "math_library.glsl"
#include "material_struct.glsl"

layout(location = COLOR_BUFFER_LOCATION) out vec4 outColorBuffer;
layout(location = NORMAL_BUFFER_LOCATION) out vec4 outNormalBuffer;
layout(location = MATERIAL_BUFFER_LOCATION) out vec4 outMaterialBuffer;

void write_to_buffer(vec3 surface_normal, Material material)
{
    vec2 emissive1 = unpackUnorm2x16(packHalf2x16(material.emissive.xy));
    vec4 emissive2 = unpackUnorm4x8(packHalf2x16(material.emissive.zz));

    outColorBuffer.xyz = material.base_color;
    outColorBuffer.w = material.occlusion;

    outNormalBuffer.xy = encode_normal(surface_normal);
    outNormalBuffer.zw = emissive1;

    outMaterialBuffer.x = material.roughness;
    outMaterialBuffer.y = material.metallic;
    outMaterialBuffer.zw = emissive2.xy;
}

#endif