#version 450 core
#extension GL_GOOGLE_include_directive : require

#define MATERIAL_DESCRIPTOR_SET 2

#include "light_data.inc"
#include "math_library.glsl"
#include "material_library.glsl"

layout(location = 0) out vec4 outFlux;
layout(location = 1) out vec2 outNormal;

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(set = 1, binding = 0) uniform LightParameterBuffer
{
    LightParameter light_parameter;
};

layout(set = 1, binding = 1, std430) readonly buffer LightDataBuffer
{
    LightData lights[];
};

layout(push_constant) uniform Constants
{
    uint light_index;
    uint capture_resolution;
};

float comnpute_pixel_area()
{
    vec2 near_size = lights[light_index].near_size;
    vec2 pixel_size = near_size / float(capture_resolution);

    return pixel_size.x * pixel_size.y; //NOTE: Where pixel_size is in meters
}

void main()
{
    vec4 base_color_opacity = lookup_base_color_opacity(inUV);
    
    if (base_color_opacity.w < EPSILON)
    {
        discard;
    }

    vec3 normal = lookup_normal(inUV, inNormal, inTangent);

    outFlux = vec4(lights[light_index].color * base_color_opacity.xyz * comnpute_pixel_area(), 1.0); //NOTE: Where color is in watt/meter^2 and where outFlux is in watt
    outNormal = encode_normal(normal);
}