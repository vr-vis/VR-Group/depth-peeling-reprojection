#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "light_data.inc"
#include "mesh_data.inc"
#include "math_library.glsl"

layout(set = 0, binding = 0) uniform MeshBuffer
{
    MeshData mesh;
};

layout(set = 1, binding = 0) uniform LightParameterBuffer
{
    LightParameter light_parameter;
};

layout(set = 1, binding = 1, std430) readonly buffer LightDataBuffer
{
    LightData lights[];
};

layout(push_constant) uniform Constants
{
    uint light_index;
    uint capture_resolution;
};

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTangent;
layout(location = 3) out vec2 outUV;

void main()
{
    outPos = vec3(mesh.localToWorldSpace * vec4(inPos, 1.0));
    outNormal = normalize((mesh.vectorToWorldSpace * vec4(inNormal, 0.0)).xyz);
    outTangent = normalize((mesh.vectorToWorldSpace * vec4(inTangent, 0.0)).xyz);
    outUV = inUV;

    mat4 shadow_matrix = lights[light_index].shadow_matrix[0];
    gl_Position = shadow_matrix * vec4(outPos, 1.0);
}
