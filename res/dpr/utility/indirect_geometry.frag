#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "indirect_data.inc"
#include "indirect_library.glsl"

layout(set = 1, binding = 0) uniform IndirectDomainBuffer
{
    IndirectDomain indirect_domain;
};

layout(set = 1, binding = 1, r32ui) uniform uimage3D image_geometry_xy_distribution;
layout(set = 1, binding = 2, r32ui) uniform uimage3D image_geometry_zw_distribution;

layout(push_constant) uniform Constants
{
    uvec3 voxel_resolution;
};

layout(location = 0) in vec3 inCoord;
layout(location = 1) in vec3 inNormal;

#define atomicAdd(image, coord, value)                                                                       \
{                                                                                                            \
    uint new_value = packHalf2x16(value);                                                                    \
    uint previous_value = 0;                                                                                 \
    uint current_value = 0;                                                                                  \
                                                                                                             \
    while ((current_value = imageAtomicCompSwap(image, coord, previous_value, new_value)) != previous_value) \
    {                                                                                                        \
        previous_value = current_value;                                                                      \
        new_value = packHalf2x16(value + unpackHalf2x16(current_value));                                     \
    }                                                                                                        \
}

void main()
{
    ivec3 cell_coord = ivec3(inCoord * indirect_domain.geometry_resolution);

    vec3 normal = normalize(inNormal);
    vec4 distribution = spherical_harmonic_cosine_lobe(normal);

    vec3 coverage = vec3(indirect_domain.geometry_resolution) / vec3(voxel_resolution);
    float opacity = 0.0;

    if (gl_ViewportIndex == 0)
    {
        opacity = coverage.y * coverage.z;
    }

    else if (gl_ViewportIndex == 1)
    {
        opacity = coverage.x * coverage.z;
    }

    else
    {
        opacity = coverage.x * coverage.y;
    }

    //NOTE: Where opacity is in percent
    vec4 geometry_distribution = opacity * distribution;

    atomicAdd(image_geometry_xy_distribution, cell_coord, geometry_distribution.xy);
    atomicAdd(image_geometry_zw_distribution, cell_coord, geometry_distribution.zw);
}