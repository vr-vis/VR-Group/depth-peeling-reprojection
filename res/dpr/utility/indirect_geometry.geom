#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "indirect_data.inc"

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(set = 1, binding = 0) uniform IndirectDomainBuffer
{
    IndirectDomain indirect_domain;
};

layout(location = 0) in vec3 inNormal[];

layout(location = 0) out vec3 outCoord;
layout(location = 1) out vec3 outNormal;

void main()
{
    vec3 position1 = gl_in[0].gl_Position.xyz;
    vec3 position2 = gl_in[1].gl_Position.xyz;
    vec3 position3 = gl_in[2].gl_Position.xyz;

    vec3 coord1 = (position1 - indirect_domain.min) / (indirect_domain.max - indirect_domain.min);
    vec3 coord2 = (position2 - indirect_domain.min) / (indirect_domain.max - indirect_domain.min);
    vec3 coord3 = (position3 - indirect_domain.min) / (indirect_domain.max - indirect_domain.min);

    vec2 screen_coord1 = vec2(0.0);
    vec2 screen_coord2 = vec2(0.0);
    vec2 screen_coord3 = vec2(0.0);

    vec3 normal_abs = abs(cross(position2 - position1, position3 - position1));
    float normal_max = max(normal_abs.x, max(normal_abs.y, normal_abs.z));

    if (normal_abs.x == normal_max)
    {
        screen_coord1 = coord1.yz;
        screen_coord2 = coord2.yz;
        screen_coord3 = coord3.yz;

        gl_ViewportIndex = 0;
    }

    else if (normal_abs.y == normal_max)
    {
        screen_coord1 = coord1.xz;
        screen_coord2 = coord2.xz;
        screen_coord3 = coord3.xz;

        gl_ViewportIndex = 1;
    }

    else
    {
        screen_coord1 = coord1.xy;
        screen_coord2 = coord2.xy;
        screen_coord3 = coord3.xy;

        gl_ViewportIndex = 2;
    }

    outCoord = coord1;
    outNormal = inNormal[0];
    gl_Position = vec4(screen_coord1 * 2.0 - 1.0, 0.5, 1.0);
    EmitVertex();

    outCoord = coord2;
    outNormal = inNormal[1];
    gl_Position = vec4(screen_coord2 * 2.0 - 1.0, 0.5, 1.0);
    EmitVertex();

    outCoord = coord3;
    outNormal = inNormal[2];
    gl_Position = vec4(screen_coord3 * 2.0 - 1.0, 0.5, 1.0);
    EmitVertex();

    EndPrimitive();
}