#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "mesh_data.inc"

layout(set = 0, binding = 0) uniform MeshBuffer
{
    MeshData mesh;
};

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUV;

layout(location = 0) out vec3 outNormal;

void main()
{
    outNormal = normalize((mesh.vectorToWorldSpace * vec4(inNormal, 0.0)).xyz);

    gl_Position = mesh.localToWorldSpace * vec4(inPos, 1.0);
}
