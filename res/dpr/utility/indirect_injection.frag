#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "indirect_library.glsl"

layout(location = 0) out vec4 outRedDistribution;
layout(location = 1) out vec4 outGreenDistribution;
layout(location = 2) out vec4 outBlueDistribution;

layout(location = 0) in vec3 inFlux;
layout(location = 1) in vec3 inNormal;

void main()
{
    vec3 normal = normalize(inNormal);
    vec4 distribution = spherical_harmonic_cosine_lobe(normal);

    //NOTE: Where inFlux is in watt
    outRedDistribution = inFlux.x * distribution;   //NOTE: Additive blending
    outGreenDistribution = inFlux.y * distribution; //NOTE: Additive blending
    outBlueDistribution = inFlux.z * distribution;  //NOTE: Additive blending
}