#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "indirect_data.inc"

layout(points) in;
layout(points, max_vertices = 1) out;

layout(set = 1, binding = 3) uniform IndirectDomainBuffer
{
    IndirectDomain indirect_domain;
};

layout(location = 0) in vec3 inFlux[];
layout(location = 1) in vec3 inNormal[];

layout(location = 0) out vec3 outFlux;
layout(location = 1) out vec3 outNormal;

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz;
    position = (position - indirect_domain.min) / (indirect_domain.max - indirect_domain.min);
    position.xy = position.xy * 2.0 - 1.0;
    position.z = position.z * indirect_domain.indirect_resolution.z;

    outFlux = inFlux[0];
    outNormal = inNormal[0];
    gl_Position = vec4(position.xy, 0.5, 1.0);
    gl_Layer = int(position.z);

    EmitVertex();
    EndPrimitive();
}