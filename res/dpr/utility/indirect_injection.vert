#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "light_data.inc"
#include "indirect_data.inc"
#include "math_library.glsl"

layout(set = 0, binding = 0) uniform LightParameterBuffer
{
    LightParameter light_parameter;
};

layout(set = 0, binding = 1, std430) readonly buffer LightDataBuffer
{
    LightData lights[];
};

layout(set = 1, binding = 0) uniform sampler2D sampler_capture_depth;
layout(set = 1, binding = 1) uniform sampler2D sampler_capture_flux;
layout(set = 1, binding = 2) uniform sampler2D sampler_capture_normal;

layout(push_constant) uniform Constants
{
    uint light_index;
    uint capture_resolution;
    float cell_size;
};

layout(location = 0) out vec3 outFlux;
layout(location = 1) out vec3 outNormal;

void main()
{
    ivec2 coord = ivec2(gl_VertexIndex / capture_resolution, gl_VertexIndex % capture_resolution);

    float depth = texelFetch(sampler_capture_depth, coord, 0).x;
    vec3 flux = texelFetch(sampler_capture_flux, coord, 0).xyz;
    vec3 normal = decode_normal(texelFetch(sampler_capture_normal, coord, 0).xy);

    vec2 screen_coord = vec2(coord + 0.5) / vec2(capture_resolution);
    screen_coord = screen_coord * 2.0 - 1.0;

    vec4 position = lights[light_index].inv_shadow_matrix[0] * vec4(screen_coord, depth, 1.0);
    position /= position.w;

    //NOTE: Shift the virtual point light along the normal in order to avoid self lighting and shadowing
    position.xyz += 0.5 * cell_size * normal;

    outFlux = flux;
    outNormal = normal;
    gl_Position = position;
}
