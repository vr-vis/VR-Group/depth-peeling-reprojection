/*
  Before including the light-library it is neccessary to set the define LIGHT_DESCRIPTOR_SET.
  This define specifies the descriptor set in which the light information is stored.
  If the define is not set, an error is emitted.
  After including the light-library, the function apply_lighting(...) can be used to compute lighting.
  The used lighting model is physically based and was taken from the GLTF-Specification.
  When the light calculation should also include shadows, the define SHADOW_DESCRIPTOR_SET has to be set.
  This define specifies the descriptor set for the shadow cache.
  If also indirect lighting should be included, the define INDIRECT_DESCRIPTOR_SET has to be set.
  This define specifies the descriptor set for the indirect cache.
  Example:
  
    #define LIGHT_DESCRIPTOR_SET 42 //Required
    #define SHADOW_DESCRIPTOR_SET 43 //Optional
    #define INDIRECT_DESCRIPTOR_SET 44 //Optional
    #include "light_library.glsl"

    void main()
    {
        //... lookup material and lookup normal from normal map

        vec3 lighting = apply_lighting(view_position, surface_position, surface_normal, material);

        //... write to framebuffer
    }
*/

#ifndef SHADER_INCLUDE_LIGHT_LIBRARY
#define SHADER_INCLUDE_LIGHT_LIBRARY

#ifndef LIGHT_DESCRIPTOR_SET
    #error "The light descripotor-set is not specified"
#endif

#include "math_library.glsl"
#include "material_struct.glsl"
#include "indirect_library.glsl"
#include "light_data.inc"
#include "shadow_data.inc"
#include "indirect_data.inc"

layout(set = LIGHT_DESCRIPTOR_SET, binding = 0) uniform LightParameterBuffer
{
    LightParameter light_parameter;
};

layout(set = LIGHT_DESCRIPTOR_SET, binding = 1, std430) readonly buffer LightDataBuffer
{
    LightData lights[];
};

#ifdef SHADOW_DESCRIPTOR_SET
layout(set = SHADOW_DESCRIPTOR_SET, binding = 0) uniform sampler2DArrayShadow sampler_shadow_directional;
layout(set = SHADOW_DESCRIPTOR_SET, binding = 1) uniform sampler2DArrayShadow sampler_shadow_spot;
layout(set = SHADOW_DESCRIPTOR_SET, binding = 2) uniform samplerCubeArrayShadow sampler_shadow_point;

layout(set = SHADOW_DESCRIPTOR_SET, binding = 3) uniform ShadowParameterBuffer
{
    ShadowParameter shadow_parameter;
};
#endif

#ifdef INDIRECT_DESCRIPTOR_SET
layout(set = INDIRECT_DESCRIPTOR_SET, binding = 0) uniform sampler3D sampler_indirect_red;
layout(set = INDIRECT_DESCRIPTOR_SET, binding = 1) uniform sampler3D sampler_indirect_green;
layout(set = INDIRECT_DESCRIPTOR_SET, binding = 2) uniform sampler3D sampler_indirect_blue;

layout(set = INDIRECT_DESCRIPTOR_SET, binding = 3) uniform IndirectDomainBuffer
{
    IndirectDomain indirect_domain;
};
#endif

//-- Shadow Functions ------------------------------------------------------------------------------

#ifdef SHADOW_DESCRIPTOR_SET
uint get_shadow_frustum(uint light, vec3 surface_position)
{
    vec3 direction = surface_position - lights[light].position;
    vec3 absolut = abs(direction);
    uint frustum = 0;

    if (absolut.x > absolut.y && absolut.x > absolut.z)
    {
        frustum = 0;

        if (direction.x < 0.0)
        {
            frustum += 1;
        }
    }

    else if (absolut.y > absolut.z)
    {
        frustum = 2;

        if (direction.y < 0.0)
        {
            frustum += 1;
        }
    }

    else
    {
        frustum = 4;

        if (direction.z < 0.0)
        {
            frustum += 1;
        }
    }

    return frustum;
}

float get_shadow(uint light, vec3 surface_position)
{
    uint light_type = lights[light].type;

    if (light_type == LIGHT_TYPE_DIRECTIONAL && shadow_parameter.use_directional != 0)
    {
        uint layer = lights[light].type_index;

        vec4 shadow_position = lights[light].shadow_matrix[0] * vec4(surface_position, 1.0);
        shadow_position /= shadow_position.w;
        shadow_position.xy = (shadow_position.xy + 1.0) / 2.0;

        return texture(sampler_shadow_directional, vec4(shadow_position.xy, layer, shadow_position.z - 0.008)).x;
    }

    else if (light_type == LIGHT_TYPE_SPOT && shadow_parameter.use_spot != 0)
    {
        uint layer = lights[light].type_index;

        vec4 shadow_position = lights[light].shadow_matrix[0] * vec4(surface_position, 1.0);
        shadow_position /= shadow_position.w;
        shadow_position.xy = (shadow_position.xy + 1.0) / 2.0;

        return texture(sampler_shadow_spot, vec4(shadow_position.xy, layer, shadow_position.z - 0.001)).x;
    }
    
    else if (light_type == LIGHT_TYPE_POINT && shadow_parameter.use_point != 0)
    {
        uint layer = lights[light].type_index;
        uint frustum = get_shadow_frustum(light, surface_position);

        vec4 shadow_position = lights[light].shadow_matrix[frustum] * vec4(surface_position, 1.0);
        shadow_position /= shadow_position.w;

        vec3 shadow_direction = surface_position - lights[light].position;

        return texture(sampler_shadow_point, vec4(shadow_direction, layer), shadow_position.z - 0.001).x;
    }

    return 1.0;
}
#endif

//-- Light Functions -------------------------------------------------------------------------------

//This function computes the incomming radiance for a light-source.
//The final unit can be seen as incomming radiance (watts/meter^2 *str), eventough the final unit is in watts/meter^2.
//This is due to the fact, that the light source only contributes like a dirca impulse during the integration of the BRDF.
vec3 get_light_radiance(uint light, vec3 surface_position)
{
    uint light_type = lights[light].type;

    if (light_type == LIGHT_TYPE_POINT) //TODO: add a scaling factor for the dimension of the scene
    {
        vec3 light_direction = (lights[light].position - surface_position); //where light_direction is in meters
        vec3 light_color = lights[light].color; //where light_color is in watts
        float light_attenuation = (4.0 * PI) * dot(light_direction, light_direction);

        return light_color / light_attenuation;
    }

    else if (light_type == LIGHT_TYPE_DIRECTIONAL)
    {
        vec3 light_color = lights[light].color; //where light_color is in watts/meter^2

        return light_color;
    }

    else if (light_type == LIGHT_TYPE_SPOT)
    {
        vec3 light_direction = (lights[light].position - surface_position); //where light_direction is in meters
        vec3 light_color = lights[light].color; //where light_color is in watts
        float light_attenuation = (4.0 * PI) * dot(light_direction, light_direction);
        float cone_attenuation = 1.0 - smoothstep(lights[light].inner_angle, lights[light].outer_angle, acos(dot(-normalize(light_direction), lights[light].direction))); //assume that direction is normalized

        return (light_color * cone_attenuation) / light_attenuation;
    }

    return vec3(0.0);
}

vec3 get_light_direction(uint light, vec3 surface_position)
{
    uint light_type = lights[light].type;

    if (light_type == LIGHT_TYPE_POINT)
    {
        return normalize(lights[light].position - surface_position);
    }

    else if (light_type == LIGHT_TYPE_DIRECTIONAL)
    {
        return -lights[light].direction; //assume that direction is normalized
    }

    else if (light_type == LIGHT_TYPE_SPOT)
    {
        return normalize(lights[light].position - surface_position);
    }

    return vec3(0.0);
}

//-- BRDF Functions -------------------------------------------------------------------------------
//The following BRDF function is based on the GLTF-Specification.
//https://github.com/KhronosGroup/glTF/tree/main/specification/2.0

#define DIELECTRIC_F_ZERO 0.04 //index of refraction of 1.5

float compute_facet_distribution(float alpha, float n_dot_h)
{
    float alpha_square = alpha * alpha;
    float factor = n_dot_h * n_dot_h * (alpha_square - 1.0) + 1.0;

    return (alpha_square * heaviside(n_dot_h)) / (PI * factor * factor);
}

float compute_visibility(float alpha, float h_dot_l, float h_dot_v, float n_dot_l, float n_dot_v)
{
    float alpha_square = alpha * alpha;
    float factor1 = abs(n_dot_l) + sqrt(alpha_square + (1.0 - alpha_square) * n_dot_l * n_dot_l);
    float factor2 = abs(n_dot_v) + sqrt(alpha_square + (1.0 - alpha_square) * n_dot_v * n_dot_v);

    return (heaviside(h_dot_l) * heaviside(h_dot_v)) / (factor1 * factor2);
}

vec3 compute_fresnel(vec3 f_zero, float h_dot_v)
{
    return f_zero + (vec3(1.0) - f_zero) * pow(1.0 - abs(h_dot_v), 5.0);
}

float compute_brdf_specular(float alpha, float h_dot_l, float h_dot_v, float n_dot_l, float n_dot_v, float n_dot_h)
{
    float visibility = compute_visibility(alpha, h_dot_l, h_dot_v, n_dot_l, n_dot_v);
    float distribution = compute_facet_distribution(alpha, n_dot_h);

    return visibility * distribution;
}

vec3 compute_brdf_diffuse(vec3 diffuse_color)
{
    return (1.0 / PI) * diffuse_color;
}

vec3 compute_brdf(vec3 light_direction, vec3 normal_direction, vec3 view_direction, Material material)
{
    float n_dot_l = dot(normal_direction, light_direction);

    if (n_dot_l <= 0.0)
    {
        return vec3(0.0);
    }

    vec3 half_direction = normalize(light_direction + view_direction);

    float h_dot_l = dot(half_direction, light_direction);
    float h_dot_v = dot(half_direction, view_direction);
    float n_dot_v = dot(normal_direction, view_direction);
    float n_dot_h = dot(normal_direction, half_direction);

    vec3 diffuse_color = mix(material.base_color, vec3(0.0), material.metallic);
    vec3 f_zero = mix(vec3(DIELECTRIC_F_ZERO), material.base_color, material.metallic);

    float alpha = material.roughness * material.roughness;
    vec3 fersnel = compute_fresnel(f_zero, h_dot_v);

    vec3 diffuse = (1.0 - fersnel) * compute_brdf_diffuse(diffuse_color);
    vec3 specular = fersnel * compute_brdf_specular(alpha, h_dot_l, h_dot_v, n_dot_l, n_dot_v, n_dot_h);

    return diffuse + specular;
}

//-- Indirect Functions ----------------------------------------------------------------------------

#ifdef INDIRECT_DESCRIPTOR_SET
vec3 get_indirect_radiance(vec3 surface_position, vec3 normal_direction)
{
    vec3 cell_coord = (surface_position - indirect_domain.min) / (indirect_domain.max - indirect_domain.min);

    vec4 red_distribution = texture(sampler_indirect_red, cell_coord);
    vec4 green_distribution = texture(sampler_indirect_green, cell_coord);
    vec4 blue_distribution = texture(sampler_indirect_blue, cell_coord);

    //Where center_distance_square is in meter^2
    const float center_distance_square = pow(indirect_domain.cell_size / 2.0, 2);

    vec3 indirect_radiance = vec3(0.0); //Where indirect light is in watts / (meter^2 * str)
    indirect_radiance.x = max(0.0, spherical_harmonic_evaluate(-normal_direction, red_distribution)) / center_distance_square;
    indirect_radiance.y = max(0.0, spherical_harmonic_evaluate(-normal_direction, green_distribution)) / center_distance_square;
    indirect_radiance.z = max(0.0, spherical_harmonic_evaluate(-normal_direction, blue_distribution)) / center_distance_square;

    return indirect_radiance;
}
#endif

//-- Tone-Mapping Functions ------------------------------------------------------------------------
// Exposure based tone mapping
//https://learnopengl.com/Advanced-Lighting/HDR

vec3 compute_tone_mapping(vec3 lighting, float exposure)
{
    return vec3(1.0) - exp(-lighting * exposure);
}

//-- Public Function -------------------------------------------------------------------------------

vec3 apply_lighting(vec3 view_position, vec3 surface_position, vec3 surface_normal, Material material)
{
    vec3 lighting = material.emissive; //where emissive is in watts/meter^2 *str
    
    if (dot(lighting, vec3(1.0)) < EPSILON) //apply direct and indirect lighting only if the material is not emissive
    {
        vec3 normal_direction = normalize(surface_normal);
        vec3 view_direction = normalize(view_position - surface_position);

        lighting += material.base_color * light_parameter.ambient; //where ambient is in watts/meter^2 *str

        for (uint light = 0; light < light_parameter.light_count; light++)
        {
            vec3 light_direction = get_light_direction(light, surface_position);

#ifdef SHADOW_DESCRIPTOR_SET
            lighting += compute_brdf(light_direction, normal_direction, view_direction, material) * get_light_radiance(light, surface_position) * get_shadow(light, surface_position) * dot(normal_direction, light_direction);  
#else
            lighting += compute_brdf(light_direction, normal_direction, view_direction, material) * get_light_radiance(light, surface_position) * dot(normal_direction, light_direction);
#endif
        }

#ifdef INDIRECT_DESCRIPTOR_SET
        material.roughness = 1.0; //NOTE: Set to one, since light propagation volumes can only deliver diffuse indirect.
        lighting += compute_brdf(normal_direction, normal_direction, view_direction, material) * get_indirect_radiance(surface_position, normal_direction) * dot(normal_direction, normal_direction);
#endif

        lighting *= (1.0 - material.occlusion);
    }

    return compute_tone_mapping(lighting, light_parameter.exposure);
}

#endif