/*
  Before including the material-library it is neccessary to set the define MATERIAL_DESCRIPTOR_SET.
  This define specifies the descriptor set in which the material information is stored.
  If the define is not set, an error is emitted.
  After including the material-library, the functions lookup_material(...) and lookup_normal(...) can be used.
  The function lookup_material(...) load the material for a given uv coordinate and the function lookup_normal(...) computes a normal based on the normal map.
  Example:
  
    #define MATERIAL_DESCRIPTOR_SET 42
    #include "material_library.glsl"

    //...

    void main()
    {
        Material material = lookup_material(uv);
    
        if (material.opacity < EPSILON)
        {
            discard;
        }
        
        vec3 normal = lookup_normal(uv, surface_normal, surface_tangent);
        
        //... compute local-lighting
    }
*/

#ifndef SHADER_INCLUDE_MATERIAL_LIBRARY
#define SHADER_INCLUDE_MATERIAL_LIBRARY

#ifndef MATERIAL_DESCRIPTOR_SET
    #error "The material descripotor-set is not specified"
#endif

#include "material_data.inc"
#include "material_struct.glsl"

layout(set = MATERIAL_DESCRIPTOR_SET, binding = 0) uniform sampler2D sampler_color_opacity;
layout(set = MATERIAL_DESCRIPTOR_SET, binding = 1) uniform sampler2D sampler_material_factor;
layout(set = MATERIAL_DESCRIPTOR_SET, binding = 2) uniform sampler2D sampler_normal;
layout(set = MATERIAL_DESCRIPTOR_SET, binding = 3) uniform sampler2D sampler_emissive;

layout(set = MATERIAL_DESCRIPTOR_SET, binding = 4) uniform MaterialDataBuffer
{
    MaterialData material_data;
};

Material lookup_material(vec2 uv_coord)
{
    vec4 color_opacity = texture(sampler_color_opacity, uv_coord);
    vec4 material_factor = texture(sampler_material_factor, uv_coord);
    vec3 emissive_color = texture(sampler_emissive, uv_coord).xyz;

    Material material;
    material.base_color = color_opacity.xyz * material_data.base_color;
    material.opacity = color_opacity.w * material_data.opacity;
    material.occlusion = material_factor.x;
    material.roughness = material_factor.y * material_data.roughness;
    material.metallic = material_factor.z  * material_data.metallic;
    material.emissive = emissive_color * material_data.emissive;

    return material;
}

vec3 lookup_normal(vec2 uv_coord, vec3 surface_normal, vec3 surface_tangent)
{
    vec3 normal = normalize(surface_normal);
    vec3 tangent = normalize(surface_tangent);
    vec3 bitangent = cross(normal, tangent);
    
    vec3 map_normal = texture(sampler_normal, uv_coord).xyz * 2.0 - 1.0;
    map_normal.z = 1.0 - sqrt(map_normal.x * map_normal.x + map_normal.y * map_normal.y);

    return mat3(tangent, bitangent, normal) * map_normal;
}

vec4 lookup_base_color_opacity(vec2 uv_coord)
{
    return texture(sampler_color_opacity, uv_coord);
}

#endif