#ifndef SHADER_INCLUDE_MATERIAL_STRUCT
#define SHADER_INCLUDE_MATERIAL_STRUCT

struct Material
{
    vec3 base_color;
    float opacity;

    float occlusion;
    float roughness;
    float metallic;

    vec3 emissive;
};

#endif