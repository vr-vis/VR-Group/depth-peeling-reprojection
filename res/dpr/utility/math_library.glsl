#ifndef SHADER_INCLUDE_MATH_LIBRARY
#define SHADER_INCLUDE_MATH_LIBRARY

#define PI 3.14159265358
#define EPSILON 0.00001

float heaviside(float value)
{
    if (value <= 0.0)
    {
        return 0.0;
    }

    return 1.0;
}

vec2 quadrant(vec2 vector)
{
    vec2 result;
    result.x = (vector.x >= 0.0) ? 1.0 : -1.0;
    result.y = (vector.y >= 0.0) ? 1.0 : -1.0;

    return result;
}

// Encoding and Decoding of normals presented in the paiper
// "On floating-point normal vectors". This method is also known
//  as oct32 and is compared with other techniques in the paiper
// "A Survey of Efficient Representationsfor Independent Unit Vectors".
vec2 encode_normal(vec3 normal)
{
    vec2 projection = normal.xy * (1.0 / (abs(normal.x) + abs(normal.y) + abs(normal.z)));
    vec2 coord = (normal.z <= 0.0) ? ((1.0 - abs(projection.yx)) * quadrant(projection)) : projection;
    
    return (coord + 1.0) / 2.0;
}

vec3 decode_normal(vec2 vector)
{
    vec2 coord = vector * 2.0 - 1.0;
    vec3 normal = vec3(coord.xy, 1.0 - abs(coord.x) - abs(coord.y));
    normal.xy = (normal.z < 0.0) ? ((1.0 - abs(normal.yx)) * quadrant(normal.xy)) : normal.xy;
    
    return normalize(normal);
}

// Converts the OpenGL left handed NDC-space (x : right, y: up, z: into the screen)
// to the right handed NDC-space (x: right, y: down, z: into the screen) that is used by Vulkan.
vec4 convert_right_handed(vec4 vector)
{
    return vec4(vector.x, -vector.y, vector.zw);
}

#endif