#version 450 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(push_constant) uniform Constants
{
    uint layer_index;
    uint light_index;
    uint frustum_index;
};

void main()
{
    for (int index = 0; index < 3; index++)
    {
        gl_Position = gl_in[index].gl_Position;
        gl_Layer = int(layer_index);

        EmitVertex();
    }

    EndPrimitive();
}