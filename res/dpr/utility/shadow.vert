#version 450 core
#extension GL_GOOGLE_include_directive : require

#include "light_data.inc"
#include "mesh_data.inc"
#include "math_library.glsl"

layout(set = 0, binding = 0) uniform LightParameterBuffer
{
    LightParameter light_parameter;
};

layout(set = 0, binding = 1, std430) readonly buffer LightDataBuffer
{
    LightData lights[];
};

layout(set = 1, binding = 0) uniform MeshBuffer
{
    MeshData mesh;
};

layout(push_constant) uniform Constants
{
    uint layer_index;
    uint light_index;
    uint frustum_index;
};

layout(location = 0) in vec3 inPos;

void main()
{
    mat4 shadow_matrix = lights[light_index].shadow_matrix[frustum_index];
    mat4 model_matrix = mesh.localToWorldSpace;

    gl_Position = shadow_matrix * model_matrix * vec4(inPos, 1.0);
}
