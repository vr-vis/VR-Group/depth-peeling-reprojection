#include "encoder.hpp"
#include "vulkan_encoder.hpp"
#include "nvidia_encoder.hpp"

void Encoder::set_mode(EncoderMode mode)
{

}

void Encoder::set_quality(double quality)
{

}

void Encoder::set_bitrate(double bitrate)
{

}

void Encoder::set_key_rate(uint32_t key_rate)
{

}

void Encoder::set_frame_rate(uint32_t frame_rate)
{

}

EncoderMode Encoder::get_mode() const
{
    return (EncoderMode)0;
}

double Encoder::get_quality() const
{
    return 0.0;
}

double Encoder::get_bitrate() const
{
    return 0.0;
}

uint32_t Encoder::get_key_rate() const
{
    return 0;
}

uint32_t Encoder::get_frame_rate() const
{
    return 0;
}

bool Encoder::is_supported(EncoderSetting setting) const
{
    return false;
}

bool setup_instance_for_encoder(EncoderType encoder_type, lava::frame_config& config)
{
    switch (encoder_type)
    {
    case ENCODER_TYPE_VULKAN:
        return setup_instance_for_vulkan_encoder(config);
    case ENCODER_TYPE_NVIDIA:
        return setup_instance_for_nvidia_encoder(config);
    default:
        lava::log()->error("Unkown encoder type!");
        break;
    }

    return false;
}

bool setup_device_for_encoder(EncoderType encoder_type, lava::instance& instance, lava::device::create_param& parameters)
{
    switch (encoder_type)
    {
    case ENCODER_TYPE_VULKAN:
        return setup_device_for_vulkan_encoder(instance, parameters);
    case ENCODER_TYPE_NVIDIA:
        return setup_device_for_nvidia_encoder(instance, parameters);
    default:
        lava::log()->error("Unkown encoder type!");
        break;
    }

    return false;
}

bool shutdown_encoder(EncoderType encoder_type)
{
    switch (encoder_type)
    {
    case ENCODER_TYPE_VULKAN:
        shutdown_vulkan_encoder();
        break;
    case ENCODER_TYPE_NVIDIA:
        shutdown_nvidia_encoder();
        break;
    default:
        lava::log()->error("Unkown encoder type!");
        return false;
    }

    return true;
}

Encoder::Ptr make_encoder(EncoderType encoder_type)
{
    switch (encoder_type)
    {
    case ENCODER_TYPE_VULKAN:
        return std::make_shared<VulkanEncoder>();
    case ENCODER_TYPE_NVIDIA:
        return std::make_shared<NvidiaEncoder>();
    default:
        lava::log()->error("Unkown encoder type!");
        break;
    }

    return nullptr;
}