/*
  The encoder can be used to create an encoded stream out of a sequence of images.
  In order to be able to use the encoder, it is neccessary to call the function setup_instance_for_encoder(...) during the setup of the vulkan instance.
  Besides that, it is neccessary to call the function setup_device_for_encoder(...) during the setup of the vulkan device.
  A frame can be submitted for encoding using the function encode(...).
  After the completion of an encode task, a callback function is executed to which the resulting ouput of the task is passed.
  During the shutdown of the application it is required to call the function shutdown_encoder(...).
  Example:

   //During on_setup_instance(...)
   setup_instance_for_encoder(...);

   //During on_setup_device(...)
   setup_device_for_encoder(...);

   //During creation and submission of a frame
   Encoder::Ptr encoder = make_encoder(...);

   encoder->create(...);

   encoder->encode(..., image, ..., [](const std::span<uint8_t>& content, bool is_config)
   {
        //Use content
   });

   //During the shutdown of the application:
   shutdown_encoder(...);
*/

#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <functional>
#include <span>
#include <memory>
#include <cstdint>

enum EncoderCodec
{
    ENCODER_CODEC_H264,
    ENCODER_CODEC_H265
};

enum EncoderMode
{
    ENCODER_MODE_CONSTANT_BITRATE,
    ENCODER_MODE_CONSTANT_QUALITY
};

// Supported formats which can be used as input for the encoder.
enum EncoderFormat
{
    ENCODER_FORMAT_R8G8B8A8_SRGB = VK_FORMAT_R8G8B8A8_SRGB,
    ENCODER_FORMAT_B8G8R8A8_SRGB = VK_FORMAT_B8G8R8A8_SRGB
};

// Settings that the encoder could provide.
enum EncoderSetting
{
    ENCODER_SETTING_MODE_CONSTANT_BITRATE,
    ENCODER_SETTING_MODE_CONSTANT_QUALITY,
    ENCODER_SETTING_QUALITY,
    ENCODER_SETTING_BITRATE,
    ENCODER_SETTING_KEY_RATE,
    ENCODER_SETTING_FRAME_RATE
};

enum EncoderResult
{
    ENCODER_RESULT_SUCCESS,
    ENCODER_RESULT_FRAME_DROPPED,
    ENCODER_RESULT_ERROR
};

// When adding or removing an encoder, the functions make_encoder(...), setup_instance_for_encoder(...), setup_device_for_encoder(...) as well as
// the function set_encoder(...) of the CommandParser need to be adapted accordingly.
enum EncoderType
{
    ENCODER_TYPE_VULKAN, // Use vulkan video for encoding
    ENCODER_TYPE_NVIDIA  // Use Nvenc for encoding
};

class Encoder
{
public:
    typedef std::shared_ptr<Encoder> Ptr;

    // If the is_config flag is true, the content passed to this callback function contains a sequence parameter set or picture parameter set.
    typedef std::function<void(const std::span<uint8_t>& content, bool is_config)> OnEncodeComplete;
    typedef std::function<void()> OnEncodeError;

public:
    Encoder() = default;
    virtual ~Encoder() = default;

    virtual bool create(lava::device_ptr device, const lava::renderer& renderer, const glm::uvec2& size, EncoderFormat format, EncoderCodec codec) = 0;
    virtual void destroy() = 0;

    // The following functions should be thread safe.
    // The callback function is executed only by the worker thread of the encoder.
    // The format of the image needs to be the same as the format that was specified during the creation of the encoder.
    virtual EncoderResult encode(VkCommandBuffer command_buffer, lava::renderer& renderer, lava::image::ptr image, VkImageLayout image_layout, OnEncodeComplete function) = 0;

    // The callbacks are executed only by the worker thread of the encoder.
    // Set the callbacks before calling create and don't do it again after calling create.
    virtual void set_on_encode_error(OnEncodeError function) = 0;

    // The following functions should be thread safe.
    // The following settings are only suggestions and can be ignored if for example the feature is not supported.
    virtual void set_mode(EncoderMode mode);            // Defines if the encoder should use constant quality or constant bitrate. Default: constant quality
    virtual void set_quality(double quality);           // The quality ranges from 0.0 (low quality) to 1.0 (high quality).        Default: 0.0
    virtual void set_bitrate(double bitrate);           // The bitrate is given in MBits/s.                                        Default: 5.0
    virtual void set_key_rate(uint32_t key_rate);       // The keyrate is defined as the number of frames between two i-frames.    Default: 90
    virtual void set_frame_rate(uint32_t frame_rate);   // The frame rate is given in frames per second.                           Default: 90

    // The following functions should be thread safe.
    virtual EncoderCodec get_codec() const = 0;
    virtual EncoderMode get_mode() const;
    virtual double get_quality() const;
    virtual double get_bitrate() const;
    virtual uint32_t get_key_rate() const;
    virtual uint32_t get_frame_rate() const;

    // The following function can be used to checker which of the settings listed above will be respected by the encoder.
    virtual bool is_supported(EncoderSetting setting) const;
};

bool setup_instance_for_encoder(EncoderType encoder_type, lava::frame_config& config);
bool setup_device_for_encoder(EncoderType encoder_type, lava::instance& instance, lava::device::create_param& parameters);

bool shutdown_encoder(EncoderType encoder_type);

Encoder::Ptr make_encoder(EncoderType encoder_type);