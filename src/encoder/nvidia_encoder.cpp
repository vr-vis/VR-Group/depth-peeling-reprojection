#include "nvidia_encoder.hpp"
#include <iostream>

#if defined(_WIN32)
    #include <windows.h>
    #include <vulkan/vulkan_win32.h>
#elif defined(__unix__)
    #include <dlfcn.h>
#else
    #error "Not implemented for this platform!"
#endif

#define NVIDIA_ENDODER_ENABLE_SUBFRAMES 0

typedef NVENCSTATUS (NVENCAPI* NvEncodeAPICreateInstance_Type)(NV_ENCODE_API_FUNCTION_LIST*);
typedef NVENCSTATUS (NVENCAPI* NvEncodeAPIGetMaxSupportedVersion_Type)(uint32_t* version);

void* nvenc_library = nullptr;
NV_ENCODE_API_FUNCTION_LIST nvenc_functions;

NvEncodeAPIGetMaxSupportedVersion_Type NvEncodeAPIGetMaxSupportedVersion_Func = nullptr;
NvEncodeAPICreateInstance_Type NvEncodeAPICreateInstance_Func = nullptr;

#if defined(_WIN32)
PFN_vkGetMemoryWin32HandleKHR vkGetMemoryWin32HandleKHR_Func = nullptr;
PFN_vkGetSemaphoreWin32HandleKHR vkGetSemaphoreWin32HandleKHR_Func = nullptr;
#endif

NvidiaEncoder::NvidiaEncoder() : worker_pool(1)
{

}

bool NvidiaEncoder::create(lava::device_ptr device, const lava::renderer& renderer, const glm::uvec2& size, EncoderFormat format, EncoderCodec codec)
{
    this->device = device;
    this->codec = codec;

    if (!this->create_context(device))
    {
        return false;
    }

    if (!this->create_session(size, codec))
    {
        return false;
    }

    for (uint32_t index = 0; index < NVIDIA_ENCODER_FRAMES; index++)
    {
        NvidiaEncoderFrame::Ptr frame = std::make_shared<NvidiaEncoderFrame>();

        if (!this->create_input_buffer(frame, device, size, format))
        {
            return false;
        }

        if (!this->create_output_buffer(frame))
        {
            return false;
        }

        if (!this->create_semaphore(frame, device))
        {
            return false;
        }

        this->frame_queue.push_back(frame);
        this->frame_list.push_back(frame);
    }

    return true;
}

void NvidiaEncoder::destroy()
{
    this->worker_running = false;
    this->worker_pool.stop();

    for (NvidiaEncoderFrame::Ptr frame : this->frame_list)
    {
        this->destroy_frame(frame);
    }

    this->frame_list.clear();
    this->frame_queue.clear();

    this->destroy_session();
    this->destroy_context();
}

EncoderResult NvidiaEncoder::encode(VkCommandBuffer command_buffer, lava::renderer& renderer, lava::image::ptr image, VkImageLayout image_layout, OnEncodeComplete function)
{
    NvidiaEncoderFrame::Ptr frame;

    if (!this->aquire_frame(frame))
    {
        return ENCODER_RESULT_FRAME_DROPPED;
    }

    frame->output_parameter = this->parameter_change;
    frame->on_encode_complete = std::move(function);

    if (this->config_change)
    {
        if (!this->apply_config())
        {
            return ENCODER_RESULT_ERROR;
        }
    }

    this->parameter_change = false;
    this->config_change = false;

    std::vector<VkImageMemoryBarrier> begin_barriers;

    if (image_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier& image_barrier = begin_barriers.emplace_back();
        image_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_barrier.pNext = nullptr;
        image_barrier.srcAccessMask = 0;
        image_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        image_barrier.oldLayout = image_layout;
        image_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        image_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.image = image->get();
        image_barrier.subresourceRange = image->get_subresource_range();
    }
    
    VkImageSubresourceRange subresource_range;
    subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_range.baseMipLevel = 0;
    subresource_range.levelCount = 1;
    subresource_range.baseArrayLayer = 0;
    subresource_range.layerCount = 1;

    VkImageMemoryBarrier& input_begin_barrier = begin_barriers.emplace_back();
    input_begin_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    input_begin_barrier.pNext = nullptr;
    input_begin_barrier.srcAccessMask = 0;
    input_begin_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    input_begin_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    input_begin_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    input_begin_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    input_begin_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    input_begin_barrier.image = frame->image;
    input_begin_barrier.subresourceRange = subresource_range;

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, begin_barriers.size(), begin_barriers.data());

    VkImageCopy image_copy;
    image_copy.srcSubresource = image->get_subresource_layers();
    image_copy.srcOffset.x = 0;
    image_copy.srcOffset.y = 0;
    image_copy.srcOffset.z = 0;
    image_copy.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_copy.dstSubresource.mipLevel = 0;
    image_copy.dstSubresource.baseArrayLayer = 0;
    image_copy.dstSubresource.layerCount = 1;
    image_copy.dstOffset.x = 0;
    image_copy.dstOffset.y = 0;
    image_copy.dstOffset.z = 0;
    image_copy.extent.width = frame->image_size.x;
    image_copy.extent.height = frame->image_size.y;
    image_copy.extent.depth = 1;
    
    vkCmdCopyImage(command_buffer, image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, frame->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);

    std::vector<VkImageMemoryBarrier> end_barriers;

    if (image_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier& image_barrier = end_barriers.emplace_back();
        image_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_barrier.pNext = nullptr;
        image_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        image_barrier.dstAccessMask = 0;
        image_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        image_barrier.newLayout = image_layout;
        image_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.image = image->get();
        image_barrier.subresourceRange = image->get_subresource_range();
    }

    VkImageMemoryBarrier& input_end_barrier = end_barriers.emplace_back();
    input_end_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    input_end_barrier.pNext = nullptr;
    input_end_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    input_end_barrier.dstAccessMask = 0;
    input_end_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    input_end_barrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
    input_end_barrier.srcQueueFamilyIndex = renderer.get_queue().family;
    input_end_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_EXTERNAL;
    input_end_barrier.image = frame->image;
    input_end_barrier.subresourceRange = subresource_range;

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, end_barriers.size(), end_barriers.data());

    this->submit_frame(frame, renderer);

    return ENCODER_RESULT_SUCCESS;
}

void NvidiaEncoder::set_on_encode_error(OnEncodeError function)
{
    this->on_encode_error = std::move(function);
}

void NvidiaEncoder::set_mode(EncoderMode mode)
{
    this->mode = mode;
    this->config_change = true;
}

void NvidiaEncoder::set_quality(double quality)
{
    this->quality = quality;
    this->config_change = true;
}

void NvidiaEncoder::set_bitrate(double bitrate)
{
    this->bitrate = bitrate;
    this->config_change = true;
}

void NvidiaEncoder::set_frame_rate(uint32_t frame_rate)
{
    this->frame_rate = frame_rate;
    this->config_change = true;
}

EncoderCodec NvidiaEncoder::get_codec() const
{
    return this->codec;
}

EncoderMode NvidiaEncoder::get_mode() const
{
    return this->mode;
}

double NvidiaEncoder::get_quality() const
{
    return this->quality;
}

double NvidiaEncoder::get_bitrate() const
{
    return this->bitrate;
}

uint32_t NvidiaEncoder::get_frame_rate() const
{
    return this->frame_rate;
}

bool NvidiaEncoder::is_supported(EncoderSetting setting) const
{
    if (setting == ENCODER_SETTING_KEY_RATE)
    {
        return false;
    }

    return true;
}

bool NvidiaEncoder::aquire_frame(NvidiaEncoderFrame::Ptr& frame)
{
    std::unique_lock<std::mutex> lock(this->frame_mutex);

    if (this->frame_queue.empty())
    {
        return false;
    }

    frame = this->frame_queue.front();
    this->frame_queue.erase(this->frame_queue.begin());

    return true;
}

void NvidiaEncoder::release_frame(NvidiaEncoderFrame::Ptr frame)
{
    std::unique_lock<std::mutex> lock(this->frame_mutex);

    this->frame_queue.push_back(frame);
}

void NvidiaEncoder::submit_frame(NvidiaEncoderFrame::Ptr frame, lava::renderer& renderer)
{
    lava::frame_submission submission;
    submission.semaphore = frame->semaphore;
    submission.callback = [this, frame]()
    {
        this->submit_encode_task(frame);

        asio::post(this->worker_pool, [this, frame]()
        {
            this->read_frame(frame);
            this->release_frame(frame);
        });
    };

    renderer.add_submission(submission);
}

void NvidiaEncoder::read_frame(NvidiaEncoderFrame::Ptr frame)
{
    if (frame->output_parameter)
    {
        std::array<uint8_t, 512> parameter_buffer;
        uint32_t parameter_size = 0;

        NV_ENC_SEQUENCE_PARAM_PAYLOAD parameter_info;
        parameter_info.version = NV_ENC_SEQUENCE_PARAM_PAYLOAD_VER;
        parameter_info.inBufferSize = parameter_buffer.size();
        parameter_info.spsId = 0;
        parameter_info.ppsId = 0;
        parameter_info.spsppsBuffer = parameter_buffer.data();
        parameter_info.outSPSPPSPayloadSize = &parameter_size;
        memset(parameter_info.reserved, 0, sizeof(parameter_info.reserved));
        memset(parameter_info.reserved2, 0, sizeof(parameter_info.reserved2));

        if (nvenc_functions.nvEncGetSequenceParams(this->nvenc_session, &parameter_info) != NV_ENC_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't get sequence and pixture parameter sets!");
            this->on_encode_error();

            return;
        }

        frame->on_encode_complete(std::span(parameter_buffer.data(), parameter_size), true);
    }

    uint32_t offset = 0;

    while (this->worker_running)
    {
        NV_ENC_LOCK_BITSTREAM lock_stream;
        memset(&lock_stream, 0, sizeof(lock_stream));
        lock_stream.version = NV_ENC_LOCK_BITSTREAM_VER;
#if NVIDIA_ENDODER_ENABLE_SUBFRAMES
        lock_stream.doNotWait = 1;
#else
        lock_stream.doNotWait = 0;
#endif
        lock_stream.getRCStats = 0;
        lock_stream.reservedBitFields = 0;
        lock_stream.outputBitstream = frame->nvenc_output_buffer;
        lock_stream.sliceOffsets = nullptr;
        memset(lock_stream.reserved, 0, sizeof(lock_stream.reserved));
        memset(lock_stream.reserved1, 0, sizeof(lock_stream.reserved1));
        memset(lock_stream.reserved2, 0, sizeof(lock_stream.reserved2));

        NVENCSTATUS result = nvenc_functions.nvEncLockBitstream(this->nvenc_session, &lock_stream);

        if (result == NV_ENC_ERR_LOCK_BUSY)
        {
            continue;
        }

        else if (result != NV_ENC_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't lock bitsteam!");
            this->on_encode_error();

            return;
        }

        uint32_t bytes = lock_stream.bitstreamSizeInBytes - offset;

        if (bytes > 0)
        {
            frame->on_encode_complete(std::span((uint8_t*)lock_stream.bitstreamBufferPtr + offset, bytes), false);
            offset += bytes;
        }

        if (nvenc_functions.nvEncUnlockBitstream(this->nvenc_session, frame->nvenc_output_buffer) != NV_ENC_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't unlock bistream!");
            this->on_encode_error();

            return;
        }

#if NVIDIA_ENDODER_ENABLE_SUBFRAMES
        if (lock_stream.hwEncodeStatus == 2)
        {
            break;
        }
#else
        break;
#endif
    }

    if (nvenc_functions.nvEncUnmapInputResource(this->nvenc_session, frame->nvenc_mapped_buffer) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't unmap input resource!");
        this->on_encode_error();
    
        return;
    }

    frame->nvenc_mapped_buffer = nullptr;
}

bool NvidiaEncoder::apply_config()
{    
    NV_ENC_INITIALIZE_PARAMS session_config = this->nvenc_session_config;
    NV_ENC_CONFIG encode_config = this->nvenc_encode_config;
    session_config.encodeConfig = &encode_config;

    if (this->mode == ENCODER_MODE_CONSTANT_QUALITY)
    {
        encode_config.rcParams.rateControlMode = NV_ENC_PARAMS_RC_CONSTQP;
        encode_config.rcParams.constQP.qpIntra = (uint32_t)((1.0 - this->quality) * 51);
        encode_config.rcParams.constQP.qpInterP = (uint32_t)((1.0 - this->quality) * 51);
        encode_config.rcParams.constQP.qpInterB = (uint32_t)((1.0 - this->quality) * 51);
    }

    else if (this->mode == ENCODER_MODE_CONSTANT_BITRATE)
    {
        encode_config.rcParams.rateControlMode = NV_ENC_PARAMS_RC_CBR;
        encode_config.rcParams.averageBitRate = (uint32_t)(this->bitrate * 1000000.0);
        encode_config.rcParams.maxBitRate = this->nvenc_encode_config.rcParams.averageBitRate;
        encode_config.rcParams.vbvBufferSize = (uint32_t)(this->nvenc_encode_config.rcParams.averageBitRate * (1.0f / this->frame_rate)) * 5;
        encode_config.rcParams.vbvInitialDelay = this->nvenc_encode_config.rcParams.vbvBufferSize;
    }

    else
    {
        lava::log()->error("Nvidia Encoder: Unsupported encoder mode!");

        return false;
    }

    NV_ENC_RECONFIGURE_PARAMS reconfig;
    reconfig.version = NV_ENC_RECONFIGURE_PARAMS_VER;
    reconfig.reInitEncodeParams = session_config;
    reconfig.resetEncoder = 1;
    reconfig.forceIDR = 1;
    reconfig.reserved = 0;

    if (nvenc_functions.nvEncReconfigureEncoder(this->nvenc_session, &reconfig) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't apply config change!");

        return false;
    }

    return true;
}

void NvidiaEncoder::submit_encode_task(NvidiaEncoderFrame::Ptr frame)
{
    if (cuCtxPushCurrent(this->cuda_context) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't push cuda context!");
        this->on_encode_error();

        return;
    }

    CUDA_EXTERNAL_SEMAPHORE_WAIT_PARAMS wait_parameters;
    memset(&wait_parameters.params, 0, sizeof(wait_parameters.params));
    wait_parameters.flags = 0;
    memset(wait_parameters.reserved, 0, sizeof(wait_parameters.reserved));
    
    if (cuWaitExternalSemaphoresAsync(&frame->cuda_external_semaphore, &wait_parameters, 1, 0) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't wait for semaphore to be signaled!");
        this->on_encode_error();

        return;
    }

    NV_ENC_MAP_INPUT_RESOURCE map_info;
    map_info.version = NV_ENC_MAP_INPUT_RESOURCE_VER;
    map_info.subResourceIndex = 0;
    map_info.inputResource = 0;
    map_info.registeredResource = frame->nvenc_input_buffer;
    map_info.mappedResource = nullptr;
    map_info.mappedBufferFmt = (NV_ENC_BUFFER_FORMAT)0;
    memset(map_info.reserved1, 0, sizeof(map_info.reserved1));
    memset(map_info.reserved2, 0, sizeof(map_info.reserved2));

    if (nvenc_functions.nvEncMapInputResource(this->nvenc_session, &map_info) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't map input resources!");
        this->on_encode_error();

        return;
    }

    frame->nvenc_mapped_buffer = map_info.mappedResource;

    if (cuCtxPopCurrent(&this->cuda_context) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't pop cuda context!");
        this->on_encode_error();

        return;
    }

    NV_ENC_PIC_PARAMS encode_parameters;
    encode_parameters.version = NV_ENC_PIC_PARAMS_VER;
    encode_parameters.inputWidth = frame->image_size.x;
    encode_parameters.inputHeight = frame->image_size.y;
    encode_parameters.inputPitch = frame->image_layout.rowPitch;
    encode_parameters.encodePicFlags = 0;
    encode_parameters.frameIdx = 0;
    encode_parameters.inputTimeStamp = 0;
    encode_parameters.inputDuration = 0;
    encode_parameters.inputBuffer = frame->nvenc_mapped_buffer;
    encode_parameters.outputBitstream = frame->nvenc_output_buffer;
    encode_parameters.completionEvent = nullptr;
    encode_parameters.bufferFmt = map_info.mappedBufferFmt;
    encode_parameters.pictureStruct = NV_ENC_PIC_STRUCT_FRAME;
    encode_parameters.pictureType = (NV_ENC_PIC_TYPE)0;
    memset(encode_parameters.meHintCountsPerBlock, 0, sizeof(encode_parameters.meHintCountsPerBlock));
    encode_parameters.meExternalHints = nullptr;
    memset(encode_parameters.reserved1, 0, sizeof(encode_parameters.reserved1));
    memset(encode_parameters.reserved2, 0, sizeof(encode_parameters.reserved2));
    encode_parameters.qpDeltaMap = nullptr;
    encode_parameters.qpDeltaMapSize = 0;
    encode_parameters.reservedBitFields = 0;
    memset(encode_parameters.meHintRefPicDist, 0, sizeof(encode_parameters.meHintRefPicDist));
    encode_parameters.alphaBuffer = nullptr;
    memset(encode_parameters.reserved3, 0, sizeof(encode_parameters.reserved3));
    memset(encode_parameters.reserved4, 0, sizeof(encode_parameters.reserved4));

    if(this->codec == ENCODER_CODEC_H264)
    {
        NV_ENC_PIC_PARAMS_MVC motion_vector_parameters;
        motion_vector_parameters.version = NV_ENC_PIC_PARAMS_MVC_VER;
        motion_vector_parameters.viewID = 0;
        motion_vector_parameters.temporalID = 0;
        motion_vector_parameters.priorityID = 0;
        memset(motion_vector_parameters.reserved1, 0, sizeof(motion_vector_parameters.reserved1));
        memset(motion_vector_parameters.reserved2, 0, sizeof(motion_vector_parameters.reserved2));

        NV_ENC_PIC_PARAMS_H264_EXT codec_extention_parameters;
        codec_extention_parameters.mvcPicParams = motion_vector_parameters;
        memset(codec_extention_parameters.reserved1, 0, sizeof(codec_extention_parameters.reserved1));
    
        NV_ENC_PIC_PARAMS_H264 codec_parameters;
        codec_parameters.displayPOCSyntax = 0;
        codec_parameters.reserved3 = 0;
        codec_parameters.refPicFlag = 0;
        codec_parameters.colourPlaneId = 0;
        codec_parameters.forceIntraRefreshWithFrameCnt = 0;
        codec_parameters.constrainedFrame = 1;
        codec_parameters.sliceModeDataUpdate = 1;
        codec_parameters.ltrMarkFrame = 0;
        codec_parameters.ltrUseFrames = 0;
        codec_parameters.reservedBitFields = 0;
        codec_parameters.sliceTypeData = 0;
        codec_parameters.sliceTypeArrayCnt = 0;
        codec_parameters.seiPayloadArrayCnt = 0;
        codec_parameters.seiPayloadArray = nullptr;
        codec_parameters.sliceMode = 3;
        codec_parameters.sliceModeData = 10;
        codec_parameters.ltrMarkFrameIdx = 0;
        codec_parameters.ltrUseFrameBitmap = 0;
        codec_parameters.ltrUsageMode = 0;
        codec_parameters.forceIntraSliceCount = 0;
        codec_parameters.forceIntraSliceIdx = 0;
        codec_parameters.h264ExtPicParams = codec_extention_parameters;
        memset(codec_parameters.reserved, 0, sizeof(codec_parameters.reserved));
        memset(codec_parameters.reserved2, 0, sizeof(codec_parameters.reserved2));

        encode_parameters.codecPicParams.h264PicParams = codec_parameters;
    }

    else if(this->codec == ENCODER_CODEC_H265)
    {
        NV_ENC_PIC_PARAMS_HEVC codec_parameters;
        codec_parameters.displayPOCSyntax = 0;
        codec_parameters.refPicFlag = 0;
        codec_parameters.temporalId = 0;
        codec_parameters.forceIntraRefreshWithFrameCnt = 0;
        codec_parameters.constrainedFrame = 1;
        codec_parameters.sliceModeDataUpdate  = 1;
        codec_parameters.ltrMarkFrame = 0;
        codec_parameters.ltrUseFrames = 0;
        codec_parameters.reservedBitFields = 0;
        codec_parameters.sliceTypeData = 0;
        codec_parameters.sliceTypeArrayCnt = 0;
        codec_parameters.sliceMode = 3;
        codec_parameters.sliceModeData = 10;
        codec_parameters.ltrMarkFrameIdx = 0;
        codec_parameters.ltrUseFrameBitmap = 0;
        codec_parameters.ltrUsageMode = 0;
        codec_parameters.seiPayloadArrayCnt = 0;
        codec_parameters.reserved = 0;
        codec_parameters.seiPayloadArray = nullptr;
        memset(codec_parameters.reserved2, 0, sizeof(codec_parameters.reserved2));
        memset(codec_parameters.reserved3, 0, sizeof(codec_parameters.reserved3));

        encode_parameters.codecPicParams.hevcPicParams = codec_parameters;
    }

    else
    {
        lava::log()->error("Nvidia Encoder: Invalid codec!");
        this->on_encode_error();
    }

    if (nvenc_functions.nvEncEncodePicture(this->nvenc_session, &encode_parameters) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't encoder image!");
        this->on_encode_error();

        return;
    }
}

bool NvidiaEncoder::create_context(lava::device_ptr device)
{
    VkPhysicalDeviceIDProperties physical_device_id;
    physical_device_id.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES;
    physical_device_id.pNext = nullptr;

    VkPhysicalDeviceProperties2 physical_device_properties;
    physical_device_properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    physical_device_properties.pNext = &physical_device_id;

    vkGetPhysicalDeviceProperties2(device->get_physical_device()->get(), &physical_device_properties);

    int32_t device_count = 0;

    if (cuDeviceGetCount(&device_count) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get cuda device count!");

        return false;
    }

    for (uint32_t index = 0; index < device_count; index++)
    {
        CUdevice device_handle;
        CUuuid device_id;

        if (cuDeviceGet(&device_handle, index) != CUDA_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't get cuda device handle!");

            return false;
        }

        if (cuDeviceGetUuid(&device_id, device_handle) != CUDA_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't get cuda device identifier!");

            return false;
        }

        if (memcmp(device_id.bytes, physical_device_id.deviceUUID, sizeof(device_id.bytes)) != 0)
        {
            continue;
        }

        this->cuda_device = device_handle;

        if (cuCtxCreate(&this->cuda_context, 0, this->cuda_device) != CUDA_SUCCESS)
        {
            lava::log()->error("Nvidia Encoder: Can't create cuda context!");

            return false;
        }

        return true;
    }

    lava::log()->error("Nvidia Encoder: Can't find matching cuda device!");

    return false;
}

void NvidiaEncoder::destroy_context()
{
    if (this->cuda_context != nullptr)
    {
        cuCtxDestroy(this->cuda_context);
        this->cuda_device = 0;
        this->cuda_context = nullptr;
    }
}

bool NvidiaEncoder::create_session(const glm::uvec2& size, EncoderCodec codec)
{
    NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS session_parameters;
    session_parameters.version = NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS_VER;
    session_parameters.deviceType = NV_ENC_DEVICE_TYPE_CUDA;
    session_parameters.device = (void*)this->cuda_context;
    session_parameters.reserved = 0;
    session_parameters.apiVersion = NVENCAPI_VERSION;
    memset(session_parameters.reserved1, 0, sizeof(session_parameters.reserved1));
    memset(session_parameters.reserved2, 0, sizeof(session_parameters.reserved2));

    if (nvenc_functions.nvEncOpenEncodeSessionEx(&session_parameters, &this->nvenc_session) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create nvenc session!");

        return false;
    }

    GUID codec_guid;
    GUID profile_guid;

    switch(codec)
    {
    case ENCODER_CODEC_H264:
        codec_guid = NV_ENC_CODEC_H264_GUID;
        profile_guid = NV_ENC_H264_PROFILE_HIGH_GUID;
        break;
    case ENCODER_CODEC_H265:
        codec_guid = NV_ENC_CODEC_HEVC_GUID;
        profile_guid = NV_ENC_HEVC_PROFILE_MAIN_GUID;
        break;
    default:
        lava::log()->error("Nvidia Encoder: Invalid codec!");
        return false;
    }

    if (!this->check_encode_support(codec_guid))
    {
        lava::log()->error("Nvidia Encoder: Codec not supported!");

        return false;
    }

    if (!this->check_profile_support(codec_guid, profile_guid))
    {
        lava::log()->error("Nvidia Encoder: Profile not supported!");

        return false;
    }

    if (!this->check_preset_support(codec_guid, NV_ENC_PRESET_P1_GUID))
    {
        lava::log()->error("Nvidia Encoder: Preset not supported!");

        return false;
    }

    if (!this->check_format_support(codec_guid, NV_ENC_BUFFER_FORMAT_ABGR))
    {
        lava::log()->error("Nvidia Encoder: Input format not supported!");

        return false;
    }

    NV_ENC_PRESET_CONFIG preset_config;
    preset_config.version = NV_ENC_PRESET_CONFIG_VER;
    memset(&preset_config.presetCfg, 0, sizeof(preset_config.presetCfg));
    preset_config.presetCfg.version = NV_ENC_CONFIG_VER;
    preset_config.presetCfg.rcParams.version = NV_ENC_RC_PARAMS_VER;
    memset(preset_config.reserved1, 0, sizeof(preset_config.reserved1));
    memset(preset_config.reserved2, 0, sizeof(preset_config.reserved2));

    if (nvenc_functions.nvEncGetEncodePresetConfigEx(this->nvenc_session, codec_guid, NV_ENC_PRESET_P1_GUID, NV_ENC_TUNING_INFO_ULTRA_LOW_LATENCY, &preset_config) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get preset for config!");

        return false;
    }

    this->nvenc_encode_config = preset_config.presetCfg;
    this->nvenc_encode_config.profileGUID = profile_guid;
    this->nvenc_encode_config.rcParams.version = NV_ENC_RC_PARAMS_VER;
    this->nvenc_encode_config.rcParams.rateControlMode = NV_ENC_PARAMS_RC_CONSTQP;
    this->nvenc_encode_config.rcParams.enableAQ = 1;
    this->nvenc_encode_config.rcParams.aqStrength = 0;

    switch(codec)
    {
    case ENCODER_CODEC_H264:
        this->nvenc_encode_config.encodeCodecConfig.h264Config.disableSPSPPS = 1;
        this->nvenc_encode_config.encodeCodecConfig.h264Config.enableIntraRefresh = 1; //NOTE: Can create problems when replayed in VLC
        this->nvenc_encode_config.encodeCodecConfig.h264Config.intraRefreshPeriod = this->frame_rate * 2;
        this->nvenc_encode_config.encodeCodecConfig.h264Config.intraRefreshCnt = 10;
        break;
    case ENCODER_CODEC_H265:
        this->nvenc_encode_config.encodeCodecConfig.hevcConfig.disableSPSPPS = 1;
        this->nvenc_encode_config.encodeCodecConfig.hevcConfig.enableIntraRefresh = 1;
        this->nvenc_encode_config.encodeCodecConfig.hevcConfig.intraRefreshPeriod = this->frame_rate * 2;
        this->nvenc_encode_config.encodeCodecConfig.hevcConfig.intraRefreshCnt = 10;
        break;
    default:
        lava::log()->error("Nvidia Encoder: Invalid codec!");
        return false;
    }

    this->nvenc_session_config.version = NV_ENC_INITIALIZE_PARAMS_VER;
    this->nvenc_session_config.encodeGUID = codec_guid;
    this->nvenc_session_config.presetGUID = NV_ENC_PRESET_P1_GUID;
    this->nvenc_session_config.encodeWidth = size.x;
    this->nvenc_session_config.encodeHeight = size.y;
    this->nvenc_session_config.darWidth = size.x;
    this->nvenc_session_config.darHeight = size.y;
    this->nvenc_session_config.frameRateNum = this->frame_rate;
    this->nvenc_session_config.frameRateDen = 1;
    this->nvenc_session_config.enableEncodeAsync = 0;
    this->nvenc_session_config.enablePTD = 1;
    this->nvenc_session_config.reportSliceOffsets = 0;
#if NVIDIA_ENDODER_ENABLE_SUBFRAMES
    this->nvenc_session_config.enableSubFrameWrite = 1;
#else
    this->nvenc_session_config.enableSubFrameWrite = 0;
#endif
    this->nvenc_session_config.enableExternalMEHints = 0;
    this->nvenc_session_config.enableMEOnlyMode = 0;
    this->nvenc_session_config.enableWeightedPrediction = 0;
    this->nvenc_session_config.enableOutputInVidmem  = 0;
    this->nvenc_session_config.reservedBitFields = 0;
    this->nvenc_session_config.privDataSize = 0;
    this->nvenc_session_config.privData = nullptr;
    this->nvenc_session_config.encodeConfig = &this->nvenc_encode_config;
    this->nvenc_session_config.maxEncodeWidth = size.x;
    this->nvenc_session_config.maxEncodeHeight = size.y;
    memset(this->nvenc_session_config.maxMEHintCountsPerBlock, 0, sizeof(this->nvenc_session_config.maxMEHintCountsPerBlock));
    this->nvenc_session_config.tuningInfo = NV_ENC_TUNING_INFO_ULTRA_LOW_LATENCY;
    this->nvenc_session_config.bufferFormat = (NV_ENC_BUFFER_FORMAT) 0;
    memset(this->nvenc_session_config.reserved, 0, sizeof(this->nvenc_session_config.reserved));
    memset(this->nvenc_session_config.reserved2, 0, sizeof(this->nvenc_session_config.reserved2));
    
    if (nvenc_functions.nvEncInitializeEncoder(this->nvenc_session, &this->nvenc_session_config) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't init nvenc session!");

        return false;
    }

    return true;
}

void NvidiaEncoder::destroy_session()
{
    if (this->nvenc_session != nullptr)
    {
        nvenc_functions.nvEncDestroyEncoder(this->nvenc_session);
        this->nvenc_session = nullptr;
    }
}

bool NvidiaEncoder::create_input_buffer(NvidiaEncoderFrame::Ptr frame, lava::device_ptr device, const glm::uvec2& size, EncoderFormat format)
{
    VkExternalMemoryImageCreateInfo export_image_info;
    export_image_info.sType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO;
    export_image_info.pNext = nullptr;
#if defined(_WIN32)
    export_image_info.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT;
#elif defined(__unix__)
    export_image_info.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
#else
    #error "Not implemented for this platform!"
#endif

    VkImageCreateInfo image_info;
    image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_info.pNext = &export_image_info;
    image_info.flags = 0;
    image_info.imageType = VK_IMAGE_TYPE_2D;
    image_info.format = (VkFormat)format;
    image_info.extent.width = size.x;
    image_info.extent.height = size.y;
    image_info.extent.depth = 1;
    image_info.mipLevels = 1;
    image_info.arrayLayers = 1;
    image_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_info.tiling = VK_IMAGE_TILING_LINEAR;
    image_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_info.queueFamilyIndexCount = 0;
    image_info.pQueueFamilyIndices = nullptr;
    image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &image_info, lava::memory::alloc(), &frame->image) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create input image!");

        return false;
    }

    VkMemoryRequirements memory_requirements;
    vkGetImageMemoryRequirements(device->get(), frame->image, &memory_requirements);

    VkPhysicalDeviceMemoryProperties memory_properties;
    vkGetPhysicalDeviceMemoryProperties(device->get_physical_device()->get(), &memory_properties);

    bool memory_found = false;
    uint32_t memory_index = 0;
    VkMemoryPropertyFlags memory_flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    for (uint32_t index = 0; index < memory_properties.memoryTypeCount; index++)
    {
        if ((memory_requirements.memoryTypeBits & (1 << index)) == 0)
        {
            continue;   
        }

        if ((memory_properties.memoryTypes[index].propertyFlags & memory_flags) == 0)
        {
            continue;
        }

        memory_found = true;
        memory_index = index;

        break;
    }

    if (!memory_found)
    {
        lava::log()->error("Nvidia Encoder: Can't find matching memory heap for input image!");

        return false;
    }

    VkExportMemoryAllocateInfo export_info;
    export_info.sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO;
    export_info.pNext = nullptr;
#if defined(_WIN32)
    export_info.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT;
#elif defined(__unix__)
    export_info.handleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
#else
    #error "Not implemented for this platform!"
#endif
    
    VkMemoryAllocateInfo allocation_info;
    allocation_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocation_info.pNext = &export_info;
    allocation_info.allocationSize = memory_requirements.size;
    allocation_info.memoryTypeIndex = memory_index;

    if (vkAllocateMemory(device->get(), &allocation_info, lava::memory::alloc(), &frame->device_memory) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't allocate memory for input image!");

        return false;
    }

    if (vkBindImageMemory(device->get(), frame->image, frame->device_memory, 0) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't bind memory to input image!");

        return false;
    }

#if defined(_WIN32)
    VkMemoryGetWin32HandleInfoKHR memory_info;
    memory_info.sType = VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR;
    memory_info.pNext = nullptr;
    memory_info.memory = frame->device_memory;
    memory_info.handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_BIT;

    if (vkGetMemoryWin32HandleKHR_Func(device->get(), &memory_info, (HANDLE*)&frame->memory_handle) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get handle to input image!");

        return false; 
    }
#elif defined(__unix__)
    VkMemoryGetFdInfoKHR memory_info;
    memory_info.sType = VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR;
    memory_info.pNext = nullptr;
    memory_info.memory = frame->device_memory;
    memory_info.handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;

    if (vkGetMemoryFdKHR(device->get(), &memory_info, (int*)&frame->memory_handle) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get handle to input image!");

        return false; 
    }
#else
    #error "Not implemented for this platform!"
#endif

    CUDA_EXTERNAL_MEMORY_HANDLE_DESC external_memory_description;
#if defined(_WIN32)
    external_memory_description.type = CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32;
    external_memory_description.handle.win32.handle = frame->memory_handle;
    external_memory_description.handle.win32.name = nullptr;
#elif defined(__unix__)
    external_memory_description.type = CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD;
    external_memory_description.handle.fd = (int)frame->memory_handle;
#else
    #error "Not implemented for this platform!"
#endif
    external_memory_description.size = memory_requirements.size;
    external_memory_description.flags = 0;
    memset(external_memory_description.reserved, 0, sizeof(external_memory_description.reserved));

    if (cuImportExternalMemory(&frame->cuda_external_memory, &external_memory_description) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't convert memory of input image to cuda external memory object!");

        return false;
    }

    VkImageSubresource image_subresources;
    image_subresources.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_subresources.mipLevel = 0;
    image_subresources.arrayLayer = 0;

    vkGetImageSubresourceLayout(device->get(), frame->image, &image_subresources, &frame->image_layout);

    CUDA_EXTERNAL_MEMORY_BUFFER_DESC buffer_description;
    buffer_description.offset = frame->image_layout.offset;
    buffer_description.size = frame->image_layout.size;
    buffer_description.flags = 0;
    memset(buffer_description.reserved, 0, sizeof(buffer_description.reserved));

    if (cuExternalMemoryGetMappedBuffer(&frame->cuda_buffer, frame->cuda_external_memory, &buffer_description) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create buffer from external memory of input image!");

        return false;
    }

    NV_ENC_BUFFER_FORMAT buffer_format;

    switch (format)
    {
    case ENCODER_FORMAT_R8G8B8A8_SRGB:
        buffer_format = NV_ENC_BUFFER_FORMAT_ABGR;
        break;
    case ENCODER_FORMAT_B8G8R8A8_SRGB:
        buffer_format = NV_ENC_BUFFER_FORMAT_ARGB;
        break;
    default:
        lava::log()->error("Nvidia Encoder: Unkown input image format!");
        return false;
    }

    NV_ENC_REGISTER_RESOURCE register_info;
    register_info.version = NV_ENC_REGISTER_RESOURCE_VER;
    register_info.resourceType = NV_ENC_INPUT_RESOURCE_TYPE_CUDADEVICEPTR;
    register_info.width = size.x;
    register_info.height = size.y;
    register_info.pitch = frame->image_layout.rowPitch;
    register_info.subResourceIndex = 0;
    register_info.resourceToRegister = (void*)frame->cuda_buffer;
    register_info.registeredResource = nullptr;
    register_info.bufferFormat = buffer_format;
    register_info.bufferUsage = NV_ENC_INPUT_IMAGE;
    register_info.pInputFencePoint = nullptr;
    memset(register_info.reserved1, 0, sizeof(register_info.reserved1));
    memset(register_info.reserved2, 0, sizeof(register_info.reserved2));

    if (nvenc_functions.nvEncRegisterResource(this->nvenc_session, &register_info) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't register input image!");

        return false;
    }

    frame->image_size = size;
    frame->nvenc_input_buffer = register_info.registeredResource;

    return true;
}

bool NvidiaEncoder::create_output_buffer(NvidiaEncoderFrame::Ptr frame)
{
    NV_ENC_CREATE_BITSTREAM_BUFFER create_info;
    create_info.version = NV_ENC_CREATE_BITSTREAM_BUFFER_VER;
    create_info.size = 0;
    create_info.memoryHeap = (NV_ENC_MEMORY_HEAP)0;
    create_info.reserved = 0;
    create_info.bitstreamBuffer = nullptr;
    create_info.bitstreamBufferPtr = nullptr;
    memset(create_info.reserved1, 0, sizeof(create_info.reserved1));
    memset(create_info.reserved2, 0, sizeof(create_info.reserved2));
    
    if (nvenc_functions.nvEncCreateBitstreamBuffer(this->nvenc_session, &create_info) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create output bistream buffer!");

        return false;
    }

    frame->nvenc_output_buffer = create_info.bitstreamBuffer;

    return true;
}

bool NvidiaEncoder::create_semaphore(NvidiaEncoderFrame::Ptr frame, lava::device_ptr device)
{
    VkExportSemaphoreCreateInfo semaphore_export_info;
    semaphore_export_info.sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
    semaphore_export_info.pNext = nullptr;
#if defined(_WIN32)
    semaphore_export_info.handleTypes = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT;
#elif defined(__unix__)
    semaphore_export_info.handleTypes = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;
#else
    #error "Not implemented for this platform!"
#endif

    VkSemaphoreCreateInfo semaphore_info;
    semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphore_info.pNext = &semaphore_export_info;
    semaphore_info.flags = 0;

    if (vkCreateSemaphore(device->get(), &semaphore_info, lava::memory::alloc(), &frame->semaphore) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create semaphore!");

        return false;
    }

#if defined(_WIN32)
    VkSemaphoreGetWin32HandleInfoKHR export_info;
    export_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR;
    export_info.pNext = nullptr;
    export_info.semaphore = frame->semaphore;
    export_info.handleType = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT;

    if (vkGetSemaphoreWin32HandleKHR_Func(device->get(), &export_info, (HANDLE*)&frame->semaphore_handle) != VK_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get handle of semaphore!");

        return false;
    }
#elif defined(__unix__)
    VkSemaphoreGetFdInfoKHR export_info;
    export_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR;
    export_info.pNext = nullptr;
    export_info.semaphore = frame->semaphore;
    export_info.handleType = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;

    if (vkGetSemaphoreFdKHR(device->get(), &export_info, (int*)&frame->semaphore_handle))
    {
        lava::log()->error("Nvidia Encoder: Can't get handle of semaphore!");

        return false;
    }
#else
    #error "Not implemented for this platform!"
#endif

    CUDA_EXTERNAL_SEMAPHORE_HANDLE_DESC semaphore_description;
#if defined(_WIN32)
    semaphore_description.type = CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32;
    semaphore_description.handle.win32.handle = frame->semaphore_handle;
#elif defined(__unix__)
    semaphore_description.type = CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD;
    semaphore_description.handle.fd = (int)frame->semaphore_handle;
#else
    #error "Not implemented for this platform!"
#endif
    semaphore_description.flags = 0;
    memset(semaphore_description.reserved, 0, sizeof(semaphore_description.reserved));

    if (cuImportExternalSemaphore(&frame->cuda_external_semaphore, &semaphore_description) != CUDA_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't import semaphore!");

        return false;
    }

    return true;
}

void NvidiaEncoder::destroy_frame(NvidiaEncoderFrame::Ptr frame)
{
    if (frame->nvenc_mapped_buffer != nullptr)
    {
        nvenc_functions.nvEncUnmapInputResource(this->nvenc_session, frame->nvenc_mapped_buffer);
        frame->nvenc_mapped_buffer = nullptr;
    }

    if (frame->nvenc_input_buffer != nullptr)
    {
        nvenc_functions.nvEncUnregisterResource(this->nvenc_session, frame->nvenc_input_buffer);
        frame->nvenc_input_buffer = nullptr;
    }

    if (frame->nvenc_output_buffer != nullptr)
    {
        nvenc_functions.nvEncDestroyBitstreamBuffer(this->nvenc_session, frame->nvenc_output_buffer);
        frame->nvenc_output_buffer = nullptr;
    }

    if (frame->cuda_buffer != 0)
    {
        cuMemFree(frame->cuda_buffer);
        frame->cuda_buffer = 0;
    }

    if (frame->cuda_external_memory != nullptr)
    {
        cuDestroyExternalMemory(frame->cuda_external_memory);
        frame->cuda_external_memory = nullptr;
    }

    if (frame->cuda_external_semaphore != nullptr)
    {
        cuDestroyExternalSemaphore(frame->cuda_external_semaphore);
        frame->cuda_external_semaphore = nullptr;
    }

    if (frame->memory_handle != nullptr)
    {
#if defined(_WIN32)
        CloseHandle(frame->memory_handle);
#elif defined(__unix__)
        close(frame->memory_handle);
#else
    #error "Not implemented for this platform!"
#endif
        frame->memory_handle = nullptr;
    }

    if (frame->semaphore_handle != nullptr)
    {
#if defined(_WIN32)
        CloseHandle(frame->semaphore_handle);
#elif defined(__unix__)
        close(frame->semaphore_handle);
#else
    #error "Not implemented for this platform!"
#endif
        frame->semaphore_handle = nullptr;
    }

    if (frame->image != VK_NULL_HANDLE)
    {
        vkDestroyImage(this->device->get(), frame->image, lava::memory::alloc());
        frame->image = VK_NULL_HANDLE;
    }

    if (frame->device_memory != VK_NULL_HANDLE)
    {
        vkFreeMemory(this->device->get(), frame->device_memory, lava::memory::alloc());
        frame->device_memory = VK_NULL_HANDLE;
    }

    if (frame->semaphore != VK_NULL_HANDLE)
    {
        vkDestroySemaphore(this->device->get(), frame->semaphore, lava::memory::alloc());
        frame->semaphore = VK_NULL_HANDLE;
    }
}

bool NvidiaEncoder::check_encode_support(GUID required_guid) const
{
    uint32_t guid_count = 0;

    if (nvenc_functions.nvEncGetEncodeGUIDCount(this->nvenc_session, &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    std::vector<GUID> guid_list;
    guid_list.resize(guid_count);

    if (nvenc_functions.nvEncGetEncodeGUIDs(this->nvenc_session, guid_list.data(), guid_count, &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    for (const GUID& guid : guid_list)
    {
        if (memcmp(&guid, &required_guid, sizeof(guid)) == 0)
        {
            return true;
        }
    }

    return false;
}

bool NvidiaEncoder::check_profile_support(GUID encode_guid, GUID required_guid) const
{
    uint32_t guid_count = 0;

    if (nvenc_functions.nvEncGetEncodeProfileGUIDCount(this->nvenc_session, encode_guid , &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    std::vector<GUID> guid_list;
    guid_list.resize(guid_count);

    if (nvenc_functions.nvEncGetEncodeProfileGUIDs(this->nvenc_session, encode_guid, guid_list.data(), guid_count, &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    for (const GUID& guid : guid_list)
    {
        if (memcmp(&guid, &required_guid, sizeof(guid)) == 0)
        {
            return true;
        }
    }

    return false;
}

bool NvidiaEncoder::check_preset_support(GUID encode_guid, GUID required_guid) const
{
    uint32_t guid_count = 0;

    if (nvenc_functions.nvEncGetEncodePresetCount(this->nvenc_session, encode_guid , &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    std::vector<GUID> guid_list;
    guid_list.resize(guid_count);

    if (nvenc_functions.nvEncGetEncodePresetGUIDs(this->nvenc_session, encode_guid, guid_list.data(), guid_count, &guid_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    for (const GUID& guid : guid_list)
    {
        if (memcmp(&guid, &required_guid, sizeof(guid)) == 0)
        {
            return true;
        }
    }

    return false;
}

bool NvidiaEncoder::check_format_support(GUID encode_guid, NV_ENC_BUFFER_FORMAT required_format) const
{
    uint32_t format_count = 0;

    if (nvenc_functions.nvEncGetInputFormatCount(this->nvenc_session, encode_guid, &format_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    std::vector<NV_ENC_BUFFER_FORMAT> format_list;
    format_list.resize(format_count);

    if (nvenc_functions.nvEncGetInputFormats(this->nvenc_session, encode_guid, format_list.data(), format_count, &format_count) != NV_ENC_SUCCESS)
    {
        return false;
    }

    for (const NV_ENC_BUFFER_FORMAT& format : format_list)
    {
        if (memcmp(&format, &required_format, sizeof(format)) == 0)
        {
            return true;
        }
    }

    return false;
}

bool load_functions(lava::instance& instance)
{
#if defined(_WIN32)
    vkGetMemoryWin32HandleKHR_Func = (PFN_vkGetMemoryWin32HandleKHR)vkGetInstanceProcAddr(instance.get(), "vkGetMemoryWin32HandleKHR");

    if (vkGetMemoryWin32HandleKHR_Func == nullptr)
    {
        lava::log()->error("Nvidia Encoder: Can't get function pointer for 'vkGetMemoryWin32HandleKHR'");

        return false;
    }

    vkGetSemaphoreWin32HandleKHR_Func = (PFN_vkGetSemaphoreWin32HandleKHR)vkGetInstanceProcAddr(instance.get(), "vkGetSemaphoreWin32HandleKHR");

    if (vkGetSemaphoreWin32HandleKHR_Func == nullptr)
    {
        lava::log()->error("Nvidia Encoder: Can't get function pointer for 'vkGetSemaphoreWin32HandleKHR'");

        return false;
    }
#elif defined(__unix__)

#else
    #error "Not implemented for this platform!"
#endif

    return true;
}

bool load_library()
{
#if defined(_WIN32)
#if defined(_WIN64)
    nvenc_library = LoadLibrary("nvEncodeAPI64.dll");
#else
    nvenc_library = LoadLibrary("nvEncodeAPI.dll");
#endif
#elif defined(__unix__)
    nvenc_library = dlopen("libnvidia-encode.so.1", RTLD_LAZY);
#else 
    #error "Not implemented for this platform!"
#endif

    if (nvenc_library == nullptr)
    {
        lava::log()->error("Nvidia Encoder: Can't load library!");

        return false;
    }

#if defined(_WIN32)
    NvEncodeAPIGetMaxSupportedVersion_Func = (NvEncodeAPIGetMaxSupportedVersion_Type)GetProcAddress((HMODULE)nvenc_library, "NvEncodeAPIGetMaxSupportedVersion");
    NvEncodeAPICreateInstance_Func = (NvEncodeAPICreateInstance_Type)GetProcAddress((HMODULE)nvenc_library, "NvEncodeAPICreateInstance");
#elif defined(__unix__)
    NvEncodeAPIGetMaxSupportedVersion_Func = (NvEncodeAPIGetMaxSupportedVersion_Type)dlsym(nvenc_library, "NvEncodeAPIGetMaxSupportedVersion");
    NvEncodeAPICreateInstance_Func = (NvEncodeAPICreateInstance_Type)dlsym(nvenc_library, "NvEncodeAPICreateInstance");
#else
    #error "Not implemented for this platform!"
#endif

    if (NvEncodeAPIGetMaxSupportedVersion_Func == nullptr)
    {
        lava::log()->error("Nvidia Encoder: Can't get function pointer for 'NvEncodeAPIGetMaxSupportedVersion'");
        
        return false;
    }

    if (NvEncodeAPICreateInstance_Func == nullptr)
    {
        lava::log()->error("Nvidia Encoder: Can't get function pointer for 'NvEncodeAPICreateInstance' !");

        return false;
    }

    uint32_t current_version = (NVENCAPI_MAJOR_VERSION << 4) | (NVENCAPI_MINOR_VERSION & 0xF);
    uint32_t max_version = 0;

    if (NvEncodeAPIGetMaxSupportedVersion_Func(&max_version) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't get max API version!");

        return false;
    }

    if (max_version < current_version)
    {
        lava::log()->error("Nvidia Encoder: Mismatch between header version and driver version!");

        return false;
    }

    nvenc_functions.version = NV_ENCODE_API_FUNCTION_LIST_VER;

    if (NvEncodeAPICreateInstance_Func(&nvenc_functions) != NV_ENC_SUCCESS)
    {
        lava::log()->error("Nvidia Encoder: Can't create function list!");

        return false;
    }

    return true;
}

void unload_library()
{
    if (nvenc_library != nullptr)
    {
#if defined(_WIN32)
        FreeLibrary((HMODULE)nvenc_library);
#elif defined(__unix__)
        dlclose(nvenc_library);
#else
    #error "Not implemented for this platform!"
#endif
    }

    nvenc_library = nullptr;
}

bool setup_instance_for_nvidia_encoder(lava::frame_config& config)
{
    if (cuInit(0) != CUDA_SUCCESS)
    {
        std::cout << "Nvidia Encoder: Can't init cuda!" << std::endl;

        return false;
    }

    return true;
}

bool setup_device_for_nvidia_encoder(lava::instance& instance, lava::device::create_param& parameters)
{
    if (!load_functions(instance))
    {
        return false;
    }

    if (!load_library())
    {
        return false;
    }

#if defined(_WIN32)
    parameters.extensions.push_back(VK_KHR_EXTERNAL_MEMORY_WIN32_EXTENSION_NAME);
    parameters.extensions.push_back(VK_KHR_EXTERNAL_SEMAPHORE_WIN32_EXTENSION_NAME);   
#elif defined(__unix__)
    parameters.extensions.push_back(VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME);
    parameters.extensions.push_back(VK_KHR_EXTERNAL_SEMAPHORE_FD_EXTENSION_NAME);
#else
    #error "Not implemented for this platform!"
#endif

    return true;
}

void shutdown_nvidia_encoder()
{
    unload_library();
}