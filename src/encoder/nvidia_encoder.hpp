#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <asio.hpp>
#include <mutex>
#include <vector>
#include <memory>

#include <cuda.h>
#include <nvEncodeAPI.h>

#include "encoder.hpp"

#define NVIDIA_ENCODER_FRAMES 10

struct NvidiaEncoderFrame
{
public:
    typedef std::shared_ptr<NvidiaEncoderFrame> Ptr;
    
public:
    glm::uvec2 image_size;
    VkSubresourceLayout image_layout;

    VkImage image = VK_NULL_HANDLE;
    VkDeviceMemory device_memory = VK_NULL_HANDLE;
    VkSemaphore semaphore = VK_NULL_HANDLE;

    void* memory_handle = nullptr;
    void* semaphore_handle = nullptr;

    CUexternalMemory cuda_external_memory = nullptr;
    CUexternalSemaphore cuda_external_semaphore = nullptr;
    CUdeviceptr cuda_buffer = 0;

    void* nvenc_input_buffer = nullptr;
    void* nvenc_output_buffer = nullptr;
    void* nvenc_mapped_buffer = nullptr;

    bool output_parameter = false;

    Encoder::OnEncodeComplete on_encode_complete;
};

class NvidiaEncoder : public Encoder
{
public:
    typedef std::shared_ptr<NvidiaEncoder> Ptr;

public:
    NvidiaEncoder();

    bool create(lava::device_ptr device, const lava::renderer& renderer, const glm::uvec2& size, EncoderFormat format, EncoderCodec codec) override;
    void destroy() override;

    EncoderResult encode(VkCommandBuffer command_buffer, lava::renderer& renderer, lava::image::ptr image, VkImageLayout image_layout, OnEncodeComplete function) override;

    void set_on_encode_error(OnEncodeError function) override;

    void set_mode(EncoderMode mode) override;
    void set_quality(double quality) override;
    void set_bitrate(double bitrate) override;
    void set_frame_rate(uint32_t frame_rate) override;

    EncoderCodec get_codec() const override;
    EncoderMode get_mode() const override;
    double get_quality() const override;
    double get_bitrate() const override;
    uint32_t get_frame_rate() const override;

    bool is_supported(EncoderSetting setting) const override;

private:
    bool aquire_frame(NvidiaEncoderFrame::Ptr& frame);
    void release_frame(NvidiaEncoderFrame::Ptr frame);
    void submit_frame(NvidiaEncoderFrame::Ptr frame, lava::renderer& renderer);
    void read_frame(NvidiaEncoderFrame::Ptr frame);

    bool apply_config();
    void submit_encode_task(NvidiaEncoderFrame::Ptr frame);

    bool create_context(lava::device_ptr device);
    void destroy_context();

    bool create_session(const glm::uvec2& size, EncoderCodec codec);
    void destroy_session();

    bool create_input_buffer(NvidiaEncoderFrame::Ptr frame, lava::device_ptr device, const glm::uvec2& size, EncoderFormat format);
    bool create_output_buffer(NvidiaEncoderFrame::Ptr frame);
    bool create_semaphore(NvidiaEncoderFrame::Ptr frame, lava::device_ptr device);
    void destroy_frame(NvidiaEncoderFrame::Ptr frame);

    bool check_encode_support(GUID required_guid) const;
    bool check_profile_support(GUID encode_guid, GUID required_guid) const;
    bool check_preset_support(GUID encode_guid, GUID required_guid) const;
    bool check_format_support(GUID encode_guid, NV_ENC_BUFFER_FORMAT required_format) const;

private:
    OnEncodeError on_encode_error;

    EncoderCodec codec = ENCODER_CODEC_H264;
    EncoderMode mode = ENCODER_MODE_CONSTANT_QUALITY;
    double quality = 0.0;
    double bitrate = 5.0;
    uint32_t frame_rate = 90;

    bool config_change = true;
    bool parameter_change = true;

private:
    asio::thread_pool worker_pool;
    bool worker_running = true;

    lava::device_ptr device;
    CUdevice cuda_device = 0;
    CUcontext cuda_context = nullptr;

    void* nvenc_session = nullptr;
    NV_ENC_INITIALIZE_PARAMS nvenc_session_config;
    NV_ENC_CONFIG nvenc_encode_config;

    std::mutex frame_mutex;
    std::vector<NvidiaEncoderFrame::Ptr> frame_queue; //NOTE: Protected by frame_mutex
    std::vector<NvidiaEncoderFrame::Ptr> frame_list;
};

bool setup_instance_for_nvidia_encoder(lava::frame_config& config);
bool setup_device_for_nvidia_encoder(lava::instance& instance, lava::device::create_param& parameters);

void shutdown_nvidia_encoder();