#include "vulkan_encoder.hpp"
#include <array>
#include <vk_video/vulkan_video_codecs_common.h>

VulkanEncoder::VulkanEncoder() : worker_pool(1)
{

}

bool VulkanEncoder::create(lava::device_ptr device, const lava::renderer& renderer, const glm::uvec2& size, EncoderFormat format, EncoderCodec codec)
{
    if (codec != ENCODER_CODEC_H264)
    {
        lava::log()->error("Vulkan encoder only supportes h264 encoding!");

        return false;
    }

    this->frame_count = VULKAN_ENCODER_FRAMES;
    this->frame_index = 0;

    //Get the default graphics queue of lava for querey ownership transiations
    this->default_queue = renderer.get_queue();

    //Search for the encode queue
    for (const lava::queue& queue : device->get_queues())
    {
        if ((queue.flags & VK_QUEUE_VIDEO_ENCODE_BIT_KHR) == 0)
        {
            continue;
        }

        if (!this->check_encode_support(device, queue))
        {
            continue;
        }

        this->encode_queue = queue;

        break;
    }

    if (!this->encode_queue.has_value())
    {
        return false;
    }

    //Create video and encode profiles. These profiles define basic information such as the bit depth of the images or the codec
    //These profiles are later used for the creation of the video session.
    //Besides that, they are also chained to every create info of every object that is used during the encoding.
    if (!this->create_profiles(device))
    {
        return false;
    }

    //Compute the size of the images and the size of the output buffer.
    //The encoding needs a special alignment for the size of images and buffers.
    if (!this->compute_settings(size))
    {
        return false;
    }

    //Check if the VK_FORMAT_G8_B8R8_2PLANE_420_UNORM can be used for the source image during encoding.
    if (!this->check_format_support(device, VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR, VK_FORMAT_G8_B8R8_2PLANE_420_UNORM))
    {
        return false;
    }

    //Check if the VK_FORMAT_G8_B8R8_2PLANE_420_UNORM can be used for the reference images during encoding.
    if (!this->check_format_support(device, VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR, VK_FORMAT_G8_B8R8_2PLANE_420_UNORM))
    {
        return false;
    }

    //Create the video session object.
    if (!this->create_session(device, size))
    {
        return false;
    }

    //The video session is simmilar to an image or an buffer. This means that it needs device memory that need to be allocated and bound to it.
    if (!this->bind_session_memory(device))
    {
        return false;
    }

    //Create meta information that is later added to the video stream. In particular, create the Sequence Parameter Set and the Picture Parameter Set for the h.264 stream.
    if (!this->create_parameters(device))
    {
        return false;
    }

    //Create a sampler object that is used during the conversion of the input image from RGB to YCrCb and during the chroma subsampling.
    //The sampler object is used for reading the input image during the conversion pass and during the subsampling pass.
    if (!this->create_sampler(device))
    {
        return false;
    }

    //Create pools such as the command buffer pool for the encode command buffers and the queue pool for the queues that are needed in order to get the number of bytes written to the output buffer.
    if (!this->create_pools(device, this->frame_count))
    {
        return false;
    }

    //Create the pipeline layout of the convert pass.
    if (!this->create_layouts(device))
    {
        return false;
    }

    //Create a pool of frames. Each frame is used for a single input image that needs to be encoded.
    for (uint32_t index = 0; index < this->frame_count; index++)
    {
        VulkanEncoderFrame::Ptr frame = std::make_shared<VulkanEncoderFrame>();
        frame->output_query_index = index;

        //Create the images that are used for the encoding as input images.
        //These images are the target of the convert pass.
        if (!this->create_input_images(device, frame))
        {
            return false;
        }

        //Create a reference image. This reference image is only used as a dummy images, since only I-frames, which do not need reference images, are used.
        if (!this->create_slot_image(device, frame))
        {
            return false;
        }

        //Create the output buffer which is used as the target for the encode operation.
        if (!this->create_output_buffer(device, frame))
        {
            return false;
        }

        //Set up the render pass for the convert pass
        if (!this->create_convert_pass(device, frame))
        {
            return false;
        }

        //Set up the pipeline for the convert pass
        if (!this->create_convert_pipeline(device, frame))
        {
            return false;
        }

        //Set up the render pass for the subsample pass
        if (!this->create_subsample_pass(device, frame))
        {
            return false;
        }

        //Set up the pipeline for the subsample pass
        if (!this->create_subsample_pipeline(device, frame))
        {
            return false;
        }

        //Allocate a command buffer as well as a semaphore and a fence for the frame.
        if (!this->create_encode_resources(device, this->worker_pool.get_executor(), frame))
        {
            return false;
        }

        this->frame_queue.push_back(frame);
        this->frame_list.push_back(frame);
    }

    this->setup_slots();

    return true;
}

void VulkanEncoder::destroy()
{
    lava::device_ptr device = this->descriptor_layout->get_device();

    for (VulkanEncoderFrame::Ptr frame : this->frame_list)
    {
        this->destroy_frame(device, frame);
    }

    this->frame_queue.clear();
    this->frame_list.clear();

    this->destroy_resources(device);
    this->destroy_session(device);
}

EncoderResult VulkanEncoder::encode(VkCommandBuffer command_buffer, lava::renderer& renderer, lava::image::ptr image, VkImageLayout image_layout, OnEncodeComplete callback)
{
    if (!this->encode_control(renderer, callback))
    {
        return ENCODER_RESULT_ERROR;
    }
    
    if (!this->encode_config(renderer, callback))
    {
        return ENCODER_RESULT_ERROR;
    }

    lava::device_ptr device = image->get_device();
    VulkanEncoderFrame::Ptr frame = nullptr;

    //Get an unused image form the frame pool
    if (!this->aquire_frame(frame))
    {
        return ENCODER_RESULT_FRAME_DROPPED;
    }

    frame->frame_index = this->frame_index;
    frame->type = VULKAN_ENCODER_FRAME_TYPE_FRAME;
    frame->callback = std::move(callback);

    //Check if the input image has the right layout. If not change the layout.
    if (image_layout != VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        lava::insert_image_memory_barrier(device, command_buffer, image->get(), 0, VK_ACCESS_SHADER_READ_BIT, image_layout, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, image->get_subresource_range());
    }

    //Add the input to a descriptor set so that it can be used during the convert pass.
    this->write_image(device, frame, image);

    //Execute the convert pass. Render a fullscreen quad and convert the input image from RGB to YCrCb. Store the result in the image frame->input_luma and frame->input_chroma.
    vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipeline_layout->get(), 0, 1, &frame->convert_descriptor, 0, nullptr);
    frame->convert_pass->process(command_buffer, 0);

    //Execute the subsample pass. Render a fullscreen quad and subsample the image frame->input_chroma. Store the result in the image frame->input_chroma_subsampled.
    vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipeline_layout->get(), 0, 1, &frame->subsample_descriptor, 0, nullptr);
    frame->subsample_pass->process(command_buffer, 0);
    
    //Check if the original layout of the input image needs to be restored.
    if (image_layout != VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        lava::insert_image_memory_barrier(device, command_buffer, image->get(), VK_ACCESS_SHADER_READ_BIT, 0, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, image_layout, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, image->get_subresource_range());
    }
    
    VkImageSubresourceRange image_range;
    image_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_range.baseMipLevel = 0;
    image_range.levelCount = 1;
    image_range.baseArrayLayer = 0;
    image_range.layerCount = 1;

    lava::insert_image_memory_barrier(device, command_buffer, frame->input_combined, 0, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, image_range);
    
    VkImageCopy copy_luma;
    copy_luma.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    copy_luma.srcSubresource.baseArrayLayer = 0;
    copy_luma.srcSubresource.mipLevel = 0;
    copy_luma.srcSubresource.layerCount = 1;
    copy_luma.srcOffset.x = 0;
    copy_luma.srcOffset.y = 0;
    copy_luma.srcOffset.z = 0;
    copy_luma.dstSubresource.aspectMask = VK_IMAGE_ASPECT_PLANE_0_BIT;
    copy_luma.dstSubresource.baseArrayLayer = 0;
    copy_luma.dstSubresource.mipLevel = 0;
    copy_luma.dstSubresource.layerCount = 1;
    copy_luma.dstOffset.x = 0;
    copy_luma.dstOffset.y = 0;
    copy_luma.dstOffset.z = 0;
    copy_luma.extent.width = this->setting_image_size.x;
    copy_luma.extent.height = this->setting_image_size.y;
    copy_luma.extent.depth = 1;

    VkImageCopy copy_chroma;
    copy_chroma.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    copy_chroma.srcSubresource.baseArrayLayer = 0;
    copy_chroma.srcSubresource.mipLevel = 0;
    copy_chroma.srcSubresource.layerCount = 1;
    copy_chroma.srcOffset.x = 0;
    copy_chroma.srcOffset.y = 0;
    copy_chroma.srcOffset.z = 0;
    copy_chroma.dstSubresource.aspectMask = VK_IMAGE_ASPECT_PLANE_1_BIT;
    copy_chroma.dstSubresource.baseArrayLayer = 0;
    copy_chroma.dstSubresource.mipLevel = 0;
    copy_chroma.dstSubresource.layerCount = 1;
    copy_chroma.dstOffset.x = 0;
    copy_chroma.dstOffset.y = 0;
    copy_chroma.dstOffset.z = 0;
    copy_chroma.extent.width = this->setting_image_size.x / 2;
    copy_chroma.extent.height = this->setting_image_size.y / 2;
    copy_chroma.extent.depth = 1;

    //TODO: Try to write to the combined image directly instead of copying the luma and chroma seperatly to the combine image.
    vkCmdCopyImage(command_buffer, frame->input_luma, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, frame->input_combined, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_luma);
    vkCmdCopyImage(command_buffer, frame->input_chroma_subsampled, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, frame->input_combined, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_chroma);

    //Create the encode command buffer.
    if (!this->record_encoding(device, frame))
    {
        return ENCODER_RESULT_ERROR;
    }

    //Submit the encode command buffer at the end of the frame and async wait for the completion of the frame.
    if (!this->submit_frame(device, frame, renderer))
    {
        return ENCODER_RESULT_ERROR;
    }

    this->frame_index++;
    
    return ENCODER_RESULT_SUCCESS;
}

void VulkanEncoder::set_on_encode_error(OnEncodeError function)
{
    this->on_encode_error = std::move(function);
}

void VulkanEncoder::set_mode(EncoderMode mode)
{
    this->setting_mode = mode;
    this->control_change = true;
}

void VulkanEncoder::set_quality(double quality)
{
    this->setting_quality_iframe = glm::clamp((uint32_t)((1.0 - quality) * 51), 0u, 51u);
    this->setting_quality_pframe = glm::clamp((uint32_t)((1.0 - quality) * 51), 0u, 51u);
    this->control_change = true;
}

void VulkanEncoder::set_bitrate(double bitrate)
{
    this->setting_bit_rate = bitrate;
    this->control_change = true;
}

void VulkanEncoder::set_key_rate(uint32_t key_rate)
{
    this->setting_key_rate = key_rate;
    this->control_change = true;
}

void VulkanEncoder::set_frame_rate(uint32_t frame_rate)
{
    this->setting_frame_rate = frame_rate;
    this->control_change = true;
}

EncoderCodec VulkanEncoder::get_codec() const
{
    return ENCODER_CODEC_H264;
}

EncoderMode VulkanEncoder::get_mode() const
{
    return this->setting_mode;
}

double VulkanEncoder::get_quality() const
{
    return 1.0 - ((double)this->setting_quality_iframe / 51.0);
}

double VulkanEncoder::get_bitrate() const
{
    return this->setting_bit_rate;
}

uint32_t VulkanEncoder::get_key_rate() const
{
    return this->setting_key_rate;
}

uint32_t VulkanEncoder::get_frame_rate() const
{
    return this->setting_frame_rate;
}

bool VulkanEncoder::is_supported(EncoderSetting setting) const
{
    return true;
}

bool VulkanEncoder::encode_control(lava::renderer& renderer, OnEncodeComplete callback)
{
    if (!this->control_change)
    {
        return true;
    }

    lava::device_ptr device = this->descriptor_layout->get_device();
    VulkanEncoderFrame::Ptr frame = nullptr;

    //Get an unused image form the frame pool
    if (!this->aquire_frame(frame))
    {
        return false;
    }

    frame->frame_index = this->frame_index;
    frame->type = VULKAN_ENCODER_FRAME_TYPE_CONTROL;
    frame->callback = std::move(callback);

    //Create the encode command buffer.
    if (!this->record_encoding(device, frame))
    {
        return false;
    }

    //Submit the encode command buffer at the end of the frame and async wait for the completion of the frame.
    if (!this->submit_frame(device, frame, renderer))
    {
        return false;
    }

    this->control_change = false;
    
    return true;
}

bool VulkanEncoder::encode_config(lava::renderer& renderer, OnEncodeComplete callback)
{
    if (!this->config_change)
    {
        return true;
    }

    lava::device_ptr device = this->descriptor_layout->get_device();
    VulkanEncoderFrame::Ptr frame = nullptr;

    //Get an unused image form the frame pool
    if (!this->aquire_frame(frame))
    {
        return false;
    }

    frame->frame_index = this->frame_index;
    frame->type = VULKAN_ENCODER_FRAME_TYPE_CONFIG;
    frame->callback = std::move(callback);

    //Create the encode command buffer.
    if (!this->record_encoding(device, frame))
    {
        return false;
    }

    //Submit the encode command buffer at the end of the frame and async wait for the completion of the frame.
    if (!this->submit_frame(device, frame, renderer))
    {
        return false;
    }

    this->config_change = false;
    
    return true;
}

bool VulkanEncoder::aquire_frame(VulkanEncoderFrame::Ptr& frame)
{
    std::unique_lock<std::mutex> lock(this->frame_mutex);

    if (this->frame_queue.size() >= 1)
    {
        frame = this->frame_queue.front();
        this->frame_queue.erase(this->frame_queue.begin());

        return true;
    }

    return false;
}

void VulkanEncoder::release_frame(VulkanEncoderFrame::Ptr frame)
{
    std::unique_lock<std::mutex> lock(this->frame_mutex);

    this->frame_queue.push_back(frame);
}

bool VulkanEncoder::submit_frame(lava::device_ptr device, VulkanEncoderFrame::Ptr frame, lava::renderer& renderer)
{
    frame->encode_fence->async_wait([this, device, frame](const asio::error_code& error_code)
    {
        if (!error_code)
        {
            this->retrive_encoding(device, frame);
        }

        this->release_frame(frame);
    });

    lava::frame_submission submission;
    submission.semaphore = frame->render_semaphore;
    submission.callback = [=]()
    {
        std::vector<VkSemaphoreSubmitInfo> wait_semaphores;
    
        VkSemaphoreSubmitInfo wait_render;
        wait_render.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        wait_render.pNext = nullptr;
        wait_render.semaphore = frame->render_semaphore;
        wait_render.value = 0;
        wait_render.stageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
        wait_render.deviceIndex = 0;
        wait_semaphores.push_back(wait_render);

        if (this->previous_semaphore != VK_NULL_HANDLE)
        {
            VkSemaphoreSubmitInfo wait_encode;
            wait_encode.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
            wait_encode.pNext = nullptr;
            wait_encode.semaphore = this->previous_semaphore;
            wait_encode.value = 0;
            wait_encode.stageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
            wait_encode.deviceIndex = 0;
            wait_semaphores.push_back(wait_encode);
        }

        VkCommandBufferSubmitInfo command_info;
        command_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        command_info.pNext = nullptr;
        command_info.commandBuffer = frame->encode_command_buffer;
        command_info.deviceMask = 0;

        VkSemaphoreSubmitInfo signal_info;
        signal_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO;
        signal_info.pNext = nullptr;
        signal_info.semaphore = frame->encode_semaphore;
        signal_info.value = 0;
        signal_info.stageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
        signal_info.deviceIndex = 0;

        VkSubmitInfo2 submit_info;
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submit_info.pNext = nullptr;
        submit_info.flags = 0;
        submit_info.waitSemaphoreInfoCount = wait_semaphores.size();
        submit_info.pWaitSemaphoreInfos = wait_semaphores.data();
        submit_info.commandBufferInfoCount = 1;
        submit_info.pCommandBufferInfos = &command_info;
        submit_info.signalSemaphoreInfoCount = 1;
        submit_info.pSignalSemaphoreInfos = &signal_info;

        vkQueueSubmit2KHR(this->encode_queue->vk_queue, 1, &submit_info, frame->encode_fence->get()); //TODO: Return value check

        this->previous_semaphore = frame->encode_semaphore;
    };
    
    renderer.add_submission(submission);

    return true;
}

void VulkanEncoder::write_image(lava::device_ptr device, VulkanEncoderFrame::Ptr frame, lava::image::ptr image)
{
    VkDescriptorImageInfo image_info;
    image_info.sampler = this->sampler;
    image_info.imageView = image->get_view();
    image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkWriteDescriptorSet write_info;
    write_info.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_info.pNext = nullptr;
    write_info.dstSet = frame->convert_descriptor;
    write_info.dstBinding = 0;
    write_info.dstArrayElement = 0;
    write_info.descriptorCount = 1;
    write_info.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_info.pImageInfo = &image_info;
    write_info.pBufferInfo = nullptr;
    write_info.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(device->get(), 1, &write_info, 0, nullptr);
}

bool VulkanEncoder::record_encoding(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkFence fence = frame->encode_fence->get();

    if (vkResetFences(device->get(), 1, &fence) != VK_SUCCESS)
    {
        return false;
    }

    if (vkResetCommandBuffer(frame->encode_command_buffer, 0) != VK_SUCCESS)
    {
        return false;
    }

    VkCommandBufferBeginInfo buffer_begin_info;
    buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    buffer_begin_info.pNext = nullptr;
    buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    buffer_begin_info.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(frame->encode_command_buffer, &buffer_begin_info) != VK_SUCCESS)
    {
        return false;
    }

    switch (frame->type)
    {
    case VULKAN_ENCODER_FRAME_TYPE_CONTROL:
        this->encode_pass_control_setup(frame->encode_command_buffer, frame);
        break;
    case VULKAN_ENCODER_FRAME_TYPE_CONFIG:
        this->encode_pass_config_setup(frame->encode_command_buffer, frame);
        break;
    case VULKAN_ENCODER_FRAME_TYPE_FRAME:
        this->encode_pass_frame_setup(frame->encode_command_buffer, frame);
        break;
    default:
        return false;
    }

    if (vkEndCommandBuffer(frame->encode_command_buffer) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

void VulkanEncoder::retrive_encoding(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    if (frame->type == VULKAN_ENCODER_FRAME_TYPE_CONTROL)
    {
        return;
    }

    std::array<uint32_t, 3> buffer_range;
    memset(buffer_range.data(), 0, sizeof(uint32_t) * buffer_range.size());

    if (vkGetQueryPoolResults(device->get(), this->query_pool, frame->output_query_index, 1, sizeof(uint32_t) * buffer_range.size(), buffer_range.data(), 0, VK_QUERY_RESULT_WAIT_BIT | VK_QUERY_RESULT_WITH_STATUS_BIT_KHR) != VK_SUCCESS)
    {
        return;
    }

    uint32_t range_offset = buffer_range[0];
    uint32_t range_size = buffer_range[1];

    vmaInvalidateAllocation(device->alloc(), frame->output_memory, range_offset, range_size);

    uint8_t* range_pointer = nullptr;

    if (vmaMapMemory(device->alloc(), frame->output_memory, (void**) (&range_pointer)))
    {
        return;
    }

    bool is_config = (frame->type == VULKAN_ENCODER_FRAME_TYPE_CONFIG);

    frame->callback(std::span(range_pointer + range_offset, range_size), is_config);

    vmaUnmapMemory(device->alloc(), frame->output_memory);
}

bool VulkanEncoder::create_profiles(lava::device_ptr device)
{
    this->encode_profile.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_PROFILE_INFO_EXT;
    this->encode_profile.pNext = nullptr;
    this->encode_profile.stdProfileIdc = STD_VIDEO_H264_PROFILE_IDC_HIGH;

    //NOTE: Not used since it leads to an error during the initialisation.
    this->usage_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_USAGE_INFO_KHR;
    this->usage_info.pNext = &this->encode_profile;
    this->usage_info.videoUsageHints = VK_VIDEO_ENCODE_USAGE_STREAMING_BIT_KHR;
    this->usage_info.videoContentHints = VK_VIDEO_ENCODE_CONTENT_RENDERED_BIT_KHR;
    this->usage_info.tuningMode = VK_VIDEO_ENCODE_TUNING_MODE_ULTRA_LOW_LATENCY_KHR;

    this->video_profile.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_INFO_KHR;
    this->video_profile.pNext = &this->encode_profile;
    this->video_profile.videoCodecOperation = VK_VIDEO_CODEC_OPERATION_ENCODE_H264_BIT_EXT;
    this->video_profile.chromaSubsampling = VK_VIDEO_CHROMA_SUBSAMPLING_420_BIT_KHR;
    this->video_profile.lumaBitDepth = VK_VIDEO_COMPONENT_BIT_DEPTH_8_BIT_KHR;
    this->video_profile.chromaBitDepth = VK_VIDEO_COMPONENT_BIT_DEPTH_8_BIT_KHR;

    this->codec_capabillities.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_CAPABILITIES_EXT;
    this->codec_capabillities.pNext = nullptr;

    this->encode_capabillities.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_CAPABILITIES_KHR;
    this->encode_capabillities.pNext = &this->codec_capabillities;

    this->video_capabillities.sType = VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR;
    this->video_capabillities.pNext = &this->encode_capabillities;

    if (vkGetPhysicalDeviceVideoCapabilitiesKHR(device->get_physical_device()->get(), &this->video_profile, &this->video_capabillities) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::compute_settings(const glm::uvec2& size)
{
    const glm::uvec2 block_size = glm::uvec2(16); //Macroblock Size of 16x16. TODO: Look up this value form the capabillities.
    const glm::uvec2 image_alignment = glm::uvec2(this->encode_capabillities.inputImageDataFillAlignment.width, this->encode_capabillities.inputImageDataFillAlignment.height);

    const uint32_t buffer_size = 1048576 * 10; //10 MB. TODO: Use a better estimation for the size of the output buffer.
    const uint32_t buffer_alignment = this->video_capabillities.minBitstreamBufferSizeAlignment;

    this->setting_image_size = glm::uvec2(glm::ceil(glm::vec2(size) / glm::vec2(image_alignment))) * image_alignment;
    this->setting_block_count = glm::uvec2(glm::ceil(glm::vec2(this->setting_image_size) / glm::vec2(block_size)));
    this->setting_frame_crop = this->setting_block_count * block_size - size;
    this->setting_buffer_size = (uint32_t)glm::ceil((float)buffer_size / (float)buffer_alignment) * buffer_alignment;

    return true;
}

bool VulkanEncoder::create_session(lava::device_ptr device, const glm::uvec2& size)
{
    VkExtent2D max_video_extend;
    max_video_extend.width = size.x;
    max_video_extend.height = size.y;

    VkExtensionProperties extension_properties;
    strncpy(extension_properties.extensionName, VK_STD_VULKAN_VIDEO_CODEC_H264_ENCODE_EXTENSION_NAME, VK_MAX_EXTENSION_NAME_SIZE);
    extension_properties.specVersion = VK_STD_VULKAN_VIDEO_CODEC_H264_ENCODE_SPEC_VERSION;
    
    VkVideoSessionCreateInfoKHR video_create_info;
    video_create_info.sType = VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR;
    video_create_info.pNext = nullptr;
    video_create_info.queueFamilyIndex = this->encode_queue->family;
    video_create_info.flags = (VkVideoSessionCreateFlagsKHR) 0;
    video_create_info.pVideoProfile = &this->video_profile;
    video_create_info.pictureFormat = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    video_create_info.maxCodedExtent = max_video_extend;
    video_create_info.referencePictureFormat = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    video_create_info.maxDpbSlots = this->setting_reference_frames + 1;
    video_create_info.maxActiveReferencePictures = this->setting_reference_frames + 1;
    video_create_info.pStdHeaderVersion = &extension_properties;

    if (vkCreateVideoSessionKHR(device->get(), &video_create_info, lava::memory::alloc(), &this->video_session) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_parameters(lava::device_ptr device)
{
    //H.264 Specification Section 7.4.2.1.1: 
    //https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-H.264-202108-I!!PDF-E&type=items
    StdVideoH264SpsFlags sps_flags;
    sps_flags.constraint_set0_flag = 0;                                            // No constraints
    sps_flags.constraint_set1_flag = 0;                                            // No constraints
    sps_flags.constraint_set2_flag = 0;                                            // No constraints
    sps_flags.constraint_set3_flag = 0;                                            // No constraints
    sps_flags.constraint_set4_flag = 0;                                            // No constraints
    sps_flags.constraint_set5_flag = 0;                                            // No constraints
    sps_flags.direct_8x8_inference_flag = 1;                                       // Not Required, only for b-frames
    sps_flags.mb_adaptive_frame_field_flag = 0;                                    // Not Required
    sps_flags.frame_mbs_only_flag = 1;                                             // Only frames no fields
    sps_flags.delta_pic_order_always_zero_flag = 0;                                // Not Required
    sps_flags.separate_colour_plane_flag = 0;                                      // Not Required
    sps_flags.gaps_in_frame_num_value_allowed_flag = 0;                            // No gaps allowed
    sps_flags.qpprime_y_zero_transform_bypass_flag = 0;                            // Not Required
    sps_flags.frame_cropping_flag = 1;                                             // Use crop to adjust the image size
    sps_flags.seq_scaling_matrix_present_flag = 0;                                 // Not Required
    sps_flags.vui_parameters_present_flag = 0;                                     // Dont send VUI

    StdVideoH264SequenceParameterSet sps_parameter;
    sps_parameter.flags = sps_flags;
    sps_parameter.profile_idc = STD_VIDEO_H264_PROFILE_IDC_HIGH;                   // High profile
    sps_parameter.level_idc = STD_VIDEO_H264_LEVEL_IDC_4_1;                        // Level 4.1
    sps_parameter.chroma_format_idc = STD_VIDEO_H264_CHROMA_FORMAT_IDC_420;        // Not Required
    sps_parameter.seq_parameter_set_id = this->sequence_parameter_id;              // Id of the sequence parameter set
    sps_parameter.bit_depth_luma_minus8 = 0;                                       // Not Required
    sps_parameter.bit_depth_chroma_minus8 = 0;                                     // Not Required
    sps_parameter.log2_max_frame_num_minus4 = 4;                                   // Use 8 bits for frame numbers
    sps_parameter.pic_order_cnt_type = STD_VIDEO_H264_POC_TYPE_0;                  // Use picture order count type 0
    sps_parameter.offset_for_non_ref_pic = 0;                                      // Not Required
    sps_parameter.offset_for_top_to_bottom_field = 0;                              // Not Required
    sps_parameter.log2_max_pic_order_cnt_lsb_minus4 = 4;                           // Use 8 bits for the ordering of the frames
    sps_parameter.num_ref_frames_in_pic_order_cnt_cycle = 0;                       // Not Required
    sps_parameter.max_num_ref_frames = this->setting_reference_frames;             // Set the number of reference frames
    sps_parameter.reserved1 = 0;                                                   // Not Required
    sps_parameter.pic_width_in_mbs_minus1 = this->setting_block_count.x - 1;       // Set image width in macroblocks
    sps_parameter.pic_height_in_map_units_minus1 = this->setting_block_count.y - 1;// Set image height in macroblocks
    sps_parameter.frame_crop_left_offset = 0;                                      // Set no crop left
    sps_parameter.frame_crop_right_offset = this->setting_frame_crop.x >> 1;       // Set crop right to get the correct frame width. Devide by two because of chroma subsampling.
    sps_parameter.frame_crop_top_offset = 0;                                       // Set no crop top
    sps_parameter.frame_crop_bottom_offset = this->setting_frame_crop.y >> 1;      // Set crop bottom to get the correct frame height. Devide by two because of chroma subsampling.
    sps_parameter.reserved2 = 0;                                                   // Not Required
    sps_parameter.pOffsetForRefFrame = nullptr;                                    // Not Required
    sps_parameter.pScalingLists = nullptr;                                         // Not Required
    sps_parameter.pSequenceParameterSetVui = nullptr;                              // Not Required
    
    //H.264 Specification Section 7.4.2.2:
    StdVideoH264PpsFlags pps_flags;
    pps_flags.transform_8x8_mode_flag = 1;                                         // Set to zero
    pps_flags.redundant_pic_cnt_present_flag = 0;                                  // Set to zero
    pps_flags.constrained_intra_pred_flag = 0;                                     // Set to zero
    pps_flags.deblocking_filter_control_present_flag = 1;                          // Set to zero, no deblocking
    pps_flags.weighted_pred_flag = 0;                                              // Set to zero
    pps_flags.bottom_field_pic_order_in_frame_present_flag = 0;                    // Set to zero
    pps_flags.entropy_coding_mode_flag = 1;                                        // Set to zero, use CABAC for encoding
    pps_flags.pic_scaling_matrix_present_flag = 0;                                 // Set to zero

    StdVideoH264PictureParameterSet pps_parameter;
    pps_parameter.flags = pps_flags;
    pps_parameter.seq_parameter_set_id = this->sequence_parameter_id;              // Id of the sequence parameter set that is referenced
    pps_parameter.pic_parameter_set_id = this->picture_parameter_id;               // Id of the picture parameter set                                        
    pps_parameter.num_ref_idx_l0_default_active_minus1 = 0;                        // Set to zero
    pps_parameter.num_ref_idx_l1_default_active_minus1 = 0;                        // Set to zero
    pps_parameter.weighted_bipred_idc = STD_VIDEO_H264_WEIGHTED_BIPRED_IDC_DEFAULT;// Set to default
    pps_parameter.pic_init_qp_minus26 = 0;                                         // Set to zero
    pps_parameter.pic_init_qs_minus26 = 0;                                         // Set to zero
    pps_parameter.chroma_qp_index_offset = 0;                                      // Set to zero
    pps_parameter.second_chroma_qp_index_offset = 0;                               // Set to zero
    pps_parameter.pScalingLists = nullptr;                                         // Not Required

    VkVideoEncodeH264SessionParametersAddInfoEXT add_info;
    add_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_ADD_INFO_EXT;
    add_info.pNext = nullptr;
    add_info.stdSPSCount = 1;
    add_info.pStdSPSs = &sps_parameter;
    add_info.stdPPSCount = 1;
    add_info.pStdPPSs = &pps_parameter;

    VkVideoEncodeH264SessionParametersCreateInfoEXT encode_create_info;
    encode_create_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_SESSION_PARAMETERS_CREATE_INFO_EXT;
    encode_create_info.pNext = nullptr;
    encode_create_info.maxStdSPSCount = 1;
    encode_create_info.maxStdPPSCount = 1;
    encode_create_info.pParametersAddInfo = &add_info;

    VkVideoSessionParametersCreateInfoKHR video_create_info;
    video_create_info.sType = VK_STRUCTURE_TYPE_VIDEO_SESSION_PARAMETERS_CREATE_INFO_KHR;
    video_create_info.pNext = &encode_create_info;
    video_create_info.flags = 0;
    video_create_info.videoSessionParametersTemplate = nullptr;
    video_create_info.videoSession = this->video_session;

    if (vkCreateVideoSessionParametersKHR(device->get(), &video_create_info, lava::memory::alloc(), &this->video_session_paremeters) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::bind_session_memory(lava::device_ptr device)
{
    uint32_t requirement_count = 0;
    if (vkGetVideoSessionMemoryRequirementsKHR(device->get(), this->video_session, &requirement_count, nullptr) != VK_SUCCESS)
    {
        return false;
    }

    std::vector<VkVideoSessionMemoryRequirementsKHR> requirement_list;
    requirement_list.resize(requirement_count);

    for (uint32_t index = 0; index < requirement_count; index++)
    {
        requirement_list[index].sType = VK_STRUCTURE_TYPE_VIDEO_SESSION_MEMORY_REQUIREMENTS_KHR;
        requirement_list[index].pNext = nullptr;
    }

    if (vkGetVideoSessionMemoryRequirementsKHR(device->get(), this->video_session, &requirement_count, requirement_list.data()) != VK_SUCCESS)
    {
        return false;
    }

    std::vector<VkBindVideoSessionMemoryInfoKHR> bind_list;

    for (const VkVideoSessionMemoryRequirementsKHR& requirement : requirement_list)
    {
        VmaAllocationCreateInfo create_info;
        create_info.flags = 0;
        create_info.usage = VMA_MEMORY_USAGE_UNKNOWN;
        create_info.requiredFlags = 0;
        create_info.preferredFlags = 0;
        create_info.memoryTypeBits = 0;
        create_info.pool = nullptr;
        create_info.pUserData = nullptr;
        create_info.priority = 1.0f;

        VmaAllocation memory;
        VmaAllocationInfo info;
        if (vmaAllocateMemory(device->alloc(), &requirement.memoryRequirements, &create_info, &memory, &info) != VK_SUCCESS)
        {
            return false;
        }

        this->video_session_memory.push_back(memory);

        VkBindVideoSessionMemoryInfoKHR bind;
        bind.sType = VK_STRUCTURE_TYPE_BIND_VIDEO_SESSION_MEMORY_INFO_KHR;
        bind.pNext = nullptr;
        bind.memoryBindIndex = requirement.memoryBindIndex;
        bind.memory = info.deviceMemory;
        bind.memoryOffset = info.offset;
        bind.memorySize = info.size;

        bind_list.push_back(bind);
    }

    if (vkBindVideoSessionMemoryKHR(device->get(), this->video_session, bind_list.size(), bind_list.data()) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_sampler(lava::device_ptr device)
{
    VkSamplerCreateInfo sampler_create_info;
    sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_create_info.pNext = nullptr;
    sampler_create_info.flags = 0;
    sampler_create_info.magFilter = VK_FILTER_NEAREST;
    sampler_create_info.minFilter = VK_FILTER_NEAREST;
    sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.mipLodBias = 0.0f;
    sampler_create_info.anisotropyEnable = VK_FALSE;
    sampler_create_info.maxAnisotropy = 1.0f;
    sampler_create_info.compareEnable = VK_FALSE;
    sampler_create_info.compareOp = VK_COMPARE_OP_LESS;
    sampler_create_info.minLod = 0.0f;
    sampler_create_info.maxLod = 0.0f;
    sampler_create_info.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
    sampler_create_info.unnormalizedCoordinates = VK_TRUE;

    if (vkCreateSampler(device->get(), &sampler_create_info, lava::memory::alloc(), &this->sampler) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_pools(lava::device_ptr device, uint32_t frame_count)
{
    VkCommandPoolCreateInfo command_create_info;
    command_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_create_info.pNext = nullptr;
    command_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    command_create_info.queueFamilyIndex = this->encode_queue->family;

    if (vkCreateCommandPool(device->get(), &command_create_info, lava::memory::alloc(), &this->command_pool) != VK_SUCCESS)
    {
        return false;
    }

    VkQueryPoolCreateInfo query_create_info;
    query_create_info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
    query_create_info.pNext = &this->video_profile;
    query_create_info.flags = 0;
    query_create_info.queryType = VK_QUERY_TYPE_VIDEO_ENCODE_BITSTREAM_BUFFER_RANGE_KHR;
    query_create_info.queryCount = frame_count;
    query_create_info.pipelineStatistics = 0;

    if (vkCreateQueryPool(device->get(), &query_create_info, lava::memory::alloc(), &this->query_pool) != VK_SUCCESS)
    {
        return false;    
    }

    this->descriptor_pool = lava::make_descriptor_pool();
    
    if (!this->descriptor_pool->create(device, {{VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, frame_count * 2}}, frame_count * 2))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_layouts(lava::device_ptr device)
{
    this->descriptor_layout = lava::make_descriptor();
    this->descriptor_layout->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);

    if (!this->descriptor_layout->create(device))
    {
        return false;
    }

    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add_descriptor(this->descriptor_layout);

    if (!this->pipeline_layout->create(device))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_image_memory(lava::device_ptr device, VkImage image, VkImageAspectFlagBits image_aspect, VmaAllocation& memory)
{
    bool is_plane = false;

    if (image_aspect == VK_IMAGE_ASPECT_PLANE_0_BIT || image_aspect == VK_IMAGE_ASPECT_PLANE_1_BIT || image_aspect == VK_IMAGE_ASPECT_PLANE_2_BIT)
    {
        is_plane = true;
    }

    VkImagePlaneMemoryRequirementsInfo plane_info;
    plane_info.sType = VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO;
    plane_info.pNext = nullptr;
    plane_info.planeAspect = image_aspect;

    VkImageMemoryRequirementsInfo2 memoy_info;
    memoy_info.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
    memoy_info.pNext = is_plane ? &plane_info : nullptr;
    memoy_info.image = image;

    VkMemoryRequirements2 memory_requiremtents;
    memory_requiremtents.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
    memory_requiremtents.pNext = nullptr;

    vkGetImageMemoryRequirements2(device->get(), &memoy_info, &memory_requiremtents);

    VmaAllocationCreateInfo create_info;
    create_info.flags = 0;
    create_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    create_info.requiredFlags = 0;
    create_info.preferredFlags = 0;
    create_info.memoryTypeBits = 0;
    create_info.pool = nullptr;
    create_info.pUserData = nullptr;
    create_info.priority = 1.0f;

    if (vmaAllocateMemory(device->alloc(), &memory_requiremtents.memoryRequirements, &create_info, &memory, nullptr) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_image_view(lava::device_ptr device, VkImage image, VkFormat image_format, VkImageView& image_view)
{
    VkComponentMapping components;
    components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    VkImageSubresourceRange subresource_range;
    subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_range.baseMipLevel = 0;
    subresource_range.levelCount = 1;
    subresource_range.baseArrayLayer = 0;
    subresource_range.layerCount = 1;

    VkImageViewCreateInfo create_info;
    create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = 0;
    create_info.image = image;
    create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    create_info.format = image_format;
    create_info.components = components;
    create_info.subresourceRange = subresource_range;

    if (vkCreateImageView(device->get(), &create_info, lava::memory::alloc(), &image_view) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_input_images(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    //TODO: Share memory between combined image and luma, chroma image.
    //The input image (input_combined) for the encoding is a multi plane images.
    //This means that it is composed of two seperate images.
    //The first image defines the luma part (Y) and the second part the chroma part (CrCb).
    //Therefore, the two planes have to be allocated seperatly.
    //Since it it is not possible to use a multiplane image as a color attachment, two additional images are created (input_luma, input_chroma).
    //These two images share the same device memory with the two planes of the input images.
    //This means when writing to input_luma or input_chroma, the corresponding plane in input_combined is also modified.

    //TODO: Use explicit queue ownership transfers.

    VkExtent3D image_extend;
    image_extend.width = this->setting_image_size.x;
    image_extend.height = this->setting_image_size.y;
    image_extend.depth = 1;

    VkExtent3D image_extend_subsampled;
    image_extend_subsampled.width = this->setting_image_size.x / 2;
    image_extend_subsampled.height = this->setting_image_size.y / 2;
    image_extend_subsampled.depth = 1;

    std::array<uint32_t, 2> queue_families;
    queue_families[0] = this->default_queue.family;
    queue_families[1] = this->encode_queue->family;

    VkImageCreateInfo luma_create_info;
    luma_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    luma_create_info.pNext = nullptr;
    luma_create_info.flags = 0;
    luma_create_info.imageType = VK_IMAGE_TYPE_2D;
    luma_create_info.format = VK_FORMAT_R8_UNORM;
    luma_create_info.extent = image_extend;
    luma_create_info.mipLevels = 1;
    luma_create_info.arrayLayers = 1;
    luma_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    luma_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    luma_create_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    luma_create_info.sharingMode = VK_SHARING_MODE_CONCURRENT;
    luma_create_info.queueFamilyIndexCount = queue_families.size();
    luma_create_info.pQueueFamilyIndices = queue_families.data();
    luma_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &luma_create_info, lava::memory::alloc(), &frame->input_luma) != VK_SUCCESS)
    {
        return false;
    }

    VkImageCreateInfo chroma_create_info;
    chroma_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    chroma_create_info.pNext = nullptr;
    chroma_create_info.flags = 0;
    chroma_create_info.imageType = VK_IMAGE_TYPE_2D;
    chroma_create_info.format = VK_FORMAT_R8G8_UNORM;
    chroma_create_info.extent = image_extend;
    chroma_create_info.mipLevels = 1;
    chroma_create_info.arrayLayers = 1;
    chroma_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    chroma_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    chroma_create_info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    chroma_create_info.sharingMode = VK_SHARING_MODE_CONCURRENT;
    chroma_create_info.queueFamilyIndexCount = queue_families.size();
    chroma_create_info.pQueueFamilyIndices = queue_families.data();
    chroma_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &chroma_create_info, lava::memory::alloc(), &frame->input_chroma) != VK_SUCCESS)
    {
        return false;
    }

    VkImageCreateInfo chroma_subsampled_create_info;
    chroma_subsampled_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    chroma_subsampled_create_info.pNext = nullptr;
    chroma_subsampled_create_info.flags = 0;
    chroma_subsampled_create_info.imageType = VK_IMAGE_TYPE_2D;
    chroma_subsampled_create_info.format = VK_FORMAT_R8G8_UNORM;
    chroma_subsampled_create_info.extent = image_extend_subsampled;
    chroma_subsampled_create_info.mipLevels = 1;
    chroma_subsampled_create_info.arrayLayers = 1;
    chroma_subsampled_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    chroma_subsampled_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    chroma_subsampled_create_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    chroma_subsampled_create_info.sharingMode = VK_SHARING_MODE_CONCURRENT;
    chroma_subsampled_create_info.queueFamilyIndexCount = queue_families.size();
    chroma_subsampled_create_info.pQueueFamilyIndices = queue_families.data();
    chroma_subsampled_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &chroma_subsampled_create_info, lava::memory::alloc(), &frame->input_chroma_subsampled) != VK_SUCCESS)
    {
        return false;
    }

    VkVideoProfileListInfoKHR profile_list;
    profile_list.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_LIST_INFO_KHR;
    profile_list.pNext = nullptr;
    profile_list.profileCount = 1;
    profile_list.pProfiles = &this->video_profile;

    VkImageCreateInfo combined_create_info;
    combined_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    combined_create_info.pNext = &profile_list;
    combined_create_info.flags = 0;
    combined_create_info.imageType = VK_IMAGE_TYPE_2D;
    combined_create_info.format = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    combined_create_info.extent = image_extend;
    combined_create_info.mipLevels = 1;
    combined_create_info.arrayLayers = 1;
    combined_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    combined_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    combined_create_info.usage = VK_IMAGE_USAGE_VIDEO_ENCODE_SRC_BIT_KHR | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    combined_create_info.sharingMode = VK_SHARING_MODE_CONCURRENT;
    combined_create_info.queueFamilyIndexCount = queue_families.size();
    combined_create_info.pQueueFamilyIndices = queue_families.data();
    combined_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &combined_create_info, lava::memory::alloc(), &frame->input_combined) != VK_SUCCESS)
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->input_combined, VK_IMAGE_ASPECT_COLOR_BIT, frame->input_memory_combined))
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->input_luma, VK_IMAGE_ASPECT_COLOR_BIT, frame->input_memory_luma))
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->input_chroma, VK_IMAGE_ASPECT_COLOR_BIT, frame->input_memory_chroma))
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->input_chroma_subsampled, VK_IMAGE_ASPECT_COLOR_BIT, frame->input_memory_chroma_subsampled))
    {
        return false;
    }
    
    VmaAllocationInfo combined_memory;
    VmaAllocationInfo luma_memory;
    VmaAllocationInfo chroma_memory;
    VmaAllocationInfo chroma_subsampled_memory;

    vmaGetAllocationInfo(device->alloc(), frame->input_memory_combined, &combined_memory);
    vmaGetAllocationInfo(device->alloc(), frame->input_memory_luma, &luma_memory);
    vmaGetAllocationInfo(device->alloc(), frame->input_memory_chroma, &chroma_memory);
    vmaGetAllocationInfo(device->alloc(), frame->input_memory_chroma_subsampled, &chroma_subsampled_memory);

    std::array<VkBindImageMemoryInfo, 4> bind_info_list;
    bind_info_list[0].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[0].pNext = nullptr;
    bind_info_list[0].image = frame->input_combined;
    bind_info_list[0].memory = combined_memory.deviceMemory;
    bind_info_list[0].memoryOffset = combined_memory.offset;

    bind_info_list[1].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[1].pNext = nullptr;
    bind_info_list[1].image = frame->input_luma;
    bind_info_list[1].memory = luma_memory.deviceMemory;
    bind_info_list[1].memoryOffset = luma_memory.offset;

    bind_info_list[2].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[2].pNext = nullptr;
    bind_info_list[2].image = frame->input_chroma;
    bind_info_list[2].memory = chroma_memory.deviceMemory;
    bind_info_list[2].memoryOffset = chroma_memory.offset;

    bind_info_list[3].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[3].pNext = nullptr;
    bind_info_list[3].image = frame->input_chroma_subsampled;
    bind_info_list[3].memory = chroma_subsampled_memory.deviceMemory;
    bind_info_list[3].memoryOffset = chroma_subsampled_memory.offset;

    if (vkBindImageMemory2(device->get(), bind_info_list.size(), bind_info_list.data()) != VK_SUCCESS)
    {
        return false;
    }

    if (!this->create_image_view(device, frame->input_luma, VK_FORMAT_R8_UNORM, frame->input_view_luma))
    {
        return false;
    }

    if (!this->create_image_view(device, frame->input_chroma, VK_FORMAT_R8G8_UNORM, frame->input_view_chroma))
    {
        return false;
    }

    if (!this->create_image_view(device, frame->input_chroma_subsampled, VK_FORMAT_R8G8_UNORM, frame->input_view_chroma_subsampled))
    {
        return false;
    }

    if (!this->create_image_view(device, frame->input_combined, VK_FORMAT_G8_B8R8_2PLANE_420_UNORM, frame->input_view_combined))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_slot_image(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkExtent3D image_extend;
    image_extend.width = this->setting_image_size.x;
    image_extend.height = this->setting_image_size.y;
    image_extend.depth = 1;

    VkVideoProfileListInfoKHR profile_list;
    profile_list.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_LIST_INFO_KHR;
    profile_list.pNext = nullptr;
    profile_list.profileCount = 1;
    profile_list.pProfiles = &this->video_profile;

    VkImageCreateInfo combined_create_info;
    combined_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    combined_create_info.pNext = &profile_list;
    combined_create_info.flags = VK_IMAGE_CREATE_DISJOINT_BIT;
    combined_create_info.imageType = VK_IMAGE_TYPE_2D;
    combined_create_info.format = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    combined_create_info.extent = image_extend;
    combined_create_info.mipLevels = 1;
    combined_create_info.arrayLayers = 1;
    combined_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    combined_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    combined_create_info.usage = VK_IMAGE_USAGE_VIDEO_ENCODE_DPB_BIT_KHR;
    combined_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    combined_create_info.queueFamilyIndexCount = 0;
    combined_create_info.pQueueFamilyIndices = nullptr; //Use explicit ownership transfers
    combined_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (vkCreateImage(device->get(), &combined_create_info, lava::memory::alloc(), &frame->slot_image) != VK_SUCCESS)
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->slot_image, VK_IMAGE_ASPECT_PLANE_0_BIT, frame->slot_memory_luma))
    {
        return false;
    }

    if (!this->create_image_memory(device, frame->slot_image, VK_IMAGE_ASPECT_PLANE_1_BIT, frame->slot_memory_chroma))
    {
        return false;
    }

    VmaAllocationInfo luma_memory;
    VmaAllocationInfo chroma_memory;

    vmaGetAllocationInfo(device->alloc(), frame->slot_memory_luma, &luma_memory);
    vmaGetAllocationInfo(device->alloc(), frame->slot_memory_chroma, &chroma_memory);

    std::array<VkBindImagePlaneMemoryInfo, 2> plane_info_list;
    plane_info_list[0].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO;
    plane_info_list[0].pNext = nullptr;
    plane_info_list[0].planeAspect = VK_IMAGE_ASPECT_PLANE_0_BIT;

    plane_info_list[1].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO;
    plane_info_list[1].pNext = nullptr;
    plane_info_list[1].planeAspect = VK_IMAGE_ASPECT_PLANE_1_BIT;

    std::array<VkBindImageMemoryInfo, 2> bind_info_list;
    bind_info_list[0].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[0].pNext = &plane_info_list[0];
    bind_info_list[0].image = frame->slot_image;
    bind_info_list[0].memory = luma_memory.deviceMemory;
    bind_info_list[0].memoryOffset = luma_memory.offset;

    bind_info_list[1].sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
    bind_info_list[1].pNext = &plane_info_list[1];
    bind_info_list[1].image = frame->slot_image;
    bind_info_list[1].memory = chroma_memory.deviceMemory;
    bind_info_list[1].memoryOffset = chroma_memory.offset;

    if (vkBindImageMemory2(device->get(), bind_info_list.size(), bind_info_list.data()) != VK_SUCCESS)
    {
        return false;
    }

    if (!this->create_image_view(device, frame->slot_image, VK_FORMAT_G8_B8R8_2PLANE_420_UNORM, frame->slot_view))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_output_buffer(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkVideoProfileListInfoKHR profile_list;
    profile_list.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_LIST_INFO_KHR;
    profile_list.pNext = nullptr;
    profile_list.profileCount = 1;
    profile_list.pProfiles = &this->video_profile;

    VkBufferCreateInfo create_info;
    create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    create_info.pNext = &profile_list;
    create_info.flags = 0;
    create_info.size = this->setting_buffer_size;
    create_info.usage = VK_BUFFER_USAGE_VIDEO_ENCODE_DST_BIT_KHR;
    create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    create_info.queueFamilyIndexCount = 0;
    create_info.pQueueFamilyIndices = nullptr;

    if (vkCreateBuffer(device->get(), &create_info, lava::memory::alloc(), &frame->output_buffer) != VK_SUCCESS)
    {
        return false;
    }

    VkBufferMemoryRequirementsInfo2 memory_info;
    memory_info.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2;
    memory_info.pNext = nullptr;
    memory_info.buffer = frame->output_buffer;

    VkMemoryRequirements2 memory_requiremtents;
    memory_requiremtents.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
    memory_requiremtents.pNext = nullptr;

    vkGetBufferMemoryRequirements2(device->get(), &memory_info, &memory_requiremtents);

    VmaAllocationCreateInfo allocation_create_info;
    allocation_create_info.flags = 0;
    allocation_create_info.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;
    allocation_create_info.requiredFlags = 0;
    allocation_create_info.preferredFlags = 0;
    allocation_create_info.memoryTypeBits = 0;
    allocation_create_info.pool = nullptr;
    allocation_create_info.pUserData = nullptr;
    allocation_create_info.priority = 1.0f;

    VmaAllocationInfo allocation_info;

    if (vmaAllocateMemory(device->alloc(), &memory_requiremtents.memoryRequirements, &allocation_create_info, &frame->output_memory, &allocation_info) != VK_SUCCESS)
    {
        return false;
    }

    VkBindBufferMemoryInfo bind_info;
    bind_info.sType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO;
    bind_info.pNext = nullptr;
    bind_info.buffer = frame->output_buffer;
    bind_info.memory = allocation_info.deviceMemory;
    bind_info.memoryOffset = allocation_info.offset;

    if (vkBindBufferMemory2(device->get(), 1, &bind_info) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_convert_pass(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkClearValue luma_clear_value;
    luma_clear_value.color.float32[0] = 0.0f;
    luma_clear_value.color.float32[1] = 0.0f;
    luma_clear_value.color.float32[2] = 0.0f;
    luma_clear_value.color.float32[3] = 0.0f;

    VkClearValue chroma_clear_value;
    chroma_clear_value.color.float32[0] = 0.0f;
    chroma_clear_value.color.float32[1] = 0.0f;
    chroma_clear_value.color.float32[2] = 0.0f;
    chroma_clear_value.color.float32[3] = 0.0f;

    lava::attachment::ptr luma_attachment = lava::make_attachment(VK_FORMAT_R8_UNORM);
    luma_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    luma_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    luma_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    lava::attachment::ptr chroma_attachment = lava::make_attachment(VK_FORMAT_R8G8_UNORM);
    chroma_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    chroma_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    chroma_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    VkAttachmentReference luma_attachment_reference;
    luma_attachment_reference.attachment = 0;
    luma_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference chroma_attachment_reference;
    chroma_attachment_reference.attachment = 1;
    chroma_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_color_attachments(
    {
        luma_attachment_reference,
        chroma_attachment_reference
    });
    
    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT | VK_PIPELINE_STAGE_TRANSFER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_TRANSFER_READ_BIT);

    frame->convert_pass = lava::make_render_pass(device);
    frame->convert_pass->add(subpass);
    frame->convert_pass->add(subpass_begin_dependency);
    frame->convert_pass->add(subpass_end_dependency);
    frame->convert_pass->add(luma_attachment);
    frame->convert_pass->add(chroma_attachment);
    frame->convert_pass->set_clear_values(
    {
        luma_clear_value,
        chroma_clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        this->setting_image_size
    };

    lava::VkImageViews framebuffer_views =
    {
        frame->input_view_luma,
        frame->input_view_chroma
    };

    if (!frame->convert_pass->create({framebuffer_views}, framebuffer_area))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_convert_pipeline(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkPipelineColorBlendAttachmentState luma_blend_state;
    luma_blend_state.blendEnable = VK_FALSE;
    luma_blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    luma_blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    luma_blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    luma_blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    luma_blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    luma_blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    luma_blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendAttachmentState chroma_blend_state;
    chroma_blend_state.blendEnable = VK_FALSE;
    chroma_blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    chroma_blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    chroma_blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    frame->convert_pipeline = lava::make_graphics_pipeline(device);
    frame->convert_pipeline->set_layout(this->pipeline_layout);
    frame->convert_pipeline->set_input_assembly_topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP);
    frame->convert_pipeline->add_color_blend_attachment(luma_blend_state);
    frame->convert_pipeline->add_color_blend_attachment(chroma_blend_state);

    if (!frame->convert_pipeline->add_shader(lava::file_data("dpr/binary/encode_color_conversion_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!frame->convert_pipeline->add_shader(lava::file_data("dpr/binary/encode_color_conversion_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        return false;
    }

    frame->convert_pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->convert_pass(command_buffer);
    };

    if (!frame->convert_pipeline->create(frame->convert_pass->get()))
    {
        return false;
    }

    frame->convert_pass->add_front(frame->convert_pipeline);
    frame->convert_descriptor = this->descriptor_layout->allocate(this->descriptor_pool->get());

    return true;
}

bool VulkanEncoder::create_subsample_pass(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkClearValue chroma_clear_value;
    chroma_clear_value.color.float32[0] = 0.0f;
    chroma_clear_value.color.float32[1] = 0.0f;
    chroma_clear_value.color.float32[2] = 0.0f;
    chroma_clear_value.color.float32[3] = 0.0f;

    lava::attachment::ptr chroma_attachment = lava::make_attachment(VK_FORMAT_R8G8_UNORM);
    chroma_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    chroma_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    chroma_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    VkAttachmentReference chroma_attachment_reference;
    chroma_attachment_reference.attachment = 0;
    chroma_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_color_attachments(
    {
        chroma_attachment_reference
    });
    
    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);

    frame->subsample_pass = lava::make_render_pass(device);
    frame->subsample_pass->add(subpass);
    frame->subsample_pass->add(subpass_begin_dependency);
    frame->subsample_pass->add(subpass_end_dependency);
    frame->subsample_pass->add(chroma_attachment);
    frame->subsample_pass->set_clear_values(
    {
        chroma_clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        this->setting_image_size / 2u
    };

    lava::VkImageViews framebuffer_views =
    {
        frame->input_view_chroma_subsampled
    };

    if (!frame->subsample_pass->create({ framebuffer_views }, framebuffer_area))
    {
        return false;
    }

    return true;
}

bool VulkanEncoder::create_subsample_pipeline(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    VkPipelineColorBlendAttachmentState chroma_blend_state;
    chroma_blend_state.blendEnable = VK_FALSE;
    chroma_blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    chroma_blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    chroma_blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    chroma_blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    frame->subsample_pipeline = lava::make_graphics_pipeline(device);
    frame->subsample_pipeline->set_layout(this->pipeline_layout);
    frame->subsample_pipeline->set_input_assembly_topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP);
    frame->subsample_pipeline->add_color_blend_attachment(chroma_blend_state);

    if (!frame->subsample_pipeline->add_shader(lava::file_data("dpr/binary/encode_chroma_subsampling_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!frame->subsample_pipeline->add_shader(lava::file_data("dpr/binary/encode_chroma_subsampling_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        return false;
    }

    frame->subsample_pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->subsample_pass(command_buffer);
    };

    if (!frame->subsample_pipeline->create(frame->subsample_pass->get()))
    {
        return false;
    }

    frame->subsample_pass->add_front(frame->subsample_pipeline);
    frame->subsample_descriptor = this->descriptor_layout->allocate(this->descriptor_pool->get());

    VkDescriptorImageInfo image_info;
    image_info.sampler = this->sampler;
    image_info.imageView = frame->input_view_chroma;
    image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkWriteDescriptorSet write_info;
    write_info.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_info.pNext = nullptr;
    write_info.dstSet = frame->subsample_descriptor;
    write_info.dstBinding = 0;
    write_info.dstArrayElement = 0;
    write_info.descriptorCount = 1;
    write_info.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_info.pImageInfo = &image_info;
    write_info.pBufferInfo = nullptr;
    write_info.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(device->get(), 1, &write_info, 0, nullptr);

    return true;
}

bool VulkanEncoder::create_encode_resources(lava::device_ptr device, asio::executor executor, VulkanEncoderFrame::Ptr frame)
{
    frame->encode_fence = make_extern_fence();

    if (!frame->encode_fence->create(device, executor, false))
    {
        return false;
    }

    VkSemaphoreCreateInfo semaphore_create_info;
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphore_create_info.pNext = nullptr;
    semaphore_create_info.flags = 0;

    if (vkCreateSemaphore(device->get(), &semaphore_create_info, lava::memory::alloc(), &frame->render_semaphore) != VK_SUCCESS)
    {
        return false;
    }

    if (vkCreateSemaphore(device->get(), &semaphore_create_info, lava::memory::alloc(), &frame->encode_semaphore) != VK_SUCCESS)
    {
        return false;
    }

    VkCommandBufferAllocateInfo buffer_allocate_info;
    buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    buffer_allocate_info.pNext = nullptr;
    buffer_allocate_info.commandPool = this->command_pool;
    buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    buffer_allocate_info.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(device->get(), &buffer_allocate_info, &frame->encode_command_buffer) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

void VulkanEncoder::destroy_session(lava::device_ptr device)
{
    vkDestroyVideoSessionKHR(device->get(), this->video_session, lava::memory::alloc());
    vkDestroyVideoSessionParametersKHR(device->get(), this->video_session_paremeters, lava::memory::alloc());

    for (VmaAllocation allocation : this->video_session_memory)
    {
        vmaFreeMemory(device->alloc(), allocation);
    }

    this->video_session_memory.clear();
}

void VulkanEncoder::destroy_resources(lava::device_ptr device)
{
    vkDestroySampler(device->get(), this->sampler, lava::memory::alloc());
    vkDestroyCommandPool(device->get(), this->command_pool, lava::memory::alloc());
    vkDestroyQueryPool(device->get(), this->query_pool, lava::memory::alloc());

    this->descriptor_pool->destroy();
    this->descriptor_layout->destroy();
    this->pipeline_layout->destroy();
}

void VulkanEncoder::destroy_frame(lava::device_ptr device, VulkanEncoderFrame::Ptr frame)
{
    vkDestroyImageView(device->get(), frame->input_view_luma, lava::memory::alloc());
    vkDestroyImageView(device->get(), frame->input_view_chroma, lava::memory::alloc());
    vkDestroyImageView(device->get(), frame->input_view_chroma_subsampled, lava::memory::alloc());
    vkDestroyImageView(device->get(), frame->input_view_combined, lava::memory::alloc());
    vkDestroyImageView(device->get(), frame->slot_view, lava::memory::alloc());

    vkDestroyImage(device->get(), frame->input_luma, lava::memory::alloc());
    vkDestroyImage(device->get(), frame->input_chroma, lava::memory::alloc());
    vkDestroyImage(device->get(), frame->input_chroma_subsampled, lava::memory::alloc());
    vkDestroyImage(device->get(), frame->input_combined, lava::memory::alloc());
    vkDestroyImage(device->get(), frame->slot_image, lava::memory::alloc());

    vmaFreeMemory(device->alloc(), frame->input_memory_luma);
    vmaFreeMemory(device->alloc(), frame->input_memory_chroma);
    vmaFreeMemory(device->alloc(), frame->input_memory_chroma_subsampled);
    vmaFreeMemory(device->alloc(), frame->input_memory_combined);
    vmaFreeMemory(device->alloc(), frame->slot_memory_luma);
    vmaFreeMemory(device->alloc(), frame->slot_memory_chroma);

    vkDestroyBuffer(device->get(), frame->output_buffer, lava::memory::alloc());
    vmaFreeMemory(device->alloc(), frame->output_memory);

    frame->convert_pipeline->destroy();
    frame->convert_pass->destroy();
    this->descriptor_layout->free(frame->convert_descriptor, this->descriptor_pool->get());

    frame->subsample_pipeline->destroy();
    frame->subsample_pass->destroy();
    this->descriptor_layout->free(frame->subsample_descriptor, this->descriptor_pool->get());

    vkFreeCommandBuffers(device->get(), this->command_pool, 1, &frame->encode_command_buffer);

    vkDestroySemaphore(device->get(), frame->render_semaphore, lava::memory::alloc());
    vkDestroySemaphore(device->get(), frame->encode_semaphore, lava::memory::alloc());
    frame->encode_fence->destroy(device);
}

bool VulkanEncoder::check_encode_support(lava::device_ptr device, const lava::queue& queue) const
{
    uint32_t property_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties2(device->get_physical_device()->get(), &property_count, nullptr);
    
    std::vector<VkQueueFamilyVideoPropertiesKHR> video_property_list;
    video_property_list.resize(property_count);
    std::vector<VkQueueFamilyProperties2> property_list;
    property_list.resize(property_count);

    for (uint32_t index = 0; index < property_count; index++)
    {
        video_property_list[index].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_VIDEO_PROPERTIES_KHR;
        video_property_list[index].pNext = nullptr;
        property_list[index].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
        property_list[index].pNext = &video_property_list[index];
    }

    vkGetPhysicalDeviceQueueFamilyProperties2(device->get_physical_device()->get(), &property_count, property_list.data());

    if ((video_property_list[queue.family].videoCodecOperations & VK_VIDEO_CODEC_OPERATION_ENCODE_H264_BIT_EXT) != 0)
    {
        return true;
    }

    return false;
}

bool VulkanEncoder::check_format_support(lava::device_ptr device, VkImageUsageFlags usage, VkFormat format) const
{
    VkVideoProfileListInfoKHR video_profiles;
    video_profiles.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_LIST_INFO_KHR;
    video_profiles.pNext = nullptr;
    video_profiles.profileCount = 1;
    video_profiles.pProfiles = &this->video_profile;

    VkPhysicalDeviceVideoFormatInfoKHR video_format_info;
    video_format_info.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR;
    video_format_info.pNext = &video_profiles;
    video_format_info.imageUsage = usage;

    uint32_t format_count = 0;

    if (vkGetPhysicalDeviceVideoFormatPropertiesKHR(device->get_physical_device()->get(), &video_format_info, &format_count, nullptr) != VK_SUCCESS)
    {
        return false;
    }

    std::vector<VkVideoFormatPropertiesKHR> format_list;
    format_list.resize(format_count);

    for (VkVideoFormatPropertiesKHR& format_properties : format_list)
    {
        format_properties.sType = VK_STRUCTURE_TYPE_VIDEO_FORMAT_PROPERTIES_KHR;
        format_properties.pNext = nullptr;
    }

    if (vkGetPhysicalDeviceVideoFormatPropertiesKHR(device->get_physical_device()->get(), &video_format_info, &format_count, format_list.data()) != VK_SUCCESS)
    {
        return false;
    }

    for (const VkVideoFormatPropertiesKHR& format_properties : format_list)
    {
        if (format_properties.format == format)
        {
            return true;
        }
    }

    return false;
}

void VulkanEncoder::convert_pass(VkCommandBuffer command_buffer)
{
    vkCmdDraw(command_buffer, 4, 1, 0, 0); //Draw fullscreen quad
}

void VulkanEncoder::subsample_pass(VkCommandBuffer command_buffer)
{
    vkCmdDraw(command_buffer, 4, 1, 0, 0); //Draw fullscreen quad
}

void VulkanEncoder::encode_pass_control_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame)
{
    VkVideoBeginCodingInfoKHR begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR;
    begin_info.pNext = nullptr;
    begin_info.flags = 0;
    begin_info.videoSession = this->video_session;
    begin_info.videoSessionParameters = this->video_session_paremeters;
    begin_info.referenceSlotCount = 0;
    begin_info.pReferenceSlots = nullptr;

    vkCmdBeginVideoCodingKHR(command_buffer, &begin_info);

    this->encode_pass_control_command(command_buffer, frame);

    VkVideoEndCodingInfoKHR end_info;
    end_info.sType = VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR;
    end_info.pNext = nullptr;
    end_info.flags = 0;

    vkCmdEndVideoCodingKHR(command_buffer, &end_info);
}

void VulkanEncoder::encode_pass_control_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame)
{
    VkVideoEncodeH264RateControlLayerInfoEXT codec_layer_info;
    codec_layer_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_RATE_CONTROL_LAYER_INFO_EXT;
    codec_layer_info.pNext = nullptr;
    codec_layer_info.temporalLayerId = 0;
    codec_layer_info.useInitialRcQp = VK_FALSE;
    codec_layer_info.initialRcQp.qpI = 0;
    codec_layer_info.initialRcQp.qpP = 0;
    codec_layer_info.initialRcQp.qpB = 0;
    codec_layer_info.useMinQp = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? VK_FALSE : VK_TRUE;
    codec_layer_info.minQp.qpI = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : this->setting_quality_iframe;
    codec_layer_info.minQp.qpP = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : this->setting_quality_pframe;
    codec_layer_info.minQp.qpB = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : 51;
    codec_layer_info.useMaxQp = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? VK_FALSE : VK_TRUE;
    codec_layer_info.maxQp.qpI = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : this->setting_quality_iframe;
    codec_layer_info.maxQp.qpP = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : this->setting_quality_pframe;
    codec_layer_info.maxQp.qpB = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 0 : 51;
    codec_layer_info.useMaxFrameSize = VK_FALSE;
    codec_layer_info.maxFrameSize.frameISize = 0;
    codec_layer_info.maxFrameSize.framePSize = 0;
    codec_layer_info.maxFrameSize.frameBSize = 0;

    VkVideoEncodeRateControlLayerInfoKHR layer_info;
    layer_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_LAYER_INFO_KHR;
    layer_info.pNext = &codec_layer_info;
    layer_info.averageBitrate = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? (uint32_t)(this->setting_bit_rate * 1000000.0) : 0;
    layer_info.maxBitrate = 0;
    layer_info.frameRateNumerator = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? this->setting_frame_rate : 0;
    layer_info.frameRateDenominator = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 1 : 0;
    layer_info.virtualBufferSizeInMs = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 10 : 0;
    layer_info.initialVirtualBufferSizeInMs = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? 10 : 0;

    VkVideoEncodeH264RateControlInfoEXT codec_rate_info; //Currently not used
    codec_rate_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_RATE_CONTROL_INFO_EXT;
    codec_rate_info.pNext = nullptr;
    codec_rate_info.gopFrameCount = this->setting_key_rate;
    codec_rate_info.idrPeriod = this->setting_key_rate;
    codec_rate_info.consecutiveBFrameCount = 0;
    codec_rate_info.rateControlStructure = VK_VIDEO_ENCODE_H264_RATE_CONTROL_STRUCTURE_UNKNOWN_EXT;
    codec_rate_info.temporalLayerCount = 1;

    VkVideoEncodeRateControlInfoKHR rate_info; //Currently not used
    rate_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_RATE_CONTROL_INFO_KHR;
    rate_info.pNext = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? &codec_rate_info : nullptr;
    rate_info.flags = 0;
    rate_info.rateControlMode = (this->setting_mode == ENCODER_MODE_CONSTANT_BITRATE) ? VK_VIDEO_ENCODE_RATE_CONTROL_MODE_CBR_BIT_KHR : VK_VIDEO_ENCODE_RATE_CONTROL_MODE_NONE_BIT_KHR;
    rate_info.layerCount = 1;
    rate_info.pLayerConfigs = &layer_info;

    VkVideoCodingControlInfoKHR control_info;
    control_info.sType = VK_STRUCTURE_TYPE_VIDEO_CODING_CONTROL_INFO_KHR;
    control_info.pNext = &layer_info;
    control_info.flags = VK_VIDEO_CODING_CONTROL_RESET_BIT_KHR | VK_VIDEO_CODING_CONTROL_ENCODE_RATE_CONTROL_LAYER_BIT_KHR;

    vkCmdControlVideoCodingKHR(command_buffer, &control_info);

    this->invalidate_slots();
}

void VulkanEncoder::encode_pass_config_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame)
{
    VkVideoBeginCodingInfoKHR begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR;
    begin_info.pNext = nullptr;
    begin_info.flags = 0;
    begin_info.videoSession = this->video_session;
    begin_info.videoSessionParameters = this->video_session_paremeters;
    begin_info.referenceSlotCount = 0;
    begin_info.pReferenceSlots = nullptr;

    vkCmdBeginVideoCodingKHR(command_buffer, &begin_info);

    vkCmdResetQueryPool(command_buffer, this->query_pool, frame->output_query_index, 1);
    vkCmdBeginQuery(command_buffer, this->query_pool, frame->output_query_index, 0);

    this->encode_pass_config_command(command_buffer, frame);

    vkCmdEndQuery(command_buffer, this->query_pool, frame->output_query_index);

    VkVideoEndCodingInfoKHR end_info;
    end_info.sType = VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR;
    end_info.pNext = nullptr;
    end_info.flags = 0;

    vkCmdEndVideoCodingKHR(command_buffer, &end_info);

    VkBufferMemoryBarrier2 buffer_barrier;
    buffer_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    buffer_barrier.pNext = nullptr;
    buffer_barrier.srcStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
    buffer_barrier.srcAccessMask = VK_ACCESS_2_VIDEO_ENCODE_WRITE_BIT_KHR;
    buffer_barrier.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT;
    buffer_barrier.dstAccessMask = VK_ACCESS_2_HOST_READ_BIT;
    buffer_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_barrier.buffer = frame->output_buffer;
    buffer_barrier.offset = 0;
    buffer_barrier.size = VK_WHOLE_SIZE;

    VkDependencyInfo buffer_dependency;
    buffer_dependency.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    buffer_dependency.pNext = nullptr;
    buffer_dependency.dependencyFlags = 0;
    buffer_dependency.memoryBarrierCount = 0;
    buffer_dependency.pMemoryBarriers = nullptr;
    buffer_dependency.bufferMemoryBarrierCount = 1;
    buffer_dependency.pBufferMemoryBarriers = &buffer_barrier;
    buffer_dependency.imageMemoryBarrierCount = 0;
    buffer_dependency.pImageMemoryBarriers = nullptr;

    vkCmdPipelineBarrier2KHR(command_buffer, &buffer_dependency);
}

void VulkanEncoder::encode_pass_config_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame)
{
    VkVideoPictureResourceInfoKHR input_resource;
    input_resource.sType = VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_INFO_KHR;
    input_resource.pNext = nullptr;
    input_resource.codedOffset.x = 0;
    input_resource.codedOffset.y = 0;
    input_resource.codedExtent.width = 0;
    input_resource.codedExtent.height = 0;
    input_resource.baseArrayLayer = 0;
    input_resource.imageViewBinding = frame->input_view_combined;

    VkVideoEncodeH264EmitPictureParametersInfoEXT config_info;
    config_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_EMIT_PICTURE_PARAMETERS_INFO_EXT;
    config_info.pNext = nullptr;
    config_info.spsId = this->sequence_parameter_id;
    config_info.emitSpsEnable = VK_TRUE;
    config_info.ppsIdEntryCount = 1;
    config_info.ppsIdEntries = &this->picture_parameter_id;

    VkVideoEncodeInfoKHR encode_info;
    encode_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR;
    encode_info.pNext = &config_info;
    encode_info.flags = 0;
    encode_info.qualityLevel = 0;
    encode_info.dstBitstreamBuffer = frame->output_buffer;
    encode_info.dstBitstreamBufferOffset = 0;
    encode_info.dstBitstreamBufferMaxRange = this->setting_buffer_size;
    encode_info.srcPictureResource = input_resource;
    encode_info.pSetupReferenceSlot = nullptr;
    encode_info.referenceSlotCount = 0;
    encode_info.pReferenceSlots = nullptr;
    encode_info.precedingExternallyEncodedBytes = 0;

    vkCmdEncodeVideoKHR(command_buffer, &encode_info);
}

void VulkanEncoder::encode_pass_frame_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame)
{
    std::vector<VulkanEncoderFrame::Ptr> reference_slots;
    this->process_slots(frame, reference_slots);

    VkImageSubresourceRange image_range;
    image_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_range.baseMipLevel = 0;
    image_range.levelCount = 1;
    image_range.baseArrayLayer = 0;
    image_range.layerCount = 1;

    std::vector<VkImageMemoryBarrier2> slot_barriers;
    std::vector<VkVideoPictureResourceInfoKHR> slot_resources;
    std::vector<VkVideoReferenceSlotInfoKHR> slot_infos;
    slot_barriers.resize(reference_slots.size() + 2);
    slot_resources.resize(reference_slots.size());
    slot_infos.resize(reference_slots.size());

    VkImageMemoryBarrier2& read_input_barrier = slot_barriers[0];
    read_input_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
    read_input_barrier.pNext = nullptr;
    read_input_barrier.srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
    read_input_barrier.srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
    read_input_barrier.dstStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
    read_input_barrier.dstAccessMask = VK_ACCESS_2_VIDEO_ENCODE_READ_BIT_KHR;
    read_input_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    read_input_barrier.newLayout = VK_IMAGE_LAYOUT_VIDEO_ENCODE_SRC_KHR;
    read_input_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    read_input_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    read_input_barrier.image = frame->input_combined;
    read_input_barrier.subresourceRange = image_range;

    VkImageMemoryBarrier2& write_slot_barrier = slot_barriers[1];
    write_slot_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
    write_slot_barrier.pNext = nullptr;
    write_slot_barrier.srcStageMask = VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT;
    write_slot_barrier.srcAccessMask = 0;
    write_slot_barrier.dstStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
    write_slot_barrier.dstAccessMask = VK_ACCESS_2_VIDEO_ENCODE_WRITE_BIT_KHR;
    write_slot_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    write_slot_barrier.newLayout = VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR;
    write_slot_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    write_slot_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    write_slot_barrier.image = frame->slot_image;
    write_slot_barrier.subresourceRange = image_range;

    for (uint32_t index = 0; index < reference_slots.size(); index++)
    {
        VulkanEncoderFrame::Ptr reference_slot = reference_slots[index];

        VkImageMemoryBarrier2& slot_barrier = slot_barriers[index + 2];
        slot_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
        slot_barrier.pNext = nullptr;
        slot_barrier.srcStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
        slot_barrier.srcAccessMask = VK_ACCESS_2_VIDEO_ENCODE_WRITE_BIT_KHR;
        slot_barrier.dstStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
        slot_barrier.dstAccessMask = VK_ACCESS_2_VIDEO_ENCODE_READ_BIT_KHR;
        slot_barrier.oldLayout = VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR;
        slot_barrier.newLayout = VK_IMAGE_LAYOUT_VIDEO_ENCODE_DPB_KHR;
        slot_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        slot_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        slot_barrier.image = frame->slot_image;
        slot_barrier.subresourceRange = image_range;

        VkVideoPictureResourceInfoKHR& slot_resource = slot_resources[index];
        slot_resource.sType = VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_INFO_KHR;
        slot_resource.pNext = nullptr;
        slot_resource.codedOffset.x = 0;
        slot_resource.codedOffset.y = 0;
        slot_resource.codedExtent.width = this->setting_image_size.x;
        slot_resource.codedExtent.height = this->setting_image_size.y;
        slot_resource.baseArrayLayer = 0;
        slot_resource.imageViewBinding = reference_slot->slot_view;

        VkVideoReferenceSlotInfoKHR& slot_info = slot_infos[index];
        slot_info.sType = VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_INFO_KHR;
        slot_info.pNext = nullptr;
        slot_info.slotIndex = reference_slot->slot_index;
        slot_info.pPictureResource = &slot_resource;
    }

    VkDependencyInfo slot_dependency;
    slot_dependency.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    slot_dependency.pNext = nullptr;
    slot_dependency.dependencyFlags = 0;
    slot_dependency.memoryBarrierCount = 0;
    slot_dependency.pMemoryBarriers = nullptr;
    slot_dependency.bufferMemoryBarrierCount = 0;
    slot_dependency.pBufferMemoryBarriers = nullptr;
    slot_dependency.imageMemoryBarrierCount = slot_barriers.size();
    slot_dependency.pImageMemoryBarriers = slot_barriers.data();

    vkCmdPipelineBarrier2KHR(command_buffer, &slot_dependency);

    VkVideoBeginCodingInfoKHR begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR;
    begin_info.pNext = nullptr;
    begin_info.flags = 0;
    begin_info.videoSession = this->video_session;
    begin_info.videoSessionParameters = this->video_session_paremeters;
    begin_info.referenceSlotCount = slot_infos.size();
    begin_info.pReferenceSlots = slot_infos.data();

    vkCmdBeginVideoCodingKHR(command_buffer, &begin_info);

    vkCmdResetQueryPool(command_buffer, this->query_pool, frame->output_query_index, 1);
    vkCmdBeginQuery(command_buffer, this->query_pool, frame->output_query_index, 0);

    this->encode_pass_frame_command(command_buffer, frame, reference_slots);

    vkCmdEndQuery(command_buffer, this->query_pool, frame->output_query_index);

    VkVideoEndCodingInfoKHR end_info;
    end_info.sType = VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR;
    end_info.pNext = nullptr;
    end_info.flags = 0;

    vkCmdEndVideoCodingKHR(command_buffer, &end_info);

    VkBufferMemoryBarrier2 buffer_barrier;
    buffer_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
    buffer_barrier.pNext = nullptr;
    buffer_barrier.srcStageMask = VK_PIPELINE_STAGE_2_VIDEO_ENCODE_BIT_KHR;
    buffer_barrier.srcAccessMask = VK_ACCESS_2_VIDEO_ENCODE_WRITE_BIT_KHR;
    buffer_barrier.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT;
    buffer_barrier.dstAccessMask = VK_ACCESS_2_HOST_READ_BIT;
    buffer_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_barrier.buffer = frame->output_buffer;
    buffer_barrier.offset = 0;
    buffer_barrier.size = VK_WHOLE_SIZE;

    VkDependencyInfo buffer_dependency;
    buffer_dependency.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
    buffer_dependency.pNext = nullptr;
    buffer_dependency.dependencyFlags = 0;
    buffer_dependency.memoryBarrierCount = 0;
    buffer_dependency.pMemoryBarriers = nullptr;
    buffer_dependency.bufferMemoryBarrierCount = 1;
    buffer_dependency.pBufferMemoryBarriers = &buffer_barrier;
    buffer_dependency.imageMemoryBarrierCount = 0;
    buffer_dependency.pImageMemoryBarriers = nullptr;

    vkCmdPipelineBarrier2KHR(command_buffer, &buffer_dependency);
}

void VulkanEncoder::encode_pass_frame_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame, const std::vector<VulkanEncoderFrame::Ptr>& reference_slots)
{
    VkOffset2D encode_offset;
    encode_offset.x = 0;
    encode_offset.y = 0;

    VkExtent2D encode_extend;
    encode_extend.width = this->setting_image_size.x;
    encode_extend.height = this->setting_image_size.y;

    VkVideoPictureResourceInfoKHR input_resource;
    input_resource.sType = VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_INFO_KHR;
    input_resource.pNext = nullptr;
    input_resource.codedOffset = encode_offset;
    input_resource.codedExtent = encode_extend;
    input_resource.baseArrayLayer = 0;
    input_resource.imageViewBinding = frame->input_view_combined;

    VkVideoPictureResourceInfoKHR slot_resource;
    slot_resource.sType = VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_INFO_KHR;
    slot_resource.pNext = nullptr;
    slot_resource.codedOffset = encode_offset;
    slot_resource.codedExtent = encode_extend;
    slot_resource.baseArrayLayer = 0;
    slot_resource.imageViewBinding = frame->slot_view;

    VkVideoReferenceSlotInfoKHR slot_reference;
    slot_reference.sType = VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_INFO_KHR;
    slot_reference.pNext = nullptr;
    slot_reference.slotIndex = frame->slot_index;
    slot_reference.pPictureResource = &slot_resource;

    //--- Reference Images ----------------------------------------------------------------

    std::vector<VkVideoPictureResourceInfoKHR> reference_slot_resources;
    std::vector<VkVideoReferenceSlotInfoKHR> reference_slot_infos;
    std::vector<StdVideoEncodeH264ReferenceInfo> reference_std_infos;
    std::vector<VkVideoEncodeH264DpbSlotInfoEXT> reference_dpb_slot_infos;
    reference_slot_resources.resize(reference_slots.size());
    reference_slot_infos.resize(reference_slots.size());
    reference_std_infos.resize(reference_slots.size());
    reference_dpb_slot_infos.resize(reference_slots.size());

    for (uint32_t index = 0; index < reference_slots.size(); index++)
    {
        VulkanEncoderFrame::Ptr reference_slot = reference_slots[index];

        VkVideoPictureResourceInfoKHR& slot_resource = reference_slot_resources[index];
        slot_resource.sType = VK_STRUCTURE_TYPE_VIDEO_PICTURE_RESOURCE_INFO_KHR;
        slot_resource.pNext = nullptr;
        slot_resource.codedOffset.x = 0;
        slot_resource.codedOffset.y = 0;
        slot_resource.codedExtent.width = this->setting_image_size.x;
        slot_resource.codedExtent.height = this->setting_image_size.y;
        slot_resource.baseArrayLayer = 0;
        slot_resource.imageViewBinding = reference_slot->slot_view;

        VkVideoReferenceSlotInfoKHR& slot_info = reference_slot_infos[index];
        slot_info.sType = VK_STRUCTURE_TYPE_VIDEO_REFERENCE_SLOT_INFO_KHR;
        slot_info.pNext = nullptr;
        slot_info.slotIndex = reference_slot->slot_index;
        slot_info.pPictureResource = &slot_resource;

        StdVideoEncodeH264ReferenceInfoFlags std_info_flags;
        std_info_flags.used_for_long_term_reference = 0;

        StdVideoEncodeH264ReferenceInfo& std_info = reference_std_infos[index];
        std_info.flags = std_info_flags;
        std_info.FrameNum = reference_slot->frame_index & 0xFF;
        std_info.PicOrderCnt = reference_slot->frame_index & 0xFF;
        std_info.long_term_pic_num = 0;
        std_info.long_term_frame_idx = 0;

        VkVideoEncodeH264DpbSlotInfoEXT& dpb_slot_info = reference_dpb_slot_infos[index];
        dpb_slot_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_DPB_SLOT_INFO_EXT;
        dpb_slot_info.pNext = nullptr;
        dpb_slot_info.slotIndex = reference_slot->slot_index;
        dpb_slot_info.pStdReferenceInfo = &std_info;
    }

    VkVideoEncodeH264ReferenceListsInfoEXT reference_lists;
    reference_lists.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_REFERENCE_LISTS_INFO_EXT;
    reference_lists.pNext = nullptr;
    reference_lists.referenceList0EntryCount = reference_dpb_slot_infos.size();
    reference_lists.pReferenceList0Entries = reference_dpb_slot_infos.data();
    reference_lists.referenceList1EntryCount = 0;
    reference_lists.pReferenceList1Entries = nullptr;
    reference_lists.pMemMgmtCtrlOperations = nullptr;

    //--- Slot Info ----------------------------------------------------------------

    StdVideoEncodeH264PictureInfoFlags slot_flags;
    slot_flags.idr_flag = this->is_key_frame() ? 1 : 0;
    slot_flags.is_reference_flag = 1;
    slot_flags.used_for_long_term_reference = 0;

    StdVideoEncodeH264PictureInfo slot_info;
    slot_info.flags = slot_flags;
    slot_info.seq_parameter_set_id = this->sequence_parameter_id;
    slot_info.pic_parameter_set_id = this->picture_parameter_id;
    slot_info.pictureType = this->is_key_frame() ? STD_VIDEO_H264_PICTURE_TYPE_I : STD_VIDEO_H264_PICTURE_TYPE_P;
    slot_info.frame_num = this->frame_index & 0xFF;
    slot_info.PicOrderCnt = this->frame_index & 0xFF;

    //--- Slice Info ----------------------------------------------------------------

    StdVideoEncodeH264SliceHeaderFlags slice_flags;
    slice_flags.direct_spatial_mv_pred_flag = 0;
    slice_flags.num_ref_idx_active_override_flag = 1;
    slice_flags.no_output_of_prior_pics_flag = 0;
    slice_flags.adaptive_ref_pic_marking_mode_flag = 0;
    slice_flags.no_prior_references_available_flag = 0;

    StdVideoEncodeH264SliceHeader slice_parameters;
    slice_parameters.flags = slice_flags;
    slice_parameters.first_mb_in_slice = 0;
    slice_parameters.slice_type = this->is_key_frame() ? STD_VIDEO_H264_SLICE_TYPE_I : STD_VIDEO_H264_SLICE_TYPE_P;
    slice_parameters.idr_pic_id = 0;
    slice_parameters.num_ref_idx_l0_active_minus1 = this->is_key_frame() ? 0 : reference_slot_infos.size();
    slice_parameters.num_ref_idx_l1_active_minus1 = 0;
    slice_parameters.cabac_init_idc = (StdVideoH264CabacInitIdc) 0;
    slice_parameters.disable_deblocking_filter_idc = (StdVideoH264DisableDeblockingFilterIdc) 0;
    slice_parameters.slice_alpha_c0_offset_div2 = 0;
    slice_parameters.slice_beta_offset_div2 = 0;
    slice_parameters.pWeightTable = nullptr;

    VkVideoEncodeH264NaluSliceInfoEXT slice_info;
    slice_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_NALU_SLICE_INFO_EXT;
    slice_info.pNext = nullptr;
    slice_info.mbCount = this->setting_block_count.x * this->setting_block_count.y;
    slice_info.pReferenceFinalLists = &reference_lists;
    slice_info.pSliceHeaderStd = &slice_parameters;

    //------------------------------------------------------------------------------

    VkVideoEncodeH264VclFrameInfoEXT frame_info;
    frame_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_H264_VCL_FRAME_INFO_EXT;
    frame_info.pNext = nullptr;
    frame_info.pReferenceFinalLists = &reference_lists;
    frame_info.naluSliceEntryCount = 1;
    frame_info.pNaluSliceEntries = &slice_info;
    frame_info.pCurrentPictureInfo = &slot_info;

    VkVideoEncodeInfoKHR encode_info;
    encode_info.sType = VK_STRUCTURE_TYPE_VIDEO_ENCODE_INFO_KHR;
    encode_info.pNext = &frame_info;
    encode_info.flags = 0;
    encode_info.qualityLevel = 0;
    encode_info.dstBitstreamBuffer = frame->output_buffer;
    encode_info.dstBitstreamBufferOffset = 0;
    encode_info.dstBitstreamBufferMaxRange = this->setting_buffer_size;
    encode_info.srcPictureResource = input_resource;
    encode_info.pSetupReferenceSlot = &slot_reference;
    encode_info.referenceSlotCount = reference_slot_infos.size();
    encode_info.pReferenceSlots = reference_slot_infos.data();
    encode_info.precedingExternallyEncodedBytes = 0;

    vkCmdEncodeVideoKHR(command_buffer, &encode_info);
}

void VulkanEncoder::setup_slots()
{
    this->frame_slots.resize(this->setting_reference_frames + 1);
    this->invalidate_slots();
}

void VulkanEncoder::process_slots(VulkanEncoderFrame::Ptr frame, std::vector<VulkanEncoderFrame::Ptr>& reference_slots)
{
    if (this->is_key_frame())
    {
        this->invalidate_slots();
    }

    bool found = false;

    for (uint32_t slot = 0; slot < this->frame_slots.size(); slot++)
    {
        if (this->frame_slots[slot] == nullptr)
        {
            this->frame_slots[slot] = frame;
            frame->slot_index = slot;

            found = true;
            break;
        }
    }

    if (!found)
    {
        uint32_t replace_slot = 0;
        uint32_t replace_index = this->frame_slots[0]->frame_index;

        for (uint32_t slot = 1; slot < this->frame_slots.size(); slot++)
        {
            if (this->frame_slots[slot]->frame_index < replace_index)
            {
                replace_index = this->frame_slots[slot]->frame_index;
                replace_slot = slot;
            }
        }

        this->frame_slots[replace_slot] = frame;
        frame->slot_index = replace_slot;
    }

    reference_slots.clear();

    for (VulkanEncoderFrame::Ptr slot : this->frame_slots)
    {
        if (slot != nullptr && slot != frame)
        {
            reference_slots.push_back(slot);
        }
    }

    std::sort(reference_slots.begin(), reference_slots.end(), [](VulkanEncoderFrame::Ptr frame1, VulkanEncoderFrame::Ptr frame2)
    {
        return frame1->frame_index > frame2->frame_index;
    });

    uint32_t reference_count = std::min((uint32_t)reference_slots.size(), this->setting_reference_frames);
    reference_slots.resize(reference_count);
}

void VulkanEncoder::invalidate_slots()
{
    for (uint32_t index = 0; index < this->frame_slots.size(); index++)
    {
        this->frame_slots[index] = nullptr;
    }
}

bool VulkanEncoder::is_key_frame() const
{
    return (this->frame_index % this->setting_key_rate) == 0;
}

VkPhysicalDeviceSynchronization2Features sync_feature;

bool setup_instance_for_vulkan_encoder(lava::frame_config& config)
{
    if (config.info.req_api_version < lava::api_version::v1_1)
    {
        config.info.req_api_version = lava::api_version::v1_1;
    }

    if (!setup_instance_for_extern_fence(config))
    {
        return false;
    }

    return true;
}

bool setup_device_for_vulkan_encoder(lava::instance& instance, lava::device::create_param& parameters)
{
    parameters.extensions.push_back(VK_KHR_VIDEO_QUEUE_EXTENSION_NAME);
    parameters.extensions.push_back(VK_KHR_VIDEO_ENCODE_QUEUE_EXTENSION_NAME);
    parameters.extensions.push_back(VK_EXT_VIDEO_ENCODE_H264_EXTENSION_NAME);

    //Dependencies of VK_KHR_video_queue and VK_KHR_video_encode_queue that are not included in Vulkan 1.1
    parameters.extensions.push_back(VK_KHR_SYNCHRONIZATION_2_EXTENSION_NAME);

    parameters.add_queue(VK_QUEUE_VIDEO_ENCODE_BIT_KHR);

    sync_feature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES;
    sync_feature.pNext = (void*) parameters.next;
    sync_feature.synchronization2 = VK_TRUE;
    parameters.next = &sync_feature;

    if (!setup_device_for_extern_fence(instance, parameters))
    {
        return false;
    }

    return true;
}

void shutdown_vulkan_encoder()
{

}