#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <asio.hpp>
#include <mutex>
#include <optional>
#include <vector>
#include <memory>

#include "encoder.hpp"
#include "utility/extern_fence.hpp"

#define VULKAN_ENCODER_FRAMES 4

enum VulkanEncoderFrameType
{
    VULKAN_ENCODER_FRAME_TYPE_CONTROL,
    VULKAN_ENCODER_FRAME_TYPE_CONFIG,
    VULKAN_ENCODER_FRAME_TYPE_FRAME
};

struct VulkanEncoderFrame
{
public:
    typedef std::shared_ptr<VulkanEncoderFrame> Ptr;

public:
    VmaAllocation input_memory_combined;
    VmaAllocation input_memory_luma;
    VmaAllocation input_memory_chroma;
    VmaAllocation input_memory_chroma_subsampled;

    VkImage input_luma = nullptr;
    VkImage input_chroma = nullptr;
    VkImage input_chroma_subsampled = nullptr;
    VkImage input_combined = nullptr;

    VkImageView input_view_luma = nullptr;
    VkImageView input_view_chroma = nullptr;
    VkImageView input_view_chroma_subsampled = nullptr;
    VkImageView input_view_combined = nullptr;

    VmaAllocation slot_memory_luma;
    VmaAllocation slot_memory_chroma;

    VkImage slot_image = nullptr;
    VkImageView slot_view = nullptr;
    uint32_t slot_index = 0;

    uint32_t output_query_index = 0;
    VkBuffer output_buffer = nullptr;
    VmaAllocation output_memory;

    VkDescriptorSet convert_descriptor = nullptr;
    lava::graphics_pipeline::ptr convert_pipeline;
    lava::render_pass::ptr convert_pass;

    VkDescriptorSet subsample_descriptor = nullptr;
    lava::graphics_pipeline::ptr subsample_pipeline;
    lava::render_pass::ptr subsample_pass;

    VkSemaphore render_semaphore = nullptr;
    VkSemaphore encode_semaphore = nullptr;
    VkCommandBuffer encode_command_buffer = nullptr;
    ExternFence::Ptr encode_fence;

    uint32_t frame_index = 0;
    VulkanEncoderFrameType type;
    Encoder::OnEncodeComplete callback;
};

class VulkanEncoder : public Encoder
{
public:
    typedef std::shared_ptr<VulkanEncoder> Ptr;

public:
    VulkanEncoder();

    bool create(lava::device_ptr device, const lava::renderer& renderer, const glm::uvec2& size, EncoderFormat format, EncoderCodec codec) override;
    void destroy() override;

    EncoderResult encode(VkCommandBuffer command_buffer, lava::renderer& renderer, lava::image::ptr image, VkImageLayout image_layout, OnEncodeComplete function) override;

    void set_on_encode_error(OnEncodeError function) override;

    void set_mode(EncoderMode mode) override;
    void set_quality(double quality) override;
    void set_bitrate(double bitrate) override;
    void set_key_rate(uint32_t key_rate) override;
    void set_frame_rate(uint32_t frame_rate) override;

    EncoderCodec get_codec() const override;
    EncoderMode get_mode() const override;
    double get_quality() const override;
    double get_bitrate() const override;
    uint32_t get_key_rate() const override;
    uint32_t get_frame_rate() const override;

    bool is_supported(EncoderSetting setting) const override;

private:
    bool encode_control(lava::renderer& renderer, OnEncodeComplete callback);
    bool encode_config(lava::renderer& renderer, OnEncodeComplete callback);

    bool aquire_frame(VulkanEncoderFrame::Ptr& frame);
    void release_frame(VulkanEncoderFrame::Ptr frame);
    bool submit_frame(lava::device_ptr device, VulkanEncoderFrame::Ptr frame, lava::renderer& renderer);

    void write_image(lava::device_ptr device, VulkanEncoderFrame::Ptr frame, lava::image::ptr image);
    bool record_encoding(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    void retrive_encoding(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);

    bool create_profiles(lava::device_ptr device);
    bool compute_settings(const glm::uvec2& size);

    bool create_session(lava::device_ptr device, const glm::uvec2& size);
    bool create_parameters(lava::device_ptr device);
    bool bind_session_memory(lava::device_ptr device);

    bool create_sampler(lava::device_ptr device);
    bool create_pools(lava::device_ptr device, uint32_t frame_count);
    bool create_layouts(lava::device_ptr device);

    bool create_image_memory(lava::device_ptr device, VkImage image, VkImageAspectFlagBits image_aspect, VmaAllocation& memory);
    bool create_image_view(lava::device_ptr device, VkImage image, VkFormat image_format, VkImageView& image_view);

    bool create_input_images(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_slot_image(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_output_buffer(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);

    bool create_convert_pass(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_convert_pipeline(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_subsample_pass(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_subsample_pipeline(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);
    bool create_encode_resources(lava::device_ptr device, asio::executor executor, VulkanEncoderFrame::Ptr frame);

    void destroy_session(lava::device_ptr device);
    void destroy_resources(lava::device_ptr device);
    void destroy_frame(lava::device_ptr device, VulkanEncoderFrame::Ptr frame);

    bool check_encode_support(lava::device_ptr device, const lava::queue& queue) const;
    bool check_format_support(lava::device_ptr device, VkImageUsageFlags usage, VkFormat format) const;

    void convert_pass(VkCommandBuffer command_buffer);
    void subsample_pass(VkCommandBuffer command_buffer);

    void encode_pass_control_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame);
    void encode_pass_control_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame);
    void encode_pass_config_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame);
    void encode_pass_config_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame);
    void encode_pass_frame_setup(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame);
    void encode_pass_frame_command(VkCommandBuffer command_buffer, VulkanEncoderFrame::Ptr frame, const std::vector<VulkanEncoderFrame::Ptr>& reference_slots);

    void setup_slots();
    void process_slots(VulkanEncoderFrame::Ptr frame, std::vector<VulkanEncoderFrame::Ptr>& reference_slots);
    void invalidate_slots();

    bool is_key_frame() const;

private:
    uint32_t frame_count = 0;
    uint32_t frame_index = 0;

    bool config_change = true;
    bool control_change = true;

    EncoderMode setting_mode = ENCODER_MODE_CONSTANT_QUALITY;
    uint32_t setting_key_rate = 120;
    double setting_bit_rate = 2.0;          //NOTE: Bitrate in Mbit per seconds
    uint32_t setting_frame_rate = 90;
    uint32_t setting_quality_iframe = 51;
    uint32_t setting_quality_pframe = 51;
    
    uint32_t setting_reference_frames = 1;

    glm::uvec2 setting_block_count;
    glm::uvec2 setting_frame_crop;
    glm::uvec2 setting_image_size;
    uint32_t setting_buffer_size;

private:
    asio::thread_pool worker_pool;

    VkSampler sampler = nullptr;
    VkCommandPool command_pool = nullptr;
    VkQueryPool query_pool = nullptr;
    lava::descriptor::pool::ptr descriptor_pool;

    lava::descriptor::ptr descriptor_layout;
    lava::pipeline_layout::ptr pipeline_layout;

    VkSemaphore previous_semaphore = VK_NULL_HANDLE;

    std::mutex frame_mutex;
    std::vector<VulkanEncoderFrame::Ptr> frame_queue; //NOTE: Protected by frame_mutex
    std::vector<VulkanEncoderFrame::Ptr> frame_list;
    std::vector<VulkanEncoderFrame::Ptr> frame_slots;
    
    OnEncodeError on_encode_error;

private:
    lava::queue default_queue;
    std::optional<lava::queue> encode_queue;

    VkVideoSessionKHR video_session = nullptr;
    VkVideoSessionParametersKHR video_session_paremeters = nullptr;
    std::vector<VmaAllocation> video_session_memory;

    VkVideoProfileInfoKHR video_profile;
    VkVideoEncodeUsageInfoKHR usage_info;
    VkVideoEncodeH264ProfileInfoEXT encode_profile;

    VkVideoCapabilitiesKHR video_capabillities;
    VkVideoEncodeCapabilitiesKHR encode_capabillities;
    VkVideoEncodeH264CapabilitiesEXT codec_capabillities;

    const uint8_t sequence_parameter_id = 0;
    const uint8_t picture_parameter_id = 0;
};

bool setup_instance_for_vulkan_encoder(lava::frame_config& config);
bool setup_device_for_vulkan_encoder(lava::instance& instance, lava::device::create_param& parameters);

void shutdown_vulkan_encoder();