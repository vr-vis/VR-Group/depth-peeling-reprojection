#include "emulated_headset.hpp"
#include "vr_application.hpp"

#include <imgui.h>
#include <glm/gtx/matrix_operation.hpp>

bool EmulatedHeadset::on_create()
{
    lava::camera& camera = this->get_application()->get_camera();
    camera.movement_speed = 1.0f;
    
    lava::window& window = this->get_application()->get_window();
    window.set_size(1280, 720);
    window.set_resizable(false);

    this->resolution = window.get_size();
    this->resolution.x /= 2;

    this->compute_matrices(EYE_LEFT);
    this->compute_matrices(EYE_RIGHT);

    if (!this->create_framebuffer())
    {
        return false;
    }

    return true;
}

void EmulatedHeadset::on_destroy()
{
    this->destroy_framebuffer();
}

bool EmulatedHeadset::on_interface()
{
    lava::camera& camera = this->get_application()->get_camera();

    if (ImGui::DragFloat("Near Plane", &this->near_plane))
    {
        this->compute_matrices(EYE_LEFT);
        this->compute_matrices(EYE_RIGHT);
    }

    if (ImGui::DragFloat("Far Plane", &this->far_plane))
    {
        this->compute_matrices(EYE_LEFT);
        this->compute_matrices(EYE_RIGHT);
    }

    ImGui::DragFloat("Movement Speed", &camera.movement_speed, 1.0f, 0.0f, 1000.0f);

    return true;
}

bool EmulatedHeadset::on_update(lava::delta delta_time)
{
    lava::camera& camera = this->get_application()->get_camera();
    lava::input& input = this->get_application()->get_input();

    camera.update_view(delta_time, input.get_mouse_position());

    this->view_matrix = camera.view;
    this->submitted = false;

    this->controller_matrices[0] = glm::inverse(this->view_matrix) * glm::translate(glm::mat4(1.0f), glm::vec3(-0.3f, -0.2f, -0.4f));
    this->controller_matrices[1] = glm::inverse(this->view_matrix) * glm::translate(glm::mat4(1.0f), glm::vec3(0.3f, -0.2f, -0.4f));

    return true;
}

void EmulatedHeadset::submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id)
{
    lava::image::ptr window_image = this->get_application()->get_target()->get_backbuffer(this->get_application()->get_frame_index());
    lava::image::ptr frame_image = this->get_framebuffer(frame_id);

    std::vector<VkImageMemoryBarrier> image_barriers;

    VkImageMemoryBarrier window_barrier = lava::image_memory_barrier(window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    window_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    window_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    window_barrier.subresourceRange = window_image->get_subresource_range();

    image_barriers.push_back(window_barrier);

    if (frame_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(frame_image->get(), frame_layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
        frame_barrier.srcAccessMask = 0;
        frame_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        frame_barrier.subresourceRange = frame_image->get_subresource_range();

        image_barriers.push_back(frame_barrier);
    }

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT | VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());

    VkImageSubresourceLayers subresource_layers;
    subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_layers.mipLevel = 0;
    subresource_layers.baseArrayLayer = 0;
    subresource_layers.layerCount = 1;

    uint32_t eye_offset = 0;

    if (frame_id == EYE_RIGHT)
    {
        eye_offset = this->resolution.x;
    }

    VkImageCopy image_region;
    image_region.srcSubresource = frame_image->get_subresource_layers();
    image_region.srcOffset.x = 0;
    image_region.srcOffset.y = 0;
    image_region.srcOffset.z = 0;
    image_region.dstSubresource = subresource_layers;
    image_region.dstOffset.x = eye_offset;
    image_region.dstOffset.y = 0;
    image_region.dstOffset.z = 0;
    image_region.extent.width = this->resolution.x;
    image_region.extent.height = this->resolution.y;
    image_region.extent.depth = 1;

    vkCmdCopyImage(command_buffer, frame_image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_region);

    if (frame_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(frame_image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, frame_layout);
        frame_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        frame_barrier.dstAccessMask = 0;
        frame_barrier.subresourceRange = frame_image->get_subresource_range();

        vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &frame_barrier);
    }
}

VkFormat EmulatedHeadset::get_format() const
{
    switch (this->get_application()->get_target()->get_format())
    {
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SRGB:
        return VK_FORMAT_B8G8R8A8_SRGB;
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SRGB:
        return VK_FORMAT_R8G8B8A8_SRGB;
    default:
        lava::log()->warn("Can't derive correct sRGB color format for headset framebuffer!");
        break;
    }

    return this->get_application()->get_target()->get_format();
}

const glm::uvec2& EmulatedHeadset::get_resolution() const
{
    return this->resolution;
}

const glm::mat4& EmulatedHeadset::get_view_matrix() const
{
    return this->view_matrix;
}

const glm::mat4& EmulatedHeadset::get_head_to_eye_matrix(Eye eye) const
{
    return this->head_to_eye_matrices[eye];
}

const glm::mat4& EmulatedHeadset::get_projection_matrix(Eye eye) const
{
    return this->projection_matrices[eye];
}

const glm::mat4& EmulatedHeadset::get_controller_matrix(Controller controller) const
{
    return this->controller_matrices[controller];
}

lava::image::ptr EmulatedHeadset::get_framebuffer(FrameId frame_id) const
{
    return this->framebuffers[frame_id];
}

lava::image::ptr EmulatedHeadset::get_framebuffer_array() const
{
    return this->framebuffer_array;
}

const char* EmulatedHeadset::get_name() const
{
    return "Emulated Headset";
}

bool EmulatedHeadset::is_attached(Controller controller) const
{
    return true;
}

bool EmulatedHeadset::create_framebuffer()
{
    this->framebuffer_array = lava::make_image(this->get_format());
    this->framebuffer_array->set_layer_count(this->framebuffers.size());
    this->framebuffer_array->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->framebuffer_array->set_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

    if (!this->framebuffer_array->create(this->get_application()->get_device(), this->resolution))
    {
        return false;
    }

    for (uint32_t index = 0; index < this->framebuffers.size(); index++)
    {
        lava::image::ptr framebuffer = lava::make_image(this->get_format(), this->framebuffer_array->get());
        framebuffer->set_base_array_layer(index);
        framebuffer->set_view_type(VK_IMAGE_VIEW_TYPE_2D);

        if (!framebuffer->create(this->get_application()->get_device(), this->resolution))
        {
            return false;
        }

        this->framebuffers[index] = framebuffer;
    }

    return true;
}

void EmulatedHeadset::destroy_framebuffer()
{
    for (lava::image::ptr& framebuffer : this->framebuffers)
    {
        if (framebuffer != nullptr)
        {
            framebuffer->destroy(true);
            framebuffer = nullptr;
        }
    }

    if (this->framebuffer_array != nullptr)
    {
        this->framebuffer_array->destroy();
        this->framebuffer_array = nullptr;
    }
}

void EmulatedHeadset::compute_matrices(Eye eye)
{
    glm::vec3 eye_location = glm::vec3(0.0f);

    switch (eye)
    {
    case EYE_LEFT:
        eye_location = glm::vec3(0.1f, 0.0f, 0.0f);
        break;
    case EYE_RIGHT:
        eye_location = glm::vec3(-0.1f, 0.0f, 0.0f);
        break;
    default:
        break;
    }

    this->projection_matrices[eye] = glm::perspective(glm::pi<float>() * 0.5f, 16.0f / 9.0f, this->near_plane, this->far_plane);
    this->head_to_eye_matrices[eye] = glm::translate(glm::mat4(1.0f), eye_location);
}