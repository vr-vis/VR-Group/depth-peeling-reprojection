#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <array>
#include <string>

#include "headset.hpp"

class EmulatedHeadset : public Headset
{
public:
    typedef std::shared_ptr<EmulatedHeadset> Ptr;

public:
    EmulatedHeadset() = default;

    bool on_create() override;
    void on_destroy() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;

    void submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id) override;

    VkFormat get_format() const override;
    const glm::uvec2& get_resolution() const override;
    const glm::mat4& get_view_matrix() const override;
    const glm::mat4& get_head_to_eye_matrix(Eye eye) const override;
    const glm::mat4& get_projection_matrix(Eye eye) const override;
    const glm::mat4& get_controller_matrix(Controller controller) const override;

    lava::image::ptr get_framebuffer(FrameId frame_id) const override;
    lava::image::ptr get_framebuffer_array() const override;

    const char* get_name() const override;

    bool is_attached(Controller controller) const override;

private:
    bool create_framebuffer();
    void destroy_framebuffer();

    void compute_matrices(Eye eye);

private:
    glm::uvec2 resolution;
    glm::mat4 view_matrix;
    std::array<glm::mat4, 2> head_to_eye_matrices;
    std::array<glm::mat4, 2> projection_matrices;
    std::array<glm::mat4, 2> controller_matrices;
    std::array<lava::image::ptr, 2> framebuffers;
    lava::image::ptr framebuffer_array;

    float near_plane = 0.1f;
    float far_plane = 1000.0f;
    bool submitted = false;
};