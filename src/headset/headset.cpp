#include "headset.hpp"
#include "emulated_headset.hpp"
#include "openvr_headset.hpp"
#include "openxr_headset.hpp"
#include "remote_headset.hpp"

void Headset::set_application(VRApplication* application)
{
    this->application = application;
}

VRApplication* Headset::get_application() const
{
    return this->application;
}

bool Headset::on_setup_instance(lava::frame_config& config)
{
    return true;
}

bool Headset::on_setup_device(lava::device::create_param& parameters)
{
    return true;
}

void Headset::on_shutdown()
{

}

void Headset::submit_metadata(const FrameMetadata& metadata)
{

}

bool Headset::is_remote() const
{
    return false;
}

Headset::Ptr make_headset(VRApplication* application, HeadsetType headset_type)
{
    Headset::Ptr headset;

    switch (headset_type)
    {
    case HEADSET_TYPE_EMULATED:
        headset = std::make_shared<EmulatedHeadset>();
        break;
    case HEADSET_TYPE_OPEN_VR:
        headset = std::make_shared<OpenVRHeadset>();
        break;
    case HEADSET_TYPE_OPEN_XR:
        headset = std::make_shared<OpenXRHeadset>();
        break;
    case HEADSET_TYPE_REMOTE:
        headset = std::make_shared<RemoteHeadset>();
        break;
    default:
        lava::log()->error("Unkown headset type!");
        return nullptr;
    }

    headset->set_application(application);

    return headset;
}