/*
  The headset class defines an interface that every type of headset must implement.
  Every headset must provide information about the current transformation that should be used for rendering.
  Besides that a headset also acts like a output device or images.
  A particular headset type can be created with the help of the function make_headset(...).
  Example:

    Headset::Ptr headset = make_headset(application, HEADSET_TYPE_EMULATED);
*/

#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <span>
#include <memory>
#include <cstdint>

class VRApplication;

#include "types.hpp"

// When adding or removing a headset, the function make_headset(...) as well as
// the function set_headset(...) of the CommandParser need to be adapted accordingly.
enum HeadsetType
{
    HEADSET_TYPE_EMULATED,
    HEADSET_TYPE_OPEN_VR,
    HEADSET_TYPE_OPEN_XR,
    HEADSET_TYPE_REMOTE
};

class Headset
{
public:
    typedef std::shared_ptr<Headset> Ptr;
    
public:
    Headset() = default;
    virtual ~Headset() = default;

    void set_application(VRApplication* application);
    VRApplication* get_application() const;

    // This function is called before the creation of the vulkan instance.
    // A headset can use this function to enable required features and extensions of the instance.
    virtual bool on_setup_instance(lava::frame_config& config);

    // This function is called before the creation of the vulkan device.
    // A headset can use this function to enable required features and extensions of the device.
    virtual bool on_setup_device(lava::device::create_param& parameters);

    // This function is called during intialisation of the application and every time the active stereo strategy changes.
    // A headset can use this function to initialise buffers or other resources.
    virtual bool on_create() = 0;

    // This function is called during the shutdown of the application and every time the active stereo strategy changes.
    // A headset can use this function to destroy all resources that it has created.
    virtual void on_destroy() = 0;

    // This function is only called during the shutdown of the application.
    // A headset can use this function to destroy all remaining resources.
    virtual void on_shutdown();

    // This function is called every frame when the interface needs to be rendered.
    // A headset can use this function to manage its own interface.
    virtual bool on_interface() = 0;

    // This function is called every frame and can be used for computations or other updates.
    virtual bool on_update(lava::delta delta_time) = 0;

    // This function needs to be called right after the framebuffer of the particular frame id has been filled and is ready to be displayed.
    // After the submission of the framebuffer, writing to the framebuffer as well as changing the image layout are not allowed.
    // In case of an non-remote headset the frame id has to be ether EYE_LEFT or EYE_RIGHT. 
    // Otherwise the frame id must not exceed the the maximum number of remote frame ids that was set by the active stereo strategy.
    virtual void submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id) = 0;

    // This function can be used to send metadata to a remote headset and is only relevant in case of an remote headset.
    // If the headset is an remote headset, this fuction has to be called every frame before submitting any frames.
    // Submiting metadata with size equal to zero is invalid.
    virtual void submit_metadata(const FrameMetadata& metadata);

    // Returns the image format of the framebuffer images.
    // The format of the provided images should be a four component
    // color type where each color channel has a bit-depth of one byte.
    virtual VkFormat get_format() const = 0;
    // Returns the image resolution of the framebuffer images.
    virtual const glm::uvec2& get_resolution() const = 0;
    // Returns the view matrix of the headset that should be used for rendering.
    virtual const glm::mat4& get_view_matrix() const = 0;
    // Returns the transformation from head to selected eye that should be used for rendering.
    virtual const glm::mat4& get_head_to_eye_matrix(Eye eye) const = 0;
    // Returns the projection of the selected eye that should be used for rendering.
    virtual const glm::mat4& get_projection_matrix(Eye eye) const = 0;
    // Returns the controller matrix of the given controller that should be used for rendering.
    virtual const glm::mat4& get_controller_matrix(Controller controller) const = 0;

    // Returns the framebuffer image for the selected frame id.
    // In case of an non-remote headset the frame id has to be ether EYE_LEFT or EYE_RIGHT. 
    // Otherwise the frame id must not exceed the maximum number of remote frame ids that was set by the active stereo strategy.
    // The images provided by the headset should support sampling, src and dest transfer operations as well as color attachment operations.
    // It is imporant to note, that the provided image handle is the same as the image array handle of the headset's entire framebuffer.
    // When using the image handle directly, the provided subresource information should be used to select the correct layer.
    virtual lava::image::ptr get_framebuffer(FrameId frame_id) const = 0;

    // Returns the image array which defines the entire framebuffer of the headset.
    // The frame id can be used to access the layer in which the framebuffer of that id is stored.
    // In case of a non-remote headset, the number of layers has to be two.
    // Otherwise the the number of layers is equal to the maximum number of remote frame ids that was set by the active stereo strategy.
    // The array image provided by the headset should support sampling, src and dest transfer operations as well as color attachment operations.
    virtual lava::image::ptr get_framebuffer_array() const = 0;

    // Returns the name of the headset.
    virtual const char* get_name() const = 0;

    // Returns true is the given controller is attached and active in which case the controller should be visible to the user.
    virtual bool is_attached(Controller controller) const = 0;
    // Returns true is the headset is remote.
    virtual bool is_remote() const;

private:
    VRApplication* application = nullptr;
};

Headset::Ptr make_headset(VRApplication* application, HeadsetType headset_type);