#include "openvr_headset.hpp"
#include "vr_application.hpp"

#include <iostream>
#include <glm/ext.hpp>
#include <imgui.h>

struct OpenVRActionButtonBinding
{
    std::string path;

    Controller controller;
    Button button;
};

OpenVRHeadset::OpenVRHeadset()
{
    this->controller_attached.fill(false);

    for (uint32_t index = 0; index < this->controller_buttons.size(); index++)
    {
        this->controller_button_handles[index].fill(0);
        this->controller_buttons[index].fill(false);
    }
}

bool OpenVRHeadset::on_setup_instance(lava::frame_config& config)
{
    vr::EVRInitError error;
    this->system = VR_Init(&error, vr::EVRApplicationType::VRApplication_Scene);

    if (this->system == nullptr)
    {
        std::cout << "OpenVR: Can't initialize OpenVR!" << std::endl;

        return false;
    }

    uint32_t extension_count = vr::VRCompositor()->GetVulkanInstanceExtensionsRequired(nullptr, 0);
    std::string extension_string(extension_count, '\0');
    vr::VRCompositor()->GetVulkanInstanceExtensionsRequired(extension_string.data(), extension_count);

    this->instance_extensions = split_string(extension_string);

    for (const std::string& extension : this->instance_extensions)
    {
        config.param.extensions.push_back(extension.c_str());            
    }
  
    config.info.req_api_version = lava::api_version::v1_1;

    return true;
}

bool OpenVRHeadset::on_setup_device(lava::device::create_param& parameters)
{
    uint32_t extension_count = vr::VRCompositor()->GetVulkanDeviceExtensionsRequired(parameters.physical_device->get(), nullptr, 0);
    std::string extension_string(extension_count, '\0');
    vr::VRCompositor()->GetVulkanDeviceExtensionsRequired(parameters.physical_device->get(), extension_string.data(), extension_count);

    this->device_extensions = split_string(extension_string);

    for (const std::string& extension : this->device_extensions)
    {
        parameters.extensions.push_back(extension.c_str());
    }

    return true;
}

bool OpenVRHeadset::on_create()
{
    this->system->GetRecommendedRenderTargetSize(&this->resolution.x, &this->resolution.y);

    this->compute_matrices(EYE_LEFT);
    this->compute_matrices(EYE_RIGHT);

    if (!this->create_framebuffer())
    {
        return false;
    }

    if (!this->create_actions())
    {
        return false;
    }

    return true;
}

void OpenVRHeadset::on_destroy()
{
    this->destroy_framebuffer();
}

void OpenVRHeadset::on_shutdown()
{
    vr::VR_Shutdown();
}

bool OpenVRHeadset::on_interface()
{
    if (ImGui::DragFloat("Near Plane", &this->near_plane))
    {
        this->compute_matrices(EYE_LEFT);
        this->compute_matrices(EYE_RIGHT);
    }

    if (ImGui::DragFloat("Far Plane", &this->far_plane))
    {
        this->compute_matrices(EYE_LEFT);
        this->compute_matrices(EYE_RIGHT);
    }

    ImGui::DragFloat("Movement Speed", &this->movement_speed, 1.0f, 0.0f, 1000.0);

    return true;
}

bool OpenVRHeadset::on_update(lava::delta delta_time)
{
    vr::VREvent_t event;

    while (this->system->PollNextEvent(&event, sizeof(event)))
    {
        if (event.eventType == vr::VREvent_Quit)
        {
            return false;
        }
    }

    if (!this->update_actions())
    {
        return false;
    }

    if (!this->update_head_pose())
    {
        return false;
    }

    this->update_stage(delta_time);

    return true;
}

void OpenVRHeadset::submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id)
{
    lava::image::ptr frame_image = this->get_framebuffer(frame_id);

    if (frame_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(frame_image->get(), frame_layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
        frame_barrier.srcAccessMask = 0;
        frame_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        frame_barrier.subresourceRange = frame_image->get_subresource_range();

        vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &frame_barrier);
    }

    lava::frame_submission submission;
    submission.semaphore = VK_NULL_HANDLE;
    submission.callback = [this, frame_id, frame_image]()
    {
        lava::device_ptr device = this->get_application()->get_device();

        vr::VRVulkanTextureArrayData_t texture_data;
        texture_data.m_nImage = (uint64_t)frame_image->get();
        texture_data.m_pDevice = device->get();
        texture_data.m_pPhysicalDevice = device->get_physical_device()->get();
        texture_data.m_pInstance = this->get_application()->get_instance().get();
        texture_data.m_pQueue = device->get_graphics_queue().vk_queue;
        texture_data.m_nQueueFamilyIndex = device->get_graphics_queue().family;
        texture_data.m_nWidth = this->resolution.x;
        texture_data.m_nHeight = this->resolution.y;
        texture_data.m_nFormat = this->get_format();
        texture_data.m_nSampleCount = VK_SAMPLE_COUNT_1_BIT;
        texture_data.m_unArrayIndex = frame_id;
        texture_data.m_unArraySize = this->framebuffers.size();

        vr::Texture_t texture;
        texture.handle = &texture_data;
        texture.eType = vr::TextureType_Vulkan;
        texture.eColorSpace = vr::ColorSpace_Auto;

        vr::EVRCompositorError error = vr::VRCompositor()->Submit((vr::EVREye)frame_id, &texture, nullptr, vr::Submit_VulkanTextureWithArrayData);
    
        if (error != vr::VRCompositorError_None)
        {
            lava::log()->error("OpenVR: Can't submit image to OpenVR. Maybe due to headset standby. Error code '{}'", error);
        }
    };

    this->get_application()->get_renderer().add_submission(submission);
}

VkFormat OpenVRHeadset::get_format() const
{
    return VK_FORMAT_R8G8B8A8_SRGB;
}

const glm::uvec2& OpenVRHeadset::get_resolution() const
{
    return this->resolution;
}

const glm::mat4& OpenVRHeadset::get_view_matrix() const
{
    return this->view_matrix;
}

const glm::mat4& OpenVRHeadset::get_head_to_eye_matrix(Eye eye) const
{
    return this->head_to_eye_matrices[eye];
}

const glm::mat4& OpenVRHeadset::get_projection_matrix(Eye eye) const
{
    return this->projection_matrices[eye];
}

const glm::mat4& OpenVRHeadset::get_controller_matrix(Controller controller) const
{
    return this->controller_matrices[controller];
}

lava::image::ptr OpenVRHeadset::get_framebuffer(FrameId frame_id) const
{
    return this->framebuffers[frame_id];
}

lava::image::ptr OpenVRHeadset::get_framebuffer_array() const
{
    return this->framebuffer_array;
}

const char* OpenVRHeadset::get_name() const
{
    return "OpenVR Headset";
}

bool OpenVRHeadset::is_attached(Controller controller) const
{
    return this->controller_attached[controller];
}

bool OpenVRHeadset::create_framebuffer()
{
    this->framebuffer_array = lava::make_image(this->get_format());
    this->framebuffer_array->set_layer_count(this->framebuffers.size());
    this->framebuffer_array->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->framebuffer_array->set_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

    if (!this->framebuffer_array->create(this->get_application()->get_device(), this->resolution))
    {
        return false;
    }

    for (uint32_t index = 0; index < this->framebuffers.size(); index++)
    {
        lava::image::ptr framebuffer = lava::make_image(this->get_format(), this->framebuffer_array->get());
        framebuffer->set_base_array_layer(index);
        framebuffer->set_view_type(VK_IMAGE_VIEW_TYPE_2D);

        if (!framebuffer->create(this->get_application()->get_device(), this->resolution))
        {
            return false;
        }

        this->framebuffers[index] = framebuffer;
    }

    return true;
}

void OpenVRHeadset::destroy_framebuffer()
{
    for (lava::image::ptr& framebuffer : this->framebuffers)
    {
        if (framebuffer != nullptr)
        {
            framebuffer->destroy(true);
            framebuffer = nullptr;
        }
    }

    if (this->framebuffer_array != nullptr)
    {
        this->framebuffer_array->destroy();
        this->framebuffer_array = nullptr;
    }
}

void OpenVRHeadset::compute_matrices(Eye eye)
{
    vr::HmdMatrix44_t projection_matrix = this->system->GetProjectionMatrix((vr::EVREye)eye, this->near_plane, this->far_plane);
    vr::HmdMatrix34_t eye_to_head_matrix = this->system->GetEyeToHeadTransform((vr::EVREye)eye);

    this->projection_matrices[eye] = glm::transpose(glm::make_mat4(&projection_matrix.m[0][0]));
    this->head_to_eye_matrices[eye] = glm::inverse(glm::mat4(glm::transpose(glm::make_mat3x4(&eye_to_head_matrix.m[0][0]))));
}

bool OpenVRHeadset::create_actions()
{
    if (vr::VRInput()->SetActionManifestPath((std::string(RESOURCE_FOLDER) + "/dpr-controller/Actions.json").c_str()) != vr::VRInputError_None)
    {
        lava::log()->error("OpenVR: Can't set action manifest!");

        return false;
    }

    std::array<OpenVRActionButtonBinding, 6> action_bindings;
    action_bindings[0].path = "/actions/controller/in/button_x";
    action_bindings[0].controller = CONTROLLER_LEFT;
    action_bindings[0].button = BUTTON_X;

    action_bindings[1].path = "/actions/controller/in/button_y";
    action_bindings[1].controller = CONTROLLER_LEFT;
    action_bindings[1].button = BUTTON_Y;

    action_bindings[2].path = "/actions/controller/in/trigger_left";
    action_bindings[2].controller = CONTROLLER_LEFT;
    action_bindings[2].button = BUTTON_TRIGGER;

    action_bindings[3].path = "/actions/controller/in/button_a";
    action_bindings[3].controller = CONTROLLER_RIGHT;
    action_bindings[3].button = BUTTON_A;

    action_bindings[4].path = "/actions/controller/in/button_b";
    action_bindings[4].controller = CONTROLLER_RIGHT;
    action_bindings[4].button = BUTTON_B;

    action_bindings[5].path = "/actions/controller/in/trigger_right";
    action_bindings[5].controller = CONTROLLER_RIGHT;
    action_bindings[5].button = BUTTON_TRIGGER;

    for (const OpenVRActionButtonBinding& binding : action_bindings)
    {
        if (vr::VRInput()->GetActionHandle(binding.path.c_str(), &this->controller_button_handles[binding.controller][binding.button]) != vr::VRInputError_None)
        {
            lava::log()->error("OpenVR: Can't get handle for action: " + binding.path);

            return false;
        }
    }

    for (uint32_t index = 0; index < 2; index++)
    {
        std::string hand = (index > 0) ? "right" : "left";

        if (vr::VRInput()->GetActionHandle(("/actions/controller/in/thumbstick_" + hand).c_str(), &this->controller_thumbsticks_handles[index]) != vr::VRInputError_None)
        {
            lava::log()->error("OpenVR: Can't get handle for action: /actions/controller/in/thumbstick_" + hand);

            return false;
        }

        if (vr::VRInput()->GetActionHandle(("/actions/controller/in/transform_" + hand).c_str(), &this->controller_transform_handles[index]) != vr::VRInputError_None)
        {
            lava::log()->error("OpenVR: Can't get handle for action: /actions/controller/in/transform_" + hand);

            return false;
        }
    }

    if (vr::VRInput()->GetActionSetHandle("/actions/controller", &this->controller_action_set) != vr::VRInputError_None)
    {
        lava::log()->error("OpenVR: Can't get handle for action set: /actions/controller");

        return false;
    }

    return true;
}

bool OpenVRHeadset::update_actions()
{
    vr::VRActiveActionSet_t active_set;
    active_set.ulActionSet = this->controller_action_set;
    active_set.ulRestrictedToDevice = vr::k_ulInvalidInputValueHandle;
    active_set.ulSecondaryActionSet = vr::k_ulInvalidInputValueHandle;
    active_set.nPriority = 0;
    active_set.unPadding = 0;

    if (vr::VRInput()->UpdateActionState(&active_set, sizeof(active_set), 1) != vr::VRInputError_None)
    {
        lava::log()->error("OpenVR: Can't update action state!");

        return false;
    }

    for (uint32_t controller = 0; controller < this->controller_button_handles.size(); controller++)
    {
        for (uint32_t button = 0; button < this->controller_button_handles[controller].size(); button++)
        {
            if (this->controller_button_handles[controller][button] == vr::k_ulInvalidActionHandle)
            {
                this->controller_buttons[controller][button] = BUTTON_STATE_RELEASED;

                continue;
            }

            vr::InputDigitalActionData_t button_data;

            if (vr::VRInput()->GetDigitalActionData(this->controller_button_handles[controller][button], &button_data, sizeof(button_data), vr::k_ulInvalidInputValueHandle) != vr::VRInputError_None)
            {
                lava::log()->error("OpenVR: Can't get button action data!");

                return false;
            }

            this->controller_buttons[controller][button] = button_data.bState;
        }
    }

    for (uint32_t index = 0; index < 2; index++)
    {
        vr::InputAnalogActionData_t thumbstick_data;
        vr::InputPoseActionData_t transform_data;

        if (vr::VRInput()->GetAnalogActionData(this->controller_thumbsticks_handles[index], &thumbstick_data, sizeof(thumbstick_data), vr::k_ulInvalidInputValueHandle) != vr::VRInputError_None)
        {
            lava::log()->error("OpenVR: Can't get thumbstick action data!");

            return false;
        }

        if (vr::VRInput()->GetPoseActionDataForNextFrame(this->controller_transform_handles[index], vr::TrackingUniverseStanding, &transform_data, sizeof(transform_data), vr::k_ulInvalidInputValueHandle) != vr::VRInputError_None)
        {
            lava::log()->error("OpenVR: Can't get transform action data!");

            return false;
        }

        this->controller_thumbsticks[index] = glm::vec2(thumbstick_data.x, thumbstick_data.y);

        if (transform_data.bActive && transform_data.pose.bPoseIsValid)    
        {
            this->controller_attached[index] = true;
            this->controller_transforms[index] = glm::mat4(glm::transpose(glm::make_mat3x4(&transform_data.pose.mDeviceToAbsoluteTracking.m[0][0])));
        }

        else
        {
            this->controller_attached[index] = false;
            this->controller_transforms[index] = glm::mat4(1.0f);
        }
    }

    return true;
}

bool OpenVRHeadset::update_head_pose()
{
    vr::EVRCompositorError error = vr::VRCompositor()->WaitGetPoses(&this->head_pose, 1, nullptr, 0);

    if (error != vr::VRCompositorError_None)
    {
        lava::log()->error("OpenVR: Can't get current pose from OpenVR: Error code '{}'", error);

        return false;
    }
    
    if (this->head_pose.bPoseIsValid)
    {
        this->head_matrix = glm::inverse(glm::mat4(glm::transpose(glm::make_mat3x4(&this->head_pose.mDeviceToAbsoluteTracking.m[0][0]))));
    }

    return true;
}

void OpenVRHeadset::update_stage(lava::delta delta_time)
{
    for (uint32_t controller = 0; controller < 2; controller++)
    {
        glm::vec2 thumbstick = this->controller_thumbsticks[controller];

        glm::mat4 controller_transform = this->controller_transforms[controller];
        glm::vec3 controller_forward = controller_transform[2];
        glm::vec3 controller_sideward = controller_transform[0];

        float strength = this->movement_speed * delta_time;

        if (this->controller_buttons[controller][BUTTON_TRIGGER])
        {
            strength *= 2.0f;
        }

        stage_position += controller_forward * thumbstick.y * strength;
        stage_position -= controller_sideward * thumbstick.x * strength;
    }

    glm::mat4 stage_matrix = glm::translate(glm::mat4(1.0f), this->stage_position);
    this->view_matrix = this->head_matrix * stage_matrix;

    for (uint32_t index = 0; index < this->controller_matrices.size(); index++)
    {
        this->controller_matrices[index] = glm::inverse(stage_matrix) * this->controller_transforms[index];
    }
}

std::vector<std::string> OpenVRHeadset::split_string(std::string string, char delimiter)
{
    std::vector<std::string> strings;

    while (true)
    {
        size_t next_occurence = string.find(delimiter);
        strings.emplace_back(string.substr(0, next_occurence));

        if (next_occurence == std::string::npos)
        {
            break;
        }

        string = string.substr(next_occurence + 1);
    }

    return strings;
}