#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <openvr.h>
#include <memory>
#include <vector>
#include <array>
#include <string>

#include "headset.hpp"

class OpenVRHeadset : public Headset
{
public:
    typedef std::shared_ptr<OpenVRHeadset> Ptr;

public:
    OpenVRHeadset();

    bool on_setup_instance(lava::frame_config& config) override;
    bool on_setup_device(lava::device::create_param& parameters) override;

    bool on_create() override;
    void on_destroy() override;
    void on_shutdown() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;

    void submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id) override;
    
    VkFormat get_format() const override;
    const glm::uvec2& get_resolution() const override;
    const glm::mat4& get_view_matrix() const override;
    const glm::mat4& get_head_to_eye_matrix(Eye eye) const override;
    const glm::mat4& get_projection_matrix(Eye eye) const override;
    const glm::mat4& get_controller_matrix(Controller controller) const override;

    lava::image::ptr get_framebuffer(FrameId frame_id) const override;
    lava::image::ptr get_framebuffer_array() const override;

    const char* get_name() const override;

    bool is_attached(Controller controller) const override;

private:
    bool create_framebuffer();
    void destroy_framebuffer();

    void compute_matrices(Eye eye);

    bool create_actions();
    bool update_actions();
    bool update_head_pose();
    void update_stage(lava::delta delta_time);

    static std::vector<std::string> split_string(std::string string, char delimiter = ' ');

private:
    vr::IVRSystem* system = nullptr;

    vr::TrackedDevicePose_t head_pose;
    vr::VRActionSetHandle_t controller_action_set;
    std::array<vr::VRActionHandle_t, 2> controller_transform_handles;
    std::array<vr::VRActionHandle_t, 2> controller_thumbsticks_handles;
    std::array<std::array<vr::VRActionHandle_t, BUTTON_MAX_COUNT>, 2> controller_button_handles;

    std::array<lava::image::ptr, 2> framebuffers;
    lava::image::ptr framebuffer_array;

    std::vector<std::string> instance_extensions;
    std::vector<std::string> device_extensions;

    float near_plane = 0.1f;
    float far_plane = 1000.0f;
    float movement_speed = 1.0f;

    glm::uvec2 resolution;
    glm::vec3 stage_position = glm::vec3(0.0f);
    glm::mat4 view_matrix;
    glm::mat4 head_matrix;
    std::array<glm::mat4, 2> head_to_eye_matrices;
    std::array<glm::mat4, 2> projection_matrices;
    std::array<glm::mat4, 2> controller_matrices;

    std::array<bool, 2> controller_attached;
    std::array<glm::mat4, 2> controller_transforms;
    std::array<glm::vec2, 2> controller_thumbsticks;
    std::array<std::array<bool, BUTTON_MAX_COUNT>, 2> controller_buttons;
};