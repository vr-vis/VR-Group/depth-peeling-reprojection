#include "openxr_headset.hpp"
#include "vr_application.hpp"

#include <iostream>
#include <imgui.h>

PFN_xrGetVulkanGraphicsRequirementsKHR xrGetVulkanGraphicsRequirementsKHR = nullptr;
PFN_xrGetVulkanInstanceExtensionsKHR xrGetVulkanInstanceExtensionsKHR = nullptr;
PFN_xrGetVulkanDeviceExtensionsKHR xrGetVulkanDeviceExtensionsKHR = nullptr;
PFN_xrGetVulkanGraphicsDeviceKHR xrGetVulkanGraphicsDeviceKHR = nullptr;

struct OpenXRActionButtonBinding
{
    std::string name;
    std::string localized_name;

    Controller controller;
    Button button;
};

OpenXRHeadset::OpenXRHeadset()
{
    memset(&this->swapchain_indices, 0, sizeof(this->swapchain_indices));

    this->controller_thumbstick_actions.fill(XR_NULL_HANDLE);
    this->controller_transform_actions.fill(XR_NULL_HANDLE);
    this->controller_transform_space.fill(XR_NULL_HANDLE);

    this->controller_attached.fill(false);

    for (uint32_t index = 0; index < this->controller_button_actions.size(); index++)
    {
        this->controller_button_actions[index].fill(XR_NULL_HANDLE);        
        this->controller_buttons[index].fill(false);
    }
}

bool OpenXRHeadset::on_setup_instance(lava::frame_config& config)
{
    std::vector<const char*> required_layers =
    {

    };

    std::vector<const char*> required_extensions =
    {
        XR_KHR_VULKAN_ENABLE_EXTENSION_NAME,
        XR_HTC_VIVE_COSMOS_CONTROLLER_INTERACTION_EXTENSION_NAME
    };

    if (!this->check_layer_support(required_layers))
    {
        return false;
    }
    
    if (!this->check_extension_support(required_extensions))
    {
        return false;
    }

    XrApplicationInfo application_info;
    strncpy(application_info.applicationName, config.info.app_name, XR_MAX_APPLICATION_NAME_SIZE);
    application_info.applicationVersion = XR_MAKE_VERSION(config.info.app_version.major, config.info.app_version.minor, config.info.app_version.patch);
    strncpy(application_info.engineName, config.info.engine_name, XR_MAX_ENGINE_NAME_SIZE);
    application_info.engineVersion = XR_MAKE_VERSION(config.info.engine_version.major, config.info.engine_version.minor, config.info.engine_version.patch);
    application_info.apiVersion = XR_CURRENT_API_VERSION;
    
    XrInstanceCreateInfo create_info;
    create_info.type = XR_TYPE_INSTANCE_CREATE_INFO;
    create_info.next = nullptr;
    create_info.createFlags = 0;
    create_info.applicationInfo = application_info;
    create_info.enabledApiLayerCount = required_layers.size();
    create_info.enabledApiLayerNames = required_layers.data();
    create_info.enabledExtensionCount = required_extensions.size();
    create_info.enabledExtensionNames = required_extensions.data();

    if (!this->check_result(xrCreateInstance(&create_info, &this->instance)))
    {
        lava::log()->error("OpenXR: Can't create instance!");

        return false;
    }

    XrSystemGetInfo get_info;
    get_info.type = XR_TYPE_SYSTEM_GET_INFO;
    get_info.next = nullptr;
    get_info.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    if (!this->check_result(xrGetSystem(this->instance, &get_info, &this->system)))
    {
        lava::log()->error("OpenXR: Can't get system!");

        return false;
    }

    if (!this->check_result(xrGetInstanceProcAddr(this->instance, "xrGetVulkanGraphicsRequirementsKHR", (PFN_xrVoidFunction*)&xrGetVulkanGraphicsRequirementsKHR)))
    {
        lava::log()->error("OpenXR: Can't get function pointer for function 'xrGetVulkanGraphicsRequirementsKHR' !");

        return false;
    }

    if (!this->check_result(xrGetInstanceProcAddr(this->instance, "xrGetVulkanInstanceExtensionsKHR", (PFN_xrVoidFunction*)&xrGetVulkanInstanceExtensionsKHR)))
    {
        lava::log()->error("OpenXR: Can't get function pointer for function 'xrGetVulkanInstanceExtensionsKHR' !");

        return false;
    }

    if (!this->check_result(xrGetInstanceProcAddr(this->instance, "xrGetVulkanDeviceExtensionsKHR", (PFN_xrVoidFunction*)&xrGetVulkanDeviceExtensionsKHR)))
    {
        lava::log()->error("OpenXR: Can't get function pointer for function 'xrGetVulkanDeviceExtensionsKHR' !");

        return false;
    }

    if (!this->check_result(xrGetInstanceProcAddr(this->instance, "xrGetVulkanGraphicsDeviceKHR", (PFN_xrVoidFunction*)&xrGetVulkanGraphicsDeviceKHR)))
    {
        lava::log()->error("OpenXR: Can't get function pointer for function 'xrGetVulkanGraphicsDeviceKHR' !");

        return false;
    }
   
    XrGraphicsRequirementsVulkanKHR requirements;
    memset(&requirements, 0, sizeof(requirements));
    requirements.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;

    if (!this->check_result(xrGetVulkanGraphicsRequirementsKHR(this->instance, this->system, &requirements))) 
    {
        lava::log()->error("OpenXR: Can't get Vulkan requirements!");

        return false;
    }

    uint32_t extension_count = 0;
    if (!this->check_result(xrGetVulkanInstanceExtensionsKHR(this->instance, this->system, 0, &extension_count, nullptr)))
    {
        lava::log()->error("OpenXR: Can't get Vulkan instance extension count!");

        return false;
    }

    std::string extension_string(extension_count, '\0');
    if (!this->check_result(xrGetVulkanInstanceExtensionsKHR(this->instance, this->system, extension_count, &extension_count, extension_string.data())))
    {
        lava::log()->error("OpenXR: Can't get Vulkan instance extension list!");

        return false;
    }

    this->instance_extensions = OpenXRHeadset::split_string(extension_string);
    this->instance_extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME); //NOTE: For some reason this extension is not added

    for (const std::string& extension : this->instance_extensions)
    {
        config.param.extensions.push_back(extension.c_str());
    }

    switch (XR_VERSION_MINOR(requirements.maxApiVersionSupported))
    {
    case 0:
        config.info.req_api_version = lava::api_version::v1_0;
        break;
    case 1:
        config.info.req_api_version = lava::api_version::v1_1;
        break;
    case 2:
        config.info.req_api_version = lava::api_version::v1_2;
        break;
    default:
        lava::log()->error("OpenXR: Unkown Vulkan version!");
        return false;
    }

    return true;
}

bool OpenXRHeadset::on_setup_device(lava::device::create_param& parameters)
{
    VkPhysicalDevice physical_device = VK_NULL_HANDLE;
    if (!this->check_result(xrGetVulkanGraphicsDeviceKHR(this->instance, this->system, this->get_application()->get_instance().get(), &physical_device)))
    {
        if (physical_device != parameters.physical_device->get())
        {
            lava::log()->error("OpenXR: Incorrect physical device selected!");

            return false;
        }
    }

    uint32_t extension_count = 0;
    if (!this->check_result(xrGetVulkanDeviceExtensionsKHR(this->instance, this->system, 0, &extension_count, nullptr)))
    {
        lava::log()->error("OpenXR: Can't get Vulkan device extension count!");

        return false;
    }

    std::string extension_string(extension_count, '\0');
    if (!this->check_result(xrGetVulkanDeviceExtensionsKHR(this->instance, this->system, extension_count, &extension_count, extension_string.data())))
    {
        lava::log()->error("OpenXR: Can't get Vulkan device extension list!");

        return false;
    }

    this->device_extensions = OpenXRHeadset::split_string(extension_string);
    
    for (const std::string& extension : this->device_extensions)
    {
        parameters.extensions.push_back(extension.c_str());
    }

    return true;
}

bool OpenXRHeadset::on_create()
{
    XrGraphicsBindingVulkanKHR vulkan_info;
    vulkan_info.type = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR;
    vulkan_info.next = nullptr;
    vulkan_info.instance = this->get_application()->get_instance().get();
    vulkan_info.physicalDevice = this->get_application()->get_device()->get_physical_device()->get();
    vulkan_info.device = this->get_application()->get_device()->get();
    vulkan_info.queueFamilyIndex = this->get_application()->get_device()->get_graphics_queue().family;
    vulkan_info.queueIndex = 0;

    XrSessionCreateInfo create_info;
    create_info.type = XR_TYPE_SESSION_CREATE_INFO;
    create_info.next = &vulkan_info;
    create_info.createFlags = 0;
    create_info.systemId = this->system;

    if (!this->check_result(xrCreateSession(this->instance, &create_info, &this->session)))
    {
        lava::log()->error("OpenXR: Can't create session!");

        return false;
    }

    XrReferenceSpaceCreateInfo space_info;
    space_info.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
    space_info.next = nullptr;
    
    // OPTION:
    // XR_REFERENCE_SPACE_TYPE_LOCAL: The origin of the world coordinate frame is the point that was defined during the calibration of the headset.
    // XR_REFERENCE_SPACE_TYPE_STAGE: The origin of the world coordinate frame is the ground point that was defined during the calibration of the environment.
    // For more information see:
    // https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#XrReferenceSpaceType
    space_info.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;

    memset(&space_info.poseInReferenceSpace, 0, sizeof(space_info.poseInReferenceSpace));
    space_info.poseInReferenceSpace.orientation.w = 1.0f;
 
    if (!this->check_result(xrCreateReferenceSpace(this->session, &space_info, &this->space)))
    {
        lava::log()->error("OpenXR: Can't create reference space!");

        return false;
    }

    if (!this->check_swapchain_format_support(this->get_format()))
    {
        return false;
    }

    if (!this->check_view_type_support(XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO))
    {
        return false;
    }

    if (!this->check_blend_mode_support(XR_ENVIRONMENT_BLEND_MODE_OPAQUE))
    {
        return false;
    }

    if (!this->create_swapchains())
    {
        return false;
    }

    if (!this->create_framebuffers())
    {
        return false;
    }

    if (!this->create_actions())
    {
        return false;
    }

    return true;
}

void OpenXRHeadset::on_destroy()
{
    this->destroy_actions();
    this->destroy_framebuffers();
    this->destroy_swapchains();

    xrDestroySpace(this->space);
    xrDestroySession(this->session);
}

void OpenXRHeadset::on_shutdown()
{
    xrDestroyInstance(this->instance);
}

bool OpenXRHeadset::on_interface()
{
    ImGui::DragFloat("Near Plane", &this->near_plane);
    ImGui::DragFloat("Far Plane", &this->far_plane);

    ImGui::DragFloat("Movement Speed", &this->movement_speed, 1.0f, 0.0f, 1000.0);

    return true;
}

bool OpenXRHeadset::on_update(lava::delta delta_time)
{
    if (!this->poll_events())
    {
        return false;
    }

    if (this->active)
    {
        if (!this->begin_frame())
        {
            return false;
        }

        if (!this->update_views())
        {
            return false;
        }

        if (this->focused)
        {
            if (!this->update_actions())
            {
                return false;
            }

            this->update_stage(delta_time);
        }

        this->submit_count = 0;
    }

    return true;
}

void OpenXRHeadset::submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id)
{
    if (this->active)
    {
        this->acquire_image(frame_id);

        lava::image::ptr frame_image = this->get_framebuffer(frame_id);
        uint32_t swapchain_index = this->swapchain_indices[frame_id];
        VkImage swapchain_image = this->swapchain_images[frame_id][swapchain_index];

        std::vector<VkImageMemoryBarrier> image_barriers;

        if (frame_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(frame_image->get(), frame_layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
            frame_barrier.srcAccessMask = 0;
            frame_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            frame_barrier.subresourceRange = frame_image->get_subresource_range();

            image_barriers.push_back(frame_barrier);
        }

        VkImageMemoryBarrier swapchain_begin_barrier = lava::image_memory_barrier(swapchain_image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        swapchain_begin_barrier.srcAccessMask = 0;
        swapchain_begin_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        swapchain_begin_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        swapchain_begin_barrier.subresourceRange.baseMipLevel = 0;
        swapchain_begin_barrier.subresourceRange.levelCount = 1;
        swapchain_begin_barrier.subresourceRange.baseArrayLayer = 0;
        swapchain_begin_barrier.subresourceRange.layerCount = 1;

        image_barriers.push_back(swapchain_begin_barrier);

        vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());

        VkImageCopy copy_region;
        copy_region.srcSubresource = frame_image->get_subresource_layers(),
        copy_region.srcOffset.x = 0;
        copy_region.srcOffset.y = 0;
        copy_region.srcOffset.z = 0;
        copy_region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        copy_region.dstSubresource.mipLevel = 0;
        copy_region.dstSubresource.baseArrayLayer = 0;
        copy_region.dstSubresource.layerCount = 1;
        copy_region.dstOffset.x = 0;
        copy_region.dstOffset.y = 0;
        copy_region.dstOffset.z = 0;
        copy_region.extent.width = this->resolution.x;
        copy_region.extent.height = this->resolution.y;
        copy_region.extent.depth = 1;

        vkCmdCopyImage(command_buffer, frame_image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, swapchain_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_region);

        //NOTE: For some reason the image layout has to be VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL even though the OpenXR specification says that it has to be VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        VkImageMemoryBarrier swapchain_end_barrier = lava::image_memory_barrier(swapchain_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
        swapchain_end_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        swapchain_end_barrier.dstAccessMask = 0;
        swapchain_end_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        swapchain_end_barrier.subresourceRange.baseMipLevel = 0;
        swapchain_end_barrier.subresourceRange.levelCount = 1;
        swapchain_end_barrier.subresourceRange.baseArrayLayer = 0;
        swapchain_end_barrier.subresourceRange.layerCount = 1;

        vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &swapchain_end_barrier);

        lava::frame_submission submission;
        submission.semaphore = VK_NULL_HANDLE;
        submission.callback = [this, frame_id]()
        {
            this->release_image(frame_id);

            this->submit_count++;

            if (this->submit_count == this->views.size())
            {
                this->end_frame();
            }
        };

        this->get_application()->get_renderer().add_submission(submission);
    }
}

VkFormat OpenXRHeadset::get_format() const
{
    return VK_FORMAT_R8G8B8A8_SRGB;
}

const glm::uvec2& OpenXRHeadset::get_resolution() const
{
    return this->resolution;
}

const glm::mat4& OpenXRHeadset::get_view_matrix() const
{
    return this->view_matrix;
}

const glm::mat4& OpenXRHeadset::get_head_to_eye_matrix(Eye eye) const
{
    return this->head_to_eye_matrices[eye];
}

const glm::mat4& OpenXRHeadset::get_projection_matrix(Eye eye) const
{
    return this->projection_matrices[eye];
}

const glm::mat4& OpenXRHeadset::get_controller_matrix(Controller controller) const
{
    return this->controller_matrices[controller];
}

lava::image::ptr OpenXRHeadset::get_framebuffer(FrameId frame_id) const
{
    return this->framebuffers[frame_id];
}

lava::image::ptr OpenXRHeadset::get_framebuffer_array() const
{
    return this->framebuffer_array;
}

const char* OpenXRHeadset::get_name() const
{
    return "OpenXR Headset";
}

bool OpenXRHeadset::is_attached(Controller controller) const
{
    return this->controller_attached[controller];
}

bool OpenXRHeadset::create_framebuffers()
{
    this->framebuffer_array = lava::make_image(this->get_format());
    this->framebuffer_array->set_layer_count(this->framebuffers.size());
    this->framebuffer_array->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->framebuffer_array->set_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

    if (!this->framebuffer_array->create(this->get_application()->get_device(), this->resolution))
    {
        lava::log()->error("OpenXR: Can't create framebuffer array!");

        return false;
    }

    for (uint32_t index = 0; index < this->framebuffers.size(); index++)
    {
        lava::image::ptr framebuffer = lava::make_image(this->get_format(), this->framebuffer_array->get());
        framebuffer->set_base_array_layer(index);
        framebuffer->set_view_type(VK_IMAGE_VIEW_TYPE_2D);

        if (!framebuffer->create(this->get_application()->get_device(), this->resolution))
        {
            lava::log()->error("OpenXR: Can't create framebuffer!");

            return false;
        }

        this->framebuffers[index] = framebuffer;
    }

    return true;
}

void OpenXRHeadset::destroy_framebuffers()
{
    for (lava::image::ptr& framebuffer : this->framebuffers)
    {
        if (framebuffer != nullptr)
        {
            framebuffer->destroy(true);
            framebuffer = nullptr;
        }
    }

    if (this->framebuffer_array != nullptr)
    {
        this->framebuffer_array->destroy();
        this->framebuffer_array = nullptr;
    }
}

bool OpenXRHeadset::create_swapchains()
{
    uint32_t view_count = 0;
    if (!this->check_result(xrEnumerateViewConfigurationViews(this->instance, this->system, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, 0, &view_count, nullptr)))
    {
        lava::log()->error("OpenXR: Can't get view count!");

        return false;
    }

    if (view_count != 2)
    {
        lava::log()->error("OpenXR: Invalid number of views!");

        return false;
    }

    XrViewConfigurationView view_template;
    memset(&view_template, 0, sizeof(view_template));
    view_template.type = XR_TYPE_VIEW_CONFIGURATION_VIEW;

    std::vector<XrViewConfigurationView> view_list(view_count, view_template);
    if (!this->check_result(xrEnumerateViewConfigurationViews(this->instance, this->system, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, view_count, &view_count, view_list.data())))
    {
        lava::log()->error("OpenXR: Can't get view list!");

        return false;
    }

    this->resolution.x = view_list[0].recommendedImageRectWidth;
    this->resolution.y = view_list[0].recommendedImageRectHeight;
    uint32_t sample_count = view_list[0].recommendedSwapchainSampleCount;

    for (uint32_t index = 0; index < view_count; index++)
    {
        XrSwapchainCreateInfo create_info;
        create_info.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
        create_info.next = nullptr;
        create_info.createFlags = 0;
        create_info.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT | XR_SWAPCHAIN_USAGE_TRANSFER_SRC_BIT | XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT | XR_SWAPCHAIN_USAGE_SAMPLED_BIT;
        create_info.format = this->get_format();
        create_info.sampleCount = sample_count;
        create_info.width = this->resolution.x;
        create_info.height = this->resolution.y;
        create_info.faceCount = 1;
        create_info.arraySize = 1;
        create_info.mipCount = 1;

        XrSwapchain swapchain = XR_NULL_HANDLE;

        if (!this->check_result(xrCreateSwapchain(this->session, &create_info, &swapchain)))
        {
            lava::log()->error("OpenXR: Can't create swapchain!");

            return false;
        }

        uint32_t image_count = 0;
        if (!this->check_result(xrEnumerateSwapchainImages(swapchain, 0, &image_count, nullptr)))
        {
            lava::log()->error("OpenXR: Can't get swapchain image count!");

            return false;
        }

        XrSwapchainImageVulkanKHR image_template;
        memset(&image_template, 0, sizeof(image_template));
        image_template.type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR;

        std::vector<XrSwapchainImageVulkanKHR> image_list(image_count, image_template);
        if (!this->check_result(xrEnumerateSwapchainImages(swapchain, image_count, &image_count, (XrSwapchainImageBaseHeader*)image_list.data())))
        {
            lava::log()->error("OpenXR: Can't get swapchain image list!");

            return false;
        }

        for (const XrSwapchainImageVulkanKHR& swapchain_image : image_list)
        {
            this->swapchain_images[index].push_back(swapchain_image.image);
        }

        this->swapchains.push_back(swapchain);
    }

    return true;
}

void OpenXRHeadset::destroy_swapchains()
{
    for (uint32_t index = 0; index < this->swapchains.size(); index++)
    {
        xrDestroySwapchain(this->swapchains[index]);
    }

    this->swapchains.clear();
}

bool OpenXRHeadset::create_actions()
{
    XrActionSetCreateInfo action_set_info;
    action_set_info.type = XR_TYPE_ACTION_SET_CREATE_INFO;
    action_set_info.next = nullptr;
    strncpy(action_set_info.actionSetName, "controller_actions", XR_MAX_ACTION_SET_NAME_SIZE);
    strncpy(action_set_info.localizedActionSetName, "Controller Actions", XR_MAX_LOCALIZED_ACTION_SET_NAME_SIZE);
    action_set_info.priority = 0;

    if (xrCreateActionSet(this->instance, &action_set_info, &this->action_set) != XR_SUCCESS)
    {
        lava::log()->error("OpenXR: Can't create action set!");

        return false;
    }

    std::array<OpenXRActionButtonBinding, 6> action_bindings;
    action_bindings[0].name = "button_x";
    action_bindings[0].localized_name = "Button X";
    action_bindings[0].controller = CONTROLLER_LEFT;
    action_bindings[0].button = BUTTON_X;

    action_bindings[1].name = "button_y";
    action_bindings[1].localized_name = "Button Y";
    action_bindings[1].controller = CONTROLLER_LEFT;
    action_bindings[1].button = BUTTON_Y;

    action_bindings[2].name = "trigger_left";
    action_bindings[2].localized_name = "Trigger LEFT";
    action_bindings[2].controller = CONTROLLER_LEFT;
    action_bindings[2].button = BUTTON_TRIGGER;

    action_bindings[3].name = "button_a";
    action_bindings[3].localized_name = "Button A";
    action_bindings[3].controller = CONTROLLER_RIGHT;
    action_bindings[3].button = BUTTON_A;

    action_bindings[4].name = "button_b";
    action_bindings[4].localized_name = "Button B";
    action_bindings[4].controller = CONTROLLER_RIGHT;
    action_bindings[4].button = BUTTON_B;

    action_bindings[5].name = "trigger_right";
    action_bindings[5].localized_name = "Trigger Right";
    action_bindings[5].controller = CONTROLLER_RIGHT;
    action_bindings[5].button = BUTTON_TRIGGER;

    for (const OpenXRActionButtonBinding& binding : action_bindings)
    {
        XrActionCreateInfo button_info;
        button_info.type = XR_TYPE_ACTION_CREATE_INFO;
        button_info.next = nullptr;
        strncpy(button_info.actionName, binding.name.c_str(), XR_MAX_ACTION_NAME_SIZE);
        button_info.actionType = XR_ACTION_TYPE_BOOLEAN_INPUT;
        button_info.countSubactionPaths = 0;
        button_info.subactionPaths = nullptr;
        strncpy(button_info.localizedActionName, binding.localized_name.c_str(), XR_MAX_LOCALIZED_ACTION_NAME_SIZE);
       
        if (xrCreateAction(this->action_set, &button_info, &this->controller_button_actions[binding.controller][binding.button]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create action: " + binding.name);

            return false;
        }
    }
    
    for (uint32_t index = 0; index < 2; index++)
    {
        std::string thumbstick_name = (index > 0) ? "thumbstick_right" : "thumbstick_left";
        std::string thumbstick_name_localized = (index > 0) ? "Thumbstick Right" : "Thumbstick Left";

        XrActionCreateInfo thumbstick_info;
        thumbstick_info.type = XR_TYPE_ACTION_CREATE_INFO;
        thumbstick_info.next = nullptr;
        strncpy(thumbstick_info.actionName, thumbstick_name.c_str(), XR_MAX_ACTION_NAME_SIZE);
        thumbstick_info.actionType = XR_ACTION_TYPE_VECTOR2F_INPUT;
        thumbstick_info.countSubactionPaths = 0;
        thumbstick_info.subactionPaths = nullptr;
        strncpy(thumbstick_info.localizedActionName, thumbstick_name_localized.c_str(), XR_MAX_LOCALIZED_ACTION_NAME_SIZE);

        if (xrCreateAction(this->action_set, &thumbstick_info, &this->controller_thumbstick_actions[index]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create action: " + thumbstick_name);

            return false;
        }

        std::string transform_name = (index > 0) ? "transform_right" : "transform_left";
        std::string transform_name_localized = (index > 0) ? "Transform Right" : "Transform Left";

        XrActionCreateInfo transform_info;
        transform_info.type = XR_TYPE_ACTION_CREATE_INFO;
        transform_info.next = nullptr;
        strncpy(transform_info.actionName, transform_name.c_str(), XR_MAX_ACTION_NAME_SIZE);
        transform_info.actionType = XR_ACTION_TYPE_POSE_INPUT;
        transform_info.countSubactionPaths = 0;
        transform_info.subactionPaths = nullptr;
        strncpy(transform_info.localizedActionName, transform_name_localized.c_str(), XR_MAX_LOCALIZED_ACTION_NAME_SIZE);

        if (xrCreateAction(this->action_set, &transform_info, &this->controller_transform_actions[index]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create action: " + transform_name);

            return false;
        }

        XrActionSpaceCreateInfo transform_space_info;
        transform_space_info.type = XR_TYPE_ACTION_SPACE_CREATE_INFO;
        transform_space_info.next = nullptr;
        transform_space_info.action = this->controller_transform_actions[index];
        transform_space_info.subactionPath = XR_NULL_PATH;

        memset(&transform_space_info.poseInActionSpace, 0, sizeof(transform_space_info.poseInActionSpace));
        transform_space_info.poseInActionSpace.orientation.w = 1.0f;

        if (xrCreateActionSpace(this->session, &transform_space_info, &this->controller_transform_space[index]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create action space!");

            return false;
        }
    }

    if (!this->suggest_vive_controller_binding())
    {
        return false;
    }

    if (!this->suggest_vive_cosmos_controller_binding())
    {
        return false;
    }

    XrSessionActionSetsAttachInfo attach_info;
    attach_info.type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO;
    attach_info.next = nullptr;
    attach_info.countActionSets = 1;
    attach_info.actionSets = &this->action_set;

    if (xrAttachSessionActionSets(this->session, &attach_info) != XR_SUCCESS)
    {
        lava::log()->error("OpenXR: Can't attach action set to session!");

        return false;
    }

    return true;
}

void OpenXRHeadset::destroy_actions()
{
    for (XrSpace& space : this->controller_transform_space)
    {
        if (space != XR_NULL_HANDLE)
        {
            xrDestroySpace(space);
            space = XR_NULL_HANDLE;
        }
    }

    for (uint32_t index = 0; index < this->controller_button_actions.size(); index++)
    {
        for (XrAction& action : this->controller_button_actions[index])
        {
            if (action != XR_NULL_HANDLE)
            {
                xrDestroyAction(action);
                action = XR_NULL_HANDLE;
            }
        }
    }

    for (XrAction& action : this->controller_transform_actions)
    {
        if (action != XR_NULL_HANDLE)
        {
            xrDestroyAction(action);
            action = XR_NULL_HANDLE;
        }
    }

    for (XrAction& action : this->controller_thumbstick_actions)
    {
        if (action != XR_NULL_HANDLE)
        {
            xrDestroyAction(action);
            action = XR_NULL_HANDLE;
        }
    }

    if (this->action_set != XR_NULL_HANDLE)
    {
        xrDestroyActionSet(this->action_set);
        this->action_set = XR_NULL_HANDLE;
    }
}

bool OpenXRHeadset::poll_events()
{
    while (true)
    {
        XrEventDataBuffer event;
        memset(&event, 0, sizeof(event));
        event.type = XR_TYPE_EVENT_DATA_BUFFER;

        XrResult result = xrPollEvent(this->instance, &event);

        if (result == XR_EVENT_UNAVAILABLE)
        {
            break;
        }

        if (!this->check_result(result))
        {
            lava::log()->error("OpenXR: Error during polling of events!");

            return false;
        }

        if (!this->process_event(event))
        {
            return false;
        }
    }

    return true;
}

bool OpenXRHeadset::process_event(const XrEventDataBuffer& event)
{
    switch (event.type)
    {
    case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED:
        {
            const XrEventDataSessionStateChanged* state_change = (const XrEventDataSessionStateChanged*)&event;

            switch (state_change->state)
            {
            case XR_SESSION_STATE_READY:
                {
                    XrSessionBeginInfo begin_info;
                    begin_info.type = XR_TYPE_SESSION_BEGIN_INFO;
                    begin_info.next = nullptr;
                    begin_info.primaryViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

                    if (!this->check_result(xrBeginSession(this->session, &begin_info)))
                    {
                        lava::log()->error("OpenXR: Can't begin session!");

                        return false;
                    }

                    this->active = true;
            
                    break;
                }
            case XR_SESSION_STATE_STOPPING:
                if (!this->check_result(xrEndSession(this->session)))
                {
                    lava::log()->error("OpenXR: Can't end session!");

                    return false;
                }

                this->active = false;
                break;
            case XR_SESSION_STATE_VISIBLE:
                this->focused = false;
                break;
            case XR_SESSION_STATE_FOCUSED:
                this->focused = true;
                break;
            case XR_SESSION_STATE_EXITING:
            case XR_SESSION_STATE_LOSS_PENDING:
                return false;
            default:
                break;
            }

            break;
        }
    case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING:
        return false;
    default:
        break;
    }

    return true;
}

bool OpenXRHeadset::update_actions()
{
    XrActiveActionSet active_set;
    active_set.actionSet = this->action_set;
    active_set.subactionPath = XR_NULL_PATH;

    XrActionsSyncInfo sync_info;
    sync_info.type = XR_TYPE_ACTIONS_SYNC_INFO;
    sync_info.next = nullptr;
    sync_info.countActiveActionSets = 1;
    sync_info.activeActionSets = &active_set;

    XrResult result = xrSyncActions(this->session, &sync_info);

    if (result == XR_SESSION_NOT_FOCUSED)
    {
        return true;
    }

    else if (result != XR_SUCCESS)
    {
        lava::log()->error("OpenXR: Can't sync action set!");

        return false;
    }

    for (uint32_t controller = 0; controller < this->controller_button_actions.size(); controller++)
    {
        for (uint32_t button = 0; button < this->controller_button_actions[controller].size(); button++)
        {
            XrAction action = this->controller_button_actions[controller][button];

            if (action == XR_NULL_HANDLE)
            {
                continue;
            }

            XrActionStateGetInfo button_info;
            button_info.type = XR_TYPE_ACTION_STATE_GET_INFO;
            button_info.next = nullptr;
            button_info.action = action;
            button_info.subactionPath = XR_NULL_PATH;

            XrActionStateBoolean button_state;
            memset(&button_state, 0, sizeof(button_state));
            button_state.type = XR_TYPE_ACTION_STATE_BOOLEAN;

            if (xrGetActionStateBoolean(this->session, &button_info, &button_state) != XR_SUCCESS)
            {
                lava::log()->error("OpenXR: Can't get controller button state!");

                return false;
            }

            this->controller_buttons[controller][button] = button_state.isActive && button_state.currentState;
        }
    }

    for (uint32_t index = 0; index < 2; index++)
    {
        XrActionStateGetInfo thumbstick_info;
        thumbstick_info.type = XR_TYPE_ACTION_STATE_GET_INFO;
        thumbstick_info.next = nullptr;
        thumbstick_info.action = this->controller_thumbstick_actions[index];
        thumbstick_info.subactionPath = XR_NULL_PATH;

        XrActionStateVector2f thumbstick_state;
        memset(&thumbstick_state, 0, sizeof(thumbstick_state));
        thumbstick_state.type = XR_TYPE_ACTION_STATE_VECTOR2F;

        if (xrGetActionStateVector2f(this->session, &thumbstick_info, &thumbstick_state) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't get controller thumbstick state!");

            return false;
        }

        XrActionStateGetInfo transform_info;
        transform_info.type = XR_TYPE_ACTION_STATE_GET_INFO;
        transform_info.next = nullptr;
        transform_info.action = this->controller_transform_actions[index];
        transform_info.subactionPath = XR_NULL_PATH;
        
        XrActionStatePose transform_state;
        memset(&transform_state, 0, sizeof(transform_state));
        transform_state.type = XR_TYPE_ACTION_STATE_POSE;

        if (xrGetActionStatePose(this->session, &transform_info, &transform_state) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't get controller transform state!");

            return false;
        }

        this->controller_attached[index] = transform_state.isActive;
        this->controller_thumbsticks[index] = glm::vec2(0.0f);
        this->controller_transforms[index] = glm::mat4(1.0f);

        if (thumbstick_state.isActive)
        {
            this->controller_thumbsticks[index] = glm::vec2(thumbstick_state.currentState.x, thumbstick_state.currentState.y);
        }

        if (transform_state.isActive)
        {
            XrSpaceLocation transform;
            memset(&transform, 0, sizeof(transform));
            transform.type = XR_TYPE_SPACE_LOCATION;
            
            if (xrLocateSpace(this->controller_transform_space[index], this->space, this->frame_state.predictedDisplayTime, &transform) != XR_SUCCESS)
            {
                lava::log()->error("OpenXR: Can't locate controller!");

                return false;
            }

            if ((transform.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT) == 0 || (transform.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) == 0)
            {
                this->controller_attached[index] = false;

                continue;
            }

            if ((transform.locationFlags & XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT) == 0 || (transform.locationFlags & XR_SPACE_LOCATION_POSITION_TRACKED_BIT) == 0)
            {
                lava::log()->warn("OpenXR: Controller not tracked correctly. Recalibration maybe neccessary!");

                continue;
            }

            XrQuaternionf quaternion = transform.pose.orientation;
            XrVector3f position = transform.pose.position;

            this->controller_transforms[index] = glm::translate(glm::mat4(1.0f), glm::vec3(position.x, position.y, position.z)) * (glm::mat4)glm::quat(quaternion.w, quaternion.x, quaternion.y, quaternion.z);
        }
    }

    return true;
}

void OpenXRHeadset::update_stage(lava::delta delta_time)
{
    for (uint32_t controller = 0; controller < 2; controller++)
    {
        glm::vec2 thumbstick = this->controller_thumbsticks[controller];

        glm::mat4 controller_transform = this->controller_transforms[controller];
        glm::vec3 controller_forward = controller_transform[2];
        glm::vec3 controller_sideward = controller_transform[0];

        float strength = this->movement_speed * delta_time;

        if (this->controller_buttons[controller][BUTTON_TRIGGER])
        {
            strength *= 2.0f;
        }

        stage_position += controller_forward * thumbstick.y * strength;
        stage_position -= controller_sideward * thumbstick.x * strength;
    }

    glm::mat4 stage_matrix = glm::translate(glm::mat4(1.0f), this->stage_position);
    this->view_matrix = stage_matrix;

    for (uint32_t index = 0; index < this->controller_matrices.size(); index++)
    {
        this->controller_matrices[index] = glm::inverse(stage_matrix) * this->controller_transforms[index];
    }
}

bool OpenXRHeadset::begin_frame()
{
    XrFrameWaitInfo wait_info;
    wait_info.type = XR_TYPE_FRAME_WAIT_INFO;
    wait_info.next = nullptr;

    memset(&this->frame_state, 0, sizeof(this->frame_state));
    this->frame_state.type = XR_TYPE_FRAME_STATE;

    if (!this->check_result(xrWaitFrame(this->session, &wait_info, &this->frame_state)))
    {
        lava::log()->error("OpenXR: Can't wait for frame!");

        return false;
    }

    XrFrameBeginInfo begin_info;
    begin_info.type = XR_TYPE_FRAME_BEGIN_INFO;
    begin_info.next = nullptr;

    if (!this->check_result(xrBeginFrame(this->session, &begin_info)))
    {
        lava::log()->error("OpenXR: Can't begin frame!");

        return false;
    }

    return true;
}

bool OpenXRHeadset::update_views()
{
    XrViewLocateInfo view_info;
    view_info.type = XR_TYPE_VIEW_LOCATE_INFO;
    view_info.next = nullptr;
    view_info.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
    view_info.displayTime = this->frame_state.predictedDisplayTime;
    view_info.space = this->space;

    XrViewState view_state;
    view_state.type = XR_TYPE_VIEW_STATE;
    view_state.next = nullptr;
    view_state.viewStateFlags = 0;

    uint32_t view_count = 0;
    if (!this->check_result(xrLocateViews(this->session, &view_info, &view_state, 0, &view_count, nullptr)))
    {
        lava::log()->error("OpenXR: Can't get location and orientation of views!");

        return false;
    }

    XrView view_template;
    memset(&view_template, 0, sizeof(view_template));
    view_template.type = XR_TYPE_VIEW;
    view_template.pose.orientation.w = 1.0f;

    this->views.resize(view_count, view_template);
    if (!this->check_result(xrLocateViews(this->session, &view_info, &view_state, view_count, &view_count, this->views.data())))
    {
        return false;
    }

    if ((view_state.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) == 0 || (view_state.viewStateFlags & XR_VIEW_STATE_POSITION_VALID_BIT) == 0)
    {
        return true;
    }

    if ((view_state.viewStateFlags & XR_VIEW_STATE_ORIENTATION_TRACKED_BIT) == 0 || (view_state.viewStateFlags & XR_VIEW_STATE_POSITION_TRACKED_BIT) == 0)
    {
        lava::log()->warn("OpenXR: Headset not tracked correctly. Recalibration maybe neccessary!");

        return true;
    }

    for (uint32_t index = 0; index < view_count; index++)
    {
        const XrView& view = this->views[index];

        XrQuaternionf quaternion = view.pose.orientation;
        XrVector3f position = view.pose.position;

        this->head_to_eye_matrices[index] = (glm::mat4)glm::inverse(glm::quat(quaternion.w, quaternion.x, quaternion.y, quaternion.z)) * glm::translate(glm::mat4(1.0f), glm::vec3(-position.x, -position.y, -position.z));

        float left = this->near_plane * glm::tan(view.fov.angleLeft);
        float right = this->near_plane * glm::tan(view.fov.angleRight);
        float bottom = this->near_plane * glm::tan(view.fov.angleDown);
        float top = this->near_plane * glm::tan(view.fov.angleUp);

        this->projection_matrices[index] = glm::frustum(left, right, bottom, top, this->near_plane, this->far_plane);
    }

    return true;
}

bool OpenXRHeadset::acquire_image(FrameId frame_id)
{
    XrSwapchainImageAcquireInfo acquire_info;
    acquire_info.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO;
    acquire_info.next = nullptr;

    if (!this->check_result(xrAcquireSwapchainImage(this->swapchains[frame_id], &acquire_info, &this->swapchain_indices[frame_id])))
    {
        lava::log()->error("OpenXR: Can't acquire swapchain image!");

        return false;
    }

    XrSwapchainImageWaitInfo wait_info;
    wait_info.type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO;
    wait_info.next = nullptr;
    wait_info.timeout = XR_INFINITE_DURATION;

    if (!this->check_result(xrWaitSwapchainImage(this->swapchains[frame_id], &wait_info)))
    {
        lava::log()->error("OpenXR: Can't wait for swapchain image!");

        return false;
    }

    return true;
}

bool OpenXRHeadset::release_image(FrameId frame_id)
{
    XrSwapchainImageReleaseInfo release_info;
    release_info.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO;
    release_info.next = nullptr;

    if (!this->check_result(xrReleaseSwapchainImage(this->swapchains[frame_id], &release_info)))
    {
        lava::log()->error("OpenXR: Can't release swapchain image!");

        return false;
    }

    return true;
}

bool OpenXRHeadset::end_frame()
{
    std::vector<XrCompositionLayerProjectionView> projection_views;

    for (uint32_t index = 0; index < this->views.size(); index++)
    {
        const XrView& view = this->views[index];

        XrCompositionLayerProjectionView projection_view;
        projection_view.type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
        projection_view.next = nullptr;
        projection_view.pose = view.pose;
        projection_view.fov = view.fov;
        projection_view.subImage.swapchain = this->swapchains[index];
        projection_view.subImage.imageRect.offset.x = 0;
        projection_view.subImage.imageRect.offset.y = 0;
        projection_view.subImage.imageRect.extent.width = this->resolution.x;
        projection_view.subImage.imageRect.extent.height = this->resolution.y;
        projection_view.subImage.imageArrayIndex = 0;

        projection_views.push_back(projection_view);
    }

    XrCompositionLayerProjection projection_layer;
    projection_layer.type = XR_TYPE_COMPOSITION_LAYER_PROJECTION;
    projection_layer.next = nullptr;
    projection_layer.layerFlags = 0;
    projection_layer.space = this->space;
    projection_layer.viewCount = projection_views.size();
    projection_layer.views = projection_views.data();

    XrCompositionLayerBaseHeader* layer_pointer = (XrCompositionLayerBaseHeader*)&projection_layer;

    XrFrameEndInfo end_info;
    end_info.type = XR_TYPE_FRAME_END_INFO;
    end_info.next = nullptr;
    end_info.displayTime = this->frame_state.predictedDisplayTime;
    end_info.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
    end_info.layerCount = 1;
    end_info.layers = &layer_pointer;

    if (!this->check_result(xrEndFrame(this->session, &end_info)))
    {
        lava::log()->error("OpenXR: Can't end frame!");

        return false;
    }

    return true;
}

bool OpenXRHeadset::check_result(XrResult result) const
{
    if (result != XR_SUCCESS)
    {
        if (this->instance != XR_NULL_HANDLE)
        {
            std::array<char, XR_MAX_RESULT_STRING_SIZE> result_string;
            xrResultToString(this->instance, result, result_string.data());        
            
            lava::log()->error("OpenXR: Error '{}' detected!", result_string.data());
        }

        else
        {
            std::cout << "OpenXR: Error detected!" << std::endl;
        }

        return false;
    }

    return true;
}

bool OpenXRHeadset::check_layer_support(const std::vector<const char*>& required_layers) const
{
    uint32_t layer_count = 0;
    if (!this->check_result(xrEnumerateApiLayerProperties(0, &layer_count, nullptr)))
    {
        return false;
    }

    XrApiLayerProperties layer_template;
    memset(&layer_template, 0, sizeof(layer_template));
    layer_template.type = XR_TYPE_API_LAYER_PROPERTIES;

    std::vector<XrApiLayerProperties> layer_list(layer_count, layer_template);
    if (!this->check_result(xrEnumerateApiLayerProperties(layer_count, &layer_count, layer_list.data())))
    {
        return false;
    }

    for (const char* required_layer : required_layers)
    {
        bool found = false;

        for (const XrApiLayerProperties& layer : layer_list)
        {
            if (strcmp(required_layer, layer.layerName) == 0)
            {
                found = true;

                break;
            }
        }

        if (!found)
        {
            lava::log()->error("OpenXR: Required layer '{}' not supported!", required_layer);

            return false;
        }
    }

    return true;
}

bool OpenXRHeadset::check_extension_support(const std::vector<const char*>& required_extensions) const
{
    uint32_t extension_count = 0;
    if (!this->check_result(xrEnumerateInstanceExtensionProperties(nullptr, 0, &extension_count, nullptr)))
    {
        return false;
    }

    XrExtensionProperties extension_template;
    memset(&extension_template, 0, sizeof(extension_template));
    extension_template.type = XR_TYPE_EXTENSION_PROPERTIES;

    std::vector<XrExtensionProperties> extension_list(extension_count, extension_template);
    if (!this->check_result(xrEnumerateInstanceExtensionProperties(nullptr, extension_count, &extension_count, extension_list.data())))
    {
        return false;
    }

    for (const char* required_extension : required_extensions)
    {
        bool found = false;

        for (const XrExtensionProperties& extension : extension_list)
        {
            if (strcmp(required_extension, extension.extensionName) == 0)
            {
                found = true;

                break;
            }
        }

        if (!found)
        {
            lava::log()->error("OpenXR: Required extension '{}' not supported!", required_extension);

            return false;
        }
    }

    return true;
}

bool OpenXRHeadset::check_blend_mode_support(XrEnvironmentBlendMode required_mode) const
{
    uint32_t mode_count = 0;
    if (!this->check_result(xrEnumerateEnvironmentBlendModes(this->instance, this->system, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, 0, &mode_count, nullptr)))
    {
        return false;
    }

    std::vector<XrEnvironmentBlendMode> mode_list(mode_count, (XrEnvironmentBlendMode)0);
    if (!this->check_result(xrEnumerateEnvironmentBlendModes(this->instance, this->system, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, mode_count, &mode_count, mode_list.data())))
    {
        return false;
    }

    for (XrEnvironmentBlendMode mode : mode_list)
    {
        if (mode == required_mode)
        {
            return true;
        }
    }

    lava::log()->error("OpenXR: The system does not support the required blend mode!");

    return false;
}

bool OpenXRHeadset::check_view_type_support(XrViewConfigurationType required_type) const
{
    uint32_t view_type_count = 0;
    if (!this->check_result(xrEnumerateViewConfigurations(this->instance, this->system, 0, &view_type_count, nullptr)))
    {
        return false;
    }

    std::vector<XrViewConfigurationType> view_type_list(view_type_count, (XrViewConfigurationType)0);
    if (!this->check_result(xrEnumerateViewConfigurations(this->instance, this->system, view_type_count, &view_type_count, view_type_list.data())))
    {
        return false;
    }

    for (XrViewConfigurationType view_type : view_type_list)
    {
        if (view_type == required_type)
        {
            return true;
        }
    }

    lava::log()->error("OpenXR: The system does not support the required view type!");

    return false;
}

bool OpenXRHeadset::check_swapchain_format_support(int64_t required_format) const
{
    uint32_t format_count = 0;
    if (!this->check_result(xrEnumerateSwapchainFormats(this->session, 0, &format_count, nullptr)))
    {
        return false;
    }

    std::vector<int64_t> format_list(format_count, 0);
    if (!this->check_result(xrEnumerateSwapchainFormats(this->session, format_count, &format_count, format_list.data())))
    {
        return false;
    }

    for (int64_t format : format_list)
    {
        if (format == required_format)
        {
            return true;
        }
    }

    lava::log()->error("OpenXR: The system does not support the required swapchain format!");

    return false;
}

bool OpenXRHeadset::suggest_vive_controller_binding()
{
    std::vector<std::string> path_strings =
    {
        "/user/hand/left/input/trackpad/click",
        "/user/hand/left/input/squeeze/click",
        "/user/hand/left/input/trigger/click",
        "/user/hand/right/input/trackpad/click",
        "/user/hand/right/input/squeeze/click",
        "/user/hand/right/input/trigger/click",
        "/user/hand/left/input/trackpad",
        "/user/hand/right/input/trackpad",
        "/user/hand/left/input/aim/pose",
        "/user/hand/right/input/aim/pose",
        "/interaction_profiles/htc/vive_controller"
    };

    std::vector<XrPath> paths;
    paths.resize(path_strings.size());

    for (uint32_t index = 0; index < path_strings.size(); index++)
    {
        if (xrStringToPath(this->instance, path_strings[index].c_str(), &paths[index]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create path for string: " + path_strings[index]);

            return false;
        }
    }

    std::vector<XrAction> actions =
    {
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_X],
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_Y],
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_TRIGGER],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_A],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_B],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_TRIGGER],
        this->controller_thumbstick_actions[CONTROLLER_LEFT],
        this->controller_thumbstick_actions[CONTROLLER_RIGHT],
        this->controller_transform_actions[CONTROLLER_LEFT],
        this->controller_transform_actions[CONTROLLER_RIGHT]
    };

    std::vector<XrActionSuggestedBinding> bindings;
    bindings.resize(actions.size());

    for (uint32_t index = 0; index < bindings.size(); index++)
    {
        bindings[index].action = actions[index];
        bindings[index].binding = paths[index];
    }

    XrInteractionProfileSuggestedBinding suggested_bindings;
    suggested_bindings.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggested_bindings.next = nullptr;
    suggested_bindings.interactionProfile = paths.back();
    suggested_bindings.countSuggestedBindings = bindings.size();
    suggested_bindings.suggestedBindings = bindings.data();
    
    if (xrSuggestInteractionProfileBindings(this->instance, &suggested_bindings) != XR_SUCCESS)
    {
        lava::log()->error("OpenXR: Can't set suggested bindings for vive controller");

        return false;
    }

    return true;
}

bool OpenXRHeadset::suggest_vive_cosmos_controller_binding()
{
    std::vector<std::string> path_strings =
    {
        "/user/hand/left/input/x/click",
        "/user/hand/left/input/y/click",
        "/user/hand/left/input/trigger/click",
        "/user/hand/right/input/a/click",
        "/user/hand/right/input/b/click",
        "/user/hand/right/input/trigger/click",
        "/user/hand/left/input/thumbstick",
        "/user/hand/right/input/thumbstick",
        "/user/hand/left/input/aim/pose",
        "/user/hand/right/input/aim/pose",
        "/interaction_profiles/htc/vive_cosmos_controller"
    };

    std::vector<XrPath> paths;
    paths.resize(path_strings.size());

    for (uint32_t index = 0; index < path_strings.size(); index++)
    {
        if (xrStringToPath(this->instance, path_strings[index].c_str(), &paths[index]) != XR_SUCCESS)
        {
            lava::log()->error("OpenXR: Can't create path for string: " + path_strings[index]);

            return false;
        }
    }

    std::vector<XrAction> actions =
    {
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_X],
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_Y],
        this->controller_button_actions[CONTROLLER_LEFT][BUTTON_TRIGGER],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_A],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_B],
        this->controller_button_actions[CONTROLLER_RIGHT][BUTTON_TRIGGER],
        this->controller_thumbstick_actions[CONTROLLER_LEFT],
        this->controller_thumbstick_actions[CONTROLLER_RIGHT],
        this->controller_transform_actions[CONTROLLER_LEFT],
        this->controller_transform_actions[CONTROLLER_RIGHT]
    };

    std::vector<XrActionSuggestedBinding> bindings;
    bindings.resize(actions.size());

    for (uint32_t index = 0; index < bindings.size(); index++)
    {
        bindings[index].action = actions[index];
        bindings[index].binding = paths[index];
    }

    XrInteractionProfileSuggestedBinding suggested_bindings;
    suggested_bindings.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggested_bindings.next = nullptr;
    suggested_bindings.interactionProfile = paths.back();
    suggested_bindings.countSuggestedBindings = bindings.size();
    suggested_bindings.suggestedBindings = bindings.data();
    
    if (xrSuggestInteractionProfileBindings(this->instance, &suggested_bindings) != XR_SUCCESS)
    {
        lava::log()->error("OpenXR: Can't set suggested bindings for vive cosmos controller");

        return false;
    }

    return true;
}

std::vector<std::string> OpenXRHeadset::split_string(std::string string, char delimiter)
{
    std::vector<std::string> strings;

    while (true)
    {
        size_t next_occurence = string.find(delimiter);
        strings.emplace_back(string.substr(0, next_occurence));

        if (next_occurence == std::string::npos)
        {
            break;
        }

        string = string.substr(next_occurence + 1);
    }

    return strings;
}