#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
#include <memory>
#include <vector>
#include <array>

#include "headset.hpp"

class OpenXRHeadset : public Headset
{
public:
    typedef std::shared_ptr<OpenXRHeadset> Ptr;

public:
    OpenXRHeadset();

    bool on_setup_instance(lava::frame_config& config) override;
    bool on_setup_device(lava::device::create_param& parameters) override;

    bool on_create() override;
    void on_destroy() override;
    void on_shutdown() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;

    void submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id) override;

    VkFormat get_format() const override;
    const glm::uvec2& get_resolution() const override;
    const glm::mat4& get_view_matrix() const override;
    const glm::mat4& get_head_to_eye_matrix(Eye eye) const override;
    const glm::mat4& get_projection_matrix(Eye eye) const override;
    const glm::mat4& get_controller_matrix(Controller controller) const override;

    lava::image::ptr get_framebuffer(FrameId frame_id) const override;
    lava::image::ptr get_framebuffer_array() const override;

    const char* get_name() const override;

    bool is_attached(Controller controller) const override;

private:
    bool create_framebuffers();
    void destroy_framebuffers();

    bool create_swapchains();
    void destroy_swapchains();

    bool create_actions();
    void destroy_actions();

    bool poll_events();
    bool process_event(const XrEventDataBuffer& event);

    bool update_actions();
    void update_stage(lava::delta delta_time);

    bool begin_frame();
    bool update_views();
    bool acquire_image(FrameId frame_id);
    bool release_image(FrameId frame_id);
    bool end_frame();

    bool check_result(XrResult result) const;
    bool check_layer_support(const std::vector<const char*>& required_layers) const;
    bool check_extension_support(const std::vector<const char*>& required_extensions) const;
    bool check_blend_mode_support(XrEnvironmentBlendMode required_mode) const;
    bool check_view_type_support(XrViewConfigurationType required_type) const;
    bool check_swapchain_format_support(int64_t required_format) const;

    bool suggest_vive_controller_binding();
    bool suggest_vive_cosmos_controller_binding();

    static std::vector<std::string> split_string(std::string string, char delimiter = ' ');

private:
    XrInstance instance = XR_NULL_HANDLE;
    XrSystemId system = XR_NULL_SYSTEM_ID;
    XrSession session = XR_NULL_HANDLE;
    XrSpace space = XR_NULL_HANDLE;
    XrFrameState frame_state;
    std::vector<XrSwapchain> swapchains;
    std::vector<XrView> views;

    XrActionSet action_set = XR_NULL_HANDLE;
    std::array<XrAction, 2> controller_transform_actions;
    std::array<XrSpace, 2> controller_transform_space;
    std::array<XrAction, 2> controller_thumbstick_actions;
    std::array<std::array<XrAction, BUTTON_MAX_COUNT>, 2> controller_button_actions;

    std::array<uint32_t, 2> swapchain_indices;
    std::array<std::vector<VkImage>, 2> swapchain_images;
    std::array<lava::image::ptr, 2> framebuffers;
    lava::image::ptr framebuffer_array;

    std::vector<std::string> instance_extensions;
    std::vector<std::string> device_extensions;

    bool active = false;
    bool focused = false;
    uint32_t submit_count = 0;

    float near_plane = 0.1f;
    float far_plane = 1000.0f;
    float movement_speed = 1.0f;

    glm::uvec2 resolution;
    glm::vec3 stage_position = glm::vec3(0.0f);
    glm::mat4 view_matrix;
    std::array<glm::mat4, 2> head_to_eye_matrices;
    std::array<glm::mat4, 2> projection_matrices;
    std::array<glm::mat4, 2> controller_matrices;

    std::array<bool, 2> controller_attached;
    std::array<glm::mat4, 2> controller_transforms;
    std::array<glm::vec2, 2> controller_thumbsticks;
    std::array<std::array<bool, BUTTON_MAX_COUNT>, 2> controller_buttons;
};