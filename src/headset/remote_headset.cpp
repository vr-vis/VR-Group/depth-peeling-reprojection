#include "remote_headset.hpp"
#include "vr_application.hpp"

#include <imgui.h>
#include <filesystem>
#include <chrono>

RemoteHeadset::RemoteHeadset()
{
    this->controller_states.fill(CONTROLLER_STATE_DETACHED);
    this->transport_controller_states.fill(CONTROLLER_STATE_DETACHED);

    for (std::array<ButtonState, BUTTON_MAX_COUNT>& buttons : this->controller_buttons)
    {
        buttons.fill(BUTTON_STATE_RELEASED);   
    }
}

bool RemoteHeadset::on_setup_instance(lava::frame_config& config)
{
    if (!setup_instance_for_encoder(this->get_application()->get_command_parser().get_encoder(), config))
    {
        return false;
    }

    return true;
}

bool RemoteHeadset::on_setup_device(lava::device::create_param& parameters)
{
    if (!setup_device_for_encoder(this->get_application()->get_command_parser().get_encoder(), this->get_application()->get_instance(), parameters))
    {
        return false;
    }

    return true;
}

bool RemoteHeadset::on_create()
{
    const CommandParser& command_parser = this->get_application()->get_command_parser();
    this->encoder_mode = command_parser.get_encoder_mode().value_or((EncoderMode)this->encoder_mode);
    this->encoder_input_rate = command_parser.get_encoder_input_rate().value_or(this->encoder_input_rate);
    this->encoder_key_rate = command_parser.get_encoder_key_rate().value_or(this->encoder_key_rate);
    this->encoder_frame_rate = command_parser.get_encoder_frame_rate().value_or(this->encoder_frame_rate);
    this->encoder_quality = command_parser.get_encoder_quality().value_or(this->encoder_quality);
    this->encoder_bit_rate = command_parser.get_encoder_bitrate().value_or(this->encoder_bit_rate);

    StatisticLog::Ptr statistic_log = this->get_application()->get_statistic_log();

    this->statistic_send_bitrate = make_statistic("host_send_bitrate", UNIT_MBITS_PER_SECOND);
    this->statistic_send_queue_size = make_statistic("host_send_queue", UNIT_KBYTES);
    this->statistic_receive_bitrate = make_statistic("host_receive_bitrate", UNIT_MBITS_PER_SECOND);
    this->statistic_receive_queue_size = make_statistic("host_receive_queue", UNIT_KBYTES);
    
    statistic_log->add_statistic(this->statistic_send_bitrate);
    statistic_log->add_statistic(this->statistic_send_queue_size);
    statistic_log->add_statistic(this->statistic_receive_bitrate);
    statistic_log->add_statistic(this->statistic_receive_queue_size);

    this->statistic_list.push_back(this->statistic_send_bitrate);
    this->statistic_list.push_back(this->statistic_send_queue_size);
    this->statistic_list.push_back(this->statistic_receive_bitrate);
    this->statistic_list.push_back(this->statistic_receive_queue_size);

    StereoStrategy::Ptr strategy = this->get_application()->get_stereo_strategy();
    this->frame_id_count = strategy->get_max_remote_frame_ids();

    if (!convert_strategy(this->get_application()->get_stereo_strategy_type(), this->remote_strategy))
    {
        return false;
    }

    if (!this->create_transport())
    {
        lava::log()->error("Remote Headset: Can't create server!");

        return false;
    }

    lava::log()->debug("Waiting for connection");

    if (!this->transport->wait_connect())
    {
        lava::log()->error("Remote Headset: Incomplete setup handshake!");

        return false;    
    }

    this->update_values();

    if (!this->create_framebuffers())
    {
        lava::log()->error("Remote Headset: Can't create framebuffers!");

        return false;        
    }

    if (!this->create_encoders())
    {
        lava::log()->error("Remote Headset: Can't create encoders!");

        return false;
    }

    return true;
}

void RemoteHeadset::on_destroy()
{
    if (this->transport != nullptr)
    {
        this->transport->destroy();
    }

    this->destroy_framebuffers();
    this->destroy_encoders();
}

bool RemoteHeadset::on_interface()
{
    if (this->transport->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        return false;
    }

    ImGui::DragFloat("Movement Speed", &this->movement_speed, 1.0f, 0.0f, 1000.0f);

    ImGui::Separator();

    Encoder::Ptr encoder = this->encoders.front();

    if (encoder->is_supported(ENCODER_SETTING_MODE_CONSTANT_BITRATE) && encoder->is_supported(ENCODER_SETTING_MODE_CONSTANT_QUALITY))
    {
        std::vector<const char*> encoder_mode_names;
        encoder_mode_names.push_back("Constant Bitrate");
        encoder_mode_names.push_back("Constant Quality");

        if (ImGui::Combo("Encoder Mode", (int32_t*)&this->encoder_mode, encoder_mode_names.data(), encoder_mode_names.size()))
        {
            for (Encoder::Ptr encoder : this->encoders)
            {
                encoder->set_mode((EncoderMode)this->encoder_mode);
            }
        }
    }

    ImGui::SliderInt("Input-Rate [Frames/s]", (int32_t*) &this->encoder_input_rate, 1, 180);

    if (encoder->is_supported(ENCODER_SETTING_KEY_RATE))
    {
        if (ImGui::SliderInt("Key-Rate [Frames]", (int32_t*) &this->encoder_key_rate, 1, 180))
        {
            for (Encoder::Ptr encoder : this->encoders)
            {
                encoder->set_key_rate(this->encoder_key_rate);
            }
        }
    }

    if (this->encoder_mode == ENCODER_MODE_CONSTANT_BITRATE)
    {
        if (encoder->is_supported(ENCODER_SETTING_BITRATE))
        {
            if (ImGui::SliderFloat("Bit-Rate [Mbit/s]", &this->encoder_bit_rate, 0.1f, 100.0f))
            {
                for (Encoder::Ptr encoder : this->encoders)
                {
                    encoder->set_bitrate(this->encoder_bit_rate);
                }
            }  
        }
      
        if (encoder->is_supported(ENCODER_SETTING_FRAME_RATE))
        {
            if (ImGui::SliderInt("Frame-Rate [Frames/s]", (int32_t*) &this->encoder_frame_rate, 1, 180))
            {
                for (Encoder::Ptr encoder : this->encoders)
                {
                    encoder->set_frame_rate(this->encoder_frame_rate);
                }
            }   
        }
    }

    else if (this->encoder_mode == ENCODER_MODE_CONSTANT_QUALITY)
    {
        if (encoder->is_supported(ENCODER_SETTING_QUALITY))
        {
            if (ImGui::SliderFloat("Quality", &this->encoder_quality, 0.0f, 1.0f))
            {
                for (Encoder::Ptr encoder : this->encoders)
                {
                    encoder->set_quality(this->encoder_quality);
                }
            }
        }
    }

    else
    {
        lava::log()->error("Remote Headset: Unknown encoder mode!");

        return false;
    }

    ImGui::Separator();

    if(ImGui::Checkbox("Overlay", &this->overlay_enable))
    {
        this->transport->send_overlay_config(this->overlay_enable);
    }

    std::vector<std::string> graph_names;
    std::vector<const char*> graph_name_pointers;

    std::unique_lock<std::mutex> lock(this->transport_mutex);
    for (const Statistic::Ptr& statistic : this->statistic_list)
    {
        if (!statistic->is_log_only())
        {
            graph_names.push_back(statistic->get_display_name());
        }
    }
    lock.unlock();

    for (const std::string& graph_name : graph_names)
    {
        graph_name_pointers.push_back(graph_name.c_str());
    }

    ImGui::Combo("Overlay Graph", (int32_t*) &this->overlay_graph, graph_name_pointers.data(), graph_name_pointers.size());
    
    ImGui::Separator();

    lock.lock();
    for (Statistic::Ptr statistic : this->statistic_list)
    {
        if (!statistic->is_log_only())
        {
            statistic->interface(128);
        }
    }
    lock.unlock();

    return true;
}

bool RemoteHeadset::on_update(lava::delta delta_time)
{
    if (this->transport->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        return false;
    }

    this->update_values();
    this->update_bitrates();
    this->update_stage(delta_time);
    this->update_overlay();
    
    this->encoder_last_submit += delta_time;
    this->encoder_enable_submit = false;

    if (this->encoder_last_submit > (1.0 / (float)this->encoder_input_rate))
    {
        this->frame_number++;
        this->encoder_enable_submit = true;
        this->encoder_last_submit = 0.0f;
    }

    return true;
}

void RemoteHeadset::submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id)
{
    if (frame_id >= this->frame_id_count)
    {
        lava::log()->error("Remote Headset: Invalid frame id during frame submission!");

        return;
    }

    if (!this->encoder_enable_submit)
    {
        return;        
    }

    lava::renderer& renderer = this->get_application()->get_renderer();
    lava::image::ptr framebuffer = this->framebuffers[frame_id];

    PassTimer::Ptr pass_timer = this->get_application()->get_pass_timer();
    Encoder::Ptr encoder = this->encoders[frame_id];    
    uint32_t frame_number = this->frame_number;
    uint32_t transform_id = this->transform_id;

    pass_timer->begin_pass(command_buffer, "encode_setup_" + std::to_string(frame_id));
    EncoderResult result = encoder->encode(command_buffer, renderer, framebuffer, frame_layout, [this, frame_number, frame_id, transform_id](const std::span<uint8_t>& content, bool is_config)
    {
        if (is_config)
        {
            this->transport->send_frame_config_nal(frame_id, content);
        }

        else
        {
            this->transport->send_frame_nal(frame_number, frame_id, transform_id, content);
        }

        if (!this->encoder_files.empty())
        {
            this->encoder_files[frame_id].write((const char*)content.data(), content.size());
        }
    });
    pass_timer->end_pass(command_buffer);

    if (result == ENCODER_RESULT_ERROR)
    {
        lava::log()->error("Remote Headset: Error during encode submission!");
    }
}

void RemoteHeadset::submit_metadata(const FrameMetadata& metadata)
{
    this->transport->send_frame_metadata(this->frame_number, metadata);
}

VkFormat RemoteHeadset::get_format() const
{
    return VK_FORMAT_R8G8B8A8_SRGB;
}

const glm::uvec2& RemoteHeadset::get_resolution() const
{
    return this->resolution;
}

const glm::mat4& RemoteHeadset::get_view_matrix() const
{
    return this->view_matrix;
}

const glm::mat4& RemoteHeadset::get_head_to_eye_matrix(Eye eye) const
{
    return this->head_to_eye_matrices[eye];
}

const glm::mat4& RemoteHeadset::get_projection_matrix(Eye eye) const
{
    return this->projection_matrices[eye];
}

const glm::mat4& RemoteHeadset::get_controller_matrix(Controller controller) const
{
    return this->controller_matrices[controller];
}

lava::image::ptr RemoteHeadset::get_framebuffer(FrameId frame_id) const
{
    return this->framebuffers[frame_id];
}

lava::image::ptr RemoteHeadset::get_framebuffer_array() const
{
    return this->framebuffer_array;
}

const char* RemoteHeadset::get_name() const
{
    return "Remote Headset";
}

bool RemoteHeadset::is_attached(Controller controller) const
{
    return this->controller_states[controller] == CONTROLLER_STATE_ATTACHED;
}

bool RemoteHeadset::is_remote() const
{
    return true;
}

bool RemoteHeadset::create_transport()
{
    this->transport = make_transport(this->get_application()->get_command_parser().get_transport());

    this->transport->set_on_setup([this](const glm::u32vec2& resolution)
    {
        this->on_setup(resolution);
    });
    this->transport->set_on_projection_change([this](const glm::mat4& left_eye_projection, const glm::mat4& right_eye_projection, const glm::mat4& left_head_to_eye, const glm::mat4& right_head_to_eye)
    {
        this->on_projection_change(left_eye_projection, right_eye_projection, left_head_to_eye, right_head_to_eye);
    });
    this->transport->set_on_head_transform([this](TransformId transform_id, const glm::mat4& head_transform)
    {
        this->on_head_transform(transform_id, head_transform);
    });
    this->transport->set_on_controller_transform([this](Controller controller, const glm::mat4& controller_transform)
    {
        this->on_controller_transform(controller, controller_transform);
    });
    this->transport->set_on_controller_event([this](Controller controller, ControllerState controller_state)
    {
        this->on_controller_event(controller, controller_state);
    });
    this->transport->set_on_controller_button([this](Controller controller, Button button, ButtonState button_state)
    {
        this->on_controller_button(controller, button, button_state);
    });
    this->transport->set_on_controller_thumbstick([this](Controller controller, const glm::vec2& thumbstick)
    {
        this->on_controller_thumbstick(controller, thumbstick);
    });
    this->transport->set_on_performance_sample([this](std::optional<FrameNumber> frame_number, std::optional<FrameId> frame_id, std::optional<TransformId> transform_id, Unit unit, bool log_only, double sample, const std::string& name)
    {
        this->on_performance_sample(frame_number, frame_id, transform_id, unit, log_only, sample, name);
    });
    this->transport->set_on_transport_error([this]()
    {
        this->on_transport_error();
    });

    if (!this->transport->create(this->server_port))
    {
        return false;
    }

    return true;
}

bool RemoteHeadset::create_framebuffers()
{
    this->framebuffers.resize(this->frame_id_count);

    this->framebuffer_array = lava::make_image(this->get_format());
    this->framebuffer_array->set_layer_count(this->framebuffers.size());
    this->framebuffer_array->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->framebuffer_array->set_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

    if (!this->framebuffer_array->create(this->get_application()->get_device(), this->resolution))
    {
        return false;
    }

    for (uint32_t index = 0; index < this->framebuffers.size(); index++)
    {
        lava::image::ptr framebuffer = lava::make_image(this->get_format(), this->framebuffer_array->get());
        framebuffer->set_base_array_layer(index);
        framebuffer->set_view_type(VK_IMAGE_VIEW_TYPE_2D);

        if (!framebuffer->create(this->get_application()->get_device(), this->resolution))
        {
            return false;
        }

        this->framebuffers[index] = framebuffer;
    }

    return true;
}

void RemoteHeadset::destroy_framebuffers()
{
    for (lava::image::ptr& framebuffer : this->framebuffers)
    {
        framebuffer->destroy(true);
    }

    this->framebuffers.clear();

    if (this->framebuffer_array != nullptr)
    {
        this->framebuffer_array->destroy();
        this->framebuffer_array = nullptr;
    }
}

bool RemoteHeadset::create_encoders()
{
    this->encoders.resize(this->frame_id_count, nullptr);

    for (uint32_t index = 0; index < this->encoders.size(); index++)
    {
        Encoder::Ptr encoder = make_encoder(this->get_application()->get_command_parser().get_encoder());

        encoder->set_on_encode_error([this]()
        {
            this->on_encode_error();
        });

        encoder->set_mode((EncoderMode) this->encoder_mode);
        encoder->set_quality(this->encoder_quality);
        encoder->set_bitrate(this->encoder_bit_rate);
        encoder->set_key_rate(this->encoder_key_rate);
        encoder->set_frame_rate(this->encoder_frame_rate);

        EncoderCodec codec = this->get_application()->get_command_parser().get_encoder_codec();
        lava::device_ptr device = this->get_application()->get_device();
        lava::renderer& renderer = this->get_application()->get_renderer();
        
        if (!encoder->create(device, renderer, this->resolution, (EncoderFormat)this->get_format(), codec))
        {
            return false;
        }

        this->encoders[index] = encoder;
    }

    if (this->get_application()->get_command_parser().should_write_video())
    {
        if (!std::filesystem::exists(this->video_directory))
        {
            std::filesystem::create_directory(this->video_directory);
        }

        EncoderCodec codec = this->get_application()->get_command_parser().get_encoder_codec();
        std::string file_extension;

        switch(codec)
        {
        case ENCODER_CODEC_H264:
            file_extension = ".h264";
            break;
        case ENCODER_CODEC_H265:
            file_extension = ".h265";
            break;
        default:
            lava::log()->error("Invalid codec. Can't derive file extension!");
            return false;
        }

        this->encoder_files.resize(this->frame_id_count);

        for (uint32_t index = 0; index < this->encoder_files.size(); index++)
        {
            std::string file_name = this->video_directory + "/" + build_file_name(index) + file_extension;

            this->encoder_files[index].open(file_name, std::ios::out | std::ios::binary);

            if (!this->encoder_files[index].good())
            {
                lava::log()->error("Can't create file '" + file_name + "' for video capture!");

                return false;
            }
        }
    }

    return true;
}

void RemoteHeadset::destroy_encoders()
{
    for (uint32_t index = 0; index < this->encoder_files.size(); index++)
    {
        this->encoder_files[index].close();
    }

    for (uint32_t index = 0; index < this->encoders.size(); index++)
    {
        if (this->encoders[index] != nullptr)
        {
            this->encoders[index]->destroy();
        }
    }

    this->encoders.clear();
}

void RemoteHeadset::update_values()
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);

    this->head_to_eye_matrices = this->transport_head_to_eye_matrices;
    this->projection_matrices = this->transport_projection_matrices;
    this->controller_states = this->transport_controller_states;
}

void RemoteHeadset::update_bitrates()
{
    this->statistic_send_bitrate->add_sample(this->transport->get_bitrate_send());
    this->statistic_receive_bitrate->add_sample(this->transport->get_bitrate_receive());

    this->statistic_send_queue_size->add_sample(this->transport->get_send_queue_size() / 1000.0f);
    this->statistic_receive_queue_size->add_sample(this->transport->get_receive_queue_size() / 1000.0f);
}

void RemoteHeadset::update_stage(lava::delta delta_time)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);

    for (uint32_t index = 0; index < this->controller_buttons.size(); index++)
    {
        const std::array<ButtonState, BUTTON_MAX_COUNT>& buttons = this->controller_buttons[index];
        glm::vec2 thumbstick = this->controller_thumbsticks[index];

        glm::mat4 controller_transform = this->controller_transforms[index];
        glm::vec3 controller_forward = controller_transform[2];
        glm::vec3 controller_sideward = controller_transform[0];

        float strength = this->movement_speed * delta_time;

        if (buttons[BUTTON_TRIGGER] == BUTTON_STATE_PRESSED)
        {
            strength *= 2.0f;
        }

        stage_position += controller_forward * thumbstick.y * strength;
        stage_position -= controller_sideward * thumbstick.x * strength;
    }

    glm::mat4 stage_matrix = glm::translate(glm::mat4(1.0f), this->stage_position);
    this->view_matrix = this->head_matrix * stage_matrix;

    for (uint32_t index = 0; index < this->controller_matrices.size(); index++)
    {
        this->controller_matrices[index] = glm::inverse(stage_matrix) * this->controller_transforms[index];
    }

    lock.unlock();

    this->transport->send_stage_transform(stage_matrix);
}

void RemoteHeadset::update_overlay()
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);

    const std::array<ButtonState, BUTTON_MAX_COUNT>& buttons = this->controller_buttons[CONTROLLER_RIGHT];

    if (buttons[BUTTON_A] == BUTTON_STATE_PRESSED && !this->overlay_enable_pressed)
    {
        this->overlay_enable = !this->overlay_enable;
        this->overlay_enable_pressed = true;

        this->transport->send_overlay_config(this->overlay_enable);
    }

    else if (buttons[BUTTON_A] == BUTTON_STATE_RELEASED)
    {
        this->overlay_enable_pressed = false;
    }

    if (this->overlay_enable)
    {
        std::vector<Statistic::Ptr> graph_list;

        for (Statistic::Ptr statistic : this->statistic_list)
        {
            if (!statistic->is_log_only())
            {
                graph_list.push_back(statistic);
            }
        }

        if (buttons[BUTTON_B] == BUTTON_STATE_PRESSED && !this->overlay_graph_pressed)
        {
            this->overlay_graph = (this->overlay_graph + 1) % graph_list.size();
            this->overlay_graph_pressed = true;
        }

        else if (buttons[BUTTON_B] == BUTTON_STATE_RELEASED)
        {
            this->overlay_graph_pressed = false;
        }

        Statistic::Ptr active_graph = graph_list[this->overlay_graph];
        std::vector<float> samples = active_graph->get_sample_values(128);
        std::string label = active_graph->get_label_name();

        this->transport->send_overlay_graph(this->frame_number, 0, label, std::span(samples));
        this->transport->send_overlay_text(this->frame_number, 1, "Headset", this->get_name());
        this->transport->send_overlay_text(this->frame_number, 2, "Strategy", this->get_application()->get_stereo_strategy()->get_name());
    }
}

void RemoteHeadset::on_setup(const glm::u32vec2& resolution)
{
    EncoderCodec codec = this->get_application()->get_command_parser().get_encoder_codec();

    this->transport->send_setup_complete(this->remote_strategy, codec, this->frame_id_count, this->near_plane, this->far_plane);

    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->resolution = resolution;
    lock.unlock();
}

void RemoteHeadset::on_projection_change(const glm::mat4& left_eye_projection, const glm::mat4& right_eye_projection, const glm::mat4& left_head_to_eye, const glm::mat4& right_head_to_eye)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->transport_head_to_eye_matrices[EYE_LEFT] = left_head_to_eye;
    this->transport_head_to_eye_matrices[EYE_RIGHT] = right_head_to_eye;
    this->transport_projection_matrices[EYE_LEFT] = left_eye_projection;
    this->transport_projection_matrices[EYE_RIGHT] = right_eye_projection;
    lock.unlock();
}

void RemoteHeadset::on_head_transform(TransformId transform_id, const glm::mat4& head_transform)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->transform_id = transform_id;
    this->head_matrix = head_transform;
    lock.unlock();
}

void RemoteHeadset::on_controller_transform(Controller controller, const glm::mat4& controller_transform)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->controller_transforms[controller] = controller_transform;
    lock.unlock();
}

void RemoteHeadset::on_controller_event(Controller controller, ControllerState controller_state)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->transport_controller_states[controller] = controller_state;
    lock.unlock();
}

void RemoteHeadset::on_controller_button(Controller controller, Button button, ButtonState button_state)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->controller_buttons[controller][button] = button_state;
    lock.unlock();
}

void RemoteHeadset::on_controller_thumbstick(Controller controller, const glm::vec2& thumbstick)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    this->controller_thumbsticks[controller] = thumbstick;
    lock.unlock();
}

void RemoteHeadset::on_performance_sample(std::optional<FrameNumber> frame_number, std::optional<FrameId> frame_id, std::optional<TransformId> transform_id, Unit unit, bool log_only, double sample, const std::string& name)
{
    std::unique_lock<std::mutex> lock(this->transport_mutex);
    Statistic::Ptr statistic;

    for (uint32_t index = 0; index < this->statistic_list.size(); index++)
    {
        if (this->statistic_list[index]->get_name() == name)
        {
            statistic = this->statistic_list[index];

            break;
        }
    }

    if (statistic == nullptr)
    {
        statistic = make_statistic(name, unit, log_only);
        this->statistic_list.push_back(statistic);
        
        StatisticLog::Ptr statistic_log = this->get_application()->get_statistic_log();
        statistic_log->add_statistic(statistic);
    }

    StatisticSample statistic_sample;
    statistic_sample.value = sample;
    statistic_sample.frame_number = frame_number;
    statistic_sample.frame_id = frame_id;
    statistic_sample.transform_id = transform_id;

    statistic->add_sample(statistic_sample);
}

void RemoteHeadset::on_transport_error()
{
    lava::log()->error("Remote Headset: Transport error detected!");
}

void RemoteHeadset::on_encode_error()
{
    lava::log()->error("Remote Headset: Encode error detected!");
}

std::string RemoteHeadset::build_file_name(uint32_t frame_id)
{
    time_t system_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    tm local_time = *localtime(&system_time);

    std::string name = "video";
    name += std::to_string(frame_id);
    name += "_" + std::to_string(local_time.tm_mday);
    name += "-" + std::to_string(local_time.tm_mon + 1);
    name += "-" + std::to_string(local_time.tm_year + 1900);
    name += "_" + std::to_string(local_time.tm_hour);
    name += "-" + std::to_string(local_time.tm_min);
    name += "-" + std::to_string(local_time.tm_sec);

    return name;
}

bool RemoteHeadset::convert_strategy(StereoStrategyType strategy_type, RemoteStrategy& remote_strategy)
{
    switch (strategy_type)
    {
    case STEREO_STRATEGY_TYPE_NATIVE_FORWARD:
        remote_strategy = REMOTE_STRATEGY_NATIVE;
        break;
    case STEREO_STRATEGY_TYPE_NATIVE_DEFERRED:
        remote_strategy = REMOTE_STRATEGY_NATIVE;
        break;
    case STEREO_STRATEGY_TYPE_MULTI_VIEW:
        remote_strategy = REMOTE_STRATEGY_NATIVE;
        break;
    case STEREO_STRATEGY_TYPE_DEPTH_PEELING_REPROJECTION:
        remote_strategy = REMOTE_STRATEGY_NATIVE;
        break;
    default:
        lava::log()->error("Remote Headset: Unkown strategy type!");
        return false;
    }

    return true;
}