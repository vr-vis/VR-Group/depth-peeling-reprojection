#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <fstream>
#include <mutex>
#include <vector>
#include <array>
#include <memory>

#include "headset.hpp"
#include "strategy/stereo_strategy.hpp"
#include "transport/transport.hpp"
#include "encoder/encoder.hpp"

class RemoteHeadset : public Headset
{
public:
    typedef std::shared_ptr<RemoteHeadset> Ptr;

public:
    RemoteHeadset();

    bool on_setup_instance(lava::frame_config& config) override;
    bool on_setup_device(lava::device::create_param& parameters) override;

    bool on_create() override;
    void on_destroy() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;

    void submit_frame(VkCommandBuffer command_buffer, VkImageLayout frame_layout, FrameId frame_id) override;
    void submit_metadata(const FrameMetadata& metadata) override;

    VkFormat get_format() const override;
    const glm::uvec2& get_resolution() const override;
    const glm::mat4& get_view_matrix() const override;
    const glm::mat4& get_head_to_eye_matrix(Eye eye) const override;
    const glm::mat4& get_projection_matrix(Eye eye) const override;
    const glm::mat4& get_controller_matrix(Controller controller) const override;

    lava::image::ptr get_framebuffer(FrameId frame_id) const override;
    lava::image::ptr get_framebuffer_array() const override;

    const char* get_name() const override;

    bool is_attached(Controller controller) const override;
    bool is_remote() const override;

private:
    bool create_transport();

    bool create_framebuffers();
    void destroy_framebuffers();

    bool create_encoders();
    void destroy_encoders();

    void update_values();
    void update_bitrates();
    void update_stage(lava::delta delta_time);
    void update_overlay();

    void on_setup(const glm::u32vec2& resolution);
    void on_projection_change(const glm::mat4& left_eye_projection, const glm::mat4& right_eye_projection, const glm::mat4& left_head_to_eye, const glm::mat4& right_head_to_eye);
    void on_head_transform(TransformId transform_id, const glm::mat4& head_transform);
    void on_controller_transform(Controller controller, const glm::mat4& controller_transform);
    void on_controller_event(Controller controller, ControllerState controller_state);
    void on_controller_button(Controller controller, Button button, ButtonState button_state);
    void on_controller_thumbstick(Controller controller, const glm::vec2& thumbstick);
    void on_performance_sample(std::optional<FrameNumber> frame_number, std::optional<FrameId> frame_id, std::optional<TransformId> transform_id, Unit unit, bool log_only, double sample, const std::string& name);
    void on_transport_error();
    void on_encode_error();

    static std::string build_file_name(uint32_t frame_id);
    static bool convert_strategy(StereoStrategyType strategy_type, RemoteStrategy& remote_strategy);

private:
    const uint32_t server_port = 4000;
    const std::string video_directory = "video_captures";

    const float near_plane = 0.1f;
    const float far_plane = 1000.0f;
    float movement_speed = 1.0f;

    glm::vec3 stage_position = glm::vec3(0.0f);
    glm::mat4 view_matrix;
    std::array<glm::mat4, 2> head_to_eye_matrices;
    std::array<glm::mat4, 2> projection_matrices;
    std::array<glm::mat4, 2> controller_matrices;
    std::array<ControllerState, 2> controller_states;

    Transport::Ptr transport;
    std::mutex transport_mutex;

    glm::uvec2 resolution;                                                                  //NOTE: Protected by transport_mutex
    glm::mat4 head_matrix;                                                                  //NOTE: Protected by transport_mutex
    std::array<glm::mat4, 2> transport_head_to_eye_matrices;                                //NOTE: Protected by transport_mutex
    std::array<glm::mat4, 2> transport_projection_matrices;                                 //NOTE: Protected by transport_mutex

    std::array<ControllerState, 2> transport_controller_states;                             //NOTE: Protected by transport_mutex
    std::array<glm::mat4, 2> controller_transforms;                                         //NOTE: Protected by transport_mutex
    std::array<glm::vec2, 2> controller_thumbsticks;                                        //NOTE: Protected by transport_mutex
    std::array<std::array<ButtonState, BUTTON_MAX_COUNT>, 2> controller_buttons;            //NOTE: Protected by transport_mutex

    RemoteStrategy remote_strategy = REMOTE_STRATEGY_NATIVE;
    uint32_t frame_number = 0;
    uint32_t frame_id_count = 0;
    uint32_t transform_id = 0;                                                              //NOTE: Protected by transport_mutex

    lava::image::ptr framebuffer_array;
    std::vector<lava::image::ptr> framebuffers;

    std::vector<Encoder::Ptr> encoders;
    std::vector<std::fstream> encoder_files;
    uint32_t encoder_mode = ENCODER_MODE_CONSTANT_BITRATE;
    uint32_t encoder_input_rate = 90;
    uint32_t encoder_key_rate = 90;
    uint32_t encoder_frame_rate = 90;
    float encoder_quality = 0.0;
    float encoder_bit_rate = 64.0;              //NOTE: Bitrate in Mbit per seconds
    float encoder_last_submit = 0.0f;
    bool encoder_enable_submit = true;

    bool overlay_enable = false;
    bool overlay_enable_pressed = false;
    uint32_t overlay_graph = 0;
    bool overlay_graph_pressed = false;

    std::vector<Statistic::Ptr> statistic_list;                                             //NOTE: Protected by transport_mutex
    Statistic::Ptr statistic_send_bitrate;
    Statistic::Ptr statistic_send_queue_size;
    Statistic::Ptr statistic_receive_bitrate;
    Statistic::Ptr statistic_receive_queue_size;
};