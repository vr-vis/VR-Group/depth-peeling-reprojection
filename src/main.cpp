#include "vr_application.hpp"

int main(int argc, char* argv[])
{
    VRApplication application;
    
    if (!application.setup("Depth Peeling Reprojection", { argc, argv }))
    {
        return -1;
    }

    application.run();

    return 0;
}