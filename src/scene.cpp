#include "scene.hpp"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include <imgui.h>
#include <stb_image.h>
#include <gli/gli.hpp>
#include <filesystem>
#include <cmath>

bool Scene::create(const SceneConfig& config, lava::device_ptr device, uint32_t frame_count)
{
    this->create_dummy_textures(device);

    std::map<std::string, SceneNodeIndex> node_indices;

    if (!config.scene_file.empty())
    {
        if (!this->load_scene(config.scene_file, config.scene_unit, config.scene_orientation, config.scene_uvs, device, node_indices))
        {
            return false;
        }
    }

    else
    {
        lava::log()->error("Can't load scene file since no file was specified!");

        return false;
    }

    if (!config.controller_left_file.empty())
    {
        if (!this->load_controller(config.controller_left_file, config.controller_left_unit, config.controller_left_orientation, config.controller_left_uvs, device, this->controller_left, node_indices))
        {
            return false;
        }

        this->controller_left_alignment = config.controller_left_alignment;
    }

    if (!config.controller_right_file.empty())
    {
        if (!this->load_controller(config.controller_right_file, config.controller_right_unit, config.controller_right_orientation, config.controller_right_uvs, device, this->controller_right, node_indices))
        {
            return false;
        }

        this->controller_right_alignment = config.controller_right_alignment;
    }

    if (!config.sky_sphere_file.empty())
    {
        //NOTE: Need to compute scene bounds for sky-sphere
        this->reset_transforms();

        if (!this->load_sky_sphere(config.sky_sphere_file, config.sky_sphere_rings, config.sky_sphere_segments, config.sky_sphere_intensity, device))
        {
            return false;
        }
    }

    if (this->lights.empty())
    {
        this->create_default_light();
    }

    if (!this->create_descriptor_layouts(device, frame_count))
    {
        return false;
    }

    if (!this->create_material_buffer(device))
    {
        return false;
    }

    if (!this->create_light_buffer(device, frame_count))
    {
        return false;
    }

    if (!this->create_mesh_buffer(device, frame_count))
    {
        return false;
    }

    if (!this->stage_textures(device))
    {
        return false;
    }

    this->exposure = config.scene_exposure;
    this->exposure_changed = true;

    //NOTE: Recompute all transfromations for final scene bounds and matrices
    this->reset_transforms();
    
    return true;
}

void Scene::destroy()
{
    if (this->mesh_descriptor != nullptr)
    {
        for (SceneMesh& mesh : this->meshes)
        {
            for (VkDescriptorSet descriptor_set : mesh.descriptor_set)
            {
                this->mesh_descriptor->free(descriptor_set, this->descriptor_pool->get());
            }
        }

        this->mesh_descriptor->destroy();
    }

    if (this->light_descriptor != nullptr)
    {
        for (VkDescriptorSet light_descriptor_set : this->light_descriptor_set)
        {
            this->light_descriptor->free(light_descriptor_set, this->descriptor_pool->get());
        }
        
        this->light_descriptor->destroy();
    }

    if (this->material_descriptor != nullptr)
    {
        for (SceneMaterial& material : this->materials)
        {
            this->material_descriptor->free(material.descriptor_set, this->descriptor_pool->get());
        }

        this->material_descriptor->destroy();
    }

    if (this->descriptor_pool != nullptr)
    {
        this->descriptor_pool->destroy();
    }
}

bool Scene::interface()
{
    if (this->animations.empty())
    {
        ImGui::Text("No Animations Present");
    }

    else
    {
        const SceneAnimation& active_animation = this->animations[this->animation_index];

        std::vector<const char*> animation_names;
        std::vector<const char*> camera_names;

        for (const SceneAnimation& animation : this->animations)
        {
            animation_names.push_back(animation.name.c_str());
        }

        for (const SceneCamera& camera : this->cameras)
        {
            camera_names.push_back(camera.name.c_str());
        }

        ImGui::Checkbox("Animation Active", &this->animation_active);
        ImGui::Checkbox("Camera Active", &this->camera_active);
        ImGui::Checkbox("Animation Loop", &this->animation_loop);
        ImGui::SliderFloat("Animation Time", &this->animation_time, 0.0f, active_animation.duration);
        ImGui::DragFloat("Playback Speed", &this->animation_playback_speed, 1.0f, -100.0f, 100.0);

        if (ImGui::Combo("Animation", &this->animation_index, animation_names.data(), animation_names.size()))
        {
            this->reset_animation();
        }

        if (ImGui::Combo("Camera", &this->camera_index, camera_names.data(), camera_names.size()))
        {
            this->reset_animation();
        }    
    }

    ImGui::Separator();

    if (ImGui::SliderFloat("Exposure", &this->exposure, 0.01f, 10.0f))
    {
        this->exposure_changed = true;
    }

    return true;
}

bool Scene::update(lava::delta delta_time, Headset::Ptr headset)
{
    bool invalid = false;

    if (this->controller_left != INVALID_NODE)
    {
        SceneNode& node = this->nodes[this->controller_left];
        
        if (node.local_visible != headset->is_attached(CONTROLLER_LEFT))
        {
            node.local_visible = !node.local_visible;
            invalid = true;
        }

        if (node.local_visible)
        {
            node.current_local_transform = headset->get_controller_matrix(CONTROLLER_LEFT) * this->controller_left_alignment;
            invalid = true;
        }
    }

    if (this->controller_right != INVALID_NODE)
    {
        SceneNode& node = this->nodes[this->controller_right];
        
        if (node.local_visible != headset->is_attached(CONTROLLER_RIGHT))
        {
            node.local_visible = !node.local_visible;
            invalid = true;
        }

        if (node.local_visible)
        {
            node.current_local_transform = headset->get_controller_matrix(CONTROLLER_RIGHT) * this->controller_right_alignment;
            invalid = true;
        }
    }

    if (this->animation_active && !this->animations.empty())
    {
        const SceneAnimation& active_animation = this->animations[this->animation_index];
        this->animation_time += delta_time * this->animation_playback_speed;

        if (this->animation_loop)
        {
            this->animation_time = std::fmod(this->animation_time, active_animation.duration);
        }

        else
        {
            this->animation_time = std::min(this->animation_time, active_animation.duration);
        }

        for (const SceneAnimationChannel& channel : active_animation.channels)
        {
            glm::vec3 position = channel.position_frames.back().second;
            aiQuaternion rotation = channel.rotation_frames.back().second;
            glm::vec3 scaling = channel.scaling_frames.back().second;

            std::pair<float, glm::vec3> last_position_frame = channel.position_frames.front();
            for (const std::pair<float, glm::vec3>& position_frame : channel.position_frames)
            {
                if (position_frame.first > this->animation_time)
                {
                    const float fraction = (this->animation_time - last_position_frame.first) / (position_frame.first - last_position_frame.first);
                    position = glm::mix(last_position_frame.second, position_frame.second, fraction);

                    break;
                }

                last_position_frame = position_frame;
            }

            std::pair<float, aiQuaternion> last_rotation_frame = channel.rotation_frames.front();
            for (const std::pair<float, aiQuaternion>& rotation_frame : channel.rotation_frames)
            {
                if (rotation_frame.first > this->animation_time)
                {
                    const float fraction = (this->animation_time - last_rotation_frame.first) / (rotation_frame.first - last_rotation_frame.first);
                    aiQuaternion::Interpolate(rotation, last_rotation_frame.second, rotation_frame.second, fraction);

                    break;
                }

                last_rotation_frame = rotation_frame;
            }

            std::pair<float, glm::vec3> last_scaling_frame = channel.scaling_frames.front();
            for (const std::pair<float, glm::vec3>& scaling_frame : channel.scaling_frames)
            {
                if (scaling_frame.first > this->animation_time)
                {
                    const float fraction = (this->animation_time - last_scaling_frame.first) / (scaling_frame.first - last_scaling_frame.first);
                    scaling = glm::mix(last_scaling_frame.second, scaling_frame.second, fraction);

                    break;
                }

                last_scaling_frame = scaling_frame;
            }

            this->nodes[channel.node_index].current_local_transform = glm::translate(glm::mat4(1.0f), position) * glm::toMat4(glm::make_quat(&rotation.w)) * glm::scale(glm::mat4(1.0f), scaling);
        }

        invalid = true;
    }

    if (invalid)
    {
        this->apply_transforms();
    }

    return true;
}

void Scene::write(lava::index frame)
{
    glsl::MeshData* mesh_data = (glsl::MeshData*)this->mesh_data_buffer[frame]->get_mapped_data();

    for (uint32_t index = 0; index < this->meshes.size(); index++)
    {
        const SceneMesh& mesh = this->meshes[index];

        memcpy(&mesh_data[index], &mesh.data, sizeof(glsl::MeshData));
    }

    this->mesh_data_buffer[frame]->flush();

    glsl::LightData* light_data = (glsl::LightData*)this->light_data_buffer[frame]->get_mapped_data();

    for (uint32_t index = 0; index < this->lights.size(); index++)
    {
        const SceneLight& light = this->lights[index];

        memcpy(&light_data[index], &light.data, sizeof(glsl::LightData));
    }

    this->light_data_buffer[frame]->flush();

    if (this->exposure_changed)
    {
        //NOTE: The Buffer could be still in used during writing. Ignore this issure, since the exposure is not changed frequently.
        glsl::LightParameter* light_parameter = (glsl::LightParameter*)this->light_parameter_buffer->get_mapped_data();
        light_parameter->exposure = this->exposure;
        this->light_parameter_buffer->flush();

        this->exposure_changed = false;
    }
}

void Scene::reset_animation()
{
    this->animation_time = 0.0f;
    this->reset_transforms();
}

void Scene::set_animation_active(bool active)
{
    this->animation_active = active;
}

void Scene::set_animation_loop(bool loop)
{
    this->animation_loop = loop;
}

bool Scene::set_animation(const std::string& name)
{
    for (uint32_t index = 0; index < this->animations.size(); index++)
    {
        if (this->animations[index].name == name)
        {
            this->animation_index = index;
            this->reset_animation();

            return true;
        }
    }

    return false;
}

bool Scene::set_animation(uint32_t index)
{
    if (index >= this->animations.size())
    {
        return false;
    }

    this->animation_index = index;
    this->reset_animation();

    return true;
}

bool Scene::set_camera(const std::string& name)
{
    for (uint32_t index = 0; index < this->cameras.size(); index++)
    {
        if (this->cameras[index].name == name)
        {
            this->camera_index = index;
            this->reset_animation();

            return true;
        }
    }

    return false;
}

bool Scene::set_camera(uint32_t index)
{
    if (index >= this->cameras.size())
    {
        return false;
    }

    this->camera_index = index;
    this->reset_animation();

    return true;
}

const glm::mat4& Scene::get_animation_view_matrix() const
{
    return this->cameras[this->camera_index].view_matrix;
}

bool Scene::is_animation_complete() const
{
    const SceneAnimation& active_animation = this->animations[this->animation_index];

    return this->animation_time >= active_animation.duration;
}

bool Scene::is_animation_active() const
{
    return this->animation_active;
}

bool Scene::is_camera_active() const
{
    return this->camera_active && !this->cameras.empty();
}

lava::descriptor::ptr Scene::get_mesh_descriptor() const
{
    return this->mesh_descriptor;
}

lava::descriptor::ptr Scene::get_light_descriptor() const
{
    return this->light_descriptor;
}

lava::descriptor::ptr Scene::get_material_descriptor() const
{
    return this->material_descriptor;
}

VkDescriptorSet Scene::get_light_descriptor_set(lava::index frame) const
{
    return this->light_descriptor_set[frame];
}

const std::vector<SceneNode>& Scene::get_nodes() const
{
    return this->nodes;
}

const std::vector<SceneCamera>& Scene::get_cameras() const
{
    return this->cameras;
}

const std::vector<SceneMesh>& Scene::get_meshes() const
{
    return this->meshes;
}

const std::vector<SceneLight>& Scene::get_lights() const
{
    return this->lights;
}

const std::vector<SceneMaterial>& Scene::get_materials() const
{
    return this->materials;
}

const std::vector<SceneTexture>& Scene::get_textures() const
{
    return this->textures;
}

const std::vector<SceneAnimation>& Scene::get_animations() const
{
    return this->animations;
}

const glm::vec3& Scene::get_scene_min() const
{
    return this->scene_min;
}

const glm::vec3& Scene::get_scene_max() const
{
    return this->scene_max;
}

void Scene::set_vertex_input(lava::graphics_pipeline* pipeline)
{
    pipeline->set_vertex_input_binding({ 0, sizeof(lava::vertex), VK_VERTEX_INPUT_RATE_VERTEX });
    pipeline->set_vertex_input_attributes(
    {
        { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)(offsetof(lava::vertex, position)) },
        { 1, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)(offsetof(lava::vertex, normal)) },
        { 2, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)(offsetof(lava::vertex, color)) }, //Color contains actually the tangents
        { 3, 0, VK_FORMAT_R32G32_SFLOAT, (uint32_t)(offsetof(lava::vertex, uv)) },
    });
}

void Scene::set_vertex_input_only_position(lava::graphics_pipeline* pipeline)
{
    pipeline->set_vertex_input_binding({ 0, sizeof(lava::vertex), VK_VERTEX_INPUT_RATE_VERTEX });
    pipeline->set_vertex_input_attributes({ { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, (uint32_t)(offsetof(lava::vertex, position)) } });
}

void Scene::create_dummy_textures(lava::device_ptr device)
{
    this->dummy_diffuse_texture = lava::create_default_texture(device, { 1, 1 }, glm::vec3(1.0, 1.0, 1.0), 1.0);  //Diffuse: (1.0, 1.0, 1.0) Opacity: 1.0
    this->dummy_specular_texture = lava::create_default_texture(device, { 1, 1 }, glm::vec3(0.0, 1.0, 0.0), 0.0); //Occlusion: 0.0 Roughness: 1.0 Metallic: 0.0
    this->dummy_normal_texture = lava::create_default_texture(device, { 1, 1 }, glm::vec3(0.5, 0.5, 0), 0.0);     //Normal: (0.5, 0.5) -> Vertex Normal
    this->dummy_emissive_texture = lava::create_default_texture(device, { 1, 1 }, glm::vec3(0.0, 0.0, 0), 0.0);   //Emissive: (0.0, 0.0, 0.0)

    SceneTexture diffuse_texture;
    diffuse_texture.file_name = "dummy_diffuse_texture";
    diffuse_texture.texture = this->dummy_diffuse_texture;
    diffuse_texture.stage_dummy = true;

    this->textures.push_back(diffuse_texture);

    SceneTexture specular_texture;
    specular_texture.file_name = "dummy_specular_texture";
    specular_texture.texture = this->dummy_specular_texture;
    specular_texture.stage_dummy = true;

    this->textures.push_back(specular_texture);

    SceneTexture normal_texture;
    normal_texture.file_name = "dummy_normal_texture";
    normal_texture.texture = this->dummy_normal_texture;
    normal_texture.stage_dummy = true;

    this->textures.push_back(normal_texture);

    SceneTexture emissive_texture;
    emissive_texture.file_name = "dummy_emissive_texture";
    emissive_texture.texture = this->dummy_emissive_texture;
    emissive_texture.stage_dummy = true;

    this->textures.push_back(emissive_texture);
}

void Scene::create_default_light()
{
    SceneLight default_light;
    default_light.node_index = INVALID_NODE;
    default_light.initial_position = glm::vec3(0.0f);
    default_light.initial_direction = glm::normalize(glm::vec3(0.1f, -1.0f, 0.25f));

    glsl::LightData& light_data = default_light.data;
    light_data.position = default_light.initial_position;
    light_data.type = LIGHT_TYPE_DIRECTIONAL;
    light_data.color = glm::vec3(10.0f);
    light_data.outer_angle = 0.0f;
    light_data.direction = default_light.initial_direction;
    light_data.inner_angle = 0.0f;
    light_data.near_size = glm::vec2(0.0f);
    light_data.padding = 0;

    switch (light_data.type)
    {
    case LIGHT_TYPE_DIRECTIONAL:
        light_data.type_index = this->light_directional_count++;
        break;
    case LIGHT_TYPE_SPOT:
        light_data.type_index = this->light_spot_count++;
        break;
    case LIGHT_TYPE_POINT:
        light_data.type_index = this->light_point_count++;
        break;
    default:
        break;
    }

    this->lights.push_back(default_light);
}

bool Scene::create_descriptor_layouts(lava::device_ptr device, uint32_t frame_count)
{
    uint32_t descriptor_count = /*Materials*/ this->materials.size() + /*Meshes*/ this->meshes.size() * frame_count + /*Lights*/ frame_count;
    lava::VkDescriptorPoolSizes descriptor_type_count =
    {
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, /*Materials*/ (uint32_t)this->materials.size() * 4 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, /*Meshes*/ (uint32_t)this->meshes.size() * frame_count + /*Lights*/ frame_count },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, /*Lights*/ frame_count },
    };

    this->descriptor_pool = lava::make_descriptor_pool();

    if (!this->descriptor_pool->create(device, descriptor_type_count, descriptor_count))
    {
        lava::log()->error("Can't create descriptor pool for scene!");

        return false;
    }

    this->mesh_descriptor = lava::make_descriptor();
    this->mesh_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);

    if (!this->mesh_descriptor->create(device))
    {
        lava::log()->error("Can't create mesh descriptor for scene!");

        return false;
    }

    this->light_descriptor = lava::make_descriptor();
    this->light_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
    this->light_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);

    if (!this->light_descriptor->create(device))
    {
        lava::log()->error("Can't create light descriptor for scene!");

        return false;
    }

    this->material_descriptor = lava::make_descriptor();
    this->material_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    this->material_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    this->material_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    this->material_descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    this->material_descriptor->add_binding(4, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT);

    if (!this->material_descriptor->create(device))
    {
        lava::log()->error("Can't create material descriptor for scene!");

        return false;
    }

    return true;
}

bool Scene::create_material_buffer(lava::device_ptr device)
{
    std::vector<glsl::MaterialData> material_data;
    material_data.reserve(this->materials.size());

    for (const SceneMaterial& material : this->materials)
    {
        material_data.push_back(material.material_data);
    }

    this->material_data_buffer = lava::make_buffer();

    if (!this->material_data_buffer->create_mapped(device, material_data.data(), sizeof(glsl::MaterialData) * material_data.size(), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT))
    {
        lava::log()->error("Can't create material data buffer!");

        return false;
    }

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.reserve(this->materials.size() * 5);

    std::vector<VkDescriptorBufferInfo> buffer_infos;
    buffer_infos.reserve(this->materials.size());

    for (uint32_t index = 0; index < this->materials.size(); index++)
    {
        SceneMaterial& material = this->materials[index];
        material.descriptor_set = this->material_descriptor->allocate_set(this->descriptor_pool->get());

        std::vector<lava::texture::ptr> textures;
        textures.push_back(material.diffuse);
        textures.push_back(material.specular);
        textures.push_back(material.normal);
        textures.push_back(material.emissive);

        for (uint32_t binding = 0; binding < textures.size(); binding++)
        {
            VkWriteDescriptorSet& texture_write = descriptor_writes.emplace_back();
            texture_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            texture_write.pNext = nullptr;
            texture_write.dstSet = material.descriptor_set;
            texture_write.dstBinding = binding;
            texture_write.dstArrayElement = 0;
            texture_write.descriptorCount = 1;
            texture_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            texture_write.pImageInfo = textures[binding]->get_descriptor_info();
            texture_write.pBufferInfo = nullptr;
            texture_write.pTexelBufferView = nullptr;
        }

        VkDescriptorBufferInfo& buffer_info = buffer_infos.emplace_back();
        buffer_info.buffer = this->material_data_buffer->get();
        buffer_info.offset = sizeof(glsl::MaterialData) * index;
        buffer_info.range = sizeof(glsl::MaterialData);

        VkWriteDescriptorSet& buffer_write = descriptor_writes.emplace_back();
        buffer_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        buffer_write.pNext = nullptr;
        buffer_write.dstSet = material.descriptor_set;
        buffer_write.dstBinding = 4;
        buffer_write.dstArrayElement = 0;
        buffer_write.descriptorCount = 1;
        buffer_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        buffer_write.pImageInfo = nullptr;
        buffer_write.pBufferInfo = &buffer_info;
        buffer_write.pTexelBufferView = nullptr;
    }

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool Scene::create_light_buffer(lava::device_ptr device, uint32_t frame_count)
{
    glsl::LightParameter light_parameter;
    light_parameter.ambient = this->ambient_color;
    light_parameter.light_count = this->lights.size();
    light_parameter.exposure = this->exposure;

    this->light_parameter_buffer = lava::make_buffer();

    if (!this->light_parameter_buffer->create_mapped(device, &light_parameter, sizeof(glsl::LightParameter), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT))
    {
        lava::log()->error("Can't create light parameter buffer for scene!");

        return false;
    }

    this->light_data_buffer.resize(frame_count);
    this->light_descriptor_set.resize(frame_count);

    for (uint32_t index = 0; index < frame_count; index++)
    {
        lava::buffer::ptr buffer = lava::make_buffer();

        if (!buffer->create_mapped(device, nullptr, this->lights.size() * sizeof(glsl::LightData), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT))
        {
            lava::log()->error("Can't create light data buffer for scene!");

            return false;
        }

        VkDescriptorSet descriptor_set = this->light_descriptor->allocate(this->descriptor_pool->get());

        this->light_data_buffer[index] = buffer;
        this->light_descriptor_set[index] = descriptor_set;
    }

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.reserve(frame_count * 2);

    for (uint32_t index = 0; index < frame_count; index++)
    {
        VkWriteDescriptorSet parameter_write;
        parameter_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        parameter_write.pNext = nullptr;
        parameter_write.dstSet = this->light_descriptor_set[index];
        parameter_write.dstBinding = 0;
        parameter_write.dstArrayElement = 0;
        parameter_write.descriptorCount = 1;
        parameter_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        parameter_write.pImageInfo = nullptr;
        parameter_write.pBufferInfo = this->light_parameter_buffer->get_descriptor_info();
        parameter_write.pTexelBufferView = nullptr;

        descriptor_writes.push_back(parameter_write);

        VkWriteDescriptorSet data_write;
        data_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        data_write.pNext = nullptr;
        data_write.dstSet = this->light_descriptor_set[index];
        data_write.dstBinding = 1;
        data_write.dstArrayElement = 0;
        data_write.descriptorCount = 1;
        data_write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        data_write.pImageInfo = nullptr;
        data_write.pBufferInfo = this->light_data_buffer[index]->get_descriptor_info();
        data_write.pTexelBufferView = nullptr;

        descriptor_writes.push_back(data_write);
    }

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool Scene::create_mesh_buffer(lava::device_ptr device, uint32_t frame_count)
{
    this->mesh_data_buffer.resize(frame_count);

    for (lava::buffer::ptr& buffer : this->mesh_data_buffer)
    {
        buffer = lava::make_buffer();

        if (!buffer->create_mapped(device, nullptr, this->meshes.size() * sizeof(glsl::MeshData), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT))
        {
            lava::log()->error("Failed to create mesh data buffer!");

            return false;
        }
    }

    std::vector<VkDescriptorBufferInfo> buffer_infos;
    buffer_infos.reserve(this->meshes.size() * frame_count);
    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.reserve(this->meshes.size() * frame_count);

    for (uint32_t index = 0; index < this->meshes.size(); index++)
    {
        SceneMesh& mesh = this->meshes[index];
        mesh.descriptor_set.resize(frame_count);

        for (uint32_t frame = 0; frame < frame_count; frame++)
        {
            VkDescriptorSet& descriptor_set = mesh.descriptor_set[frame];
            descriptor_set = this->mesh_descriptor->allocate_set(this->descriptor_pool->get());

            lava::buffer::ptr buffer = this->mesh_data_buffer[frame];

            VkDescriptorBufferInfo buffer_info;
            buffer_info.buffer = buffer->get();
            buffer_info.offset = index * sizeof(glsl::MeshData);
            buffer_info.range = sizeof(glsl::MeshData);

            buffer_infos.push_back(buffer_info);

            VkWriteDescriptorSet descriptor_write;
            descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptor_write.pNext = nullptr;
            descriptor_write.dstSet = descriptor_set;
            descriptor_write.dstBinding = 0;
            descriptor_write.dstArrayElement = 0;
            descriptor_write.descriptorCount = 1;
            descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            descriptor_write.pImageInfo = nullptr;
            descriptor_write.pBufferInfo = &buffer_infos.back();
            descriptor_write.pTexelBufferView = nullptr;

            descriptor_writes.push_back(descriptor_write);
        }
    }

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool Scene::load_scene(const std::string& file_name, SceneUnit unit, SceneOrientation orientation, SceneUVs uvs, lava::device_ptr device, std::map<std::string, SceneNodeIndex>& node_indices)
{
    uint32_t post_processes = 0;
    float scale = 1.0f;

    if (!Scene::setup_post_processes(orientation, uvs, post_processes))
    {
        return false;
    }

    if (!Scene::compute_scale(unit, scale))
    {
        return false;
    }

    Assimp::Importer importer;
    
    lava::log()->info("Loading scene file '{}'", file_name.c_str());
    const aiScene* scene = importer.ReadFile(file_name, post_processes);

    if (scene == nullptr)
    {
        lava::log()->error("Failed to load scene file '{}': {}", file_name, importer.GetErrorString());
        
        return false;
    }

    this->cameras.reserve(scene->mNumCameras);
    this->meshes.reserve(scene->mNumMeshes);
    this->lights.reserve(scene->mNumLights);
    this->materials.reserve(scene->mNumMaterials);
    this->animations.reserve(scene->mNumAnimations);

    if (!this->load_materials(scene, file_name, device))
    {
        return false;
    }

    if (!this->load_meshes(scene, scale, 0, true, device))
    {
        return false;
    }

    if (!this->load_nodes(scene->mRootNode, scale, 0, INVALID_NODE, node_indices))
    {
        return false;
    }

    if (!this->load_cameras(scene, scale, node_indices))
    {
        return false;
    }

    if (!this->load_animations(scene, scale, node_indices))
    {
        return false;
    }

    if (!this->load_lights(scene, scale, node_indices))
    {
        return false;
    }

    return true;
}

bool Scene::load_controller(const std::string& file_name, SceneUnit unit, SceneOrientation orientation, SceneUVs uvs, lava::device_ptr device, SceneNodeIndex& node_index, std::map<std::string, SceneNodeIndex>& node_indices)
{
    uint32_t post_processes = 0;
    float scale = 1.0f;

    if (!Scene::setup_post_processes(orientation, uvs, post_processes))
    {
        return false;
    }

    if (!Scene::compute_scale(unit, scale))
    {
        return false;
    }

    Assimp::Importer importer;

    lava::log()->info("Loading controller file '{}'", file_name.c_str());
    const aiScene* scene = importer.ReadFile(file_name, post_processes);

    if (scene == nullptr)
    {
        lava::log()->error("Failed to load controller file '{}': {}", file_name, importer.GetErrorString());
        
        return false;
    }

    uint32_t base_mesh = this->meshes.size();
    uint32_t base_material = this->materials.size();

    this->meshes.reserve(scene->mNumMeshes);
    this->materials.reserve(scene->mNumMaterials);

    if (!this->load_materials(scene, file_name, device))
    {
        return false;
    }

    if (!this->load_meshes(scene, scale, base_material, false, device))
    {
        return false;
    }

    if (!this->load_nodes(scene->mRootNode, scale, base_mesh, 0, node_indices))
    {
        return false;
    }

    node_index = node_indices.at(scene->mRootNode->mName.C_Str());

    return true;
}

bool Scene::load_sky_sphere(const std::string& file_name, uint32_t ring_count, uint32_t segment_count, float intensity, lava::device_ptr device)
{
    lava::log()->info("Loading sky-sphere file '{}'", file_name.c_str());

    if (!this->load_texture(file_name, device, true, false, false, this->sky_emissive_texture))
    {
        lava::log()->error("Faild to load sky-sphere '{}'", file_name.c_str());

        return false;
    }

    uint32_t sky_material_index = this->materials.size();
    SceneMaterial& sky_material = this->materials.emplace_back();
    sky_material.diffuse = this->dummy_diffuse_texture;    
    sky_material.specular = this->dummy_specular_texture;
    sky_material.normal = this->dummy_normal_texture;
    sky_material.emissive = this->sky_emissive_texture;
    
    glsl::MaterialData& sky_material_data = sky_material.material_data;
    sky_material_data.base_color = glm::vec3(1.0f);
    sky_material_data.opacity = 1.0f;
    sky_material_data.emissive = glm::vec3(intensity);
    sky_material_data.roughness = 1.0f;
    sky_material_data.padding1 = glm::uvec3(0);
    sky_material_data.metallic = 1.0f;
    sky_material_data.padding2 = glm::uvec4(0);
    sky_material_data.padding3 = glm::mat4(0.0f);
    sky_material_data.padding4 = glm::mat4(0.0f);
    sky_material_data.padding5 = glm::mat4(0.0f);

    if (ring_count < 3)
    {
        lava::log()->error("Invalid number of rings for sky-sphere. The number of rings needs to be at least 3!");

        return false;
    }

    if (segment_count < 3)
    {
        lava::log()->error("Invalid number of segments for sky-sphere. The number of segments needs to be at least 3!");

        return false;
    }

    glm::vec3 center = (this->scene_min + this->scene_max) / 2.0f;
    float radius = glm::distance(center, this->scene_max);

    uint32_t vertex_count = ring_count * segment_count;
    uint32_t index_count = (ring_count - 1) * segment_count * 2;
    
    lava::mesh_data geometry;
    geometry.vertices.reserve(vertex_count);
    geometry.indices.reserve(index_count);

    for (uint32_t ring = 0; ring < ring_count; ring++)
    {
        float coord_v = (float)ring / (float)(ring_count - 1);
        float latitude = glm::pi<float>() * coord_v;

        for (uint32_t segment = 0; segment < segment_count; segment++)
        {
            float coord_u = (float)segment / (float)(segment_count - 1);
            float longitude = glm::two_pi<float>() * coord_u;

            glm::vec3 position = center;
            position.x += radius * glm::cos(longitude) * glm::sin(latitude);
            position.y -= radius * glm::cos(latitude);
            position.z += radius * glm::sin(longitude) * glm::sin(latitude);

            glm::vec2 uv = glm::vec2(coord_u, 1.0 - coord_v);
            glm::vec3 normal = glm::normalize(center - position);
            glm::vec3 tangent = glm::vec3(1.0f, 0.0f, 0.0f);
          
            if (glm::abs(glm::dot(normal, glm::vec3(0.0f, 1.0f, 0.0f))) < 0.99f)
            {
                tangent = glm::normalize(glm::cross(normal, glm::vec3(0.0f, 1.0f, 0.0f)));
            }

            lava::vertex vertex;
            vertex.position = position;
            vertex.normal = normal;
            vertex.color = glm::vec4(tangent, 0.0f);
            vertex.uv = uv;

            geometry.vertices.push_back(vertex);
        }
    }

    for (uint32_t ring = 0; ring < (ring_count - 1); ring++)
    {
        uint32_t lower_ring_offset = ring * segment_count;
        uint32_t upper_ring_offset = lower_ring_offset + segment_count;

        for (uint32_t segment = 0; segment < segment_count; segment++)
        {
            uint32_t current_index = segment;
            uint32_t next_index = (segment + 1) % segment_count;

            geometry.indices.push_back(lower_ring_offset + current_index);
            geometry.indices.push_back(lower_ring_offset + next_index);
            geometry.indices.push_back(upper_ring_offset + next_index);

            geometry.indices.push_back(upper_ring_offset + next_index);
            geometry.indices.push_back(upper_ring_offset + current_index);
            geometry.indices.push_back(lower_ring_offset + current_index);
        }
    }

    SceneMesh& sky_mesh = this->meshes.emplace_back();
    sky_mesh.node_index = INVALID_NODE;
    sky_mesh.visible = true;
    sky_mesh.cast_shadow = false;
    sky_mesh.scene_bounds = false;
    sky_mesh.material_index = sky_material_index;
    sky_mesh.local_min = center - glm::vec3(radius);
    sky_mesh.local_max = center + glm::vec3(radius);
    sky_mesh.mesh = lava::make_mesh();
    sky_mesh.mesh->add_data(geometry);

    if (!sky_mesh.mesh->create(device))
    {
        lava::log()->error("Can't create mesh for sky-sphere!");

        return false;
    }

    return true;
}

bool Scene::load_nodes(const aiNode* node, float scale, uint32_t base_mesh, SceneNodeIndex parent_index, std::map<std::string, SceneNodeIndex>& node_indices)
{
    SceneNode scene_node;
    scene_node.parent_index = parent_index;
    scene_node.initial_local_transform = glm::transpose(glm::make_mat4(&node->mTransformation[0][0]));
    scene_node.initial_local_transform[3] *= glm::vec4(glm::vec3(scale), 1.0f);
    
    uint32_t node_index = this->nodes.size();
    this->nodes.push_back(scene_node);

    node_indices[node->mName.C_Str()] = node_index;

    for (uint32_t index = 0; index < node->mNumMeshes; index++)
    {
        uint32_t mesh_index = base_mesh + node->mMeshes[index];
        this->meshes[mesh_index].node_index = node_index;
    }

    for (uint32_t index = 0; index < node->mNumChildren; index++)
    {
        if (!this->load_nodes(node->mChildren[index], scale, base_mesh, node_index, node_indices))
        {
            return false;
        }
    }

    return true;
}

bool Scene::load_cameras(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices)
{
    for (uint32_t index = 0; index < scene->mNumCameras; index++)
    {
        const aiCamera* camera = scene->mCameras[index];

        if (!node_indices.contains(camera->mName.C_Str()))
        {
            lava::log()->error("Can not attach camera to node. Unknown node '{}' !", camera->mName.C_Str());

            return false;
        }
        
        SceneCamera& scene_camera = this->cameras.emplace_back();
        scene_camera.node_index = node_indices.at(camera->mName.C_Str());
        scene_camera.name = camera->mName.C_Str();
        scene_camera.projection_matrix = glm::perspective(camera->mHorizontalFOV, camera->mAspect, camera->mClipPlaneNear * scale, camera->mClipPlaneFar * scale);
        scene_camera.local_position = glm::make_vec3(&camera->mPosition.x);
        scene_camera.local_look_at = glm::make_vec3(&camera->mLookAt.x);
        scene_camera.local_up = glm::make_vec3(&camera->mUp.x);
        scene_camera.near_plane = camera->mClipPlaneNear * scale;
        scene_camera.far_plane = camera->mClipPlaneFar * scale;
    }

    return true;
}

bool Scene::load_animations(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices) 
{
    for (uint32_t index = 0; index < scene->mNumAnimations; index++)
    {
        const aiAnimation* animation = scene->mAnimations[index];
        SceneAnimation& scene_animation = this->animations.emplace_back();

        scene_animation.name = animation->mName.C_Str();
        scene_animation.duration = animation->mDuration / animation->mTicksPerSecond;
        scene_animation.channels.resize(animation->mNumChannels);

        for (uint32_t channel_index = 0; channel_index < animation->mNumChannels; channel_index++)
        {
            const aiNodeAnim* channel = animation->mChannels[channel_index];
            SceneAnimationChannel& scene_channel = scene_animation.channels[channel_index];

            if (!node_indices.contains(channel->mNodeName.C_Str()))
            {
                lava::log()->error("Can not attach animation channel to node. Unknown node '{}' !", channel->mNodeName.C_Str());

                return false;
            }

            scene_channel.node_index = node_indices.at(channel->mNodeName.C_Str());
            scene_channel.position_frames.resize(channel->mNumPositionKeys);
            scene_channel.rotation_frames.resize(channel->mNumRotationKeys);
            scene_channel.scaling_frames.resize(channel->mNumScalingKeys);

            for (uint32_t frame_index = 0; frame_index < channel->mNumPositionKeys; frame_index++)
            {
                const aiVectorKey key = channel->mPositionKeys[frame_index];
                scene_channel.position_frames[frame_index] = std::make_pair((float)key.mTime / animation->mTicksPerSecond, glm::make_vec3(&key.mValue.x) * scale);
            }

            for (uint32_t frame_index = 0; frame_index < channel->mNumRotationKeys; frame_index++)
            {
                const aiQuatKey key = channel->mRotationKeys[frame_index];
                scene_channel.rotation_frames[frame_index] = std::make_pair((float)key.mTime / animation->mTicksPerSecond, key.mValue);
            }

            for (uint32_t frame_index = 0; frame_index < channel->mNumScalingKeys; frame_index++)
            {
                const aiVectorKey key = channel->mScalingKeys[frame_index];
                scene_channel.scaling_frames[frame_index] = std::make_pair((float)key.mTime / animation->mTicksPerSecond, glm::make_vec3(&key.mValue.x));
            }
        }
    }

    return true;
}

bool Scene::load_lights(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices)
{
    for (uint32_t index = 0; index < scene->mNumLights; index++)
    {
        const aiLight* light = scene->mLights[index];
       
        if (!node_indices.contains(light->mName.C_Str()))
        {
            lava::log()->error("Can not attach light to node. Unknown node '{}' !", light->mName.C_Str());

            return false;
        }

        glm::vec3 color = glm::make_vec3(&light->mColorDiffuse.r);
        glm::vec3 position = glm::make_vec3(&light->mPosition.x);
        glm::vec3 direction = glm::make_vec3(&light->mDirection.x);
        float direction_length = glm::length(direction);

        if (direction_length > 0.0f)
        {
            direction /= direction_length;            
        }

        SceneLight& scene_light = this->lights.emplace_back();
        scene_light.node_index = node_indices.at(light->mName.C_Str());
        scene_light.initial_position = position * scale;
        scene_light.initial_direction = direction;

        glsl::LightData& light_data = scene_light.data;
        light_data.position = scene_light.initial_position;
        light_data.color = color;
        light_data.outer_angle = light->mAngleOuterCone;
        light_data.direction = scene_light.initial_direction;
        light_data.inner_angle = light->mAngleInnerCone;
        light_data.near_size = glm::vec2(0.0f);
        light_data.padding = 0;
        
        switch (light->mType)
        {
        case aiLightSource_POINT:
            light_data.type = LIGHT_TYPE_POINT;
            light_data.type_index = this->light_point_count++;
            break;
        case aiLightSource_DIRECTIONAL:
            light_data.type = LIGHT_TYPE_DIRECTIONAL;
            light_data.type_index = this->light_directional_count++;
            break;
        case aiLightSource_SPOT:
            light_data.type = LIGHT_TYPE_SPOT;
            light_data.type_index = this->light_spot_count++;
            break;
        default:
            continue;
        }
    }

    return true;
}

bool Scene::load_meshes(const aiScene* scene, float scale, uint32_t base_material, bool scene_bounds, lava::device_ptr device)
{
    for (uint32_t index = 0; index < scene->mNumMeshes; index++)
    {
        const aiMesh* mesh = scene->mMeshes[index];
        
        lava::mesh_data geometry;
        geometry.vertices.resize(mesh->mNumVertices);
        geometry.indices.reserve(mesh->mNumFaces * 3);

        for (uint32_t vertex_index = 0; vertex_index < mesh->mNumVertices; vertex_index++)
        {
            geometry.vertices[vertex_index].position = glm::make_vec3(&mesh->mVertices[vertex_index].x) * scale;

            if (mesh->HasNormals())
            {
                geometry.vertices[vertex_index].normal = glm::make_vec3(&mesh->mNormals[vertex_index].x);
            }

            if (mesh->HasTangentsAndBitangents())
            {
                geometry.vertices[vertex_index].color = glm::vec4(glm::make_vec3(&mesh->mTangents[vertex_index].x), 0.0f);
            }

            if (mesh->HasTextureCoords(0))
            {
                geometry.vertices[vertex_index].uv = glm::vec2(glm::make_vec3(&mesh->mTextureCoords[0][vertex_index].x));
            }
        }

        for (uint32_t face_index = 0; face_index < mesh->mNumFaces; face_index++)
        {
            const aiFace& face = mesh->mFaces[face_index];

            if (face.mNumIndices != 3)
            {
                break;
            }

            geometry.indices.push_back(face.mIndices[0]);
            geometry.indices.push_back(face.mIndices[1]);
            geometry.indices.push_back(face.mIndices[2]);
        }

        if (geometry.indices.size() == 0)
        {
            continue;
        }

        SceneMesh& scene_mesh = this->meshes.emplace_back();
        scene_mesh.scene_bounds = scene_bounds;
        scene_mesh.material_index = base_material + mesh->mMaterialIndex;
        scene_mesh.local_min = glm::make_vec3(&mesh->mAABB.mMin.x) * scale;
        scene_mesh.local_max = glm::make_vec3(&mesh->mAABB.mMax.x) * scale;
        scene_mesh.mesh = lava::make_mesh();
        scene_mesh.mesh->add_data(geometry);

        if (!scene_mesh.mesh->create(device))
        {
            lava::log()->error("Can't create mesh for scene mesh!");

            return false;
        }
    }

    return true;
}

bool Scene::load_materials(const aiScene* scene, const std::string& base_name, lava::device_ptr device)
{
    for (uint32_t index = 0; index < scene->mNumMaterials; index++)
    {
        const aiMaterial* material = scene->mMaterials[index];
        SceneMaterial& scene_material = this->materials.emplace_back();

        aiColor3D diffuse_color = aiColor3D(0.0f, 0.0f, 0.0f);
        material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse_color);

        aiColor3D emissive_color = aiColor3D(0.0f, 0.0f, 0.0f);
        material->Get(AI_MATKEY_COLOR_EMISSIVE, emissive_color);

        float opacity = 1.0f;
        material->Get(AI_MATKEY_OPACITY, opacity);

        float shininess = 0.0f;
        material->Get(AI_MATKEY_SHININESS, shininess);

        float reflectivity = 0.0f;
        material->Get(AI_MATKEY_REFLECTIVITY, reflectivity);

        glsl::MaterialData& material_data = scene_material.material_data;
        material_data.base_color = glm::vec3(1.0f);
        material_data.opacity = 1.0f;
        material_data.emissive = glm::vec3(1.0f);
        material_data.roughness = 1.0f;
        material_data.padding1 = glm::uvec3(0);
        material_data.metallic = 1.0f;
        material_data.padding2 = glm::uvec4(0);
        material_data.padding3 = glm::mat4(0);
        material_data.padding4 = glm::mat4(0);
        material_data.padding5 = glm::mat4(0);

        if (!this->load_texture(material, base_name, aiTextureType_DIFFUSE, true, device, scene_material.diffuse))
        {
            scene_material.diffuse = this->dummy_diffuse_texture;

            material_data.base_color = glm::make_vec3(&diffuse_color.r);
            material_data.opacity = glm::clamp(opacity, 0.0f, 1.0f);
        }

        if (!this->load_texture(material, base_name, aiTextureType_SPECULAR, false, device, scene_material.specular))
        {
            scene_material.specular = this->dummy_specular_texture;

            material_data.roughness = glm::clamp(1.0f - (glm::sqrt(shininess) / 10.0f), 0.0f, 1.0f);
            material_data.metallic = glm::clamp(reflectivity, 0.0f, 1.0f);
        }

        if (!this->load_texture(material, base_name, aiTextureType_NORMALS, false, device, scene_material.normal))
        {
            scene_material.normal = this->dummy_normal_texture;
        }

        if (!this->load_texture(material, base_name, aiTextureType_EMISSIVE, false, device, scene_material.emissive))
        {
            scene_material.emissive = this->dummy_emissive_texture;

            material_data.emissive = glm::make_vec3(&emissive_color.r);
        }
    }

    return true;
}

bool Scene::load_texture(const aiMaterial* material, const std::string& base_name, aiTextureType type, bool use_srgb, lava::device_ptr device, lava::texture::ptr& texture)
{
    aiString texture_path;

    if (material->GetTexture(type, 0, &texture_path) != aiReturn_SUCCESS)
    {
        lava::log()->warn("Can't get texture path for material '{}'. Using dummy texture instead.", material->GetName().C_Str());

        return false;
    }

    if (texture_path.length <= 0)
    {
        lava::log()->warn("Invalid texture path for material '{}'. Using dummy texture instead.", material->GetName().C_Str());

        return false;
    }

    std::filesystem::path base_path = std::filesystem::path(base_name).parent_path();
    std::string file_name = (base_path / texture_path.C_Str()).string();
    std::replace(file_name.begin(), file_name.end(), '\\', '/');
    
    if (!this->load_texture(file_name, device, false, true, use_srgb, texture))
    {
        lava::log()->warn("Faild to load texture file '{}'. Using dummy texture instead.", file_name.c_str());

        return false;
    }

    return true;
}

bool Scene::load_texture(const std::string& file_name, lava::device_ptr device, bool use_float, bool use_mipmaps, bool use_srgb, lava::texture::ptr& texture)
{
    for (const SceneTexture& scene_texture : this->textures)
    {
        if (scene_texture.file_name == file_name)
        {
            texture = scene_texture.texture;

            return true;
        }
    }
    
    lava::log()->info("Loading texture file '{}'", file_name.c_str());

    lava::file file(file_name.c_str());

    if (!file.opened())
    {
        lava::log()->error("Can't open texture: " + file_name + " !");

        return false;
    }

    lava::unique_data image_data(file.get_size(), false);

    if (!image_data.allocate())
    {
        lava::log()->error("Can't allocate memory for texture: " + file_name + " !");

        return false;
    }

    if (lava::file_error(file.read(image_data.ptr)))
    {
        lava::log()->error("Can't read texture: " + file_name + " !");

        return false;
    }

    VkFormat format = VK_FORMAT_UNDEFINED;
    int32_t width = 0;
    int32_t height = 0;
    lava::texture::layer layer;

    lava::buffer::ptr stage_buffer = lava::make_buffer();
    uint32_t stage_levels = 0;

    bool use_gli = lava::extension(file_name.c_str(), { "DDS", "KTX", "KMG" });
    bool use_stbi = lava::extension(file_name.c_str(), { "JPG", "PNG", "TGA", "BMP", "PSD", "GIF", "HDR", "PIC" });

    if (use_gli)
    {
        gli::texture texture = gli::load(image_data.ptr, image_data.size);

        if (texture.empty())
        {
            lava::log()->error("Can't load texture: " + file_name + " !");

            return false;
        }

        switch (texture.format())
        {
        case gli::FORMAT_RGBA_DXT1_SRGB_BLOCK8:
        case gli::FORMAT_RGBA_DXT1_UNORM_BLOCK8:
            if (use_srgb)
            {
                format = VK_FORMAT_BC1_RGBA_SRGB_BLOCK;
            }

            else
            {
                format = VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
            }
            break;
        case gli::FORMAT_RGBA_DXT5_SRGB_BLOCK16:
        case gli::FORMAT_RGBA_DXT5_UNORM_BLOCK16:
            if (use_srgb)
            {
                format = VK_FORMAT_BC3_SRGB_BLOCK;
            }

            else
            {
                format = VK_FORMAT_BC3_UNORM_BLOCK;
            }
            break;
        case gli::FORMAT_RG_ATI2N_UNORM_BLOCK16:
            format = VK_FORMAT_BC5_UNORM_BLOCK;
            break;
        default:
            lava::log()->error("Unkown texture format. Can't load texture: " + file_name + " !");
            return false;
        }

        width = texture.extent().x;
        height = texture.extent().y;
        stage_levels = (use_mipmaps) ? texture.levels() : 1;

        uint32_t data_size = 0;

        for (uint32_t index = 0; index < stage_levels; index++)
        {
            lava::texture::mip_level level;
            level.extent = texture.extent(index);
            level.size = texture.size(index);

            layer.levels.push_back(level);
            data_size += level.size;
        }

        if (!stage_buffer->create_mapped(device, texture.data(), data_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY))
        {
            lava::log()->error("Can't create staging buffer for texture: " + file_name + " !");
            
            return false;
        }
    }

    else if (use_stbi)
    {
        uint8_t* data_pointer = nullptr;
        uint32_t data_bitdepth = 0;

        if (use_float)
        {
            format = VK_FORMAT_R32G32B32A32_SFLOAT;

            data_pointer = (uint8_t*) stbi_loadf_from_memory((const stbi_uc*) image_data.ptr, image_data.size, &width, &height, nullptr, STBI_rgb_alpha);
            data_bitdepth = 4 * sizeof(float);
        }

        else
        {
            if (use_srgb)
            {
                format = VK_FORMAT_R8G8B8A8_SRGB;
            }

            else
            {
                format = VK_FORMAT_R8G8B8A8_UNORM;
            }

            data_pointer = (uint8_t*) stbi_load_from_memory((const stbi_uc*) image_data.ptr, image_data.size, &width, &height, nullptr, STBI_rgb_alpha);
            data_bitdepth = 4 * sizeof(uint8_t);
        }

        if (data_pointer == nullptr)
        {
            lava::log()->error("Can't load texture: " + file_name + " !");

            return false;
        }

        uint32_t data_size = width * height * data_bitdepth;
        stage_levels = 1;

        if (!stage_buffer->create_mapped(device, data_pointer, data_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY))
        {
            lava::log()->error("Can't create staging buffer for texture: " + file_name + " !");
            stbi_image_free(data_pointer);

            return false;
        }

        stbi_image_free(data_pointer);

        if (use_mipmaps)
        {
            uint32_t mipmap_width = width;
            uint32_t mipmap_height = height;

            while (mipmap_width > 1 || mipmap_width > 1)
            {
                lava::texture::mip_level level;
                level.extent = glm::uvec2(mipmap_width, mipmap_height);
                level.size = mipmap_width * mipmap_height * data_bitdepth;

                layer.levels.push_back(level);
                
                mipmap_width = glm::max((uint32_t)1, mipmap_width >> 1);
                mipmap_height = glm::max((uint32_t)1, mipmap_height >> 1);
            }
        }

        else
        {
            lava::texture::mip_level level;
            level.extent = glm::uvec2(width, height);
            level.size = width * height * data_bitdepth;

            layer.levels.push_back(level);
        }
    }

    else
    {
        lava::log()->error("Unsupported texture format. Can't load texture: " + file_name + " !");

        return false;
    }

    texture = lava::make_texture();

    if (!texture->create(device, glm::uvec2(width, height), format, {layer}, lava::texture_type::tex_2d))
    {
        lava::log()->error("Can't create texture: " + file_name + " !");

        return false;
    }

    SceneTexture scene_texture;
    scene_texture.file_name = file_name;
    scene_texture.texture = texture;
    scene_texture.stage_levels = stage_levels;
    scene_texture.stage_buffer = stage_buffer;

    this->textures.push_back(scene_texture);

    return true;
}

bool Scene::stage_textures(lava::device_ptr device)
{
    VkCommandPoolCreateInfo pool_info;
    pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pool_info.pNext = nullptr;
    pool_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    pool_info.queueFamilyIndex = device->get_graphics_queue().family;

    VkCommandPool command_pool = VK_NULL_HANDLE;

    if (vkCreateCommandPool(device->get(), &pool_info, lava::memory::alloc(), &command_pool) != VK_SUCCESS)
    {
        lava::log()->error("Can't create command pool for texture staging!");

        return false;
    }

    VkCommandBufferAllocateInfo allocate_info;
    allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocate_info.pNext = nullptr;
    allocate_info.commandPool = command_pool;
    allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocate_info.commandBufferCount = 1;

    VkCommandBuffer command_buffer = VK_NULL_HANDLE;

    if (vkAllocateCommandBuffers(device->get(), &allocate_info, &command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't create command buffer for texture staging!");

        return false;
    }

    VkCommandBufferBeginInfo begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    begin_info.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(command_buffer, &begin_info) != VK_SUCCESS)
    {
        lava::log()->error("Can't begin command buffer during texture staging!");

        return false;
    }

    for (const SceneTexture& scene_texture : this->textures)
    {
        lava::texture::ptr texture = scene_texture.texture;
        lava::buffer::ptr stage_buffer = scene_texture.stage_buffer;

        if (scene_texture.stage_dummy)
        {
            texture->stage(command_buffer);
        }

        else if (stage_buffer != nullptr)
        {
            VkImageMemoryBarrier begin_barrier;
            begin_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            begin_barrier.pNext = nullptr;
            begin_barrier.srcAccessMask = 0;
            begin_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            begin_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            begin_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            begin_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            begin_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            begin_barrier.image = texture->get_image()->get();
            begin_barrier.subresourceRange = texture->get_image()->get_subresource_range();

            vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &begin_barrier);

            const std::vector<lava::texture::mip_level>& levels = texture->get_layers().front().levels;
            uint32_t stage_levels = scene_texture.stage_levels;
            uint32_t total_levels = texture->get_image()->get_info().mipLevels;

            std::vector<VkBufferImageCopy> regions;
            uint32_t buffer_offset = 0;

            for (uint32_t index = 0; index < scene_texture.stage_levels; index++)
            {
                VkImageSubresourceLayers subresource;
                subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                subresource.mipLevel = index;
                subresource.baseArrayLayer = 0;
                subresource.layerCount = 1;

                VkOffset3D offset;
                offset.x = 0;
                offset.y = 0;
                offset.z = 0;

                VkExtent3D extent;
                extent.width = levels[index].extent.x;
                extent.height = levels[index].extent.y;
                extent.depth = 1;

                VkBufferImageCopy region;
                region.bufferOffset = buffer_offset;
                region.bufferRowLength = 0;
                region.bufferImageHeight = 0;
                region.imageSubresource = subresource;
                region.imageOffset = offset;
                region.imageExtent = extent;

                regions.push_back(region);
                buffer_offset += levels[index].size;
            }

            vkCmdCopyBufferToImage(command_buffer, stage_buffer->get(), texture->get_image()->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, regions.size(), regions.data());

            for (uint32_t index = stage_levels; index < total_levels; index++)
            {
                glm::uvec2 src_size = levels[index - 1].extent;
                glm::uvec2 dst_size = levels[index].extent;

                VkImageMemoryBarrier src_begin_barrier;
                src_begin_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                src_begin_barrier.pNext = nullptr;
                src_begin_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                src_begin_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                src_begin_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                src_begin_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                src_begin_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                src_begin_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                src_begin_barrier.image = texture->get_image()->get();
                src_begin_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                src_begin_barrier.subresourceRange.baseMipLevel = index - 1;
                src_begin_barrier.subresourceRange.levelCount = 1;
                src_begin_barrier.subresourceRange.baseArrayLayer = 0;
                src_begin_barrier.subresourceRange.layerCount = 1;

                vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &src_begin_barrier);

                VkImageBlit region;
                region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                region.srcSubresource.mipLevel = index - 1;
                region.srcSubresource.baseArrayLayer = 0;
                region.srcSubresource.layerCount = 1;
                region.srcOffsets[0].x = 0;
                region.srcOffsets[0].y = 0;
                region.srcOffsets[0].z = 0;
                region.srcOffsets[1].x = src_size.x;
                region.srcOffsets[1].y = src_size.y;
                region.srcOffsets[1].z = 1;
                region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                region.dstSubresource.mipLevel = index;
                region.dstSubresource.baseArrayLayer = 0;
                region.dstSubresource.layerCount = 1;
                region.dstOffsets[0].x = 0;
                region.dstOffsets[0].y = 0;
                region.dstOffsets[0].z = 0;
                region.dstOffsets[1].x = dst_size.x;
                region.dstOffsets[1].y = dst_size.y;
                region.dstOffsets[1].z = 1;

                vkCmdBlitImage(command_buffer, texture->get_image()->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, texture->get_image()->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region, VK_FILTER_LINEAR);

                VkImageMemoryBarrier src_end_barrier;
                src_end_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                src_end_barrier.pNext = nullptr;
                src_end_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                src_end_barrier.dstAccessMask = 0;
                src_end_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                src_end_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                src_end_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                src_end_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
                src_end_barrier.image = texture->get_image()->get();
                src_end_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                src_end_barrier.subresourceRange.baseMipLevel = index - 1;
                src_end_barrier.subresourceRange.levelCount = 1;
                src_end_barrier.subresourceRange.baseArrayLayer = 0;
                src_end_barrier.subresourceRange.layerCount = 1;

                vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &src_end_barrier);
            }

            VkImageMemoryBarrier end_barrier;
            end_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            end_barrier.pNext = nullptr;
            end_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            end_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            end_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            end_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            end_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            end_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            end_barrier.image = texture->get_image()->get();
            end_barrier.subresourceRange = texture->get_image()->get_subresource_range();

            vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &end_barrier);
        }        
    }

    if (vkEndCommandBuffer(command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't end command buffer during texture staging!");

        return false;
    }

    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;

    if (vkQueueSubmit(device->get_graphics_queue().vk_queue, 1, &submit_info, VK_NULL_HANDLE) != VK_SUCCESS)
    {
        lava::log()->error("Can't submit command buffer for texture staging!");

        return false;
    }

    if (vkDeviceWaitIdle(device->get()) != VK_SUCCESS)
    {
        lava::log()->error("Can't wait for texture staging completion!");

        return false;
    }

    vkDestroyCommandPool(device->get(), command_pool, lava::memory::alloc());

    for (SceneTexture& scene_texture : this->textures)
    {
        if (scene_texture.stage_dummy)
        {
            scene_texture.texture->destroy_upload_buffer();
            scene_texture.stage_dummy = false;
        }

        else if (scene_texture.stage_buffer != nullptr)
        {
            scene_texture.stage_buffer->destroy();
            scene_texture.stage_buffer = nullptr;
            scene_texture.stage_levels = 0;
        }
    }

    return true;
}

void Scene::compute_scene_bounds()
{
    if (!this->meshes.empty())
    {
        this->scene_min = glm::vec3(0.0f);
        this->scene_max = glm::vec3(0.0f);

        for (const SceneMesh& mesh : this->meshes)
        {
            if (!mesh.scene_bounds)
            {
                continue;
            }

            this->scene_min = mesh.global_min;
            this->scene_max = mesh.global_max;

            break;
        }

        for (const SceneMesh& mesh : this->meshes)
        {
            if (!mesh.scene_bounds)
            {
                continue;
            }

            this->scene_min = glm::min(mesh.global_min, this->scene_min);
            this->scene_max = glm::max(mesh.global_max, this->scene_max);
        }
    }

    else
    {
        this->scene_min = glm::vec3(0.0f);
        this->scene_max = glm::vec3(0.0f);
    }
}

void Scene::compute_mesh_bounds(SceneMesh& mesh, const glm::mat4& local_to_world) 
{
    glm::vec3 local_min = mesh.local_min;
    glm::vec3 local_max = mesh.local_max;

    std::array<glm::vec3, 8> mesh_local_box = 
    {
        glm::vec3(local_min.x, local_min.y, local_min.z),
        glm::vec3(local_min.x, local_min.y, local_max.z),
        glm::vec3(local_min.x, local_max.y, local_min.z),
        glm::vec3(local_min.x, local_max.y, local_max.z),
        glm::vec3(local_max.x, local_min.y, local_min.z),
        glm::vec3(local_max.x, local_min.y, local_max.z),
        glm::vec3(local_max.x, local_max.y, local_min.z),
        glm::vec3(local_max.x, local_max.y, local_max.z)
    };

    for (glm::vec3& corner_point : mesh_local_box)
    {
        corner_point = glm::vec3(local_to_world * glm::vec4(corner_point, 1.0));
    }

    glm::vec3 global_min = mesh_local_box.front();
    glm::vec3 global_max = mesh_local_box.front();

    for (const glm::vec3& corner_point : mesh_local_box)
    {
        global_min = glm::min(global_min, corner_point);
        global_max = glm::max(global_max, corner_point);
    }

    mesh.global_min = global_min;
    mesh.global_max = global_max;
}

void Scene::compute_light_matrices(SceneLight& light)
{
    glsl::LightData& light_data = light.data;

    for (uint32_t index = 0; index < MAX_LIGHT_SHADOW_MATRIX_COUNT; index++)
    {
        light_data.shadow_matrix[index] = glm::mat4(1.0f);
    }

    if (light_data.type == LIGHT_TYPE_POINT)
    {
        std::array<glm::vec3, 6> directions = 
        {
            glm::vec3(1.0f, 0.0f, 0.0f),
            glm::vec3(-1.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f),
            glm::vec3(0.0f, -1.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, 1.0f),
            glm::vec3(0.0f, 0.0f, -1.0f)
        };

        std::array<glm::vec3, 6> sides =
        {
            glm::vec3(0.0f, -1.0f, 0.0f),
            glm::vec3(0.0f, -1.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, 1.0f),
            glm::vec3(0.0f, 0.0f, -1.0f),
            glm::vec3(0.0f, -1.0f, 0.0f),
            glm::vec3(0.0f, -1.0f, 0.0f)
        };

        glm::vec3 scene_size = this->scene_max - this->scene_min;
        float scene_diagonal = glm::length(scene_size);

        glm::mat4 projection_matrix = glm::perspective(glm::pi<float>() / 2.0f, 1.0f, 10.0f, scene_diagonal);

        for (uint32_t index = 0; index < directions.size(); index++)
        {
            glm::mat4 view_matrix = glm::lookAt(light_data.position, light_data.position + directions[index], sides[index]);
            glm::mat4 shadow_matrix = projection_matrix * view_matrix;

            light_data.shadow_matrix[index] = shadow_matrix;
            light_data.inv_shadow_matrix[index] = glm::inverse(shadow_matrix);
        }
    }

    else if (light_data.type == LIGHT_TYPE_DIRECTIONAL)
    {
        glm::vec3 direction = glm::normalize(light_data.direction);
        glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

        if (glm::dot(direction, up) > 0.9)
        {
            up = glm::vec3(1.0f, 0.0f, 0.0f);
        }

        glm::mat4 view_matrix = glm::lookAt(glm::vec3(0.0f), direction, up);

        std::array<glm::vec3, 8> box_points =
        {
            glm::vec3(this->scene_min.x, this->scene_min.y, this->scene_min.z),
            glm::vec3(this->scene_min.x, this->scene_min.y, this->scene_max.z),
            glm::vec3(this->scene_min.x, this->scene_max.y, this->scene_min.z),
            glm::vec3(this->scene_min.x, this->scene_max.y, this->scene_max.z),
            glm::vec3(this->scene_max.x, this->scene_min.y, this->scene_min.z),
            glm::vec3(this->scene_max.x, this->scene_min.y, this->scene_max.z),
            glm::vec3(this->scene_max.x, this->scene_max.y, this->scene_min.z),
            glm::vec3(this->scene_max.x, this->scene_max.y, this->scene_max.z)
        };

        for (glm::vec3& point : box_points)
        {
            point = glm::vec3(view_matrix * glm::vec4(point, 1.0));
        }

        glm::vec3 light_min = box_points.front();
        glm::vec3 light_max = box_points.front();

        for (const glm::vec3& point : box_points)
        {
            light_min = glm::min(light_min, point);
            light_max = glm::max(light_max, point);
        }

        glm::vec2 near_size = light_max - light_min;
        glm::mat4 projection_matrix = glm::ortho(light_min.x, light_max.x, light_min.y, light_max.y, -light_max.z, -light_min.z);
        glm::mat4 shadow_matrix = projection_matrix * view_matrix;

        light_data.near_size = near_size;
        light_data.shadow_matrix[0] = shadow_matrix;
        light_data.inv_shadow_matrix[0] = glm::inverse(shadow_matrix);
    }

    else if (light_data.type == LIGHT_TYPE_SPOT)
    {
        glm::vec3 scene_size = this->scene_max - this->scene_min;
        float scene_diagonal = glm::length(scene_size);

        glm::vec3 position = light_data.position;
        glm::vec3 direction = glm::normalize(light_data.direction);
        float angle = light_data.outer_angle;

        glm::vec3 side = glm::vec3(1.0f, 0.0f, 0.0f);

        if (glm::dot(direction, side) > 0.9)
        {
            side = glm::vec3(0.0f, 1.0f, 0.0f);
        }

        glm::mat4 view_matrix = glm::lookAt(position, position + direction, side);
        glm::mat4 projection_matrix = glm::perspective(2.0f * angle, 1.0f, 10.0f, scene_diagonal);
        glm::mat4 shadow_matrix = projection_matrix * view_matrix;

        light_data.shadow_matrix[0] = shadow_matrix;
        light_data.inv_shadow_matrix[0] = glm::inverse(shadow_matrix);
    }
}

void Scene::reset_transforms() 
{
    for (SceneNode& node : this->nodes)
    {
        node.current_local_transform = node.initial_local_transform;
    }
    
    this->apply_transforms();
}

void Scene::apply_transforms()
{
    for (uint32_t node_index = 0; node_index < this->nodes.size(); node_index++)
    {
        SceneNode& node = this->nodes[node_index];
        
        if (node.parent_index >= 0)
        {
            assert(node.parent_index < node_index);

            node.global_visible = nodes[node.parent_index].global_visible && node.local_visible;
            node.current_global_transform = nodes[node.parent_index].current_global_transform * node.current_local_transform;
        }

        else
        {
            node.global_visible = node.local_visible;
            node.current_global_transform = node.current_local_transform;
        }
    }

    for (SceneCamera& camera : this->cameras)
    {
        const SceneNode& node = this->nodes[camera.node_index];

        glm::vec3 global_location = node.current_global_transform * glm::vec4(camera.local_position, 1.0f);
        glm::vec3 global_look_at = node.current_global_transform * glm::vec4(camera.local_look_at, 1.0f);
        glm::vec3 global_up = node.current_global_transform * glm::vec4(camera.local_up, 0.0f);

        camera.view_matrix = glm::lookAt(global_location, global_look_at, global_up);
    }

    for (SceneMesh& mesh : this->meshes)
    {
        bool visible = true;
        glm::mat4 localToWorldSpace = glm::mat4(1.0f);
        glm::mat4 vectorToWorldSpace = glm::mat4(1.0f);

        if (mesh.node_index >= 0)
        {
            const SceneNode& node = this->nodes[mesh.node_index];
            visible = node.global_visible;
            localToWorldSpace = node.current_global_transform;
            vectorToWorldSpace = glm::transpose(glm::inverse(node.current_global_transform));
        }

        mesh.visible = visible;
        mesh.data.localToWorldSpace = localToWorldSpace;
        mesh.data.vectorToWorldSpace = vectorToWorldSpace;

        this->compute_mesh_bounds(mesh, localToWorldSpace);
    }

    this->compute_scene_bounds();

    for (SceneLight& light : this->lights)
    {
        glm::mat4 current_global_transform = glm::mat4(1.0f);

        if (light.node_index >= 0)
        {
            const SceneNode& node = this->nodes[light.node_index];
            current_global_transform = node.current_global_transform;
        }

        light.data.position = current_global_transform * glm::vec4(light.initial_position, 1.0f);
        light.data.direction = current_global_transform * glm::vec4(light.initial_direction, 0.0f);
        light.data.direction = glm::normalize(light.data.direction);

        this->compute_light_matrices(light);
    }
}

bool Scene::setup_post_processes(SceneOrientation orientation, SceneUVs uvs, uint32_t& post_processes)
{
    post_processes = aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType | aiProcess_GenBoundingBoxes;

    switch (orientation)
    {
    case SCENE_ORIENTATION_RIGHT_HANDED:
        break;
    case SCENE_ORIENTATION_LEFT_HANDED:
        post_processes |= aiProcess_ConvertToLeftHanded; //Convert to right-handed
        break;
    default:
        lava::log()->error("Can't setup post processes due to unknown scene orientation!");
        return false;
    }

    switch (uvs)
    {
    case SCENE_UVS_UNCHANGED:
        break;
    case SCENE_UVS_Y_FLIP:
        post_processes ^= aiProcess_FlipUVs;
        break;
    default:
        lava::log()->error("Can't setup post processes due to unknown scene uvs!");
        return false;
    }

    return true;
}

bool Scene::compute_scale(SceneUnit unit, float& scale)
{
    switch (unit)
    {
    case SCENE_UNIT_METERS:
        scale = 1.0f;
        break;
    case SCENE_UNIT_CENTIMETERS:
        scale = 0.01f;
        break;
    default:
        lava::log()->error("Can't compute scene scale due to unknown scene unit!");
        return false;
    }

    return true;
}

Scene::Ptr make_scene()
{
    return std::make_shared<Scene>();
}
