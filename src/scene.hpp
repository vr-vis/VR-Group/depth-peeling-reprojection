#pragma once
#include <liblava/lava.hpp>
#include <assimp/scene.h>
#include <memory>
#include <vector>
#include <cstdint>

#include "headset/headset.hpp"

namespace glsl
{
using namespace glm;
using uint = glm::uint32;
#include "res/dpr/data/mesh_data.inc"
#include "res/dpr/data/light_data.inc"
#include "res/dpr/data/material_data.inc"
}

#define INVALID_NODE -1
typedef int32_t SceneNodeIndex;

enum SceneUnit
{
    SCENE_UNIT_METERS,
    SCENE_UNIT_CENTIMETERS
};

enum SceneOrientation
{
    SCENE_ORIENTATION_RIGHT_HANDED,
    SCENE_ORIENTATION_LEFT_HANDED
};

enum SceneUVs
{
    SCENE_UVS_UNCHANGED,
    SCENE_UVS_Y_FLIP
};

struct SceneConfig
{
    std::string scene_file;
    std::string controller_left_file;
    std::string controller_right_file;

    float scene_exposure = 1.0f;

    std::string sky_sphere_file;                //NOTE: The given file has to be a .hdr file.
    uint32_t sky_sphere_rings = 16;             //NOTE: Defines the number of rings used in the mesh of the sky-sphere. Needs to be at least 3.
    uint32_t sky_sphere_segments = 32;          //NOTE: Defines the number of segments used in the mesh of the sky-sphere. Needs to be at least 3.
    float sky_sphere_intensity = 1.0f;

    SceneUnit scene_unit = SCENE_UNIT_METERS;
    SceneUnit controller_left_unit = SCENE_UNIT_METERS;
    SceneUnit controller_right_unit = SCENE_UNIT_METERS;

    SceneOrientation scene_orientation = SCENE_ORIENTATION_RIGHT_HANDED;
    SceneOrientation controller_left_orientation = SCENE_ORIENTATION_RIGHT_HANDED;
    SceneOrientation controller_right_orientation = SCENE_ORIENTATION_RIGHT_HANDED;

    SceneUVs scene_uvs = SCENE_UVS_UNCHANGED;
    SceneUVs controller_left_uvs = SCENE_UVS_UNCHANGED;
    SceneUVs controller_right_uvs = SCENE_UVS_UNCHANGED;

    glm::mat4 controller_left_alignment = glm::mat4(1.0f); //NOTE: This additonal transformation can be used to align the left controller model so that it matches with the controller in real world.
    glm::mat4 controller_right_alignment = glm::mat4(1.0f); //NOTE: This additonal transformation can be used to align the left controller model so that it matches with the controller in real world.
};

struct SceneNode
{
    SceneNodeIndex parent_index = INVALID_NODE;
    bool local_visible = true;
    bool global_visible = true;
    glm::mat4 initial_local_transform = glm::mat4(1.0f);
    glm::mat4 current_local_transform = glm::mat4(1.0f);
    glm::mat4 current_global_transform = glm::mat4(1.0f);
};

struct SceneCamera
{
    SceneNodeIndex node_index = INVALID_NODE;
    std::string name;
    glm::mat4 projection_matrix = glm::mat4(1.0f);
    glm::mat4 view_matrix = glm::mat4(1.0f);
    glm::vec3 local_position = glm::vec3(0.0f);
    glm::vec3 local_look_at = glm::vec3(0.0f);
    glm::vec3 local_up = glm::vec3(0.0f);
    float near_plane = 0.0f;
    float far_plane = 0.0f;
};

struct SceneMesh
{
    SceneNodeIndex node_index = INVALID_NODE;
    uint32_t material_index = 0;
    bool visible = true;
    bool cast_shadow = true;
    bool scene_bounds = true;
    glm::vec3 local_min = glm::vec3(0.0f);
    glm::vec3 local_max = glm::vec3(0.0f);
    glm::vec3 global_min = glm::vec3(0.0f);
    glm::vec3 global_max = glm::vec3(0.0f);
    glsl::MeshData data;
    lava::mesh::ptr mesh;
    std::vector<VkDescriptorSet> descriptor_set;
};

struct SceneLight
{
    SceneNodeIndex node_index = INVALID_NODE;
    glm::vec3 initial_position = glm::vec3(0.0f);
    glm::vec3 initial_direction = glm::vec3(0.0f);
    glsl::LightData data;
};

struct SceneMaterial
{
    lava::texture::ptr diffuse;
    lava::texture::ptr specular;
    lava::texture::ptr normal;
    lava::texture::ptr emissive;
    glsl::MaterialData material_data;
    VkDescriptorSet descriptor_set = VK_NULL_HANDLE;
};

struct SceneTexture
{
    std::string file_name;
    lava::texture::ptr texture;

    uint32_t stage_levels = 0;
    lava::buffer::ptr stage_buffer;
    bool stage_dummy = false;
};

struct SceneAnimationChannel
{
    SceneNodeIndex node_index = INVALID_NODE;
    std::vector<std::pair<float, glm::vec3>> position_frames;
    std::vector<std::pair<float, aiQuaternion>> rotation_frames;
    std::vector<std::pair<float, glm::vec3>> scaling_frames;
};

struct SceneAnimation
{
    std::string name;
    float duration = 0.0f;
    std::vector<SceneAnimationChannel> channels;
};

class Scene
{
public:
    typedef std::shared_ptr<Scene> Ptr;

public:
    Scene() = default;
    
    bool create(const SceneConfig& config, lava::device_ptr device, uint32_t frame_count);
    void destroy();

    bool interface();
    bool update(lava::delta delta_time, Headset::Ptr headset);
    void write(lava::index frame);

    void reset_animation();
    void set_animation_active(bool active);
    void set_animation_loop(bool loop);
    bool set_animation(const std::string& name);
    bool set_animation(uint32_t index);
    bool set_camera(const std::string& name);
    bool set_camera(uint32_t index);

    const glm::mat4& get_animation_view_matrix() const;
    bool is_animation_complete() const;
    bool is_animation_active() const;
    bool is_camera_active() const;

    lava::descriptor::ptr get_mesh_descriptor() const;
    lava::descriptor::ptr get_light_descriptor() const;
    lava::descriptor::ptr get_material_descriptor() const;

    VkDescriptorSet get_light_descriptor_set(lava::index frame) const;

    const std::vector<SceneNode>& get_nodes() const;
    const std::vector<SceneCamera>& get_cameras() const;
    const std::vector<SceneMesh>& get_meshes() const;
    const std::vector<SceneLight>& get_lights() const;
    const std::vector<SceneMaterial>& get_materials() const;
    const std::vector<SceneTexture>& get_textures() const;
    const std::vector<SceneAnimation>& get_animations() const;

    const glm::vec3& get_scene_min() const;
    const glm::vec3& get_scene_max() const;

    static void set_vertex_input(lava::graphics_pipeline* pipeline);
    static void set_vertex_input_only_position(lava::graphics_pipeline* pipeline);

private:
    void create_dummy_textures(lava::device_ptr device);
    void create_default_light();

    bool create_descriptor_layouts(lava::device_ptr device, uint32_t frame_count);
    bool create_material_buffer(lava::device_ptr device);
    bool create_light_buffer(lava::device_ptr device, uint32_t frame_count);
    bool create_mesh_buffer(lava::device_ptr device, uint32_t frame_count);

    bool load_scene(const std::string& file_name, SceneUnit unit, SceneOrientation orientation, SceneUVs uvs, lava::device_ptr device, std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_controller(const std::string& file_name, SceneUnit unit, SceneOrientation orientation, SceneUVs uvs, lava::device_ptr device, SceneNodeIndex& node_index, std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_sky_sphere(const std::string& file_name, uint32_t ring_count, uint32_t segment_count, float intensity, lava::device_ptr device);

    bool load_nodes(const aiNode* node, float scale, uint32_t base_mesh, SceneNodeIndex parent_index, std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_cameras(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_animations(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_lights(const aiScene* scene, float scale, const std::map<std::string, SceneNodeIndex>& node_indices);
    bool load_meshes(const aiScene* scene, float scale, uint32_t base_material, bool scene_bounds, lava::device_ptr device);
    bool load_materials(const aiScene* scene, const std::string& base_name, lava::device_ptr device);
    bool load_texture(const aiMaterial* material, const std::string& base_name, aiTextureType type, bool use_srgb, lava::device_ptr device, lava::texture::ptr& texture);
    bool load_texture(const std::string& file_name, lava::device_ptr device, bool use_float, bool use_mipmaps, bool use_srgb, lava::texture::ptr& texture);

    bool stage_textures(lava::device_ptr device);

    void compute_scene_bounds();
    void compute_mesh_bounds(SceneMesh& mesh, const glm::mat4& local_to_world);
    void compute_light_matrices(SceneLight& light);

    void reset_transforms();
    void apply_transforms();

    static bool setup_post_processes(SceneOrientation orientation, SceneUVs uvs, uint32_t& post_processes);
    static bool compute_scale(SceneUnit unit, float& scale);

private:
    SceneNodeIndex controller_left = INVALID_NODE;
    SceneNodeIndex controller_right = INVALID_NODE;

    glm::mat4 controller_left_alignment = glm::mat4(1.0f);
    glm::mat4 controller_right_alignment = glm::mat4(1.0f);

    int32_t animation_index = 0;
    bool animation_active = false;
    bool animation_loop = true;
    float animation_time = 0.0f;
    float animation_playback_speed = 1.0f;

    int32_t camera_index = 0;
    bool camera_active = true;

    glm::vec3 scene_min = glm::vec3(0.0f);
    glm::vec3 scene_max = glm::vec3(0.0f);

    const glm::vec3 ambient_color = glm::vec3(0.0f); //NOTE: Indirect should be computed using an indirect cache.
    float exposure = 1.0f;
    bool exposure_changed = false;

    uint32_t light_directional_count = 0;
    uint32_t light_spot_count = 0;
    uint32_t light_point_count = 0;

    std::vector<SceneNode> nodes;
    std::vector<SceneCamera> cameras;
    std::vector<SceneMesh> meshes;
    std::vector<SceneLight> lights;
    std::vector<SceneMaterial> materials;
    std::vector<SceneTexture> textures;
    std::vector<SceneAnimation> animations;

    std::vector<lava::buffer::ptr> mesh_data_buffer;
    std::vector<lava::buffer::ptr> light_data_buffer;
    lava::buffer::ptr light_parameter_buffer;
    lava::buffer::ptr material_data_buffer;

    lava::descriptor::pool::ptr descriptor_pool;
    lava::descriptor::ptr mesh_descriptor;
    lava::descriptor::ptr light_descriptor;
    lava::descriptor::ptr material_descriptor;
    std::vector<VkDescriptorSet> light_descriptor_set;

    lava::texture::ptr dummy_diffuse_texture;
    lava::texture::ptr dummy_specular_texture;
    lava::texture::ptr dummy_normal_texture;
    lava::texture::ptr dummy_emissive_texture;

    lava::texture::ptr sky_emissive_texture;
};

Scene::Ptr make_scene();