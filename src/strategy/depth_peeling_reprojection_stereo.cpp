#include "depth_peeling_reprojection_stereo.hpp"
#include "imgui.h"
#include "vr_application.hpp"

using namespace lava;

//---------------------------------------------------------------------------------------------------
// Implementation of the functions introcued by the new software architecture.

bool DepthPeelingReprojectionStereo::on_create()
{
    VkSamplerCreateInfo const sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR
    };
    if (!get_device()->vkCreateSampler(&sampler_info, &sampler)) {
        return false;
    }

    color_attachment = make_attachment(get_headset()->get_format());
    color_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    color_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    color_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    return this->create();
}

void DepthPeelingReprojectionStereo::on_destroy()
{
    vkDestroySampler(get_device()->get(), sampler, lava::memory::alloc());

    this->destructor();
}

bool DepthPeelingReprojectionStereo::on_interface()
{
    this->draw_imgui();

    return true;
}

bool DepthPeelingReprojectionStereo::on_update(lava::delta delta_time)
{
    this->update();

    return true;
}

bool DepthPeelingReprojectionStereo::on_render(VkCommandBuffer command_buffer, lava::index frame)
{
    this->write_buffer(frame);

    insert_image_memory_barrier(get_device(), command_buffer, this->get_headset()->get_framebuffer(EYE_LEFT)->get(),
                                VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                                VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                this->get_headset()->get_framebuffer(EYE_LEFT)->get_subresource_range());
    insert_image_memory_barrier(get_device(), command_buffer, this->get_headset()->get_framebuffer(EYE_RIGHT)->get(),
                                VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                                VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                this->get_headset()->get_framebuffer(EYE_LEFT)->get_subresource_range());

    std::array<uint8_t, 1> metadata = { 0x00 };
    this->get_headset()->submit_metadata(metadata);

    this->render(command_buffer, frame);
    this->render_companion_window(command_buffer);

    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_LEFT);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_RIGHT);

    return true;
}

uint32_t DepthPeelingReprojectionStereo::get_max_remote_frame_ids() const
{
    return 2;
}

const char* DepthPeelingReprojectionStereo::get_name() const
{
    return "Depth Peeling Reprojection";
}

DepthPeelingReprojectionStereo::Ptr make_depth_peeling_reprojection_stereo()
{
    return std::make_shared<DepthPeelingReprojectionStereo>();
}

//---------------------------------------------------------------------------------------------------
// Mostly untouched code of the depth peeling reprojection strategy

DepthPeelingReprojectionStereo::DepthPeelingReprojectionStereo()
: current_render_pass_(0) {

}

void DepthPeelingReprojectionStereo::destructor() {
    pipeline_layout_->destroy();
    for (auto& render_pass : render_passes_) {
        render_pass->destroy();
    }

    if (reprojection_pipeline_ != nullptr) {
        reprojection_pipeline_->destroy();
    }

    if (reprojection_pipeline_layout_ != nullptr) {
        reprojection_pipeline_layout_->destroy();
    }

    if (reprojection_render_pass_ != nullptr) {
        reprojection_render_pass_->destroy();
    }

    if (companion_window_pipeline_ != nullptr) {
        companion_window_pipeline_->destroy();
    }

    if (companion_window_pipeline_layout_ != nullptr) {
        companion_window_pipeline_layout_->destroy();
    }

    if (color_layers_ != nullptr) {
        color_layers_->destroy();
    }

    for (auto& depth_stencil_layer : depth_stencil_layers_) {
        if (depth_stencil_layer != nullptr) {
            depth_stencil_layer->destroy();
        }
    }

    if (second_layer_image_ != nullptr) {
        second_layer_image_->destroy();
    }

    for (auto& descriptor_set : descriptor_sets_) {
        if (descriptor_set != nullptr) {
            descriptor_->free(descriptor_set, descriptor_pool_->get());
        }
    }

    if (descriptor_ != nullptr) {
        descriptor_->destroy();
    }

    for (auto& descriptor_set_list : reprojection_pipeline_descriptor_set_) {
        for (auto& descriptor_set : descriptor_set_list) {
            reprojection_pipeline_descriptor_->free(descriptor_set, descriptor_pool_->get());
        }
    }

    if (reprojection_pipeline_descriptor_ != nullptr) {
        reprojection_pipeline_descriptor_->destroy();
    }

    if (companion_window_pipeline_descriptor_set_ != nullptr) {
        companion_window_pipeline_descriptor_->free(companion_window_pipeline_descriptor_set_, descriptor_pool_->get());
    }

    if (companion_window_pipeline_descriptor_ != nullptr) {
        companion_window_pipeline_descriptor_->destroy();
    }

    if (descriptor_pool_ != nullptr) {
        descriptor_pool_->destroy();
    }
}

bool DepthPeelingReprojectionStereo::create() {
    framebuffer_size_ = get_headset()->get_resolution();
    descriptor_ = make_descriptor();
    descriptor_->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT);
    descriptor_->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    if (!descriptor_->create(get_device())) {
        return false;
    }

    reprojection_pipeline_descriptor_ = make_descriptor();
    reprojection_pipeline_descriptor_->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT | VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT | VK_SHADER_STAGE_FRAGMENT_BIT);
    reprojection_pipeline_descriptor_->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT);
    reprojection_pipeline_descriptor_->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    if (!reprojection_pipeline_descriptor_->create(get_device())) {
        return false;
    }

    pipeline_layout_ = make_pipeline_layout();
    pipeline_layout_->add(get_stereo_transform()->get_descriptor());
    pipeline_layout_->add(get_scene()->get_material_descriptor());
    pipeline_layout_->add(get_scene()->get_mesh_descriptor());
    pipeline_layout_->add(descriptor_);
    pipeline_layout_->add(reprojection_pipeline_descriptor_);
    if (!pipeline_layout_->create(get_device())) {
        return false;
    }

    descriptor_pool_ = make_descriptor_pool();
    if (!descriptor_pool_->create(get_device(), {
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 2 + 4 * get_application()->get_frame_count() }, //4 * app()->frame_count() for reprojection_pipeline_descriptor_set_
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2 + 2 * get_application()->get_frame_count() }, //2 * app()->frame_count() for reprojection_pipeline_descriptor_set_
        }, 2 + 2 * get_application()->get_frame_count())) { //2 * app()->frame_count() for reprojection_pipeline_descriptor_set_
        return false;
    }

    color_layers_ = make_image_array(VK_FORMAT_R8G8B8A8_UNORM);
    color_layers_->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    color_layers_->set_tiling(VK_IMAGE_TILING_OPTIMAL);
    if (!color_layers_->create(get_device(), framebuffer_size_, 2)) {
        log()->error("Failed to create color layer images");
        return {};
    }
    color_layers_attachment_ = make_attachment(VK_FORMAT_R8G8B8A8_UNORM);
    color_layers_attachment_->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    color_layers_attachment_->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    color_layers_attachment_->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    depth_stencil_layers_[0] = make_image_array(VK_FORMAT_D32_SFLOAT);
    depth_stencil_layers_[0]->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    depth_stencil_layers_[0]->set_tiling(VK_IMAGE_TILING_OPTIMAL);
    if (!depth_stencil_layers_[0]->create(get_device(), framebuffer_size_, 2)) {
        log()->error("Failed to depth stencil layer images 1");
        return {};
    }
    depth_stencil_layers_attachment_[0] = make_attachment(VK_FORMAT_D32_SFLOAT);
    depth_stencil_layers_attachment_[0]->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    depth_stencil_layers_attachment_[0]->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    depth_stencil_layers_attachment_[0]->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    depth_stencil_layers_[1] = make_image_array(VK_FORMAT_D32_SFLOAT);
    depth_stencil_layers_[1]->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    depth_stencil_layers_[1]->set_tiling(VK_IMAGE_TILING_OPTIMAL);
    if (!depth_stencil_layers_[1]->create(get_device(), framebuffer_size_, 2)) {
        log()->error("Failed to depth stencil layer images 1");
        return {};
    }
    depth_stencil_layers_attachment_[1] = make_attachment(VK_FORMAT_D32_SFLOAT);
    depth_stencil_layers_attachment_[1]->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    depth_stencil_layers_attachment_[1]->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    depth_stencil_layers_attachment_[1]->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    render_passes_[0] = create_render_pass(0);
    render_passes_[1] = create_render_pass(1);
    if (!create_reprojection_render_pass()) {
      return false;
    }

    return true;
}

void DepthPeelingReprojectionStereo::render(VkCommandBuffer command_buffer, lava::index frame) {
    current_render_pass_ = (current_render_pass_ + 1) & 1;
    uint32_t depth_read_layer = (current_render_pass_ + 1) & 1;
    VkImageSubresourceRange const image_range{
        .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
        .levelCount = 1,
        .layerCount = 1,
    };
    insert_image_memory_barrier(get_device(), command_buffer, depth_stencil_layers_[depth_read_layer]->get(),
                        VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        image_range);

    render_passes_[current_render_pass_]->process(command_buffer, 0);

    VkImageSubresourceRange const color_range {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .levelCount = 1,
        .layerCount = 2,
    };
    insert_image_memory_barrier(get_device(), command_buffer, color_layers_->get(),
                        VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        color_range);
    reprojection_render_pass_->process(command_buffer, 0);
    // insert_image_memory_barrier(app()->device(), command_buffer, depth_stencil_layers_[depth_read_layer]->get(),
    //                     VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
    //                     VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    //                     VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
    //                     image_range);
}

void DepthPeelingReprojectionStereo::render_companion_window(VkCommandBuffer command_buffer) {
    VkImageSubresourceRange const color_range {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .levelCount = 1,
        .layerCount = 1,
    };
    insert_image_memory_barrier(get_device(), command_buffer, color_layers_->get(),
                        VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        color_range);
    VkImageCopy left_eye_region {
        .srcSubresource = VkImageSubresourceLayers {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .srcOffset = VkOffset3D {
            .x = 0,
            .y = 0,
            .z = 0,
        },
        .dstSubresource = get_headset()->get_framebuffer(EYE_LEFT)->get_subresource_layers(),
        .dstOffset = VkOffset3D {
            .x = 0,
            .y = 0,
            .z = 0,
        },
        .extent = VkExtent3D {
            .width = get_headset()->get_resolution().x,
            .height = get_headset()->get_resolution().y,
            .depth = 1,
        },
    };

    vkCmdCopyImage(
        command_buffer,
        color_layers_->get(),
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        get_headset()->get_framebuffer(EYE_LEFT)->get(),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1, &left_eye_region);

    VkImageCopy right_eye_region {
        .srcSubresource = VkImageSubresourceLayers {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 1,
            .layerCount = 1,
        },
        .srcOffset = VkOffset3D {
            .x = 0,
            .y = 0,
            .z = 0,
        },
        .dstSubresource = VkImageSubresourceLayers {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .dstOffset = VkOffset3D {
            .x = 0,
            .y = 0,
            .z = 0,
        },
        .extent = VkExtent3D {
            .width = get_headset()->get_resolution().x,
            .height = get_headset()->get_resolution().y,
            .depth = 1,
        },
    };

    VkImageSubresourceRange const image_range{
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .levelCount = 1,
        .layerCount = 1,
    };
    insert_image_memory_barrier(get_device(), command_buffer, get_headset()->get_framebuffer(EYE_LEFT)->get(),
                        VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
                        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        get_headset()->get_framebuffer(EYE_LEFT)->get_subresource_range());
    // insert_image_memory_barrier(app()->device(), command_buffer, app()->right_eye_framebuffer()->color_image->get(),
    //                     VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT,
    //                     VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    //                     VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
    //                     image_range);
}

void DepthPeelingReprojectionStereo::update() {
    const glm::dmat4 right_eye_transform = get_stereo_transform()->get_view_eye_projection_matrix(EYE_RIGHT);
    const glm::dmat4 left_eye_transform = get_stereo_transform()->get_view_eye_projection_matrix(EYE_LEFT);

    params.left_to_right_eye = glm::mat4(right_eye_transform * glm::inverse(left_eye_transform));
}

void DepthPeelingReprojectionStereo::write_buffer(lava::index frame) {
    std::memcpy(reproject_ubo_[frame]->get_mapped_data(), &params, sizeof(glsl::DprParameters));
}

void DepthPeelingReprojectionStereo::draw_imgui() {
    if (ImGui::CollapsingHeader("Depth Peeling Reprojection", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::InputFloat("Discard Threshold", &params.discard_threshold, 0.0001, 0.001, "%.10f");
        ImGui::InputInt("Tesselation Level", &params.tesselation_level, 1.0);
        ImGui::InputFloat("Z-min", &params.z_min, 0.00001, 0.00001, "%.10f");
        ImGui::InputFloat("Z-max", &params.z_max, 0.00001, 0.00001, "%.10f");
        ImGui::InputFloat("Depth Threshold", &params.zThreshold, 0.00001, 0.00001, "%.20f");

        bool draw_first_layer = (layers_to_draw_ & 0b01) != 0;
        ImGui::Checkbox("First Layer", &draw_first_layer);
        bool draw_second_layer = (layers_to_draw_ & 0b10) != 0;
        ImGui::Checkbox("Second Layer", &draw_second_layer);
        layers_to_draw_ = (draw_first_layer ? 0b01 : 0) | (draw_second_layer ? 0b10 : 0);
    }
}

render_pass::ptr DepthPeelingReprojectionStereo::create_render_pass(lava::index render_pass_index) {
    descriptor_sets_[render_pass_index] = descriptor_->allocate_set(descriptor_pool_->get());
    VkDescriptorImageInfo previous_depth_buffer;
    previous_depth_buffer.sampler = sampler;
    previous_depth_buffer.imageView = depth_stencil_layers_[(render_pass_index + 1) % 2]->get_layer_view(0);
    previous_depth_buffer.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    get_device()->vkUpdateDescriptorSets({
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = descriptor_sets_[render_pass_index],
            .dstBinding = 1,
            .descriptorCount = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .pImageInfo = &previous_depth_buffer,
        },
    });

    auto pipeline = make_graphics_pipeline(get_device());

    Scene::set_vertex_input(pipeline.get());

    const VkPipelineColorBlendAttachmentState blend_state = {
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };
    pipeline->add_color_blend_attachment(blend_state);
    pipeline->set_depth_test_and_write(true, true);
    pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);

    pipeline->on_process = [this, render_pass_index](VkCommandBuffer command_buffer) {
        pipeline_layout_->bind(command_buffer, get_stereo_transform()->get_descriptor_set(EYE_LEFT, get_application()->get_frame_index()), 0);
        pipeline_layout_->bind(command_buffer, descriptor_sets_[render_pass_index], 3);
        pipeline_layout_->bind(command_buffer, reprojection_pipeline_descriptor_set_[0][get_application()->get_frame_index()], 4);

        for (size_t i = 0; i < get_scene()->get_meshes().size(); ++i) {
            const auto& mesh = get_scene()->get_meshes()[i];
            const auto& material = get_scene()->get_materials()[mesh.material_index];

            if (!mesh.visible) {
                continue;
            }

            pipeline_layout_->bind(command_buffer, material.descriptor_set, 1);
            pipeline_layout_->bind(command_buffer, mesh.descriptor_set[get_application()->get_frame_index()], 2);

            mesh.mesh->bind_draw(command_buffer);
        }
    };
    pipeline->set_layout(pipeline_layout_);

    if (!pipeline->add_shader(file_data("dpr/binary/depth_peeling_reprojection_stereo_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT)) {
        log()->error("Failed to load shader");
        throw std::runtime_error("Failed to load shader");
    }

    if (!pipeline->add_shader(file_data("dpr/binary/depth_peeling_reprojection_stereo_geometry.spirv"), VK_SHADER_STAGE_GEOMETRY_BIT)) {
        log()->error("Failed to load shader");
        throw std::runtime_error("Failed to load shader");
    }

    if (!pipeline->add_shader(file_data("dpr/binary/depth_peeling_reprojection_stereo_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT)) {
        log()->error("Failed to load shader");
        throw std::runtime_error("Failed to load shader");
    }

    auto render_pass = make_render_pass(get_device());
    render_pass->set_clear_color();
    render_pass->set_layers(2);

    subpass::ptr subpass = make_subpass();

    VkAttachmentReference color_attachment {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };
    subpass->set_color_attachment(color_attachment);

    VkAttachmentReference depth_stencil_attachment;
    depth_stencil_attachment.attachment = 1;
    depth_stencil_attachment.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    subpass->set_depth_stencil_attachment(depth_stencil_attachment);

    render_pass->add(color_layers_attachment_);
    render_pass->add(depth_stencil_layers_attachment_[render_pass_index]);
    render_pass->add(subpass);

    subpass_dependency::ptr dependency = make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    // wait for previous fragment shader to finish reading before clearing attachments
    dependency->set_stage_mask(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    // we need a memory barrier because this isn't a standard write-after-read hazard
    // subpass deps have an implicit attachment layout transition, so the dst access mask must be correct
    dependency->set_access_mask(0,
                                VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
    render_pass->add(dependency);

    dependency = make_subpass_dependency(render_pass->get_subpass_count() - 1, VK_SUBPASS_EXTERNAL);
    // don't run any fragment shader (sample attachments) before we're done writing to attachments
    dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                               VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    // make attachment writes visible to subsequent reads
    dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                                VK_ACCESS_SHADER_READ_BIT);
    render_pass->add(dependency);

    dependency = make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    dependency->set_stage_mask(VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    dependency->set_access_mask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    render_pass->add(dependency);

    dependency = make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    dependency->set_stage_mask(VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    dependency->set_access_mask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);
    render_pass->add(dependency);

    rect area = { glm::vec2(0.0f, 0.0f), get_headset()->get_resolution() };
    VkImageViews views {
        color_layers_->get_view(),
        depth_stencil_layers_[render_pass_index]->get_view(),
    };
    render_pass->create({ views }, area);

    pipeline->create(render_pass->get());
    render_pass->add_front(pipeline);

    return render_pass;
}

bool DepthPeelingReprojectionStereo::create_reprojection_render_pass() {
    reprojection_mesh_ = make_mesh();
    int blocks_x = 1024 / 64;
    int blocks_y = 1024 / 64;

    std::vector<lava::vertex> vertices((blocks_x + 1) * (blocks_y + 1));
    for (int y = 0; y < blocks_y + 1; ++y) {
        for (int x = 0; x < blocks_x + 1; ++x) {
            auto& vertex = vertices[y * (blocks_x + 1) + x];
            vertex.position.x = x / static_cast<float>(blocks_x);
            vertex.position.y = y / static_cast<float>(blocks_y);
            vertex.position.z = 0.0f;
        }
    }
    std::vector<lava::index> indices;
    indices.reserve(blocks_x * blocks_y * 4);
    for (int y = 0; y < blocks_y; ++y) {
        for (int x = 0; x < blocks_x; ++x) {
            indices.push_back(y * (blocks_x + 1) + x);
            indices.push_back((y + 1) * (blocks_x + 1) + x);
            indices.push_back((y + 1) * (blocks_x + 1) + x + 1);
            indices.push_back(y * (blocks_x + 1) + x + 1);
        }
    }
    reprojection_mesh_->get_vertices() = vertices;
    reprojection_mesh_->get_indices() = indices;

    if (!reprojection_mesh_->create(get_device())) {
        log()->error("Failed to create mesh");
        return false;
    }
    reprojection_pipeline_ = make_graphics_pipeline(get_device());

    if (!reprojection_pipeline_->add_shader(file_data("dpr/binary/dpr_reproject_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT)) {
        return false;
    }

    if (!reprojection_pipeline_->add_shader(file_data("dpr/binary/dpr_reproject_tesselation_control.spirv"), VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT)) {
        return false;
    }

    if (!reprojection_pipeline_->add_shader(file_data("dpr/binary/dpr_reproject_tesselation_evaluation.spirv"), VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT)) {
        return false;
    }

    if (!reprojection_pipeline_->add_shader(file_data("dpr/binary/dpr_reproject_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT)) {
        return false;
    }
    reprojection_pipeline_->set_tesselation_patch_control_points(4);
    reprojection_pipeline_->set_input_assembly_topology(VK_PRIMITIVE_TOPOLOGY_PATCH_LIST);

    const VkPipelineColorBlendAttachmentState blend_state = {
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };
    reprojection_pipeline_->set_depth_test_and_write(false, false);
    reprojection_pipeline_->add_color_blend_attachment(blend_state);

    reprojection_pipeline_->set_vertex_input_binding({ 0, sizeof(vertex), VK_VERTEX_INPUT_RATE_VERTEX });
    reprojection_pipeline_->set_vertex_input_attributes({
        { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, to_ui32(offsetof(vertex, position)) },
        { 1, 0, VK_FORMAT_R32G32_SFLOAT, to_ui32(offsetof(vertex, uv)) },
    });
    // reprojection_pipeline_->set_rasterization_polygon_mode(VK_POLYGON_MODE_LINE);

    params.left_to_right_eye = glm::mat4(1.0f);
    params.discard_threshold = 0.0001;
    params.tesselation_level = 16.0f;
    params.z_min = 0.0;
    params.z_max = 0.99987784;

    reproject_ubo_.resize(get_application()->get_frame_count());

    for (auto& buffer : reproject_ubo_) {
        buffer = make_buffer();
        
        if (!buffer->create_mapped(get_device(), &params, sizeof(glsl::DprParameters), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT)) {
            return {};
        }
    }

    VkDescriptorImageInfo left_depth_buffers[2];
    left_depth_buffers[0].sampler = sampler;
    left_depth_buffers[0].imageView = depth_stencil_layers_[0]->get_view();
    left_depth_buffers[0].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    left_depth_buffers[1].sampler = sampler;
    left_depth_buffers[1].imageView = depth_stencil_layers_[1]->get_view();
    left_depth_buffers[1].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo color_layers;
    color_layers.sampler = sampler;
    color_layers.imageView = color_layers_->get_view();
    color_layers.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    for (uint32_t eye = 0; eye < 2; eye++) {
        auto& descriptor_set_list = reprojection_pipeline_descriptor_set_[eye];
        descriptor_set_list.resize(get_application()->get_frame_count());

        for (uint32_t frame = 0; frame < get_application()->get_frame_count(); frame++) {
            descriptor_set_list[frame] = reprojection_pipeline_descriptor_->allocate_set(descriptor_pool_->get());
            
            get_device()->vkUpdateDescriptorSets({
                {
                    .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                    .dstSet = descriptor_set_list[frame],
                    .dstBinding = 0,
                    .descriptorCount = 1,
                    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .pBufferInfo = reproject_ubo_[frame]->get_descriptor_info(),
                },
                { .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                  .dstSet = descriptor_set_list[frame],
                  .dstBinding = 1,
                  .descriptorCount = 1,
                  .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                  .pImageInfo = &left_depth_buffers[eye]
                },
                { .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                  .dstSet = descriptor_set_list[frame],
                  .dstBinding = 2,
                  .descriptorCount = 1,
                  .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                  .pImageInfo = &color_layers
                },
            });
        }
    }

    reprojection_pipeline_layout_ = make_pipeline_layout();
    reprojection_pipeline_layout_->add(reprojection_pipeline_descriptor_);
    if (!reprojection_pipeline_layout_ ->create(get_device())) {
        return false;
    }

    reprojection_render_pass_ = make_render_pass(get_device());
    reprojection_render_pass_->set_clear_color();

    subpass::ptr subpass = make_subpass();

    VkAttachmentReference color_attachment {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };
    subpass->set_color_attachment(color_attachment);

    reprojection_render_pass_->add(this->color_attachment);
    reprojection_render_pass_->add(subpass);

    reprojection_pipeline_->on_process = [this](VkCommandBuffer command_buffer) {
        reprojection_pipeline_layout_->bind(command_buffer, reprojection_pipeline_descriptor_set_[current_render_pass_][get_application()->get_frame_index()], 0);
        switch (layers_to_draw_) {
          case 0b01:
            reprojection_mesh_->bind_draw(command_buffer, 1, 1);
            break;

          case 0b10:
            reprojection_mesh_->bind_draw(command_buffer, 1, 0);
            break;

          case 0b11:
            reprojection_mesh_->bind_draw(command_buffer, 2);
            break;
        }
    };
    reprojection_pipeline_->set_layout(reprojection_pipeline_layout_);

    subpass_dependency::ptr dependency = make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    // wait for previous fragment shader to finish reading before clearing attachments
    dependency->set_stage_mask(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    // we need a memory barrier because this isn't a standard write-after-read hazard
    // subpass deps have an implicit attachment layout transition, so the dst access mask must be correct
    dependency->set_access_mask(0,
                                VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
    reprojection_render_pass_->add(dependency);

    dependency = make_subpass_dependency(reprojection_render_pass_->get_subpass_count() - 1, VK_SUBPASS_EXTERNAL);
    // don't run any fragment shader (sample attachments) before we're done writing to attachments
    dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                               VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    // make attachment writes visible to subsequent reads
    dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                                VK_ACCESS_SHADER_READ_BIT);
    reprojection_render_pass_->add(dependency);

    rect area = { glm::vec2(0.0f, 0.0f), get_headset()->get_resolution()};
    VkImageViews views {
        get_headset()->get_framebuffer(EYE_RIGHT)->get_view(),
    };
    if (!reprojection_render_pass_->create({ views }, area)) {
      return false;
    }

    reprojection_pipeline_->create(reprojection_render_pass_->get());
    reprojection_render_pass_->add_front(reprojection_pipeline_);

    return true;
}

bool DepthPeelingReprojectionStereo::setup_companion_window_pipeline() {
    // companion_window_pipeline_ = make_graphics_pipeline(app()->device());

    // if (!companion_window_pipeline_->add_shader(file_data("dpr/binary/dpr_companion_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT)) {
    //     return false;
    // }

    // if (!companion_window_pipeline_->add_shader(file_data("dpr/binary/dpr_companion_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT)) {
    //     return false;
    // }

    // companion_window_pipeline_->add_color_blend_attachment();

    // companion_window_pipeline_->set_vertex_input_binding({ 0, sizeof(vertex), VK_VERTEX_INPUT_RATE_VERTEX });
    // companion_window_pipeline_->set_vertex_input_attributes({
    //     { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, to_ui32(offsetof(vertex, position)) },
    //     { 1, 0, VK_FORMAT_R32G32_SFLOAT, to_ui32(offsetof(vertex, uv)) },
    // });

    // companion_window_pipeline_descriptor_ = make_descriptor();
    // companion_window_pipeline_descriptor_->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    // companion_window_pipeline_descriptor_->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT);
    // if (!companion_window_pipeline_descriptor_->create(app()->device())) {
    //     return false;
    // }

    // companion_window_pipeline_layout_ = make_pipeline_layout();
    // companion_window_pipeline_layout_->add(companion_window_pipeline_descriptor_);
    // if (!companion_window_pipeline_layout_ ->create(app()->device())) {
    //     return false;
    // }

    // companion_window_pipeline_descriptor_set_ = companion_window_pipeline_descriptor_->allocate(descriptor_pool_->get());

    // VkSamplerCreateInfo const sampler_info = {
    //     .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
    //     .magFilter = VK_FILTER_NEAREST,
    //     .minFilter = VK_FILTER_NEAREST,
    //     .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST
    // };
    // if (!device()->vkCreateSampler(&sampler_info, &sampler)) {
    //     return false;
    // }
    
    // VkDescriptorImageInfo left_eye_image_descriptor_info = {
    //     .sampler = sampler,
    //     .imageView = framebuffers_[0]->color_image->get_view(),
    //     .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    // };
    // VkDescriptorImageInfo right_eye_image_descriptor_info = {
    //     .sampler = sampler,
    //     .imageView = framebuffers_[1]->color_image->get_view(),
    //     .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    // };

    // VkWriteDescriptorSet const write_desc_left_eye {
    //     .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
    //     .dstSet = companion_window_pipeline_descriptor_set_,
    //     .dstBinding = 0,
    //     .descriptorCount = 1,
    //     .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    //     .pImageInfo = &left_eye_image_descriptor_info,
    // };

    // VkWriteDescriptorSet const write_desc_right_eye {
    //     .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
    //     .dstSet = companion_window_pipeline_descriptor_set_,
    //     .dstBinding = 1,
    //     .descriptorCount = 1,
    //     .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    //     .pImageInfo = &right_eye_image_descriptor_info,
    // };

    // app()->device()->vkUpdateDescriptorSets({ write_desc_left_eye, write_desc_right_eye });

    // companion_window_pipeline_->on_process = [this](VkCommandBuffer command_buffer) {
    //     companion_window_pipeline_layout_->bind(command_buffer, companion_window_pipeline_descriptor_set_);
    //     quad_mesh_->bind_draw(command_buffer);
    // };

    // companion_window_pipeline_->set_layout(companion_window_pipeline_layout_);
    // companion_window_pipeline_->create(app()->shading.get_pass()->get());
    // app()->shading.get_pass()->add_front(companion_window_pipeline_);

    // app()->add_run_end([this]() {
    //     companion_window_pipeline_descriptor_->free(companion_window_pipeline_descriptor_set_, descriptor_pool_->get());
    //     descriptor_pool_->destroy();
    //     companion_window_pipeline_descriptor_->destroy();
    //     companion_window_pipeline_->destroy();
    //     companion_window_pipeline_layout_->destroy();
    //     for (auto& strategy : stereo_strategies_) {
    //       strategy.reset();
    //     }
    //     device()->vkDestroySampler(sampler);
    // });

    return true;
}