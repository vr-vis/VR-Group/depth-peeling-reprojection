#pragma once
#include <memory>

#include "stereo_strategy.hpp"

namespace glsl {
using namespace glm;
#include "res/dpr/strategy/depth_peeling_reprojection/dpr_parameters.inc"
}

class DepthPeelingReprojectionStereo final : public StereoStrategy {
public:
    typedef std::shared_ptr<DepthPeelingReprojectionStereo> Ptr;

public:
    DepthPeelingReprojectionStereo();
    void destructor();

    bool create();
    void render(VkCommandBuffer command_buffer, lava::index frame);
    void render_companion_window(VkCommandBuffer command_buffer);
    void update();
    void write_buffer(lava::index frame);
    void draw_imgui();

    //---------------------------------------------------------------------------------------------------
    // Virtual functions introduced by the new software architecture.
    
    bool on_create() override;
    void on_destroy() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;
    bool on_render(VkCommandBuffer command_buffer, lava::index frame) override;

    uint32_t get_max_remote_frame_ids() const override;
    const char* get_name() const override;

    //---------------------------------------------------------------------------------------------------
    // Moved objects

    VkSampler sampler = VK_NULL_HANDLE;         //Moved from vr_app to strategy
    lava::attachment::ptr color_attachment;     //Moved from stereo_framebuffer to strategy

    //---------------------------------------------------------------------------------------------------

private:
    lava::uv2 framebuffer_size_;
    lava::pipeline_layout::ptr pipeline_layout_;
    lava::render_pass::ptr render_passes_[2];
    VkDescriptorSet descriptor_sets_[2];
    lava::index current_render_pass_;
    lava::descriptor::ptr descriptor_;
    lava::descriptor::pool::ptr descriptor_pool_;

    lava::image_array::ptr color_layers_;
    lava::attachment::ptr color_layers_attachment_;

    lava::image_array::ptr depth_stencil_layers_[2];
    lava::attachment::ptr depth_stencil_layers_attachment_[2];

    lava::image::ptr second_layer_image_;
    lava::attachment::ptr second_layer_attachment_;

    lava::render_pass::ptr create_render_pass(lava::index render_pass_index);

    lava::mesh::ptr reprojection_mesh_;
    lava::graphics_pipeline::ptr reprojection_pipeline_;
    lava::pipeline_layout::ptr reprojection_pipeline_layout_;
    lava::descriptor::ptr reprojection_pipeline_descriptor_;
    std::vector<VkDescriptorSet> reprojection_pipeline_descriptor_set_[2];
    lava::render_pass::ptr reprojection_render_pass_;
    glsl::DprParameters params;
    std::vector<lava::buffer::ptr> reproject_ubo_;
    bool create_reprojection_render_pass();

    lava::mesh::ptr quad_mesh_;
    lava::graphics_pipeline::ptr companion_window_pipeline_;
    lava::pipeline_layout::ptr companion_window_pipeline_layout_;
    lava::descriptor::ptr companion_window_pipeline_descriptor_;
    VkDescriptorSet companion_window_pipeline_descriptor_set_ = nullptr;

    bool setup_companion_window_pipeline();

    uint32_t layers_to_draw_ = 0b01 | 0b10;
};

DepthPeelingReprojectionStereo::Ptr make_depth_peeling_reprojection_stereo();