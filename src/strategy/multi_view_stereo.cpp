#include "multi_view_stereo.hpp"
#include "vr_application.hpp"

bool MultiViewStereo::on_setup_instance(lava::frame_config& config)
{
    config.info.req_api_version = lava::api_version::v1_1;

    return true;
}

bool MultiViewStereo::on_setup_device(lava::device::create_param& parameters)
{
    memset(&this->features, 0, sizeof(this->features));
    this->features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
    this->features.pNext = (void*)parameters.next;
    this->features.multiview = VK_TRUE;

    parameters.next = &this->features;

    return true;
}

bool MultiViewStereo::on_create()
{
    if (!this->create_pipeline_layout())
    {
        return false;
    }

    if (!this->create_render_pass())
    {
        return false;
    }

    if (!this->create_pipeline())
    {
        return false;
    }

    return true;
}

void MultiViewStereo::on_destroy()
{
    if (this->pipeline_layout != nullptr)
    {
        this->pipeline_layout->destroy();
    }

    if (this->pipeline != nullptr)
    {
        this->pipeline->destroy();
    }

    if (this->render_pass != nullptr)
    {
        this->render_pass->destroy();
    }

    if (this->depth_buffer != nullptr)
    {
        this->depth_buffer->destroy();
    }
}

bool MultiViewStereo::on_interface()
{
    return true;
}

bool MultiViewStereo::on_update(lava::delta delta_time)
{
    return true;
}

bool MultiViewStereo::on_render(VkCommandBuffer command_buffer, lava::index frame)
{
    std::array<uint8_t, 1> metadata = { 0x00 };
    this->get_headset()->submit_metadata(metadata);

    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();

    if (shadow_cache != nullptr)
    {
        this->get_pass_timer()->begin_pass(command_buffer, "shadow");
        shadow_cache->compute_shadow(command_buffer, frame);
        this->get_pass_timer()->end_pass(command_buffer);
    }

    this->get_pass_timer()->begin_pass(command_buffer, "multi_eye");
    this->render_pass->process(command_buffer, 0);
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "left_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_LEFT, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_LEFT);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "right_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_RIGHT, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_RIGHT);

    return true;
}

uint32_t MultiViewStereo::get_max_remote_frame_ids() const
{
    return 2;
}

const char* MultiViewStereo::get_name() const
{
    return "Multi-View Stereo";
}

bool MultiViewStereo::create_pipeline_layout()
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();

    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add(this->get_stereo_transform()->get_descriptor());
    this->pipeline_layout->add(this->get_stereo_transform()->get_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_mesh_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_material_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_light_descriptor());
    
    if (shadow_cache != nullptr)
    {
        this->pipeline_layout->add(shadow_cache->get_descriptor());    
    }
    
    if (indirect_cache != nullptr)
    {
        this->pipeline_layout->add(indirect_cache->get_descriptor());
    }

    if (!this->pipeline_layout->create(this->get_device()))
    {
        lava::log()->error("Can't create pipeline layout for multi-view stereo!");

        return false;
    }

    return true;
}

bool MultiViewStereo::create_render_pass()
{
    this->depth_buffer = lava::make_image(VK_FORMAT_D32_SFLOAT);
    this->depth_buffer->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
    this->depth_buffer->set_tiling(VK_IMAGE_TILING_OPTIMAL);
    this->depth_buffer->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->depth_buffer->set_layer_count(2);

    if (!this->depth_buffer->create(this->get_device(), this->get_headset()->get_resolution()))
    {
        lava::log()->error("Can't create layered depth buffer for multi-view stereo!");

        return false;
    }

    this->depth_attachment = lava::make_attachment(VK_FORMAT_D32_SFLOAT);
    this->depth_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->depth_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->depth_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    this->color_attachment = lava::make_attachment(this->get_headset()->get_format());
    this->color_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->color_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->color_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    VkClearValue depth_clear_value;
    depth_clear_value.depthStencil.depth = 1.0f;
    depth_clear_value.depthStencil.stencil = 0;

    VkClearValue color_clear_value;
    color_clear_value.color.float32[0] = 0.0f;
    color_clear_value.color.float32[1] = 0.0f;
    color_clear_value.color.float32[2] = 0.0f;
    color_clear_value.color.float32[3] = 1.0f;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_depth_stencil_attachment(0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    subpass->set_color_attachment(1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    // wait for previous operations to finish reading before clearing attachments
    // we need a memory barrier because this isn't a standard write-after-read hazard
    // subpass deps have an implicit attachment layout transition, so the dst access mask must be correct
    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

    // don't run any transfer operations before we're done writing to attachments
    // make attachment writes visible to subsequent reads
    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);

    this->render_pass = lava::make_render_pass(this->get_device());
    this->render_pass->add(subpass);
    this->render_pass->add(subpass_begin_dependency);
    this->render_pass->add(subpass_end_dependency);
    this->render_pass->add(this->depth_attachment);
    this->render_pass->add(this->color_attachment);
    this->render_pass->set_clear_values(
    {
        depth_clear_value,
        color_clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        this->get_headset()->get_resolution()
    };

    lava::VkImageViews framebuffer_views =
    {
        this->depth_buffer->get_view(),
        this->get_headset()->get_framebuffer_array()->get_view()
    };

    uint32_t view_mask = 0x03;         //NOTE: First and second bit set, meaning that the first and second layer of the framebuffer will be modified.
    uint32_t correleation_mask = 0x03; //NOTE: First and second bit set, meaning that left and right eye should be rendered concurrently if possible.

    VkRenderPassMultiviewCreateInfo multi_view_info;
    multi_view_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
    multi_view_info.pNext = nullptr;
    multi_view_info.subpassCount = 1;
    multi_view_info.pViewMasks = &view_mask;
    multi_view_info.dependencyCount = 0;
    multi_view_info.pViewOffsets = nullptr;
    multi_view_info.correlationMaskCount = 1;
    multi_view_info.pCorrelationMasks = &correleation_mask;

    if (!this->render_pass->create({ framebuffer_views }, framebuffer_area, &multi_view_info))
    {
        lava::log()->error("Can't create render pass for multi-view stereo!");

        return false;
    }

    return true;
}

bool MultiViewStereo::create_pipeline()
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();

    VkPipelineColorBlendAttachmentState blend_state;
    blend_state.blendEnable = VK_FALSE;
    blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    this->pipeline = lava::make_graphics_pipeline(this->get_device());
    this->pipeline->set_layout(this->pipeline_layout);
    this->pipeline->set_rasterization_cull_mode(VK_CULL_MODE_BACK_BIT);
    this->pipeline->set_depth_test_and_write(true, true);
    this->pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);
    this->pipeline->add_color_blend_attachment(blend_state);

    Scene::set_vertex_input(this->pipeline.get());

    if (!this->pipeline->add_shader(lava::file_data("dpr/binary/multi_view_stereo_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        lava::log()->error("Can't load vertex shader for multi-view stereo!");

        return false;
    }

    std::string shader_name = "dpr/binary/multi_view_stereo";

    if (indirect_cache != nullptr)
    {
        shader_name += "_indirect";
    }

    if (shadow_cache != nullptr)
    {
        shader_name += "_shadow";
    }

    shader_name += "_fragment.spirv";

    if (!this->pipeline->add_shader(lava::file_data(shader_name), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        lava::log()->error("Can't load fragment shader for multi-view stereo!");

        return false;
    }

    this->pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipeline_function(command_buffer);
    };

    if (!this->pipeline->create(this->render_pass->get()))
    {
        lava::log()->error("Can't create pipeline for native stereo forward!");

        return false;
    }

    this->render_pass->add_front(this->pipeline);

    return true;
}

void MultiViewStereo::pipeline_function(VkCommandBuffer command_buffer)
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();
    uint32_t frame_index = this->get_application()->get_frame_index();

    this->pipeline_layout->bind(command_buffer, this->get_stereo_transform()->get_descriptor_set(EYE_LEFT, frame_index), 0);
    this->pipeline_layout->bind(command_buffer, this->get_stereo_transform()->get_descriptor_set(EYE_RIGHT, frame_index), 1);
    this->pipeline_layout->bind(command_buffer, this->get_scene()->get_light_descriptor_set(frame_index), 4);
    
    if (shadow_cache != nullptr && indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, shadow_cache->get_descriptor_set(), 5);
        this->pipeline_layout->bind(command_buffer, indirect_cache->get_descriptor_set(), 6);
    }

    else if (shadow_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, shadow_cache->get_descriptor_set(), 5);
    }

    else if (indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, indirect_cache->get_descriptor_set(), 5);
    }

    const std::vector<SceneMaterial>& materials = this->get_scene()->get_materials();

    for (const SceneMesh& mesh : this->get_scene()->get_meshes())
    {
        const SceneMaterial& material = materials[mesh.material_index];

        this->pipeline_layout->bind(command_buffer, material.descriptor_set, 3);
        this->pipeline_layout->bind(command_buffer, mesh.descriptor_set[frame_index], 2);
        
        mesh.mesh->bind_draw(command_buffer);
    }
}

MultiViewStereo::Ptr make_multi_view_stereo()
{
    return std::make_shared<MultiViewStereo>();
}