#pragma once
#include <memory>
#include <array>

#include "stereo_strategy.hpp"

class MultiViewStereo final : public StereoStrategy
{
public:
    typedef std::shared_ptr<MultiViewStereo> Ptr;

public:
    MultiViewStereo() = default;

    bool on_setup_instance(lava::frame_config& config) override;
    bool on_setup_device(lava::device::create_param& parameters) override;

    bool on_create() override;
    void on_destroy() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;
    bool on_render(VkCommandBuffer command_buffer, lava::index frame) override;

    uint32_t get_max_remote_frame_ids() const override;
    const char* get_name() const override;

private:
    bool create_pipeline_layout();
    bool create_render_pass();
    bool create_pipeline();

    void pipeline_function(VkCommandBuffer command_buffer);

private:
    VkPhysicalDeviceMultiviewFeatures features;

    lava::pipeline_layout::ptr pipeline_layout;

    lava::graphics_pipeline::ptr pipeline;
    lava::render_pass::ptr render_pass;

    lava::attachment::ptr color_attachment;
    lava::attachment::ptr depth_attachment;
    lava::image::ptr depth_buffer;
};

MultiViewStereo::Ptr make_multi_view_stereo();