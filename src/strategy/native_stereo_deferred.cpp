#include "native_stereo_deferred.hpp"
#include "vr_application.hpp"

bool NativeStereoDeferred::on_create()
{
    if (!this->create_pipeline_layout())
    {
        return false;
    }

    if (!this->create_render_pass(EYE_LEFT))
    {
        return false;
    }

    if (!this->create_pipeline(EYE_LEFT))
    {
        return false;
    }

    if (!this->create_render_pass(EYE_RIGHT))
    {
        return false;
    }

    if (!this->create_pipeline(EYE_RIGHT))
    {
        return false;
    }

    return true;
}

void NativeStereoDeferred::on_destroy()
{
    if (this->pipeline_layout != nullptr)
    {
        this->pipeline_layout->destroy();
    }

    for (NativeStereoDeferredPass& eye_pass : this->eye_passes)
    {
        if (eye_pass.pipeline != nullptr)
        {
            eye_pass.pipeline->destroy();
        }

        if (eye_pass.render_pass != nullptr)
        {
            eye_pass.render_pass->destroy();
        }

        if (eye_pass.geometry_buffer != nullptr)
        {
            eye_pass.geometry_buffer->destroy();
        }
    }
}

bool NativeStereoDeferred::on_interface()
{
    return true;
}

bool NativeStereoDeferred::on_update(lava::delta delta_time)
{
    return true;
}

bool NativeStereoDeferred::on_render(VkCommandBuffer command_buffer, lava::index frame)
{
    std::array<uint8_t, 1> metadata = { 0x00 };
    this->get_headset()->submit_metadata(metadata);

    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();

    if (shadow_cache != nullptr)
    {
        this->get_pass_timer()->begin_pass(command_buffer, "shadow");
        shadow_cache->compute_shadow(command_buffer, frame);
        this->get_pass_timer()->end_pass(command_buffer);
    }

    this->get_pass_timer()->begin_pass(command_buffer, "left_eye");
    this->eye_passes[0].render_pass->process(command_buffer, 0);
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_pass_timer()->begin_pass(command_buffer, "left_eye_lighting");
    this->eye_passes[0].geometry_buffer->apply_lighting(command_buffer, frame, this->get_stereo_transform()->get_descriptor_set(EYE_LEFT, get_application()->get_frame_index()));
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "left_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_LEFT, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_LEFT);

    this->get_pass_timer()->begin_pass(command_buffer, "right_eye");
    this->eye_passes[1].render_pass->process(command_buffer, 0);
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_pass_timer()->begin_pass(command_buffer, "right_eye_lighting");
    this->eye_passes[1].geometry_buffer->apply_lighting(command_buffer, frame, this->get_stereo_transform()->get_descriptor_set(EYE_RIGHT, get_application()->get_frame_index()));
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "right_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_RIGHT, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_RIGHT);

    return true;
}

uint32_t NativeStereoDeferred::get_max_remote_frame_ids() const
{
    return 2;
}

const char* NativeStereoDeferred::get_name() const
{
    return "Native Stereo Deferred";
}

bool NativeStereoDeferred::create_pipeline_layout()
{
    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add(this->get_stereo_transform()->get_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_material_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_mesh_descriptor());

    if (!this->pipeline_layout->create(this->get_device()))
    {
        lava::log()->error("Can't create pipeline layout for native stereo deferred!");

        return false;
    }

    return true;
}

bool NativeStereoDeferred::create_render_pass(Eye eye)
{
    GeometryBuffer::Ptr geometry_buffer = make_geometry_buffer();

    if (!geometry_buffer->create(this->get_headset()->get_framebuffer(eye), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, this->get_stereo_transform()->get_descriptor(), this->get_scene(), this->get_application()->get_shadow_cache(), this->get_application()->get_indirect_cache()))
    {
        lava::log()->error("Can't create geometry buffer for native stereo deferred!");

        return false;
    }

    VkAttachmentReference buffer_color_attachment;
    buffer_color_attachment.attachment = 1;
    buffer_color_attachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference buffer_normal_attachment;
    buffer_normal_attachment.attachment = 2;
    buffer_normal_attachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference buffer_material_attachment;
    buffer_material_attachment.attachment = 3;
    buffer_material_attachment.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_depth_stencil_attachment(0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    subpass->set_color_attachments(
    {
        buffer_color_attachment,
        buffer_normal_attachment,
        buffer_material_attachment
    });

    // wait for previous fragment shader to finish reading before clearing attachments
    // we need a memory barrier because this isn't a standard write-after-read hazard
    // subpass deps have an implicit attachment layout transition, so the dst access mask must be correct
    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

    // don't run any fragment shader (sample attachments) before we're done writing to attachments
    // make attachment writes visible to subsequent reads
    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

    lava::render_pass::ptr render_pass = lava::make_render_pass(this->get_device());
    render_pass->add(subpass);
    render_pass->add(subpass_begin_dependency);
    render_pass->add(subpass_end_dependency);
    render_pass->add(geometry_buffer->get_depth_attachment());
    render_pass->add(geometry_buffer->get_color_attachment());
    render_pass->add(geometry_buffer->get_normal_attachment());
    render_pass->add(geometry_buffer->get_material_attachment());
    render_pass->set_clear_values(
    {
        geometry_buffer->get_depth_clear_value(),
        geometry_buffer->get_color_clear_value(),
        geometry_buffer->get_normal_clear_value(),
        geometry_buffer->get_material_clear_value()
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        this->get_headset()->get_resolution()
    };

    lava::VkImageViews framebuffer_views =
    {
        geometry_buffer->get_depth_image()->get_view(),
        geometry_buffer->get_color_image()->get_view(),
        geometry_buffer->get_normal_image()->get_view(),
        geometry_buffer->get_material_image()->get_view()
    };

    if (!render_pass->create({framebuffer_views}, framebuffer_area))
    {
        lava::log()->error("Can't create render pass for native stereo deferred!");

        return false;
    }

    NativeStereoDeferredPass& eye_pass = this->eye_passes[eye];
    eye_pass.render_pass = render_pass;
    eye_pass.geometry_buffer = geometry_buffer;

    return true;
}

bool NativeStereoDeferred::create_pipeline(Eye eye)
{
    lava::graphics_pipeline::ptr pipeline = lava::make_graphics_pipeline(this->get_device());
    pipeline->set_layout(this->pipeline_layout);
    pipeline->set_rasterization_cull_mode(VK_CULL_MODE_BACK_BIT);
    pipeline->set_depth_test_and_write(true, true);
    pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);

    Scene::set_vertex_input(pipeline.get());

    if (!pipeline->add_shader(lava::file_data("dpr/binary/native_stereo_deferred_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        lava::log()->error("Can't load vertex shader for native stereo deferred!");

        return false;
    }

    if (!pipeline->add_shader(lava::file_data("dpr/binary/native_stereo_deferred_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        lava::log()->error("Can't load fragment shader for native stereo deferred!");

        return false;
    }

    for (uint32_t index = 0; index < 3; index++)
    {
        VkPipelineColorBlendAttachmentState blend_state;
        blend_state.blendEnable = VK_FALSE;
        blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend_state.colorBlendOp = VK_BLEND_OP_ADD;
        blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
        blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        pipeline->add_color_blend_attachment(blend_state);
    }

    pipeline->on_process = [this, eye](VkCommandBuffer command_buffer)
    {
        this->pipeline_function(command_buffer, eye);
    };

    NativeStereoDeferredPass& eye_pass = this->eye_passes[eye];

    if (!pipeline->create(eye_pass.render_pass->get()))
    {
        lava::log()->error("Can't create pipeline for native stereo deferred!");

        return false;
    }

    eye_pass.render_pass->add_front(pipeline);
    eye_pass.pipeline = pipeline;
    
    return true;
}

void NativeStereoDeferred::pipeline_function(VkCommandBuffer command_buffer, Eye eye)
{
    uint32_t frame_index = this->get_application()->get_frame_index();

    this->pipeline_layout->bind(command_buffer, this->get_stereo_transform()->get_descriptor_set(eye, frame_index), 0);

    const std::vector<SceneMaterial>& materials = this->get_scene()->get_materials();

    for (const SceneMesh& mesh : this->get_scene()->get_meshes())
    {
        if (!mesh.visible)
        {
            continue;
        }

        const SceneMaterial& material = materials[mesh.material_index];

        this->pipeline_layout->bind(command_buffer, material.descriptor_set, 1);
        this->pipeline_layout->bind(command_buffer, mesh.descriptor_set[frame_index], 2);

        mesh.mesh->bind_draw(command_buffer);
    }
}

NativeStereoDeferred::Ptr make_native_stereo_deferred()
{
    return std::make_shared<NativeStereoDeferred>();
}