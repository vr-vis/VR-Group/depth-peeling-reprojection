#include "native_stereo_forward.hpp"
#include "vr_application.hpp"

bool NativeStereoForward::on_create()
{
    if (!this->create_pipeline_layout())
    {
        return false;
    }

    if (!this->create_render_pass(EYE_LEFT))
    {
        return false;
    }

    if (!this->create_pipeline(EYE_LEFT))
    {
        return false;
    }

    if (!this->create_render_pass(EYE_RIGHT))
    {
        return false;
    }

    if (!this->create_pipeline(EYE_RIGHT))
    {
        return false;
    }

    return true;
}

void NativeStereoForward::on_destroy()
{
    if (this->pipeline_layout != nullptr)
    {
        this->pipeline_layout->destroy();
    }

    for (NativeStereoForwardPass& eye_pass : this->eye_passes)
    {
        if (eye_pass.pipeline != nullptr)
        {
            eye_pass.pipeline->destroy();
        }

        if (eye_pass.render_pass != nullptr)
        {
            eye_pass.render_pass->destroy();
        }

        if (eye_pass.depth_buffer != nullptr)
        {
            eye_pass.depth_buffer->destroy();
        }
    }
}

bool NativeStereoForward::on_interface()
{
    return true;
}

bool NativeStereoForward::on_update(lava::delta delta_time)
{
    return true;
}

bool NativeStereoForward::on_render(VkCommandBuffer command_buffer, lava::index frame)
{
    std::array<uint8_t, 1> metadata = { 0x00 };
    this->get_headset()->submit_metadata(metadata);

    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();

    if (shadow_cache != nullptr)
    {
        this->get_pass_timer()->begin_pass(command_buffer, "shadow");
        shadow_cache->compute_shadow(command_buffer, frame);
        this->get_pass_timer()->end_pass(command_buffer);
    }

    this->get_pass_timer()->begin_pass(command_buffer, "left_eye");
    this->eye_passes[0].render_pass->process(command_buffer, 0);
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "left_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_LEFT, this->get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_LEFT);

    this->get_pass_timer()->begin_pass(command_buffer, "right_eye");
    this->eye_passes[1].render_pass->process(command_buffer, 0);
    this->get_pass_timer()->end_pass(command_buffer);

    this->get_frame_capture()->capture_image(command_buffer, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, "right_eye");
    this->get_companion_window()->submit_image(command_buffer, EYE_RIGHT, this->get_headset()->get_framebuffer(EYE_RIGHT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    this->get_headset()->submit_frame(command_buffer, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, EYE_RIGHT);

    return true;
}

uint32_t NativeStereoForward::get_max_remote_frame_ids() const
{
    return 2;
}

const char* NativeStereoForward::get_name() const
{
    return "Native Stereo Forward";
}

bool NativeStereoForward::create_pipeline_layout()
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();

    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add(this->get_stereo_transform()->get_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_material_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_mesh_descriptor());
    this->pipeline_layout->add(this->get_scene()->get_light_descriptor());

    if (shadow_cache != nullptr)
    {
        this->pipeline_layout->add(shadow_cache->get_descriptor());    
    }
    
    if (indirect_cache != nullptr)
    {
        this->pipeline_layout->add(indirect_cache->get_descriptor());
    }
    
    if (!this->pipeline_layout->create(this->get_device()))
    {
        lava::log()->error("Can't create pipeline layout for native stereo forward!");

        return false;
    }

    return true;
}

bool NativeStereoForward::create_render_pass(Eye eye)
{
    lava::image::ptr depth_buffer = lava::make_image(VK_FORMAT_D32_SFLOAT);
    depth_buffer->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    depth_buffer->set_tiling(VK_IMAGE_TILING_OPTIMAL);

    if (!depth_buffer->create(this->get_device(), this->get_headset()->get_resolution()))
    {
        lava::log()->error("Can't create depth buffer for native stereo forward!");

        return false;
    }

    this->depth_attachment = lava::make_attachment(VK_FORMAT_D32_SFLOAT);
    this->depth_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->depth_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->depth_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    this->color_attachment = lava::make_attachment(this->get_headset()->get_format());
    this->color_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->color_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->color_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    VkClearValue depth_clear_value;
    depth_clear_value.depthStencil.depth = 1.0f;
    depth_clear_value.depthStencil.stencil = 0;

    VkClearValue color_clear_value;
    color_clear_value.color.float32[0] = 0.0f;
    color_clear_value.color.float32[1] = 0.0f;
    color_clear_value.color.float32[2] = 0.0f;
    color_clear_value.color.float32[3] = 1.0f;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_depth_stencil_attachment(0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    subpass->set_color_attachment(1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    // wait for previous fragment shader to finish reading before clearing attachments
    // we need a memory barrier because this isn't a standard write-after-read hazard
    // subpass deps have an implicit attachment layout transition, so the dst access mask must be correct
    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

    // don't run any fragment shader (sample attachments) before we're done writing to attachments
    // make attachment writes visible to subsequent reads
    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

    lava::render_pass::ptr render_pass = lava::make_render_pass(this->get_device());
    render_pass->add(subpass);
    render_pass->add(subpass_begin_dependency);
    render_pass->add(subpass_end_dependency);
    render_pass->add(this->depth_attachment);
    render_pass->add(this->color_attachment);
    render_pass->set_clear_values(
    {
        depth_clear_value,
        color_clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        this->get_headset()->get_resolution()
    };

    lava::VkImageViews framebuffer_views =
    {
        depth_buffer->get_view(),
        this->get_headset()->get_framebuffer(eye)->get_view()
    };

    if (!render_pass->create({framebuffer_views}, framebuffer_area))
    {
        lava::log()->error("Can't create render pass for native stereo forward!");

        return false;
    }

    NativeStereoForwardPass& eye_pass = this->eye_passes[eye];
    eye_pass.render_pass = render_pass;
    eye_pass.depth_buffer = depth_buffer;

    return true;
}

bool NativeStereoForward::create_pipeline(Eye eye)
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();

    VkPipelineColorBlendAttachmentState blend_state;
    blend_state.blendEnable = VK_FALSE;
    blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    lava::graphics_pipeline::ptr pipeline = lava::make_graphics_pipeline(this->get_device());
    pipeline->set_layout(this->pipeline_layout);
    pipeline->set_rasterization_cull_mode(VK_CULL_MODE_BACK_BIT);
    pipeline->set_depth_test_and_write(true, true);
    pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);
    pipeline->add_color_blend_attachment(blend_state);

    Scene::set_vertex_input(pipeline.get());

    if (!pipeline->add_shader(lava::file_data("dpr/binary/native_stereo_forward_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        lava::log()->error("Can't load vertex shader for native stereo forward!");

        return false;
    }

    std::string shader_name = "dpr/binary/native_stereo_forward";

    if (indirect_cache != nullptr)
    {
        shader_name += "_indirect";
    }

    if (shadow_cache != nullptr)
    {
        shader_name += "_shadow";
    }

    shader_name += "_fragment.spirv";

    if (!pipeline->add_shader(lava::file_data(shader_name), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        lava::log()->error("Can't load fragment shader for native stereo forward!");

        return false;
    }

    pipeline->on_process = [this, eye](VkCommandBuffer command_buffer)
    {
        this->pipeline_function(command_buffer, eye);
    };

    NativeStereoForwardPass& eye_pass = this->eye_passes[eye];

    if (!pipeline->create(eye_pass.render_pass->get()))
    {
        lava::log()->error("Can't create pipeline for native stereo forward!");

        return false;
    }

    eye_pass.render_pass->add_front(pipeline);
    eye_pass.pipeline = pipeline;

    return true;
}

void NativeStereoForward::pipeline_function(VkCommandBuffer command_buffer, Eye eye)
{
    ShadowCache::Ptr shadow_cache = this->get_application()->get_shadow_cache();
    IndirectCache::Ptr indirect_cache = this->get_application()->get_indirect_cache();
    uint32_t frame_index = this->get_application()->get_frame_index();

    this->pipeline_layout->bind(command_buffer, this->get_stereo_transform()->get_descriptor_set(eye, frame_index), 0);
    this->pipeline_layout->bind(command_buffer, this->get_scene()->get_light_descriptor_set(frame_index), 3);

    if (shadow_cache != nullptr && indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, shadow_cache->get_descriptor_set(), 4);
        this->pipeline_layout->bind(command_buffer, indirect_cache->get_descriptor_set(), 5);
    }

    else if (shadow_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, shadow_cache->get_descriptor_set(), 4);
    }

    else if (indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, indirect_cache->get_descriptor_set(), 4);
    }

    const std::vector<SceneMaterial>& materials = this->get_scene()->get_materials();

    for (const SceneMesh& mesh : this->get_scene()->get_meshes())
    {
        if (!mesh.visible)
        {
            continue;
        }

        const SceneMaterial& material = materials[mesh.material_index];

        this->pipeline_layout->bind(command_buffer, material.descriptor_set, 1);
        this->pipeline_layout->bind(command_buffer, mesh.descriptor_set[frame_index], 2);
        
        mesh.mesh->bind_draw(command_buffer);
    }
}

NativeStereoForward::Ptr make_native_stereo_forward()
{
    return std::make_shared<NativeStereoForward>();
}