#pragma once
#include <memory>
#include <array>

#include "stereo_strategy.hpp"

struct NativeStereoForwardPass
{
    lava::graphics_pipeline::ptr pipeline;
    lava::render_pass::ptr render_pass;
    lava::image::ptr depth_buffer;
};

class NativeStereoForward final : public StereoStrategy
{
public:
    typedef std::shared_ptr<NativeStereoForward> Ptr;

public:
    NativeStereoForward() = default;
 
    bool on_create() override;
    void on_destroy() override;

    bool on_interface() override;
    bool on_update(lava::delta delta_time) override;
    bool on_render(VkCommandBuffer command_buffer, lava::index frame) override;

    uint32_t get_max_remote_frame_ids() const override;
    const char* get_name() const override;

private:
    bool create_pipeline_layout();
    bool create_render_pass(Eye eye);
    bool create_pipeline(Eye eye);

    void pipeline_function(VkCommandBuffer command_buffer, Eye eye);

private:
    lava::pipeline_layout::ptr pipeline_layout;
    lava::attachment::ptr depth_attachment;
    lava::attachment::ptr color_attachment;
    std::array<NativeStereoForwardPass, 2> eye_passes;
};

NativeStereoForward::Ptr make_native_stereo_forward();