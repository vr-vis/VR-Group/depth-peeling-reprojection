#include "stereo_strategy.hpp"
#include "native_stereo_forward.hpp"
#include "native_stereo_deferred.hpp"
#include "multi_view_stereo.hpp"
#include "depth_peeling_reprojection_stereo.hpp"

#include "vr_application.hpp"

void StereoStrategy::set_application(VRApplication* application)
{
    this->application = application;
}

VRApplication* StereoStrategy::get_application() const
{
    return this->application;
}

lava::device_ptr StereoStrategy::get_device() const
{
    return this->application->get_device();
}

Scene::Ptr StereoStrategy::get_scene() const
{
    return this->application->get_scene();
}

Headset::Ptr StereoStrategy::get_headset() const
{
    return this->application->get_headset();
}

CompanionWindow::Ptr StereoStrategy::get_companion_window() const
{
    return this->application->get_companion_window();
}

FrameCapture::Ptr StereoStrategy::get_frame_capture() const
{
    return this->application->get_frame_capture();
}

PassTimer::Ptr StereoStrategy::get_pass_timer() const
{
    return this->application->get_pass_timer();
}

StereoTransform::Ptr StereoStrategy::get_stereo_transform() const
{
    return this->application->get_stereo_transform();
}

bool StereoStrategy::on_setup_instance(lava::frame_config& config)
{
    return true;
}

bool StereoStrategy::on_setup_device(lava::device::create_param& parameters)
{
    return true;
}

StereoStrategy::Ptr make_stereo_strategy(VRApplication* application, StereoStrategyType strategy_type)
{
    StereoStrategy::Ptr strategy;

    switch (strategy_type)
    {
    case STEREO_STRATEGY_TYPE_NATIVE_FORWARD:
        strategy = make_native_stereo_forward();
        break;
    case STEREO_STRATEGY_TYPE_NATIVE_DEFERRED:
        strategy = make_native_stereo_deferred();
        break;
    case STEREO_STRATEGY_TYPE_MULTI_VIEW:
        strategy = make_multi_view_stereo();
        break;
    case STEREO_STRATEGY_TYPE_DEPTH_PEELING_REPROJECTION:
        strategy = make_depth_peeling_reprojection_stereo();
        break;
    default:
        lava::log()->error("Unkown strategy type!");
        return nullptr;
    }

    strategy->set_application(application);

    return strategy;
}

std::vector<StereoStrategy::Ptr> make_all_stereo_strategies(VRApplication* application)
{
    std::vector<StereoStrategy::Ptr> strategies;
    strategies.push_back(make_native_stereo_forward());
    strategies.push_back(make_native_stereo_deferred());
    strategies.push_back(make_multi_view_stereo());
    strategies.push_back(make_depth_peeling_reprojection_stereo());

    for (StereoStrategy::Ptr strategy : strategies)
    {
        strategy->set_application(application);
    }

    return strategies;
}