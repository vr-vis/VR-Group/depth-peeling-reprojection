/*
  The stereo strategy class defines an interface that every stereo strategy type must implement.
  Stereo strategies define different strategies with which the scene can be render in a stereo view setup.
  A particular strategy type can be created with the help of the function make_stereo_strategy(...).
  Example:

    StereoStrategy::Ptr strategy = make_stereo_strategy(application, STEREO_STRATEGY_TYPE_NATIVE_FORWARD);
*/

#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <cstdint>

#include "scene.hpp"
#include "headset/headset.hpp"
#include "utility/companion_window.hpp"
#include "utility/frame_capture.hpp"
#include "utility/pass_timer.hpp"
#include "utility/stereo_transform.hpp"

class VRApplication;

// When adding or removing a stereo strategy, the function make_stereo_strategy(...), make_all_stereo_strategies(...) as well as
// the function set_stereo_strategy(...) of the CommandParser need to be adapted accordingly.
enum StereoStrategyType
{
    STEREO_STRATEGY_TYPE_NATIVE_FORWARD,
    STEREO_STRATEGY_TYPE_NATIVE_DEFERRED,
    STEREO_STRATEGY_TYPE_MULTI_VIEW,
    STEREO_STRATEGY_TYPE_DEPTH_PEELING_REPROJECTION
};

class StereoStrategy
{
public:
    typedef std::shared_ptr<StereoStrategy> Ptr;

public:
    StereoStrategy() = default;
    virtual ~StereoStrategy() = default;

    void set_application(VRApplication* application);
    VRApplication* get_application() const;

    // Returns the vulkan device.
    lava::device_ptr get_device() const;

    // Returns the scene.
    Scene::Ptr get_scene() const;
    // Returns the active headset.
    Headset::Ptr get_headset() const;
    // Returns the companion window, that can be used for debugging purpose.
    CompanionWindow::Ptr get_companion_window() const;
    // Returns the frame capture system, that can be used to write images to files.
    FrameCapture::Ptr get_frame_capture() const;
    // Returns the pass timer, that can be used to messure the time and perfromance of a particular pass.
    PassTimer::Ptr get_pass_timer() const;
    // Returns the stereo transform, that holds the per frame transformations.
    StereoTransform::Ptr get_stereo_transform() const;

    // This function is called before the creation of the vulkan instance.
    // A stereo strategy can use this function to enable required features and extensions of the instance.
    virtual bool on_setup_instance(lava::frame_config& config);

    // This function is called before the creation of the vulkan device.
    // A stereo strategy can use this function to enable required features and extensions of the device.
    virtual bool on_setup_device(lava::device::create_param& parameters);

    // This function is called during intialisation of the application and every time the active stereo strategy changes.
    // A stereo strategy can use this function to initialise buffers or other resources.
    virtual bool on_create() = 0;

    // This function is called during the shutdown of the application and every time the active stereo strategy changes.
    // A stero strategy can use this function to destroy all reqources that it has created.
    virtual void on_destroy() = 0;

    // This function is called every frame when the interface needs to be rendered.
    // A stero strategy can use this function to manage its own interface.
    virtual bool on_interface() = 0;

    // This function is called every frame and can be used for computations or other updates.
    virtual bool on_update(lava::delta delta_time) = 0;

    // This function is called every frame and sould be used to render the scene to the framebuffers of the headset.
    // A stereo strategy is also responsable for calling submit_frame(...) when it has completed writing to a framebuffer.
    virtual bool on_render(VkCommandBuffer command_buffer, lava::index frame) = 0;

    // Returns the number of frame ids that should be used in case of an remote headset configuration.
    virtual uint32_t get_max_remote_frame_ids() const = 0;
    // Returns the name of the stereo strategy.
    virtual const char* get_name() const = 0;

private:
    VRApplication* application = nullptr;
};

StereoStrategy::Ptr make_stereo_strategy(VRApplication* application, StereoStrategyType strategy_type);
std::vector<StereoStrategy::Ptr> make_all_stereo_strategies(VRApplication* application);