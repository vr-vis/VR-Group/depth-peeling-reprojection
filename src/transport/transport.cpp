#include "transport.hpp"
#include "udp_transport.hpp"

#include <liblava/lava.hpp>

Transport::Ptr make_transport(TransportType transport_type)
{
    switch (transport_type)
    {
    case TRANSPORT_TYPE_UDP:
        return std::make_shared<UDPTransport>();
    default:
        lava::log()->error("Unkown transport type!");
        break;
    }

    return nullptr;
}