/*
  The transport class can be used to manage the connection to a remote headset.
  Before an connection to a remote headset can be established, it is nessesary to call the function create(...).
  The communication between hedaset and host applications starts with the sending of a setup packet from the headset to the host.
  Due to unreliable protocolls such as UDP for example it could be the case that packages get reordered during transmission.
  The transport class however has to allways make sure that the callback for the setup is called before any other callbacks during the startup of the connection.
  With the help of the function wait_connect() it should be possible to wait for the arrival of the setup packet.
  After the setup packet has been received, the host-system should responde with a setup-complete packet which maks the end of the handshake that is used to initialize the connection between the two devices.
  After the handshake phase the host-system can send images in form of encoded nal packets and the headset can send transformation information or controller events.
  It is strongly advised to use a single worker thread that waits for packets and sends packets.
  When a worker thread is used, all callbacks set by the host-system need to be called only by the worker.
  Example:

    Transport::Ptr transport = make_transport(...);

    transport->set_on_setup([transport](const glm::u32vec2& resolution)
    {
        transport->send_setup_complete(...);
    });

    transport->create(42);
    transport->wait_connect();
*/

#pragma once
#include <glm/glm.hpp>
#include <functional>
#include <memory>
#include <optional>
#include <span>

#include "types.hpp"
#include "../encoder/encoder.hpp"

enum TransportState
{
    TRANSPORT_STATE_UNINITIALIZED, // Initial state of the transport class
    TRANSPORT_STATE_WAITING,       // State after calling create(...) and before wait_connect() returns
    TRANSPORT_STATE_CONNECTED,     // State after wait_connect() returns successfully
    TRANSPORT_STATE_ERROR          // State after any error during the communication or if an disconnect was detected. By calling destroy() the transport should return to the uninitalized state
};

// When adding or removing a transport method, the function make_transport(...) as well as
// the function set_transport(...) of the CommandParser need to be adapted accordingly.
enum TransportType
{
    TRANSPORT_TYPE_UDP
};

class Transport
{
public:
    typedef std::shared_ptr<Transport> Ptr;

    // OnSetup: Used during the initial handshake
    //  resolution: The resolution that should be used for rendering
    typedef std::function<void(const glm::u32vec2& resolution)> OnSetup;

    // OnProjectionChange: Used to change the projection matrices. Needs to be send once after the handshake.
    //   left_eye_projection: The projection matrix that should be used for the left eye
    //  right_eye_projection: The projection matrix that should be used for the right eye
    //      left_head_to_eye: The matrix that defines the transformation from head to left eye
    //     right_head_to_eye: The matrix that defines the transformation from head to right eye
    typedef std::function<void(const glm::mat4& left_eye_projection, const glm::mat4& right_eye_projection, const glm::mat4& left_head_to_eye, const glm::mat4& right_head_to_eye)> OnProjectionChange;

    // OnHeadTransform: Used to define the head transformation and is send requently be the headset
    //    transform_id: Unique identifier for the head transformation. The id is send back to the headset in an image packet, if the head transform to that id was used for the rendering of an image.
    //  head_transform: The matrix that defines the rotation and location of the users head.
    typedef std::function<void(TransformId transform_id, const glm::mat4& head_transform)> OnHeadTransform;

    // OnControllerTransform: Used to transmit the rotation and location of a controller. Should be send requently by the headset
    //            controller: Identifies the controller to which the information in this packet belongs
    //  controller_transform: The matrix that defines the latest rotation and location of the specified controller
    typedef std::function<void(Controller controller, const glm::mat4& controller_transform)> OnControllerTransform;

    // OnControllerEvent: Used to transmit the current state of a controller. Should be send once after the handshake
    //        controller: Identifies the controller to which the information in this packet belongs
    //  controller_state: The state of the specified controller
    typedef std::function<void(Controller controller, ControllerState controller_state)> OnControllerEvent;

    // OnControllerButton: Used to transmit button events. Should be send once after the handshake
    //    controller: Identifies the controller to which the information in this packet belongs
    //        button: Identifies the button to which the information in this packet belongs
    //  button_state: The state of the specified button on the specified controller
    typedef std::function<void(Controller controller, Button button, ButtonState button_state)> OnControllerButton;

    // OnControllerThumbstick: Used to transmit changes to the thumbsticks. Should be send once after the handshake
    //  controller: Identifies the controller to which the information in this packet belongs
    //  thumbstick: Position of the thumbstick on the specified controller
    typedef std::function<void(Controller controller, const glm::vec2& thumbstick)> OnControllerThumbstick;

    // OnPerformanceSample: Used to transmit performance related samples.
    //  frame_number: Identifies the frame to which the information in this packet belongs
    //      frame_id: Identifies the frame id (e.g. left or right eye) to which the information in this packet belongs
    //  transform_id: Identifies the head transformation which was used during rendering
    //          unit: Specifies the unit of the value parameter
    //      log_only: Indicates whether the given value should only be loged
    //         value: The value of the sample
    //          name: The name of the sample
    typedef std::function<void(std::optional<FrameNumber> frame_number, std::optional<FrameId> frame_id, std::optional<TransformId> transform_id, Unit unit, bool log_only, double sample, const std::string& name)> OnPerformanceSample;

    // OnTransportError: Called in case of error or disconnect. After that the transport method transitions to the error state
    typedef std::function<void()> OnTransportError;

public:
    Transport() = default;
    virtual ~Transport() = default;

    virtual bool create(uint32_t port_number) = 0;
    virtual void destroy() = 0;
    virtual bool wait_connect() = 0;

    // send_setup_complete: Used to send a setup-complete packet
    //                      This function should be threadsafe and callable by main and worker thread
    //  remote_strategy: The postprocessing method that should be used on the headset
    //            codec: The codec that is used for the encoding of the images.
    //    max_frame_ids: The number of images that define a frame and that are send in parallel (normaly 2 for left and right eye). The headset application should setup this many decoders
    //       near_plane: The distance to the near plane that should be used for the projection matrices
    //        far_plane: The distance to the far plane that should be used for the projection matrices
    virtual bool send_setup_complete(RemoteStrategy remote_strategy, EncoderCodec codec, uint32_t max_frame_ids, float near_plane, float far_plane) = 0;

    // send_stage_transform: Used to send the current transformation for the origin
    //                       This transformation can change for example if the user moves by pressing a controller button
    //                       This function should be threadsafe and callable by main and worker thread
    //  stage_transform: Current stage transform
    virtual bool send_stage_transform(const glm::mat4& stage_transform) = 0;

    // send_frame_config_nal: Used to send meta infromation that is required for the decoding of the images
    //                        In case of an h264 codec this packet would contain the sequence and picture parameter sets
    //                        This function should be threadsafe and callable by main and worker thread
    //  frame_id: The frame id to which the information in this packet belong (e.g. left or right eye)
    //   content: Encoding related meta information
    virtual bool send_frame_config_nal(FrameId frame_id, const std::span<const uint8_t>& content) = 0;

    // send_frame_nal: Use to send encoded images
    //                 This could be for example an h264 encoded image
    //                 This function should be threadsafe and callable by main and worker thread
    //  frame_number: The the number of the frame. The frame number of the first rendered frame would be zero
    //      frame_id: The frame id to which the information in this packet belong (e.g. left or right eye)
    //  transform_id: The id of the head transformation that was used to render the image in this packet
    //       content: Encoded image
    virtual bool send_frame_nal(FrameNumber frame_number, FrameId frame_id, TransformId transform_id, const std::span<const uint8_t>& content) = 0;

    // send_frame_metadata: Used to send additional information that could be needed by the post processing step that was selected in the setup-complete packet
    //                      This function should be threadsafe and callable by main and worker thread
    //  frame_number: The the number of the frame. The frame number of the first rendered frame would be zero
    //       content: Information required by the post processing step
    virtual bool send_frame_metadata(FrameNumber frame_number, const std::span<const uint8_t>& content) = 0;

    // send_overlay_config: Used to enable or disable the overlay on the remote headset
    //                      This function should be threadsafe and callable by main and worker thread
    //  overlay_enable: Indicates if the overlay should be enabled
    virtual bool send_overlay_config(bool overlay_enable) = 0;

    // send_overlay_text: Used to send text that should be shown in the overlay of the remote headset
    //                      This function should be threadsafe and callable by main and worker thread
    //   frame_number: The the number of the frame. The frame number of the first rendered frame would be zero
    //  overlay_index: The the number which is used to sort the overlay items
    //          label: The label that should be used for the text
    //           text: The text that should be shown in the overlay
    virtual bool send_overlay_text(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::string& text) = 0;

    // send_overlay_graph: Used to send graph that should be shown in the overlay of the remote headset
    //                      This function should be threadsafe and callable by main and worker thread
    //   frame_number: The the number of the frame. The frame number of the first rendered frame would be zero
    //  overlay_index: The the number which is used to sort the overlay items
    //          label: The label that should be used for the text
    //        samples: The samples of the graph that should be shown in the overlay
    virtual bool send_overlay_graph(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::span<const float>& samples) = 0;

    // The callbacks are only executed by the worker thread of the transport class.
    // Set the callbacks before calling create and don't do it again after calling create.
    virtual void set_on_setup(OnSetup function) = 0;
    virtual void set_on_projection_change(OnProjectionChange function) = 0;
    virtual void set_on_head_transform(OnHeadTransform function) = 0;
    virtual void set_on_controller_transform(OnControllerTransform function) = 0;
    virtual void set_on_controller_event(OnControllerEvent function) = 0;
    virtual void set_on_controller_button(OnControllerButton function) = 0;
    virtual void set_on_controller_thumbstick(OnControllerThumbstick function) = 0;
    virtual void set_on_performance_sample(OnPerformanceSample function) = 0;
    virtual void set_on_transport_error(OnTransportError function) = 0;

    // The following functions should be thread safe
    virtual void set_max_send_queue_enabled(bool enabled) = 0; // Default: disabled
    virtual void set_max_send_queue_size(uint32_t size) = 0;   // The size is given in Bytes. Default: 128 * DATAGRAM_SIZE

    // The following function should be thread safe
    virtual TransportState get_state() = 0;
    virtual double get_bitrate_send() = 0;          // The bitrate is given in MBits/s
    virtual double get_bitrate_receive() = 0;       // The bitrate is given in MBits/s
    virtual uint32_t get_max_send_queue_size() = 0; // The size is given in Bytes
    virtual uint32_t get_send_queue_size() = 0;     // In Bytes
    virtual uint32_t get_receive_queue_size() = 0;  // In Bytes
    virtual bool is_max_send_queue_enabled() = 0;
};

Transport::Ptr make_transport(TransportType transport_type);