#include "udp_packet.hpp"
#include <liblava/lava.hpp>

bool convert_udp_stereo_strategy(RemoteStrategy remote_strategey, UDPStereoStrategyId& udp_strategy)
{
    switch (remote_strategey)
    {
    case REMOTE_STRATEGY_DEBUG:
        udp_strategy = UDP_STEREO_STRATEGY_ID_DEBUG;
        break;
    case REMOTE_STRATEGY_NATIVE:
        udp_strategy = UDP_STEREO_STRATEGY_ID_NATIVE_STEREO;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown remote strategy!");
        return false;
    }

    return true;
}

bool convert_udp_codec(EncoderCodec codec, UDPCodec& udp_codec)
{
    switch(codec)
    {
    case ENCODER_CODEC_H264:
        udp_codec = UDP_CODEC_H264;
        break;
    case ENCODER_CODEC_H265:
        udp_codec = UDP_CODEC_H265;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown coded!");
        return false;
    }

    return true;
}

bool convert_udp_controller(UDPControllerId udp_controller, Controller& controller)
{
    switch (udp_controller)
    {
    case UDP_CONTROLLER_ID_LEFT:
        controller = CONTROLLER_LEFT;
        break;
    case UDP_CONTROLLER_ID_RIGHT:
        controller = CONTROLLER_RIGHT;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown controller!");
        return false;
    }

    return true;
}

bool convert_udp_controller_state(UDPControllerState udp_controller_state, ControllerState& controller_state)
{
    switch (udp_controller_state)
    {
    case UDP_CONTROLLER_STATE_DETACHED:
        controller_state = CONTROLLER_STATE_DETACHED;
        break;
    case UDP_CONTROLLER_STATE_ATTACHED:
        controller_state = CONTROLLER_STATE_ATTACHED;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown controller state!");
        return false;
    }

    return true;
}

bool convert_udp_button(UDPButtonId udp_button, Button& button)
{
    switch (udp_button)
    {
    case UDP_BUTTON_ID_A:
        button = BUTTON_A;
        break;
    case UDP_BUTTON_ID_B:
        button = BUTTON_B;
        break;
    case UDP_BUTTON_ID_X:
        button = BUTTON_X;
        break;
    case UDP_BUTTON_ID_Y:
        button = BUTTON_Y;
        break;
    case UDP_BUTTON_ID_TRIGGER:
        button = BUTTON_TRIGGER;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown button!");
        return false;
    }

    return true;
}

bool convert_udp_button_state(UDPButtonState udp_button_state, ButtonState& button_state)
{
    switch (udp_button_state)
    {
    case UDP_BUTTON_STATE_PRESSED:
        button_state = BUTTON_STATE_PRESSED;
        break;
    case UDP_BUTTON_STATE_RELEASED:
        button_state = BUTTON_STATE_RELEASED;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown button state!");
        return false;
    }

    return true;
}

bool convert_udp_performance_sample_unit(UDPPerformanceSampleUnit udp_unit, Unit& unit)
{
    switch (udp_unit)
    {
    case UDP_PERFORMANCE_SAMPLE_UNIT_UNDEFINED:
        unit = UNIT_UNDEFINED;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_HOURS:
        unit = UNIT_HOURS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MINUTES:
        unit = UNIT_MINUTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_SECONDS:
        unit = UNIT_SECONDS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MILLISECONDS:
        unit = UNIT_MILLISECONDS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MIRCOSECONDS:
        unit = UNIT_MIRCOSECONDS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_BITS:
        unit = UNIT_BITS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_KBITS:
        unit = UNIT_KBITS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MBITS:
        unit = UNIT_MBITS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_KIBITS:
        unit = UNIT_KIBITS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MIBITS:
        unit = UNIT_MIBITS;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_BYTES:
        unit = UNIT_BYTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_KBYTES:
        unit = UNIT_KBYTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MBYTES:
        unit = UNIT_MBYTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_KIBYTES:
        unit = UNIT_KIBYTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MIBYTES:
        unit = UNIT_MIBYTES;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_UNDEFINED_PER_SECOND:
        unit = UNIT_UNDEFINED_PER_SECOND;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_BITS_PER_SECOND:
        unit = UNIT_BITS_PER_SECOND;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_KBITS_PER_SECOND:
        unit = UNIT_KBITS_PER_SECOND;
        break;
    case UDP_PERFORMANCE_SAMPLE_UNIT_MBITS_PER_SECOND:
        unit = UNIT_MBITS_PER_SECOND;
        break;
    default:
        lava::log()->error("UDP-Transport: Unkown performance sample unit!");
        return false;
    }

    return true;
}