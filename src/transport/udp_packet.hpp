#pragma once
#include <glm/glm.hpp>
#include <cstdint>

#include "types.hpp"
#include "../encoder/encoder.hpp"

#define UDP_PACKET_PERFORMANCE_SAMPLE_NAME_MAX_LENGTH 128
#define UDP_PACKET_OVERLAY_TEXT_LABEL_MAX_LENGTH      128
#define UDP_PACKET_OVERLAY_TEXT_TEXT_MAX_LENGTH       128
#define UDP_PACKET_OVERLAY_GRAPH_LABEL_MAX_LENGTH     128
#define UDP_PACKET_OVERLAY_GRAPH_SAMPLE_MAX_COUNT     64

#define UDP_UNDEFINED_FRAME_NUMBER 0xFFFFFFFF   //NOTE: Only allowed for PacketPerformanceSample
#define UDP_UNDEFINED_FRAME_ID     0xFFFFFFFF   //NOTE: Only allowed for PacketPerformanceSample
#define UDP_UNDEFINED_TRANSFORM_ID 0xFFFFFFFF   //NOTE: Only allowed for PacketPerformanceSample

enum UDPPacketId : uint32_t
{
    UDP_PACKET_ID_SETUP                 = 0x00,
    UDP_PACKET_ID_SETUP_COMPLETE        = 0x01,
    UDP_PACKET_ID_PROJECTION_CHANGE     = 0x02,
    UDP_PACKET_ID_STAGE_TRANSFORM       = 0x03,
    UDP_PACKET_ID_HEAD_TRANSFORM        = 0x04,
    UDP_PACKET_ID_CONTROLLER_TRANSFORM  = 0x05,
    UDP_PACKET_ID_CONTROLLER_EVENT      = 0x06,
    UDP_PACKET_ID_CONTROLLER_BUTTON     = 0x07,
    UDP_PACKET_ID_CONTROLLER_THUMBSTICK = 0x08,
    UDP_PACKET_ID_FRAME_CONFIG_NAL      = 0x09,
    UDP_PACKET_ID_FRAME_NAL             = 0x0A,
    UDP_PACKET_ID_FRAME_METADATA        = 0x0B,
    UDP_PACKET_ID_PERFORMANCE_SAMPLE    = 0x0C,
    UDP_PACKET_ID_OVERLAY_CONFIG        = 0x0D,
    UDP_PACKET_ID_OVERLAY_TEXT          = 0x0E, 
    UDP_PACKET_ID_OVERLAY_GRAPH         = 0x0F,
    UDP_PACKET_ID_MAX_COUNT
};

enum UDPStereoStrategyId : uint32_t
{
    UDP_STEREO_STRATEGY_ID_DEBUG = 0x00,
    UDP_STEREO_STRATEGY_ID_NATIVE_STEREO  = 0x01,   
    UDP_STEREO_STRATEGY_ID_MAX_COUNT
};

enum UDPCodec : uint32_t
{
    UDP_CODEC_H264 = 0x00,
    UDP_CODEC_H265 = 0x01
};

enum UDPControllerId : uint32_t
{
    UDP_CONTROLLER_ID_LEFT  = 0x00,
    UDP_CONTROLLER_ID_RIGHT = 0x01,
    UDP_CONTROLLER_ID_MAX_COUNT
};

enum UDPControllerState : uint32_t
{
    UDP_CONTROLLER_STATE_DETACHED = 0x00,
    UDP_CONTROLLER_STATE_ATTACHED = 0x01
};

enum UDPButtonId : uint32_t
{
    UDP_BUTTON_ID_A       = 0x00,
    UDP_BUTTON_ID_B       = 0x01,
    UDP_BUTTON_ID_X       = 0x02,
    UDP_BUTTON_ID_Y       = 0x03,
    UDP_BUTTON_ID_TRIGGER = 0x04,
    UDP_BUTTON_ID_MAX_COUNT
};

enum UDPButtonState : uint32_t
{
    UDP_BUTTON_STATE_PRESSED  = 0x00,
    UDP_BUTTON_STATE_RELEASED = 0x01
};

enum UDPPerformanceSampleUnit : uint32_t
{
    UDP_PERFORMANCE_SAMPLE_UNIT_UNDEFINED            = 0x00,
    UDP_PERFORMANCE_SAMPLE_UNIT_HOURS                = 0x01,
    UDP_PERFORMANCE_SAMPLE_UNIT_MINUTES              = 0x02,
    UDP_PERFORMANCE_SAMPLE_UNIT_SECONDS              = 0x03,
    UDP_PERFORMANCE_SAMPLE_UNIT_MILLISECONDS         = 0x04,
    UDP_PERFORMANCE_SAMPLE_UNIT_MIRCOSECONDS         = 0x05,
    UDP_PERFORMANCE_SAMPLE_UNIT_BITS                 = 0x06,
    UDP_PERFORMANCE_SAMPLE_UNIT_KBITS                = 0x07,  //NOTE: Base 1000
    UDP_PERFORMANCE_SAMPLE_UNIT_MBITS                = 0x08,  //NOTE: Base 1000
    UDP_PERFORMANCE_SAMPLE_UNIT_KIBITS               = 0x09,  //NOTE: Base 1024
    UDP_PERFORMANCE_SAMPLE_UNIT_MIBITS               = 0x0A,  //NOTE: Base 1024
    UDP_PERFORMANCE_SAMPLE_UNIT_BYTES                = 0x0B,
    UDP_PERFORMANCE_SAMPLE_UNIT_KBYTES               = 0x0C,  //NOTE: Base 1000
    UDP_PERFORMANCE_SAMPLE_UNIT_MBYTES               = 0x0D,  //NOTE: Base 1000
    UDP_PERFORMANCE_SAMPLE_UNIT_KIBYTES              = 0x0E,  //NOTE: Base 1024
    UDP_PERFORMANCE_SAMPLE_UNIT_MIBYTES              = 0x0F,  //NOTE: Base 1024
    UDP_PERFORMANCE_SAMPLE_UNIT_UNDEFINED_PER_SECOND = 0x10,
    UDP_PERFORMANCE_SAMPLE_UNIT_BITS_PER_SECOND      = 0x11,
    UDP_PERFORMANCE_SAMPLE_UNIT_KBITS_PER_SECOND     = 0x12,  //NOTE: Base 1000
    UDP_PERFORMANCE_SAMPLE_UNIT_MBITS_PER_SECOND     = 0x13,  //NOTE: Base 1000
};

typedef uint32_t UDPFrameId;
typedef uint32_t UDPFrameNumber;
typedef uint32_t UDPTransformId;

bool convert_udp_stereo_strategy(RemoteStrategy remote_strategey, UDPStereoStrategyId& udp_strategy);
bool convert_udp_codec(EncoderCodec codec, UDPCodec& udp_codec);
bool convert_udp_controller(UDPControllerId udp_controller, Controller& controller);
bool convert_udp_controller_state(UDPControllerState udp_controller_state, ControllerState& controller_state);
bool convert_udp_button(UDPButtonId udp_button, Button& button);
bool convert_udp_button_state(UDPButtonState udp_button_state, ButtonState& button_state);
bool convert_udp_performance_sample_unit(UDPPerformanceSampleUnit udp_unit, Unit& unit);

//All packages need to be smaller than 512, otherwise the package needs to be fragmented.
//In case of an fragmented package, all parts of it are gathered and sorted based on the information provide in the packet.
//Currently only PacketFrameNal, PacketFrameMetadata packages could get fragmented during transmission.
//Besides that the transport system has to wait for the PacketSetup, PacketSetupComplete to arrive before it can distribute other packages.
//This means that the application always sees the PacketSetup, PacketSetupComplete before any other package.

//NOTE: Not an actual packet.
struct UDPPacketHeader
{
    UDPPacketId id = (UDPPacketId)0;
    uint32_t size = 0;
};

struct UDPPacketSetup
{
    UDPPacketId id = UDP_PACKET_ID_SETUP;
    uint32_t size = 0;

    glm::u32vec2 resolution;
};

struct UDPPacketSetupComplete
{
    UDPPacketId id = UDP_PACKET_ID_SETUP_COMPLETE;
    uint32_t size = 0;

    UDPStereoStrategyId strategy_id;
    UDPCodec codec;
    uint32_t max_frame_ids;

    float near_plane;
    float far_plane;
};

struct UDPPacketProjectionChange
{
    UDPPacketId id = UDP_PACKET_ID_PROJECTION_CHANGE;
    uint32_t size = 0;

    glm::mat4 left_eye_projection;
    glm::mat4 right_eye_projection;

    glm::mat4 left_head_to_eye;
    glm::mat4 right_head_to_eye;
};

struct UDPPacketStageTransform
{
    UDPPacketId id = UDP_PACKET_ID_STAGE_TRANSFORM;
    uint32_t size = 0;

    glm::mat4 stage_transform;
};

struct UDPPacketHeadTransform
{
    UDPPacketId id = UDP_PACKET_ID_HEAD_TRANSFORM;
    uint32_t size = 0;

    UDPTransformId transform_id;
    glm::mat4 head_transform;
};

struct UDPPacketControllerTransform
{
    UDPPacketId id = UDP_PACKET_ID_CONTROLLER_TRANSFORM;
    uint32_t size = 0;

    UDPControllerId controller_id;
    glm::mat4 controller_transform;
};

struct UDPPacketControllerEvent
{
    UDPPacketId id = UDP_PACKET_ID_CONTROLLER_EVENT;
    uint32_t size = 0;

    UDPControllerId controller_id;
    UDPControllerState controller_state;
};

struct UDPPacketControllerButton
{
    UDPPacketId id = UDP_PACKET_ID_CONTROLLER_TRANSFORM;
    uint32_t size = 0;

    UDPControllerId controller_id;
    UDPButtonId button_id;
    UDPButtonState button_state;
};

struct UDPPacketControllerThumbstick
{
    UDPPacketId id = UDP_PACKET_ID_CONTROLLER_THUMBSTICK;
    uint32_t size = 0;

    UDPControllerId controller_id;
    glm::vec2 thumbstick;
};

//NOTE: The entire size of the packet including the payload needs to be less than 512 bytes
struct UDPPacketFrameConfigNal
{
    UDPPacketId id = UDP_PACKET_ID_FRAME_NAL;
    uint32_t size = 0;

    UDPFrameId frame_id;

    //Followed by: Config Nal
};

//NOTE: The packet needs to be fragmented if the payload is too big
struct UDPPacketFrameNal
{
    UDPPacketId id = UDP_PACKET_ID_FRAME_NAL;
    uint32_t size = 0;

    UDPFrameNumber frame_number; //In case of fragmentation, frame number and frame id are used together as identifier
    UDPFrameId frame_id;
    UDPTransformId transform_id;

    uint32_t payload_offset;
    uint32_t payload_size;

    //Followed by: Nal
};

//NOTE: Assume frame metadata of a particular frame number is always send before any frame nals of the same frame number
//NOTE: The packet needs to be fragmented if the payload is too big
struct UDPPacketFrameMetadata
{
    UDPPacketId id = UDP_PACKET_ID_FRAME_METADATA;
    uint32_t size = 0;

    UDPFrameNumber frame_number; //In case of fragmentation, the frame number is used as identifier

    uint32_t payload_offset;
    uint32_t payload_size;

    //Followed by: Metadata
};

struct UDPPacketOverlayConfig
{
    UDPPacketId id = UDP_PACKET_ID_OVERLAY_CONFIG;
    uint32_t size = 0;

    bool overlay_enable;
};

struct UDPPacketPerformanceSample
{
    UDPPacketId id = UDP_PACKET_ID_PERFORMANCE_SAMPLE;
    uint32_t size = 0;

    UDPFrameNumber frame_number;
    UDPFrameId frame_id;
    UDPTransformId transform_id;

    UDPPerformanceSampleUnit unit;
    bool log_only;
    double sample = 0.0;

    uint8_t name[UDP_PACKET_PERFORMANCE_SAMPLE_NAME_MAX_LENGTH];
};

struct UDPPacketOverlayText
{
    UDPPacketId id = UDP_PACKET_ID_OVERLAY_TEXT;
    uint32_t size = 0;

    UDPFrameNumber frame_number;
    uint32_t overlay_index;

    uint8_t label[UDP_PACKET_OVERLAY_TEXT_LABEL_MAX_LENGTH];
    uint8_t text[UDP_PACKET_OVERLAY_TEXT_TEXT_MAX_LENGTH];
};

struct UDPPacketOverlayGraph
{
    UDPPacketId id = UDP_PACKET_ID_OVERLAY_GRAPH;
    uint32_t size = 0;

    UDPFrameNumber frame_number;
    uint32_t overlay_index;
    uint32_t sample_count;

    uint8_t label[UDP_PACKET_OVERLAY_GRAPH_LABEL_MAX_LENGTH];
    float samples[UDP_PACKET_OVERLAY_GRAPH_SAMPLE_MAX_COUNT];
};