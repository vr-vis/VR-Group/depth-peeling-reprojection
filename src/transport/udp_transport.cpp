#include "udp_transport.hpp"
#include "udp_packet.hpp"

#include <liblava/lava.hpp>
#include <chrono>

bool UDPTransport::create(uint32_t port_number)
{
    this->server_timer = asio::high_resolution_timer(this->server_context);
    this->server_endpoint = asio::ip::udp::endpoint(asio::ip::udp::v4(), port_number);
    this->server_socket = asio::ip::udp::socket(this->server_context);
    this->server_socket->open(asio::ip::udp::v4());
    this->server_socket->bind(this->server_endpoint);

    this->set_state(TRANSPORT_STATE_WAITING);

    this->receive_datagram();

    this->worker_thread = std::thread([this]()
	{
        this->server_context.run();
    });

    return true;
}

void UDPTransport::destroy()
{
    this->server_context.stop();
    this->worker_thread.join();

    this->client_endpoint.reset();

    if (this->server_socket.has_value())
    {
        this->server_socket->close();
        this->server_socket.reset();
    }

    if (this->server_timer.has_value())
    {
        this->server_timer->cancel();
        this->server_timer.reset();
    }

    for (const Datagram& datagram : this->send_queue)
    {
        this->datagram_pool.release_datagram(datagram);
    }

    for (const Datagram& datagram : this->receive_queue)
    {
        this->datagram_pool.release_datagram(datagram);
    }

    for (const Datagram& datagram : this->error_queue)
    {
        this->datagram_pool.release_datagram(datagram);
    }

    this->send_queue.clear();
    this->receive_queue.clear();
    this->error_queue.clear();

    this->bits_send = 0;
    this->bits_received = 0;
    this->bitrate_send = 0.0;
    this->bitrate_receive = 0.0;

    this->set_state(TRANSPORT_STATE_UNINITIALIZED);
}

bool UDPTransport::wait_connect()
{
    if (this->get_state() != TRANSPORT_STATE_WAITING)
    {
        lava::log()->error("UDP-Transport: Can't wait for connection!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);

    std::chrono::seconds wait_time(UDP_TRANSPORT_CLIENT_CONNECT_TIMEOUT);
    std::chrono::high_resolution_clock::time_point last_time = std::chrono::high_resolution_clock::now();

	while (this->state == TRANSPORT_STATE_WAITING && wait_time.count() > 0)
	{
        this->worker_setup_condition.wait_for(lock, wait_time);

        std::chrono::high_resolution_clock::time_point current_time = std::chrono::high_resolution_clock::now();
        wait_time -= std::chrono::duration_cast<std::chrono::seconds>(current_time - last_time);
        last_time = current_time;
	}

    lock.unlock();

    return this->get_state() == TRANSPORT_STATE_CONNECTED;
}

bool UDPTransport::send_setup_complete(RemoteStrategy remote_strategy, EncoderCodec codec, uint32_t max_frame_ids, float near_plane, float far_plane)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send setup complete packet since not connection is established!");

        return false;
    }

    UDPStereoStrategyId udp_strategy;

    if (!convert_udp_stereo_strategy(remote_strategy, udp_strategy))
    {
        return false;
    }

    UDPCodec udp_codec;

    if (!convert_udp_codec(codec, udp_codec))
    {
        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    UDPPacketSetupComplete* packet = (UDPPacketSetupComplete*)datagram.buffer;
    packet->id = UDP_PACKET_ID_SETUP_COMPLETE;
    packet->size = sizeof(UDPPacketSetupComplete);
    packet->strategy_id = udp_strategy;
    packet->codec = udp_codec;
    packet->max_frame_ids = max_frame_ids;
    packet->near_plane = near_plane;
    packet->far_plane = far_plane;

    this->send_datagram(datagram);

    return true;
}

bool UDPTransport::send_stage_transform(const glm::mat4& stage_transform)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send stage transform packet since not connection is established!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    UDPPacketStageTransform* packet = (UDPPacketStageTransform*)datagram.buffer;
    packet->id = UDP_PACKET_ID_STAGE_TRANSFORM;
    packet->size = sizeof(UDPPacketStageTransform);
    packet->stage_transform = stage_transform;

    this->send_datagram(datagram);

    return true;
}

bool UDPTransport::send_frame_config_nal(FrameId frame_id, const std::span<const uint8_t>& content)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send frame config nal packet since not connection is established!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    uint32_t size = sizeof(UDPPacketFrameConfigNal) + sizeof(uint8_t) * content.size();

    if (size > datagram.size)
    {
        lava::log()->error("UDP-Transport: Frame config nal packet exceeds datagram size!");

        return false;
    }

    UDPPacketFrameConfigNal* packet = (UDPPacketFrameConfigNal*)datagram.buffer;
    packet->id = UDP_PACKET_ID_FRAME_CONFIG_NAL;
    packet->size = size;
    packet->frame_id = frame_id;

    memcpy(datagram.buffer + sizeof(UDPPacketFrameConfigNal), content.data(), sizeof(uint8_t) * content.size());

    this->send_datagram(datagram);

    return true;
}

bool UDPTransport::send_frame_nal(FrameNumber frame_number, FrameId frame_id, TransformId transform_id, const std::span<const uint8_t>& content)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send frame nal packet since not connection is established!");

        return false;
    }

    for (uint32_t offset = 0; offset < content.size();)
    {
        std::unique_lock<std::mutex> lock(this->worker_mutex);
        Datagram datagram = this->datagram_pool.acquire_datagram();
        lock.unlock();

        uint32_t content_size = glm::min(datagram.size - (uint32_t)sizeof(UDPPacketFrameNal), (uint32_t) content.size() - offset);

        UDPPacketFrameNal* packet = (UDPPacketFrameNal*)datagram.buffer;
        packet->id = UDP_PACKET_ID_FRAME_NAL;
        packet->size = sizeof(UDPPacketFrameNal) + content_size;
        packet->frame_number = frame_number;
        packet->frame_id = frame_id;
        packet->transform_id = transform_id;
        packet->payload_offset = offset;
        packet->payload_size = content.size();

        memcpy(datagram.buffer + sizeof(UDPPacketFrameNal), (uint8_t*)content.data() + offset, sizeof(uint8_t) * content_size);

        this->send_datagram(datagram);

        offset += content_size;
    }

    return true;
}

bool UDPTransport::send_frame_metadata(FrameNumber frame_number, const std::span<const uint8_t>& content)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send frame metadata packet since not connection is established!");

        return false;
    }

    for (uint32_t offset = 0; offset < content.size();)
    {
        std::unique_lock<std::mutex> lock(this->worker_mutex);
        Datagram datagram = this->datagram_pool.acquire_datagram();
        lock.unlock();

        uint32_t content_size = glm::min(datagram.size - (uint32_t)sizeof(UDPPacketFrameMetadata), (uint32_t) content.size() - offset);

        UDPPacketFrameMetadata* packet = (UDPPacketFrameMetadata*)datagram.buffer;
        packet->id = UDP_PACKET_ID_FRAME_METADATA;
        packet->size = sizeof(UDPPacketFrameMetadata) + content_size;
        packet->frame_number = frame_number;
        packet->payload_offset = offset;
        packet->payload_size = content.size();

        memcpy(datagram.buffer + sizeof(UDPPacketFrameMetadata), (uint8_t*)content.data() + offset, sizeof(uint8_t) * content_size);

        this->send_datagram(datagram);

        offset += content_size;
    }

    return true;
}

bool UDPTransport::send_overlay_config(bool overlay_enable)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send overlay config packet since not connection is established!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    UDPPacketOverlayConfig* packet = (UDPPacketOverlayConfig*)datagram.buffer;
    packet->id = UDP_PACKET_ID_OVERLAY_CONFIG;
    packet->size = sizeof(UDPPacketOverlayConfig);
    packet->overlay_enable = overlay_enable;

    this->send_datagram(datagram);

    return true;
}

bool UDPTransport::send_overlay_text(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::string& text)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send overlay text packet since not connection is established!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    UDPPacketOverlayText* packet = (UDPPacketOverlayText*)datagram.buffer;
    packet->id = UDP_PACKET_ID_OVERLAY_TEXT;
    packet->size = sizeof(UDPPacketOverlayText);
    packet->frame_number = frame_number;
    packet->overlay_index = overlay_index;
    memset(packet->label, 0, sizeof(packet->label));
    memcpy(packet->label, label.data(), glm::min(sizeof(packet->label), label.size()));
    memset(packet->text, 0, sizeof(packet->text));
    memcpy(packet->text, text.data(), glm::min(sizeof(packet->text), text.size()));
    
    this->send_datagram(datagram);

    return true;
}

bool UDPTransport::send_overlay_graph(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::span<const float>& samples)
{
    if (this->get_state() != TRANSPORT_STATE_CONNECTED)
    {
        lava::log()->error("UDP-Transport: Can't send overlay graph packet since not connection is established!");

        return false;
    }

    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    UDPPacketOverlayGraph* packet = (UDPPacketOverlayGraph*)datagram.buffer;
    packet->id = UDP_PACKET_ID_OVERLAY_GRAPH;
    packet->size = sizeof(UDPPacketOverlayGraph);
    packet->frame_number = frame_number;
    packet->overlay_index = overlay_index;
    packet->sample_count = glm::min((int32_t)samples.size(), UDP_PACKET_OVERLAY_GRAPH_SAMPLE_MAX_COUNT);
    memset(packet->label, 0, sizeof(packet->label));
    memcpy(packet->label, label.data(), glm::min(sizeof(packet->label), label.size()));
    memset(packet->samples, 0, sizeof(packet->samples));
    memcpy(packet->samples, samples.data(), glm::min(sizeof(packet->samples), samples.size_bytes()));
    
    this->send_datagram(datagram);

    return true;
}

void UDPTransport::set_on_setup(OnSetup function)
{
    this->on_setup = std::move(function);
}

void UDPTransport::set_on_projection_change(OnProjectionChange function)
{
    this->on_projection_change = std::move(function);
}

void UDPTransport::set_on_head_transform(OnHeadTransform function)
{
    this->on_head_transform = std::move(function);
}

void UDPTransport::set_on_controller_transform(OnControllerTransform function)
{
    this->on_controller_transform = std::move(function);
}

void UDPTransport::set_on_controller_event(OnControllerEvent function)
{
    this->on_controller_event = std::move(function);
}

void UDPTransport::set_on_controller_button(OnControllerButton function)
{
    this->on_controller_button = std::move(function);
}

void UDPTransport::set_on_controller_thumbstick(OnControllerThumbstick function)
{
    this->on_controller_thumbstick = std::move(function);
}

void UDPTransport::set_on_performance_sample(OnPerformanceSample function)
{
    this->on_performance_sample = std::move(function);
}

void UDPTransport::set_on_transport_error(OnTransportError function)
{
    this->on_transport_error = std::move(function);
}

void UDPTransport::set_max_send_queue_enabled(bool enabled)
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    this->max_send_queue_enabled = enabled;
}

void UDPTransport::set_max_send_queue_size(uint32_t size)
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    this->max_send_queue_size = size;
}

TransportState UDPTransport::get_state()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    TransportState value = this->state;
    lock.unlock();

    return value;
}

double UDPTransport::get_bitrate_send()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    double value = this->bitrate_send;
    lock.unlock();

    return value;
}

double UDPTransport::get_bitrate_receive()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    double value = this->bitrate_receive;
    lock.unlock();

    return value;
}

uint32_t UDPTransport::get_max_send_queue_size()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    uint32_t value = this->max_send_queue_size;
    lock.unlock();

    return value;
}

uint32_t UDPTransport::get_send_queue_size()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    uint32_t bytes = 0;

    for (const Datagram& datagram : this->send_queue)
    {
        const UDPPacketHeader* header = (const UDPPacketHeader*)datagram.buffer;
        bytes += header->size;
    }
    lock.unlock();

    return bytes;
}

uint32_t UDPTransport::get_receive_queue_size()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    uint32_t bytes = 0;

    for (const Datagram& datagram : this->receive_queue)
    {
        const UDPPacketHeader* header = (const UDPPacketHeader*)datagram.buffer;
        bytes += header->size;
    }
    lock.unlock();

    return bytes;
}

bool UDPTransport::is_max_send_queue_enabled()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    bool value = this->max_send_queue_enabled;
    lock.unlock();

    return value;
}

void UDPTransport::set_state(TransportState state)
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    this->state = state;
    lock.unlock();
}

void UDPTransport::check_state()
{
    std::chrono::high_resolution_clock::time_point start_time = std::chrono::high_resolution_clock::now();

    this->server_timer->expires_at(start_time + std::chrono::milliseconds(UDP_TRANSPORT_STATE_CHECK_INTERVAL));
    this->server_timer->async_wait([this, start_time](const asio::error_code& error_code)
    {
        if (error_code)
        {
            lava::log()->error("UDP-Transport: Error during state check! Message: {}", error_code.message());
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);

            return;
        }

        std::chrono::high_resolution_clock::time_point end_time = std::chrono::high_resolution_clock::now();
        double delta_time = (double)std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() / 1000.0; //NOTE: delta_time in seconds

        std::unique_lock<std::mutex> lock(this->worker_mutex);
        this->bitrate_send = ((double)this->bits_send / (double)delta_time) / (1000.0 * 1000.0);
        this->bitrate_receive = ((double)this->bits_received / (double)delta_time) / (1000.0 * 1000.0);

        if (!this->send_active && !this->send_queue.empty())
        {
            this->process_send_queue();
        }
        lock.unlock();

        if (this->bits_received > 0)
        {
            this->inactive_time = 0;
        }

        else
        {
            this->inactive_time += delta_time;
        }

        if (this->inactive_time > UDP_TRANSPORT_CLIENT_TIMEOUT)
        {
            lava::log()->error("UDP-Transport: Client timeout");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);

            return;
        }

        this->bits_send = 0;
        this->bits_received = 0;
        
        this->check_state();
    });
}

void UDPTransport::send_datagram(const Datagram& datagram)
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);

    if (this->max_send_queue_enabled)
    {
        uint32_t bytes = 0;

        for (uint32_t index = 1; index < this->send_queue.size(); index++)
        {
            const Datagram& datagram = this->send_queue[index];
            const UDPPacketHeader* header = (const UDPPacketHeader*)datagram.buffer;
            bytes += header->size;
        }

        while (bytes > this->max_send_queue_size && this->send_queue.size() > 1)
        {
            const Datagram& datagram = this->send_queue[1];
            const UDPPacketHeader* header = (const UDPPacketHeader*)datagram.buffer;
            bytes -= header->size;

            this->datagram_pool.release_datagram(datagram);
            this->send_queue.erase(this->send_queue.begin() + 1);
        }
    }

    this->send_queue.push_back(datagram);
    
    if (!this->send_active)
    {
        this->process_send_queue();
    }
}

void UDPTransport::receive_datagram()
{
    std::unique_lock<std::mutex> lock(this->worker_mutex);
    Datagram datagram = this->datagram_pool.acquire_datagram();
    lock.unlock();

    this->server_socket->async_receive_from(asio::buffer(datagram.buffer, datagram.size), this->receive_endpoint, [this, datagram](const asio::error_code& error_code, std::size_t bytes)
    {
        if (error_code)
		{
            lava::log()->error("UDP-Transport: Error during datagram receive! Message: {}", error_code.message());
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);

            this->error_queue.push_back(datagram);

            return;
		}

        std::unique_lock<std::mutex> lock(this->worker_mutex);
        this->bits_received += bytes * 8;
        lock.unlock();

        if (!this->client_endpoint.has_value())
        {
            lava::log()->debug("UDP-Transport: Client connected");

            this->client_endpoint = this->receive_endpoint;
            this->check_state();
        }

        else if (this->receive_endpoint != this->client_endpoint)
        {
            lava::log()->error("UDP-Transport: Client already connected!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);

            this->error_queue.push_back(datagram);

            return;
        }

        const UDPPacketHeader* packet = (const UDPPacketHeader*)datagram.buffer;

        switch (packet->id)
		{
        case UDP_PACKET_ID_SETUP:
        case UDP_PACKET_ID_PROJECTION_CHANGE:
        case UDP_PACKET_ID_HEAD_TRANSFORM:
        case UDP_PACKET_ID_CONTROLLER_TRANSFORM:  
        case UDP_PACKET_ID_CONTROLLER_EVENT:    
        case UDP_PACKET_ID_CONTROLLER_BUTTON:     
        case UDP_PACKET_ID_CONTROLLER_THUMBSTICK:
        case UDP_PACKET_ID_PERFORMANCE_SAMPLE:
            this->receive_queue.push_back(datagram);
            break;
        case UDP_PACKET_ID_SETUP_COMPLETE:
            lava::log()->error("UDP-Transport: Received setup complete packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_FRAME_CONFIG_NAL:
            lava::log()->error("UDP-Transport: Received frame config nal packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_FRAME_NAL:
            lava::log()->error("UDP-Transport: Received frame nal packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_FRAME_METADATA:
            lava::log()->error("UDP-Transport: Received frame metadata packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_OVERLAY_CONFIG:
            lava::log()->error("UDP-Transport: Received frame overlay config packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_OVERLAY_TEXT:
            lava::log()->error("UDP-Transport: Received frame overlay text packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        case UDP_PACKET_ID_OVERLAY_GRAPH:
            lava::log()->error("UDP-Transport: Received frame overlay graph packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        default:
            lava::log()->error("UDP-Transport: Received unknown packet!");
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);
            this->error_queue.push_back(datagram);
            return;
        }

        this->process_receive_queue();
        this->receive_datagram();
    });
}

void UDPTransport::process_send_queue()
{
    if (this->send_queue.empty())
    {
        return;
    }

    if (!this->client_endpoint.has_value())
    {
        return;
    }

    Datagram datagram = this->send_queue.front();
    const UDPPacketHeader* header = (const UDPPacketHeader*)datagram.buffer;

    this->server_socket->async_send_to(asio::buffer(datagram.buffer, header->size), this->client_endpoint.value(), [this, datagram](const asio::error_code& error_code, std::size_t bytes)
    {
        if (error_code)
        {
            lava::log()->error("UDP-Transport: Error during send of datagram! Message: {}", error_code.message());
            this->on_transport_error();
            this->set_state(TRANSPORT_STATE_ERROR);

            this->error_queue.push_back(datagram);

            return;
        }

        std::unique_lock<std::mutex> lock(this->worker_mutex);
        this->send_active = false;
        this->bits_send += bytes * 8;

        this->send_queue.erase(this->send_queue.begin());
        this->datagram_pool.release_datagram(datagram);

        this->process_send_queue();
        lock.unlock();
    });

    this->send_active = true;
}

void UDPTransport::process_receive_queue()
{
    if (this->receive_queue.empty())
    {
        return;
    }

    if (this->get_state() == TRANSPORT_STATE_WAITING)
    {
        const Datagram& datagram = this->receive_queue.back();
        const UDPPacketHeader* packet = (const UDPPacketHeader*)datagram.buffer;

        if (packet->id == UDP_PACKET_ID_SETUP)
        {
            this->set_state(TRANSPORT_STATE_CONNECTED);
            this->parse_setup(datagram);

            std::unique_lock<std::mutex> lock(this->worker_mutex);
            this->datagram_pool.release_datagram(datagram);
            lock.unlock();

            this->receive_queue.pop_back();
            this->worker_setup_condition.notify_all();
        }
    }

    if (this->get_state() == TRANSPORT_STATE_CONNECTED)
    {
        for (const Datagram& datagram : this->receive_queue)
        {
            const UDPPacketHeader* packet = (const UDPPacketHeader*)datagram.buffer;

            switch (packet->id)
            {
            case UDP_PACKET_ID_SETUP:
                this->parse_setup(datagram);
                break;
            case UDP_PACKET_ID_PROJECTION_CHANGE:
                this->parse_projection_change(datagram);
                break;
            case UDP_PACKET_ID_HEAD_TRANSFORM:
                this->parse_head_transform(datagram);
                break;
            case UDP_PACKET_ID_CONTROLLER_TRANSFORM:
                this->parse_controller_transform(datagram);
                break;
            case UDP_PACKET_ID_CONTROLLER_EVENT:
                this->parse_controller_event(datagram);
                break;
            case UDP_PACKET_ID_CONTROLLER_BUTTON:
                this->parse_controller_button(datagram);
                break;
            case UDP_PACKET_ID_CONTROLLER_THUMBSTICK:
                this->parse_controller_thumbstick(datagram);
                break;
            case UDP_PACKET_ID_PERFORMANCE_SAMPLE:
                this->parse_performance_sample(datagram);
                break;
            default:
                lava::log()->error("UDP-Transport: Unknown packet during receive queue process!");
                this->on_transport_error();
                this->set_state(TRANSPORT_STATE_ERROR);
                return;
            }

            std::unique_lock<std::mutex> lock(this->worker_mutex);
            this->datagram_pool.release_datagram(datagram);
            lock.unlock();
        }

        this->receive_queue.clear();
    }
}

void UDPTransport::parse_setup(const Datagram& datagram)
{
    UDPPacketSetup* packet = (UDPPacketSetup*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_SETUP)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for setup packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_setup(packet->resolution);
}

void UDPTransport::parse_projection_change(const Datagram& datagram)
{
    UDPPacketProjectionChange* packet = (UDPPacketProjectionChange*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_PROJECTION_CHANGE)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for projection change packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_projection_change(packet->left_eye_projection, packet->right_eye_projection, packet->left_head_to_eye, packet->right_head_to_eye);
}

void UDPTransport::parse_head_transform(const Datagram& datagram)
{
    UDPPacketHeadTransform* packet = (UDPPacketHeadTransform*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_HEAD_TRANSFORM)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for head transform packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_head_transform(packet->transform_id, packet->head_transform);
}

void UDPTransport::parse_controller_transform(const Datagram& datagram)
{
    UDPPacketControllerTransform* packet = (UDPPacketControllerTransform*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_CONTROLLER_TRANSFORM)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for controller transform packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    Controller controller;
    
    if (!convert_udp_controller(packet->controller_id, controller))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_controller_transform(controller, packet->controller_transform);
}

void UDPTransport::parse_controller_event(const Datagram& datagram)
{
    UDPPacketControllerEvent* packet = (UDPPacketControllerEvent*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_CONTROLLER_EVENT)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for controller event packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    Controller controller;
    ControllerState controller_state;

    if (!convert_udp_controller(packet->controller_id, controller))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    if (!convert_udp_controller_state(packet->controller_state, controller_state))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_controller_event(controller, controller_state);
}

void UDPTransport::parse_controller_button(const Datagram& datagram)
{
    UDPPacketControllerButton* packet = (UDPPacketControllerButton*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_CONTROLLER_BUTTON)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for controller button packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    Controller controller;
    Button button;
    ButtonState button_state;

    if (!convert_udp_controller(packet->controller_id, controller))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    if (!convert_udp_button(packet->button_id, button))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    if (!convert_udp_button_state(packet->button_state, button_state))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_controller_button(controller, button, button_state);
}

void UDPTransport::parse_controller_thumbstick(const Datagram& datagram)
{
    UDPPacketControllerThumbstick* packet = (UDPPacketControllerThumbstick*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_CONTROLLER_THUMBSTICK)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for controller thumbstick packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    Controller controller;

    if (!convert_udp_controller(packet->controller_id, controller))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    this->on_controller_thumbstick(controller, packet->thumbstick);
}

void UDPTransport::parse_performance_sample(const Datagram& datagram)
{
    UDPPacketPerformanceSample* packet = (UDPPacketPerformanceSample*)datagram.buffer;

    if (packet->id != UDP_PACKET_ID_PERFORMANCE_SAMPLE)
    {
        lava::log()->error("UDP-Transport: Wrong packet id for performance sample packet!");
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    std::optional<FrameNumber> frame_number;
    std::optional<FrameId> frame_id;
    std::optional<TransformId> transform_id;

    if (packet->frame_number != UDP_UNDEFINED_FRAME_NUMBER)
    {
        frame_number = packet->frame_number;
    }

    if (packet->frame_id != UDP_UNDEFINED_FRAME_ID)
    {
        frame_id = packet->frame_id;
    }

    if (packet->transform_id != UDP_UNDEFINED_TRANSFORM_ID)
    {
        transform_id = packet->transform_id;
    }

    Unit unit;

    if (!convert_udp_performance_sample_unit(packet->unit, unit))
    {
        this->on_transport_error();
        this->set_state(TRANSPORT_STATE_ERROR);

        return;
    }

    uint32_t name_length = strnlen((const char*)packet->name, sizeof(packet->name));
    std::string name((const char*)packet->name, name_length);

    this->on_performance_sample(frame_number, frame_id, transform_id, unit, packet->log_only, packet->sample, name);
}