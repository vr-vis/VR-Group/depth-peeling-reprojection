#pragma once
#include <glm/glm.hpp>
#include <asio.hpp>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <optional>

#include "transport.hpp"
#include "utility/datagram.hpp"

#define UDP_TRANSPORT_STATE_CHECK_INTERVAL   10   //NOTE: In milliseconds
#define UDP_TRANSPORT_CLIENT_CONNECT_TIMEOUT 60   //NOTE: In seconds
#define UDP_TRANSPORT_CLIENT_TIMEOUT         20   //NOTE: In seconds

class UDPTransport : public Transport
{
public:
    UDPTransport() = default;

    bool create(uint32_t port_number) override;
    void destroy() override;
    bool wait_connect() override;

    bool send_setup_complete(RemoteStrategy remote_strategy, EncoderCodec codec, uint32_t max_frame_ids, float near_plane, float far_plane) override;
    bool send_stage_transform(const glm::mat4& stage_transform) override;
    bool send_frame_config_nal(FrameId frame_id, const std::span<const uint8_t>& content) override;
    bool send_frame_nal(FrameNumber frame_number, FrameId frame_id, TransformId transform_id, const std::span<const uint8_t>& content) override;
    bool send_frame_metadata(FrameNumber frame_number, const std::span<const uint8_t>& content) override;
    bool send_overlay_config(bool overlay_enable) override;
    bool send_overlay_text(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::string& text) override;
    bool send_overlay_graph(FrameNumber frame_number, uint32_t overlay_index, const std::string& label, const std::span<const float>& samples) override;

    void set_on_setup(OnSetup function) override;
    void set_on_projection_change(OnProjectionChange function) override;
    void set_on_head_transform(OnHeadTransform function) override;
    void set_on_controller_transform(OnControllerTransform function) override;
    void set_on_controller_event(OnControllerEvent function) override;
    void set_on_controller_button(OnControllerButton function) override;
    void set_on_controller_thumbstick(OnControllerThumbstick function) override;
    void set_on_performance_sample(OnPerformanceSample function) override;
    void set_on_transport_error(OnTransportError function) override;

    void set_max_send_queue_enabled(bool enabled) override;
    void set_max_send_queue_size(uint32_t size) override;

    TransportState get_state() override;
    double get_bitrate_send() override;
    double get_bitrate_receive() override;
    uint32_t get_max_send_queue_size() override;
    uint32_t get_send_queue_size() override;
    uint32_t get_receive_queue_size() override;
    bool is_max_send_queue_enabled() override;

private:
    void set_state(TransportState state);
    void check_state();

    void send_datagram(const Datagram& datagram);
    void receive_datagram();

    void process_send_queue();              //NOTE: Protected by worker_mutex
    void process_receive_queue();

    void parse_setup(const Datagram& datagram);
    void parse_projection_change(const Datagram& datagram);
    void parse_head_transform(const Datagram& datagram);
    void parse_controller_transform(const Datagram& datagram);
    void parse_controller_event(const Datagram& datagram);
    void parse_controller_button(const Datagram& datagram);
    void parse_controller_thumbstick(const Datagram& datagram);
    void parse_performance_sample(const Datagram& datagram);

private:
    std::thread worker_thread;
    std::mutex worker_mutex;
    std::condition_variable worker_setup_condition;

    asio::io_context server_context;
    std::optional<asio::high_resolution_timer> server_timer;
    asio::ip::udp::endpoint server_endpoint;
    std::optional<asio::ip::udp::socket> server_socket;
    asio::ip::udp::endpoint receive_endpoint;
    std::optional<asio::ip::udp::endpoint> client_endpoint;

    DatagramPool datagram_pool;                           //NOTE: Protected by worker_mutex
    std::vector<Datagram> send_queue;                     //NOTE: Protected by worker_mutex
    std::vector<Datagram> receive_queue;                  //NOTE: Owned by worker_thread and accessible after the thread has been stoped
    std::vector<Datagram> error_queue;                    //NOTE: Owned by worker_thread and accessible after the thread has been stoped
    
    TransportState state = TRANSPORT_STATE_UNINITIALIZED; //NOTE: Protected by worker_mutex
    uint32_t bits_send = 0;                               //NOTE: Owned by worker_thread
    uint32_t bits_received = 0;                           //NOTE: Owned by worker_thread
    double bitrate_send = 0.0;                            //NOTE: Protected by worker_mutex
    double bitrate_receive = 0.0;                         //NOTE: Protected by worker_mutex
    bool max_send_queue_enabled = false;                  //NOTE: Protected by worker_mutex
    uint32_t max_send_queue_size = DATAGRAM_SIZE * 128;   //NOTE: Protected by worker_mutex
    bool send_active = false;                             //NOTE: Protected by worker_mutex
    double inactive_time = 0.0;                           //NOTE: Owned by worker_thread

    OnSetup on_setup;
    OnProjectionChange on_projection_change;
    OnHeadTransform on_head_transform;
    OnControllerTransform on_controller_transform;
    OnControllerEvent on_controller_event;
    OnControllerButton on_controller_button;
    OnControllerThumbstick on_controller_thumbstick;
    OnPerformanceSample on_performance_sample;
    OnTransportError on_transport_error;
};