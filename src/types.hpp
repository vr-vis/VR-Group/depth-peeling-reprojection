#pragma once
#include <span>
#include <cstdint>

typedef uint32_t            TransformId;
typedef uint32_t            FrameId;
typedef uint32_t            FrameNumber;
typedef std::span<uint8_t>  FrameMetadata;

// Define the frame ids for the left and right eye in case of an non-remote headset.
enum Eye
{
    EYE_LEFT,
    EYE_RIGHT
};

enum Controller
{
    CONTROLLER_LEFT,
    CONTROLLER_RIGHT,
    CONTROLLER_MAX_COUNT
};

enum ControllerState
{
    CONTROLLER_STATE_ATTACHED,
    CONTROLLER_STATE_DETACHED
};

enum Button
{
    BUTTON_A,
    BUTTON_B,
    BUTTON_X,
    BUTTON_Y,
    BUTTON_TRIGGER,
    BUTTON_MAX_COUNT
};

enum ButtonState
{
    BUTTON_STATE_PRESSED,
    BUTTON_STATE_RELEASED
};

enum RemoteStrategy
{
    REMOTE_STRATEGY_DEBUG,
    REMOTE_STRATEGY_NATIVE
};

enum Unit
{
    UNIT_UNDEFINED,
    UNIT_HOURS,
    UNIT_MINUTES,
    UNIT_SECONDS,
    UNIT_MILLISECONDS,
    UNIT_MIRCOSECONDS,
    UNIT_BITS,
    UNIT_KBITS,                 //NOTE: Base 1000
    UNIT_MBITS,                 //NOTE: Base 1000
    UNIT_KIBITS,                //NOTE: Base 1024
    UNIT_MIBITS,                //NOTE: Base 1024
    UNIT_BYTES,
    UNIT_KBYTES,                //NOTE: Base 1000
    UNIT_MBYTES,                //NOTE: Base 1000
    UNIT_KIBYTES,               //NOTE: Base 1024
    UNIT_MIBYTES,               //NOTE: Base 1024
    UNIT_UNDEFINED_PER_SECOND,
    UNIT_BITS_PER_SECOND,
    UNIT_KBITS_PER_SECOND,      //NOTE: Base 1000
    UNIT_MBITS_PER_SECOND,      //NOTE: Base 1000
};