#include "command_parser.hpp"
#include <iostream>

bool CommandParser::parse_command(const argh::parser& cmd_line)
{
    for (const std::string& flag : cmd_line.flags())
    {
        if (flag == "benchmark")
        {
            this->benchmark = true;
        }

        else if (flag == "show-companion-window")
        {
            this->show_companion_window = true;
        }

        else if (flag == "write-frames")
        {
            this->write_frames = true;
        }

        else if (flag == "write-video")
        {
            this->write_video = true;
        }

        else if (flag == "disable-indirect-lighting")
        {
            this->disable_indirect_lighting = true;
        }

        else if (flag == "disable-shadows")
        {
            this->disable_shadows = true;
        }

        else if (flag == "help")
        {
            this->show_help();

            return false;
        }

        else
        {
            std::cout << "Unknown command line flag '" << flag << "'. Use option --help to get more information." << std::endl;

            return false;
        }
    }

    for (const std::pair<std::string, std::string>& parameter : cmd_line.params())
    {
        if (parameter.first == "sky-sphere")
        {
            std::string path = parameter.second;
            std::size_t extension_begin = path.find_last_of('.');

            if (extension_begin == std::string::npos)
            {
                std::cout << "Invalid file format for parameter 'sky-sphere'. Only .hdr images supported. Use option --help to get more information." << std::endl;

                return false;
            }

            std::string extension = path.substr(extension_begin);

            if (extension != ".hdr")
            {
                std::cout << "Invalid file format for parameter 'sky-sphere'. Only .hdr images supported. Use option --help to get more information." << std::endl;

                return false;
            }

            this->sky_sphere_path = path;
        }

        else if (parameter.first == "sky-sphere-intensity")
        {
            this->sky_sphere_intensity = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "scene-exposure")
        {
            this->scene_exposure = std::atof(parameter.second.c_str());
        }

        else if (parameter.first == "scene-unit")
        {
            if (!this->set_scene_unit(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "scene-orientation")
        {
            if (!this->set_scene_orientation(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "scene-uvs")
        {
            if (!this->set_scene_uvs(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "headset")
        {
            if (!this->set_headset(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "stereo-strategy")
        {
            if (!this->set_stero_strategy(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "transport")
        {
            if (!this->set_transport(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "encoder")
        {
            if (!this->set_encoder(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "encoder-codec")
        {
            if(!this->set_encoder_codec(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "encoder-mode")
        {
            if (!this->set_encoder_mode(parameter.second))
            {
                return false;
            }
        }

        else if (parameter.first == "encoder-input-rate")
        {
            this->encoder_input_rate = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "encoder-key-rate")
        {
            this->encoder_key_rate = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "encoder-frame-rate")
        {
            this->encoder_frame_rate = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "encoder-quality")
        {
            this->encoder_quality = (float)std::atof(parameter.second.c_str());
        }

        else if (parameter.first == "encoder-bitrate")
        {
            this->encoder_bitrate = (float)std::atof(parameter.second.c_str());
        }

        else if (parameter.first == "animation")
        {
            this->animation_name = parameter.second;
        }

        else if (parameter.first == "animation-index")
        {
            this->animation_index = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "camera")
        {
            this->camera_name = parameter.second;
        }

        else if (parameter.first == "camera-index")
        {
            this->camera_index = std::atoi(parameter.second.c_str());
        }

        else if (parameter.first == "fix-update-rate")
        {
            this->update_rate = (float)std::atof(parameter.second.c_str());
        }

        else
        {
            std::cout << "Unknown command line parameter '" << parameter.first << "'. Use option --help to get more information." << std::endl;
       
            return false;
        }
    }

    if (cmd_line.pos_args().size() >= 2)
    {
        this->scene_path = cmd_line.pos_args().back();
    }

    else
    {
        std::cout << "No scene file selected. Use option --help to get more information." << std::endl;

        return false;
    }

    if (this->benchmark)
    {
        if (!this->animation_name.has_value() && !this->animation_index.has_value())
        {
            std::cout << "No animation selected for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->animation_name.has_value() && this->animation_index.has_value())
        {
            std::cout << "Parameter 'animation' and 'animation-index' are mutually exclusive. Use option --help to get more information." << std::endl;

            return false;
        }

        if (!this->camera_name.has_value() && !this->camera_index.has_value())
        {
            std::cout << "No camera selected for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->camera_name.has_value() && this->camera_index.has_value())
        {
            std::cout << "Parameter 'camera' and 'camera-index' are mutually exclusive. Use option --help to get more information." << std::endl;

            return false;
        }

        if (!this->update_rate.has_value()) 
        {
            this->update_rate = 60.0f;
        }
    }

    else
    {
        if (this->animation_name.has_value())
        {
            std::cout << "Parameter 'animation' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->animation_index.has_value())
        {
            std::cout << "Parameter 'animation-index' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->camera_name.has_value())
        {
            std::cout << "Parameter 'camera' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->camera_index.has_value())
        {
            std::cout << "Parameter 'camera-index' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->update_rate.has_value())
        {
            std::cout << "Parameter 'fixed-update-rate' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }

        if (this->write_frames)
        {
            std::cout << "Parameter 'write-frames' only allowed for benchmark. Use option --help to get more information." << std::endl;

            return false;
        }
    }

    if (this->encoder == ENCODER_TYPE_VULKAN && this->encoder_codec == ENCODER_CODEC_H265)
    {
        std::cout << "Configuration 'encoder = vulkan' and 'encoder-codec = h265' not supported. Use option --help to get more information" << std::endl;

        return false;
    }

    return true;
}

HeadsetType CommandParser::get_headset() const
{
    return this->headset;
}

StereoStrategyType CommandParser::get_stereo_strategy() const
{
    return this->stereo_strategy;
}

TransportType CommandParser::get_transport() const
{
    return this->transport;
}

EncoderType CommandParser::get_encoder() const
{
    return this->encoder;
}

EncoderCodec CommandParser::get_encoder_codec() const
{
    return this->encoder_codec;
}

SceneUnit CommandParser::get_scene_unit() const
{
    return this->scene_unit;
}

SceneOrientation CommandParser::get_scene_orientation() const
{
    return this->scene_orientation;
}

SceneUVs CommandParser::get_scene_uvs() const
{
    return this->scene_uvs;
}

const std::string& CommandParser::get_scene_path() const
{
    return this->scene_path;
}

std::optional<std::string> CommandParser::get_sky_sphere_path() const
{
    return this->sky_sphere_path;
}

float CommandParser::get_scene_exposure() const
{
    return this->scene_exposure;
}

float CommandParser::get_sky_sphere_intensity() const
{
    return this->sky_sphere_intensity;
}

std::optional<std::string> CommandParser::get_animation_name() const
{
    return this->animation_name;
}

std::optional<uint32_t> CommandParser::get_animation_index() const
{
    return this->animation_index;
}

std::optional<std::string> CommandParser::get_camera_name() const
{
    return this->camera_name;
}

std::optional<uint32_t> CommandParser::get_camera_index() const
{
    return this->camera_index;
}

std::optional<float> CommandParser::get_update_rate() const
{
    return this->update_rate;
}

std::optional<EncoderMode> CommandParser::get_encoder_mode() const
{
    return this->encoder_mode;
}

std::optional<uint32_t> CommandParser::get_encoder_input_rate() const
{
    return this->encoder_input_rate;
}

std::optional<uint32_t> CommandParser::get_encoder_key_rate() const
{
    return this->encoder_key_rate;
}

std::optional<uint32_t> CommandParser::get_encoder_frame_rate() const
{
    return this->encoder_frame_rate;
}

std::optional<float> CommandParser::get_encoder_quality() const
{
    return this->encoder_quality;
}

std::optional<float> CommandParser::get_encoder_bitrate() const
{
    return this->encoder_bitrate;
}

bool CommandParser::should_benchmark() const
{
    return this->benchmark;
}

bool CommandParser::should_show_companion_window() const
{
    return this->show_companion_window;
}

bool CommandParser::should_write_frames() const
{
    return this->write_frames;
}

bool CommandParser::should_write_video() const
{
    return this->write_video;
}

bool CommandParser::should_disable_indirect_lighting() const
{
    return this->disable_indirect_lighting;
}

bool CommandParser::should_disbale_shadows() const
{
    return this->disable_shadows;
}

void CommandParser::show_help()
{
    std::cout << "Usage:" << std::endl;
    std::cout << "   depth-peeling-reprojection.exe [Options] [Scene File]" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "   --sky-sphere={file}                  The sky sphere that should be shown. The given file has to be a .hdr file." << std::endl;
    std::cout << "                                        If no file is specified, sky stays black." << std::endl;
    std::cout << "   --sky-sphere-intensity={intensity}   The value with which the sky sphere is multiplied." << std::endl;
    std::cout << "                                        The default value is 1.0." << std::endl;
    std::cout << "   --scene-exposure={exposure}          The exposure value that should be used during rendering." << std::endl;
    std::cout << "                                        The default value is 1.0." << std::endl;
    std::cout << "   --scene-unit={unit}                  The unit in which the geometry of the scene is defined." << std::endl;
    std::cout << "                                        Options: meters (default), centimeters" << std::endl;
    std::cout << "   --scene-orientation={orientation}    The coordinate system in which the geometry of the scene is defined." << std::endl;
    std::cout << "                                        Options: right-handed (default), left-handed" << std::endl;
    std::cout << "   --scene-uvs={uvs}                    The operation that is applied to the uvs of the scene during loading." << std::endl;
    std::cout << "                                        Options: y-flip (default), unchanged" << std::endl;
    std::cout << "   --headset={headset}                  The headset that should be used." << std::endl;
    std::cout << "                                        Options: emulated (default), openvr, openxr, remote" << std::endl;
    std::cout << "   --stereo-strategy={strategy}         The stereo strategy that should be used." << std::endl;
    std::cout << "                                        Options: native-forward (default), native-deferred, multi-view, dpr" << std::endl;
    std::cout << "   --transport={transport}              The transport method that should be used." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "                                        Options: udp (default)" << std::endl;
    std::cout << "   --encoder={encoder}                  The encoder that should be used when a remote headset is used." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "                                        Options: vulkan (default), nvidia" << std::endl;
    std::cout << "   --encoder-codec={codec}              The codec that should be used by the encoder." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "                                        The vulkan encoder only supports h264 encoding." << std::endl;
    std::cout << "                                        Options: h264 (default), h265" << std::endl;
    std::cout << "   --encoder-mode={mode}                The mode which determins the quality of the encoded images." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "                                        Options: constant-bitrate (default), constant-quality" << std::endl;
    std::cout << "   --encoder-input-rate={rate}          The rate with which images are submitted to the encoders. The default value is 90 hz." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --encoder-key-rate={rate}            The rate with which the endoder inserts key-frames. The default value is 90 frames." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --encoder-frame-rate={rate}          The frame rate which is used to determine the quality when constant bitrate is used. The default value is 90 hz." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --encoder-quality={quailty}          The quality that is used in constant quality mode." << std::endl;
    std::cout << "                                        A value of 0.0 specifies the lowest quality and a value of 1.0 specifies the highest quaility. The default value is 0.0." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --encoder-bitrate={bitrate}          The bitrate which should be used in constant bitrate mode. The default value is 25.0 Mbit/s." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --benchmark                          Play animation once and close program after completion." << std::endl;
    std::cout << "                                        If not set, the application runs indefinitely and the interface is enabled." << std::endl;
    std::cout << "   --animation={animation_name}         The name of the animation that should be played." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --animation-index={animation_index}  The index of the animation that should be played." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --camera={camera_name}               The name of the camera that should be used." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --camera-index={camera_index}        The index of the camera that should be used." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --fix-update-rate={update_rate}      The animation update rate in hz that should be used when benchmarking." << std::endl;
    std::cout << "                                        If the update rate is not set during banchmark, a default update rate of 60 hz is used." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --show-companion-window              Show companion window. If not set, writes to the companion window are ignored." << std::endl;
    std::cout << "   --write-frames                       Write frames to files. If not set, frames will not be written to files." << std::endl;
    std::cout << "                                        This parameter is only allowed during a benchmark." << std::endl;
    std::cout << "   --write-video                        Write video stream to file." << std::endl;
    std::cout << "                                        This parameter should only be used when the parameter headset is set to remote." << std::endl;
    std::cout << "   --disable-indirect-lighting          Disable indirect lighting and the allocation and computation of an indirect light cache." << std::endl;
    std::cout << "   --disable-shadows                    Disable shadows and the allocation and computation of shadow maps." << std::endl;
    std::cout << "   --help                               Show help information." << std::endl;
}

bool CommandParser::set_headset(const std::string& name)
{
    if (name == "emulated")
    {
        this->headset = HEADSET_TYPE_EMULATED;
    }

    else if (name == "openvr")
    {
        this->headset = HEADSET_TYPE_OPEN_VR;
    }

    else if (name == "openxr")
    {
        this->headset = HEADSET_TYPE_OPEN_XR;
    }

    else if (name == "remote")
    {
        this->headset = HEADSET_TYPE_REMOTE;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'headset'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_stero_strategy(const std::string& name)
{
    if (name == "native-forward")
    {
        this->stereo_strategy = STEREO_STRATEGY_TYPE_NATIVE_FORWARD;
    }

    else if (name == "native-deferred")
    {
        this->stereo_strategy = STEREO_STRATEGY_TYPE_NATIVE_DEFERRED;
    }

    else if (name == "multi-view")
    {
        this->stereo_strategy = STEREO_STRATEGY_TYPE_MULTI_VIEW;
    }

    else if (name == "dpr")
    {
        this->stereo_strategy = STEREO_STRATEGY_TYPE_DEPTH_PEELING_REPROJECTION;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'stereo-strategy'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_transport(const std::string& name)
{
    if (name == "udp")
    {
        this->transport = TRANSPORT_TYPE_UDP;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'transport'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_encoder(const std::string& name)
{
    if (name == "vulkan")
    {
        this->encoder = ENCODER_TYPE_VULKAN;
    }

    else if (name == "nvidia")
    {
        this->encoder = ENCODER_TYPE_NVIDIA;
    }

    else
    {
        std::cout << "Invalid option set of parameter 'encoder'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_encoder_codec(const std::string& name)
{
    if (name == "h264")
    {
        this->encoder_codec = ENCODER_CODEC_H264;
    }

    else if(name == "h265")
    {
        this->encoder_codec = ENCODER_CODEC_H265;
    }

    else
    {
        std::cout << "Invalid option set of parameter 'encoder-codec'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_encoder_mode(const std::string& name)
{
    if (name == "constant-quality")
    {
        this->encoder_mode = ENCODER_MODE_CONSTANT_QUALITY;
    }

    else if (name == "constant-bitrate")
    {
        this->encoder_mode = ENCODER_MODE_CONSTANT_BITRATE;
    }

    else
    {
        std::cout << "Invalid option set of parameter 'encoder-mode'. Use option --help to get more information." << std::endl;

        return false;
    }

    return true;
}

bool CommandParser::set_scene_unit(const std::string& name)
{
    if (name == "meters")
    {
        this->scene_unit = SCENE_UNIT_METERS;
    }

    else if (name == "centimeters")
    {
        this->scene_unit = SCENE_UNIT_CENTIMETERS;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'scene-unit'. Use option --help to get more information." << std::endl;
    
        return false;
    }

    return true;
}

bool CommandParser::set_scene_orientation(const std::string& name)
{
    if (name == "right-handed")
    {
        this->scene_orientation = SCENE_ORIENTATION_RIGHT_HANDED;
    }

    else if (name == "left-handed")
    {
        this->scene_orientation = SCENE_ORIENTATION_LEFT_HANDED;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'scene-orientation'. Use option --help to get more information." << std::endl;
    
        return false;
    }

    return true;
}

bool CommandParser::set_scene_uvs(const std::string& name)
{
    if (name == "y-flip")
    {
        this->scene_uvs = SCENE_UVS_Y_FLIP;
    }

    else if (name == "unchanged")
    {
        this->scene_uvs = SCENE_UVS_UNCHANGED;
    }

    else
    {
        std::cout << "Invalid option set for parameter 'scene-uvs'. Use option --help to get more information." << std::endl;
    
        return false;
    }

    return true;
}