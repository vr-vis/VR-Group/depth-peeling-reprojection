/*
  The command parser is used for the parsing of the application startup parameters.
  Besides that it also defines the default headset type as well as the default stereo strategy type.
*/

#pragma once
#include <liblava/lava.hpp>
#include <optional>
#include <string>

#include "../scene.hpp"
#include "headset/headset.hpp"
#include "strategy/stereo_strategy.hpp"
#include "transport/transport.hpp"
#include "encoder/encoder.hpp"

class CommandParser
{
public:
    CommandParser() = default;

    bool parse_command(const argh::parser& cmd_line);

    HeadsetType get_headset() const;
    StereoStrategyType get_stereo_strategy() const;
    TransportType get_transport() const;
    EncoderType get_encoder() const;
    EncoderCodec get_encoder_codec() const;
    SceneUnit get_scene_unit() const;
    SceneOrientation get_scene_orientation() const;
    SceneUVs get_scene_uvs() const;

    const std::string& get_scene_path() const;
    std::optional<std::string> get_sky_sphere_path() const;
    float get_scene_exposure() const;
    float get_sky_sphere_intensity() const;

    std::optional<std::string> get_animation_name() const;
    std::optional<uint32_t> get_animation_index() const;

    std::optional<std::string> get_camera_name() const;
    std::optional<uint32_t> get_camera_index() const;

    std::optional<float> get_update_rate() const;

    std::optional<EncoderMode> get_encoder_mode() const;
    std::optional<uint32_t> get_encoder_input_rate() const;
    std::optional<uint32_t> get_encoder_key_rate() const;
    std::optional<uint32_t> get_encoder_frame_rate() const;
    std::optional<float> get_encoder_quality() const;
    std::optional<float> get_encoder_bitrate() const;

    bool should_benchmark() const;
    bool should_show_companion_window() const;
    bool should_write_frames() const;
    bool should_write_video() const;
    bool should_disable_indirect_lighting() const;
    bool should_disbale_shadows() const;

private:
    void show_help();

    bool set_headset(const std::string& name);
    bool set_stero_strategy(const std::string& name);
    bool set_transport(const std::string& name);
    bool set_encoder(const std::string& name);
    bool set_encoder_codec(const std::string& name);
    bool set_encoder_mode(const std::string& name);
    bool set_scene_unit(const std::string& name);
    bool set_scene_orientation(const std::string& name);
    bool set_scene_uvs(const std::string& name);

private:
    HeadsetType headset = HEADSET_TYPE_EMULATED;
    StereoStrategyType stereo_strategy = STEREO_STRATEGY_TYPE_NATIVE_FORWARD;
    TransportType transport = TRANSPORT_TYPE_UDP;
    EncoderType encoder = ENCODER_TYPE_VULKAN;
    EncoderCodec encoder_codec = ENCODER_CODEC_H264;
    SceneUnit scene_unit = SCENE_UNIT_METERS;
    SceneOrientation scene_orientation = SCENE_ORIENTATION_RIGHT_HANDED;
    SceneUVs scene_uvs = SCENE_UVS_Y_FLIP;

    std::string scene_path = "";
    std::optional<std::string> sky_sphere_path;
    float scene_exposure = 1.0f;
    float sky_sphere_intensity = 1.0f;

    std::optional<std::string> animation_name;
    std::optional<uint32_t> animation_index;

    std::optional<std::string> camera_name;
    std::optional<uint32_t> camera_index;

    std::optional<float> update_rate;

    std::optional<EncoderMode> encoder_mode;
    std::optional<uint32_t> encoder_input_rate;
    std::optional<uint32_t> encoder_key_rate;
    std::optional<uint32_t> encoder_frame_rate;
    std::optional<float> encoder_quality;
    std::optional<float> encoder_bitrate;

    bool benchmark = false;
    bool show_companion_window = false;
    bool write_frames = false;
    bool write_video = false;
    bool disable_indirect_lighting = false;
    bool disable_shadows = false;
};