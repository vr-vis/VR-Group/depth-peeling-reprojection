#include "companion_window.hpp"

bool CompanionWindow::create(lava::device_ptr device, lava::render_target::ptr target)
{
    VkFormat format = target->get_format();

    switch (format)
    {
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SRGB:
        format = VK_FORMAT_B8G8R8A8_SRGB;
        break;
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SRGB:
        format = VK_FORMAT_R8G8B8A8_SRGB;
        break;
    default:
        lava::log()->warn("Can't derive correct sRGB color format for companion window!");
        break;
    }

    this->framebuffer = lava::make_image(format, device, target->get_size());

    if (this->framebuffer == nullptr)
    {
        lava::log()->error("Can't create framebuffer for companion window!");

        return false;
    }

    return true;
}

void CompanionWindow::destroy()
{
    this->framebuffer->destroy();
    this->framebuffer = nullptr;
}

void CompanionWindow::render(VkCommandBuffer command_buffer, lava::render_target::ptr target, lava::index frame)
{
    if (!this->enabled || !this->submitted)
    {
        return;
    }

    lava::image::ptr window_image = target->get_backbuffer(frame);
    glm::uvec2 resolution = this->framebuffer->get_size();

    std::vector<VkImageMemoryBarrier> image_barriers;

    VkImageMemoryBarrier window_barrier = lava::image_memory_barrier(window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    window_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    window_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    window_barrier.subresourceRange = window_image->get_subresource_range();

    image_barriers.push_back(window_barrier);

    VkImageMemoryBarrier buffer_barrier = lava::image_memory_barrier(this->framebuffer->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    buffer_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    buffer_barrier.subresourceRange = this->framebuffer->get_subresource_range();

    image_barriers.push_back(buffer_barrier);

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT | VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());

    VkImageSubresourceLayers subresource_layers;
    subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_layers.mipLevel = 0;
    subresource_layers.baseArrayLayer = 0;
    subresource_layers.layerCount = 1;

    VkImageCopy image_region;
    image_region.srcSubresource = subresource_layers;
    image_region.srcOffset.x = 0;
    image_region.srcOffset.y = 0;
    image_region.srcOffset.z = 0;
    image_region.dstSubresource = subresource_layers;
    image_region.dstOffset.x = 0;
    image_region.dstOffset.y = 0;
    image_region.dstOffset.z = 0;
    image_region.extent.width = resolution.x;
    image_region.extent.height = resolution.y;
    image_region.extent.depth = 1;

    vkCmdCopyImage(command_buffer, this->framebuffer->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_region);

    this->submitted = false;
}

void CompanionWindow::submit_image(VkCommandBuffer command_buffer, Eye eye, lava::image::ptr image, VkImageLayout image_layout)
{
    if (!this->enabled)
    {
        return;
    }

    std::vector<VkImageMemoryBarrier> image_barriers;

    if (!this->submitted)
    {
        VkImageMemoryBarrier buffer_barrier = lava::image_memory_barrier(this->framebuffer->get(), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        buffer_barrier.srcAccessMask = 0;
        buffer_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        buffer_barrier.subresourceRange = this->framebuffer->get_subresource_range();

        image_barriers.push_back(buffer_barrier);
    }

    else
    {
        VkImageMemoryBarrier buffer_barrier = lava::image_memory_barrier(this->framebuffer->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        buffer_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        buffer_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        buffer_barrier.subresourceRange = this->framebuffer->get_subresource_range();

        image_barriers.push_back(buffer_barrier);
    }

    if (image_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier image_barrier = lava::image_memory_barrier(image->get(), image_layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
        image_barrier.srcAccessMask = 0;
        image_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        image_barrier.subresourceRange = image->get_subresource_range();

        image_barriers.push_back(image_barrier);
    }

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT | VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());

    VkImageSubresourceLayers subresource_layers;
    subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_layers.mipLevel = 0;
    subresource_layers.baseArrayLayer = 0;
    subresource_layers.layerCount = 1;

    glm::uvec2 buffer_resolution = this->framebuffer->get_size();
    glm::uvec2 image_resolution = image->get_size();
    uint32_t eye_offset = 0;

    if (eye == EYE_RIGHT)
    {
        eye_offset = buffer_resolution.x / 2;
    }

    VkImageBlit image_blit;
    image_blit.srcSubresource = image->get_subresource_layers();
    image_blit.srcOffsets[0].x = 0;
    image_blit.srcOffsets[0].y = 0;
    image_blit.srcOffsets[0].z = 0;
    image_blit.srcOffsets[1].x = image_resolution.x;
    image_blit.srcOffsets[1].y = image_resolution.y;
    image_blit.srcOffsets[1].z = 1;
    image_blit.dstSubresource = subresource_layers;
    image_blit.dstOffsets[0].x = eye_offset;
    image_blit.dstOffsets[0].y = 0;
    image_blit.dstOffsets[0].z = 0;
    image_blit.dstOffsets[1].x = eye_offset + buffer_resolution.x / 2;
    image_blit.dstOffsets[1].y = buffer_resolution.y;
    image_blit.dstOffsets[1].z = 1;

    vkCmdBlitImage(command_buffer, image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->framebuffer->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_blit, VK_FILTER_LINEAR);

    if (image_layout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier image_barrier = lava::image_memory_barrier(image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image_layout);
        image_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        image_barrier.dstAccessMask = 0;
        image_barrier.subresourceRange = image->get_subresource_range();

        vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &image_barrier);
    }

    this->submitted = true;
}

void CompanionWindow::set_enabled(bool enabled)
{
    this->enabled = enabled;
}

bool CompanionWindow::is_enabled() const
{
    return this->enabled;
}

CompanionWindow::Ptr make_companion_window()
{
    return std::make_shared<CompanionWindow>();
}