/*
  The companion window can be used to display images on the windows of the application.
  It can be access inside of a stereo strategy by calling the function get_companion_window() 
  and can be used as an alternative output device for debugging purposes.
  In case of an emulated headset, the companion window acts like an overlay and renders on top of everythign that was submitted to the emulted headset.
  Example:

    get_companion_window()->submit_image(command_buffer, EYE_LEFT, get_headset()->get_framebuffer(EYE_LEFT), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
*/

#pragma once
#include <liblava/lava.hpp>
#include <memory>

#include "headset/headset.hpp"

class CompanionWindow
{
public:
    typedef std::shared_ptr<CompanionWindow> Ptr;

public:
    CompanionWindow() = default;

    bool create(lava::device_ptr device, lava::render_target::ptr target);
    void destroy();
    void render(VkCommandBuffer command_buffer, lava::render_target::ptr target, lava::index frame);

    // Copies an image to the specified eye of the companion window.
    // The image format of the given image needs to be a four channel color type where each channel has a bit-depth of one byte.
    // After the image is copied to the companion window, its image layout is restored.
    void submit_image(VkCommandBuffer command_buffer, Eye eye, lava::image::ptr image, VkImageLayout image_layout);

    void set_enabled(bool enabled);
    bool is_enabled() const;

private:
    lava::image::ptr framebuffer;

    bool submitted = false;
    bool enabled = false;
};

CompanionWindow::Ptr make_companion_window();