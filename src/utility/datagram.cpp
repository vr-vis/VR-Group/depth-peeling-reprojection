#include "datagram.hpp"
#include <liblava/lava.hpp>

DatagramPool::~DatagramPool()
{
    for (const Datagram& datagam : this->pool)
    {
        lava::free_data(datagam.buffer);
    }

    this->pool.clear();
}

Datagram DatagramPool::acquire_datagram()
{
    Datagram datagram;

    if (this->pool.empty())
    {
        datagram.size = DATAGRAM_SIZE;
        datagram.buffer = (uint8_t*)lava::alloc_data(datagram.size);
    }

    else
    {
        datagram = this->pool.back();
        this->pool.pop_back();
    }

    return datagram;
}

void DatagramPool::release_datagram(const Datagram& datagram)
{
    this->pool.push_back(datagram);
}