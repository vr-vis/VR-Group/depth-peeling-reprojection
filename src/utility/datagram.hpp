#pragma once
#include <cstdint>
#include <vector>

#define DATAGRAM_SIZE 1024

struct Datagram
{
    uint32_t size = 0;
    uint8_t* buffer = nullptr;
};

class DatagramPool
{
private:
    std::vector<Datagram> pool;

public:
    DatagramPool() = default;
    ~DatagramPool();

    Datagram acquire_datagram();
    void release_datagram(const Datagram& datagram);
};