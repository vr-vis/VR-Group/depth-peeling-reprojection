#include "extern_fence.hpp"
#include <optional>

VkFence ExternFence::get() const
{
    return this->fence;
}

#ifdef WIN32
#include <vulkan/vulkan_win32.h>
PFN_vkGetFenceWin32HandleKHR vkGetFenceWin32HandleKHR = nullptr;

class ExternFenceWin : public ExternFence
{
private:
    typedef asio::windows::basic_object_handle<asio::executor> ObjectType;

private:
    std::optional<ObjectType> object;

public:
    ExternFenceWin() = default;

    bool create(lava::device_ptr device, asio::executor executor, bool signaled) override
    {
        VkExportFenceCreateInfo export_info;
        export_info.sType = VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO;
        export_info.pNext = nullptr;
        export_info.handleTypes = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT;

        VkFenceCreateInfo create_info;
        create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        create_info.pNext = &export_info;
        create_info.flags = signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

        if (vkCreateFence(device->get(), &create_info, lava::memory::alloc(), &this->fence) != VK_SUCCESS)
        {
            return false;
        }

        VkFenceGetWin32HandleInfoKHR get_info;
        get_info.sType = VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR;
        get_info.pNext = nullptr;
        get_info.fence = fence;
        get_info.handleType = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_WIN32_BIT;

        HANDLE object_handle = NULL;
        
        if (vkGetFenceWin32HandleKHR(device->get(), &get_info, &object_handle) != VK_SUCCESS)
        {
            return false;    
        }

        this->object = ObjectType(executor, object_handle);

        return true;
    }

    void destroy(lava::device_ptr device) override
    {
        if (this->fence != VK_NULL_HANDLE)
        {
            this->object->close();
            vkDestroyFence(device->get(), this->fence, lava::memory::alloc());
        }

        this->object.reset();
        this->fence = VK_NULL_HANDLE;
    }

    void async_wait(WaitFunction function) override
    {
        this->object->async_wait(function);
    }
};
#endif

#ifdef __unix__
class ExternFencePosix : public ExternFence
{
private:
    typedef asio::posix::basic_stream_descriptor<asio::executor> StreamType;

private:
    std::optional<StreamType> stream;

public:
    ExternFencePosix() = default;

    bool create(lava::device_ptr device, asio::executor executor, bool signaled)
    {
        VkExportFenceCreateInfo export_info;
        export_info.sType = VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO;
        export_info.pNext = nullptr;
        export_info.handleTypes = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT;

        VkFenceCreateInfo create_info;
        create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        create_info.pNext = &export_info;
        create_info.flags = signaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

        if (vkCreateFence(device->get(), &create_info, lava::memory::alloc(), &this->fence) != VK_SUCCESS)
        {
            return false;
        }

        VkFenceGetFdInfoKHR get_info;
        get_info.sType = VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR;
        get_info.pNext = nullptr;
        get_info.fence = fence;
        get_info.handleType = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT;

        int file_descriptor = 0;
        
        if (vkGetFenceFdKHR(device->get(), &get_info, &file_descriptor) != VK_SUCCESS)
        {
            return false;    
        }

        this->stream = StreamType(executor, file_descriptor);

        return true;
    }

    void destroy(lava::device_ptr device)
    {
        if (this->fence != VK_NULL_HANDLE)
        {
            this->stream->close();
            vkDestroyFence(device->get(), this->fence, lava::memory::alloc());
        }

        this->stream.reset();
        this->fence = VK_NULL_HANDLE;
    }

    void async_wait(WaitFunction function)
    {
        this->stream->async_wait(StreamType::wait_read, function);
    }
};
#endif

bool setup_instance_for_extern_fence(lava::frame_config& config)
{
    if (config.info.req_api_version < lava::api_version::v1_1)
    {
        // Required in oder to get:
        // VK_KHR_external_fence,
        // VK_KHR_external_fence_capabilities
        config.info.req_api_version = lava::api_version::v1_1;
    }

    return true;
}

bool setup_device_for_extern_fence(lava::instance& instance, lava::device::create_param& parameters)
{
#if defined(WIN32)
    vkGetFenceWin32HandleKHR = (PFN_vkGetFenceWin32HandleKHR)vkGetInstanceProcAddr(instance.get(), "vkGetFenceWin32HandleKHR");
    parameters.extensions.push_back(VK_KHR_EXTERNAL_FENCE_WIN32_EXTENSION_NAME);

    return true;
#elif defined(__unix__)
    parameters.extensions.push_back(VK_KHR_EXTERNAL_FENCE_FD_EXTENSION_NAME);

    return true;
#else
    #error "No extren fence implemented for this platform!"
#endif
}

ExternFence::Ptr make_extern_fence()
{
#if defined(WIN32)
    return std::make_shared<ExternFenceWin>();
#elif defined(__unix__)
    return std::make_shared<ExternFencePosix>();
#else
    #error "No extren fence implemented for this platform!"
#endif
}