/*
  An extren fence can be used to create a special vulkan fence that can also be used together with asio.
  In order to be able to use extern fences, it is neccessary to call the function setup_instance_for_extern_fence(...) during the setup of the vulkan instance.
  Besides that, the function setup_device_for_extern_fence(...) has to be called during the setup of the vulkan device.
  Example:

   //During on_setup_instance(...)
   setup_instance_for_extern_fence();

   //During on_setup_device(...)
   setup_device_for_extern_fence(...);

   //During execution
   ExternFence::Ptr extern_fence = make_extern_fence();

   extern_fence->create(..., asio_executor, ...);

   extren_fence->async_wait([](...)
   {
        //Called when fence is signaled
   });
*/

#pragma once
#include <liblava/lava.hpp>
#include <asio.hpp>
#include <functional>
#include <memory>

typedef std::function<void(const asio::error_code& error_code)> WaitFunction;

class ExternFence
{
public:
    typedef std::shared_ptr<ExternFence> Ptr;

protected:
    VkFence fence = VK_NULL_HANDLE;

public:
    ExternFence() = default;
    virtual ~ExternFence() = default;

    virtual bool create(lava::device_ptr device, asio::executor executor, bool signaled) = 0;
    virtual void destroy(lava::device_ptr device) = 0;
    virtual void async_wait(WaitFunction function) = 0;

    VkFence get() const;
};

bool setup_instance_for_extern_fence(lava::frame_config& config);
bool setup_device_for_extern_fence(lava::instance& instance, lava::device::create_param& parameters);

ExternFence::Ptr make_extern_fence();