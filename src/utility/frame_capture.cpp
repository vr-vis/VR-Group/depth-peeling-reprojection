#include "frame_capture.hpp"
#include <algorithm>
#include <filesystem>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

FrameCapture::FrameCapture(const std::string& base_directory) : base_directory(base_directory)
{

}

bool FrameCapture::create(uint32_t frame_count)
{
    this->frame_count = frame_count;

    return true;
}

void FrameCapture::destroy()
{
    for (uint32_t offset = 0; offset < this->frame_count; offset++)
    {
        uint32_t frame = (this->current_frame + offset + 1) % this->frame_count;

        this->load_captures(frame);
    }
}

void FrameCapture::next_frame(lava::index frame)
{
    this->load_captures(frame);

    this->current_frame = frame;
}

void FrameCapture::capture_image(VkCommandBuffer command_buffer, lava::image::ptr image, VkImageLayout image_layout, const std::string& image_name)
{
    if (!this->enabled)
    {
        return;
    }

    lava::device_ptr device = image->get_device();
    lava::uv2 image_size = image->get_size();
    VkFormat image_format = image->get_format();

    uint32_t staging_buffer_size = image_size.x * image_size.y * this->get_component_count(image_format) * this->get_component_size(image_format);

    lava::buffer::ptr staging_buffer = lava::make_buffer();
    staging_buffer->create(device, nullptr, staging_buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT, false, VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_TO_CPU);

    VkOffset3D image_offset;
    image_offset.x = 0;
    image_offset.y = 0;
    image_offset.z = 0;

    VkExtent3D image_extend;
    image_extend.width = image_size.x;
    image_extend.height = image_size.y;
    image_extend.depth = 1;

    VkImageMemoryBarrier begin_barrier;
    begin_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    begin_barrier.pNext = nullptr;
    begin_barrier.srcAccessMask = 0;
    begin_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    begin_barrier.oldLayout = image_layout;
    begin_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    begin_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    begin_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    begin_barrier.image = image->get();
    begin_barrier.subresourceRange = image->get_subresource_range();

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &begin_barrier);

    VkBufferImageCopy region;
    region.bufferOffset = 0;
    region.bufferRowLength = 0; //Tightly packed
    region.bufferImageHeight = 0;
    region.imageSubresource = image->get_subresource_layers();
    region.imageOffset = image_offset;
    region.imageExtent = image_extend;

    vkCmdCopyImageToBuffer(command_buffer, image->get(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, staging_buffer->get(), 1, &region);

    VkImageMemoryBarrier end_image_barrier;
    end_image_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    end_image_barrier.pNext = nullptr;
    end_image_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    end_image_barrier.dstAccessMask = 0;
    end_image_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    end_image_barrier.newLayout = image_layout;
    end_image_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    end_image_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    end_image_barrier.image = image->get();
    end_image_barrier.subresourceRange = image->get_subresource_range();

    VkBufferMemoryBarrier end_buffer_barrier;
    end_buffer_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    end_buffer_barrier.pNext = nullptr;
    end_buffer_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    end_buffer_barrier.dstAccessMask = VK_ACCESS_HOST_READ_BIT;
    end_buffer_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    end_buffer_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    end_buffer_barrier.buffer = staging_buffer->get();
    end_buffer_barrier.offset = 0;
    end_buffer_barrier.size = VK_WHOLE_SIZE;

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_HOST_BIT | VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 1, &end_buffer_barrier, 1, &end_image_barrier);

    ImageCaptureDirectory directory;
    auto directory_iter = this->directories.find(image_name);

    if (directory_iter == this->directories.end())
    {
        directory.name = this->build_directory_name(image_name);
        directory.index = 0;

        this->directories[image_name] = directory;
    }

    else
    {
        directory_iter->second.index++;
        directory = directory_iter->second;
    }

    ImageCapture capture;
    capture.frame_index = this->current_frame;
    capture.name = image_name;
    capture.size = image_size;
    capture.format = image_format;
    capture.store_directory = directory.name;
    capture.store_index = directory.index;
    capture.staging_buffer = staging_buffer;

    this->captures.push_back(capture);
}

void FrameCapture::set_enabled(bool enabled)
{
    this->enabled = enabled;

    if (!enabled)
    {
        this->directories.clear();
    }
}

bool FrameCapture::is_enabled() const
{
    return this->enabled;
}

void FrameCapture::load_captures(lava::index frame)
{
    for (const ImageCapture& capture : this->captures)
    {
        if (capture.frame_index == frame)
        {
            lava::buffer::ptr staging_buffer = capture.staging_buffer;
            lava::device_ptr device = staging_buffer->get_device();

            vmaInvalidateAllocation(device->alloc(), staging_buffer->get_allocation(), 0, VK_WHOLE_SIZE);

            uint8_t* image_content = nullptr;

            if (vmaMapMemory(device->alloc(), staging_buffer->get_allocation(), (void**)(&image_content)))
            {
                lava::log()->error("can't map the staging buffer for the frame capture of image '" + capture.name + "'");
            }

            else
            {
                this->write_to_file(capture, image_content);

                vmaUnmapMemory(device->alloc(), staging_buffer->get_allocation());
            }

            staging_buffer->destroy();
        }
    }

    this->captures.erase(std::remove_if(this->captures.begin(), this->captures.end(), [=](const ImageCapture& capture)
    {
        return capture.frame_index == frame;
    }), this->captures.end());
}

bool FrameCapture::write_to_file(const ImageCapture& image_capture, const uint8_t* image_content)
{
    if (!this->is_format_supported(image_capture.format))
    {
        lava::log()->error("the format of image '" + image_capture.name + "' is not supported for frame capturing!");

        return false;
    }

    std::filesystem::path base_path = this->base_directory;
    std::filesystem::path directory_path = this->base_directory + "/" + image_capture.store_directory;
    std::string file_name = this->base_directory + "/" + image_capture.store_directory + "/frame_" + std::to_string(image_capture.store_index) + ".png";

    if (!std::filesystem::exists(base_path))
    {
        std::filesystem::create_directory(base_path);
    }

    if (!std::filesystem::exists(directory_path))
    {
        std::filesystem::create_directory(directory_path);
    }

    if (image_capture.format == VK_FORMAT_R8G8B8_UNORM || image_capture.format == VK_FORMAT_R8G8B8_SRGB || image_capture.format == VK_FORMAT_R8G8B8A8_UNORM || image_capture.format == VK_FORMAT_R8G8B8A8_SRGB)
    {
        uint32_t component_count = this->get_component_count(image_capture.format);

        if (stbi_write_png(file_name.c_str(), image_capture.size.x, image_capture.size.y, component_count, image_content, image_capture.size.x * component_count * sizeof(uint8_t)) == 0)
        {
            lava::log()->error("Can't store image '" + image_capture.name + "'");

            return false;
        }
    }

    else
    {
        std::vector<uint8_t> converted_content = this->convert_content(image_content, image_capture.size, image_capture.format);

        if (stbi_write_png(file_name.c_str(), image_capture.size.x, image_capture.size.y, 4, converted_content.data(), image_capture.size.x * 4 * sizeof(uint8_t)) == 0)
        {
            lava::log()->error("can't store image '" + image_capture.name + "'");

            return false;
        }
    }

    return true;
}

std::string FrameCapture::build_directory_name(const std::string& image_name)
{
    time_t system_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    tm local_time = *localtime(&system_time);

    std::string name = image_name;
    name += "_" + std::to_string(local_time.tm_mday);
    name += "-" + std::to_string(local_time.tm_mon + 1);
    name += "-" + std::to_string(local_time.tm_year + 1900);
    name += "_" + std::to_string(local_time.tm_hour);
    name += "-" + std::to_string(local_time.tm_min);
    name += "-" + std::to_string(local_time.tm_sec);

    return name;
}

std::vector<uint8_t> FrameCapture::convert_content(const uint8_t* image_content, const lava::uv2& image_size, VkFormat image_format)
{
    std::vector<uint8_t> output_buffer;
    output_buffer.resize(image_size.x * image_size.y * 4 * sizeof(uint8_t));

    const uint8_t* input_pointer = image_content;

    for (uint32_t y = 0; y < image_size.y; y++)
    {
        for (uint32_t x = 0; x < image_size.x; x++)
        {
            uint32_t pixel_index = y * image_size.x + x;
            
            //initialize the pixel to black
            output_buffer[pixel_index * 4 + 0] = 0;
            output_buffer[pixel_index * 4 + 1] = 0;
            output_buffer[pixel_index * 4 + 2] = 0;
            output_buffer[pixel_index * 4 + 3] = 0xFF;

            for (uint32_t component = 0; component < this->get_component_count(image_format); component++)
            {
                uint32_t component_offset = this->get_component_offset(image_format, component);
                uint32_t component_range = (1 << (this->get_component_size(image_format) * 8)) - 1;
                uint32_t component_value = 0;

                for (int32_t byte = this->get_component_size(image_format) - 1; byte >= 0; byte--)
                {
                    component_value |= *input_pointer << byte * 8;
                    input_pointer++;
                }

                output_buffer[pixel_index * 4 + component_offset] = (uint8_t)(((float)component_value / (float)component_range) * 255.0f);
            }
        }
    }

    return output_buffer;
}

uint32_t FrameCapture::get_component_count(VkFormat format)
{
    switch (format)
    {
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8_SRGB:
    case VK_FORMAT_R16_UNORM:
        return 1;
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R8G8_SRGB:
    case VK_FORMAT_R16G16_UNORM:
        return 2;
    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_R8G8B8_SRGB:
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_B8G8R8_SRGB:
    case VK_FORMAT_R16G16B16_UNORM:
        return 3;
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SRGB:
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SRGB:
    case VK_FORMAT_R16G16B16A16_UNORM:
        return 4;
    default:
        break;
    }

    return 0;
}

uint32_t FrameCapture::get_component_size(VkFormat format)
{
    switch (format)
    {
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_R8_SRGB:
    case VK_FORMAT_R8G8_SRGB:
    case VK_FORMAT_R8G8B8_SRGB:
    case VK_FORMAT_B8G8R8_SRGB:
    case VK_FORMAT_R8G8B8A8_SRGB:
    case VK_FORMAT_B8G8R8A8_SRGB:
        return 1;
    case VK_FORMAT_R16_UNORM:
    case VK_FORMAT_R16G16_UNORM:
    case VK_FORMAT_R16G16B16_UNORM:
    case VK_FORMAT_R16G16B16A16_UNORM:
        return 2;
    default:
        break;
    }

    return 0;
}

uint32_t FrameCapture::get_component_offset(VkFormat format, uint32_t component)
{
    switch (format)
    {
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8_SRGB:
    case VK_FORMAT_B8G8R8A8_SRGB:
        if (component == 0)
        {
            return 2;
        }
        if (component == 2)
        {
            return 0;
        }
        break;
    default:
        break;
    }

    return component;
}

bool FrameCapture::is_format_supported(VkFormat format)
{
    switch (format)
    {
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R16_UNORM:
    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_R16G16_UNORM:
    case VK_FORMAT_R16G16B16_UNORM:
    case VK_FORMAT_R16G16B16A16_UNORM:
    case VK_FORMAT_R8_SRGB:
    case VK_FORMAT_R8G8_SRGB:
    case VK_FORMAT_R8G8B8_SRGB:
    case VK_FORMAT_B8G8R8_SRGB:
    case VK_FORMAT_R8G8B8A8_SRGB:
    case VK_FORMAT_B8G8R8A8_SRGB:
        return true;
    default:
        break;
    }

    return false;
}

FrameCapture::Ptr make_frame_capture(const std::string& base_directory)
{
    return std::make_shared<FrameCapture>(base_directory);
}