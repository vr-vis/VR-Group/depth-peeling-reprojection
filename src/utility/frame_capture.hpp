/*
  The frame capture system can be access inside of a stereo strategy by calling the function get_frame_capture().
  An image can then be captured and written to a file by calling the function capture_image(...).
  Example:

   get_frame_capture().capture_image(command_buffer, image, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, "image_name");
  
  Notes:
  - The parameter image_layout has to match the current image_layout of the source image.
  - It is only possible to capture color-images.
  - Before calling the function destroy() it is neccessary ensure a device-host synchronisation by calling for example vkDeviceWaitIdle().
  - At the start of a frame it is neccessary to call the function next_frame(..).
*/

#pragma once
#include <liblava/lava.hpp>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <cstdint>

struct ImageCapture
{
    uint32_t frame_index;

    std::string name;
    lava::uv2 size;
    VkFormat format;

    std::string store_directory;
    uint32_t store_index;

    lava::buffer::ptr staging_buffer;
};

struct ImageCaptureDirectory
{
    std::string name;
    uint32_t index;
};

class FrameCapture
{
public:
    typedef std::shared_ptr<FrameCapture> Ptr;

public:
    FrameCapture(const std::string& base_directory);

    bool create(uint32_t frame_count);
    void destroy();

    void next_frame(lava::index frame);
    void capture_image(VkCommandBuffer command_buffer, lava::image::ptr image, VkImageLayout image_layout, const std::string& image_name);

    void set_enabled(bool enabled);
    bool is_enabled() const;

private:
    void load_captures(lava::index frame);

    bool write_to_file(const ImageCapture& image_capture, const uint8_t* image_content);
    std::string build_directory_name(const std::string& image_name);

    std::vector<uint8_t> convert_content(const uint8_t* image_content, const lava::uv2& image_size, VkFormat image_format);
    uint32_t get_component_count(VkFormat format);
    uint32_t get_component_size(VkFormat format);
    uint32_t get_component_offset(VkFormat format, uint32_t component);
    bool is_format_supported(VkFormat format);

private:
    std::string base_directory = "";

    uint32_t frame_count = 0;
    uint32_t current_frame = 0;

    bool enabled = false;

    std::vector<ImageCapture> captures;
    std::map<std::string, ImageCaptureDirectory> directories;
};

FrameCapture::Ptr make_frame_capture(const std::string& base_directory);