#include "geometry_buffer.hpp"

bool GeometryBuffer::create(lava::image::ptr output_image, VkImageLayout output_layout, lava::descriptor::ptr per_frame_descriptor, Scene::Ptr scene, ShadowCache::Ptr shadow_cache, IndirectCache::Ptr indirect_cache)
{
    lava::device_ptr device = output_image->get_device();
    glm::uvec2 size = output_image->get_size();

    this->scene = scene;
    this->shadow_cache = shadow_cache;
    this->indirect_cache = indirect_cache;
    this->output_image = output_image;
    this->output_layout = output_layout;

    if (!this->create_images(device, size))
    {
        this->destroy();

        return false;
    }

    if (!this->create_attachments())
    {
        this->destroy();

        return false;
    }

    if (!this->create_sampler(device))
    {
        this->destroy();

        return false;
    }

    if (!this->create_descriptors(device))
    {
        this->destroy();

        return false;
    }

    if (!this->create_render_pass(device, output_image, output_layout))
    {
        this->destroy();

        return false;
    }

    if (!this->create_pipeline(device, per_frame_descriptor))
    {
        this->destroy();

        return false;
    }

    return true;
}

void GeometryBuffer::destroy()
{
    lava::device_ptr device = this->output_image->get_device();

    if (this->sampler != nullptr)
    {
        vkDestroySampler(device->get(), this->sampler, lava::memory::alloc());
        this->sampler = nullptr;
    }

    if (this->buffer_descriptor_set != nullptr)
    {
        this->buffer_descriptor->free(this->buffer_descriptor_set, this->descriptor_pool->get());
        this->buffer_descriptor_set = nullptr;
    }

    if (this->buffer_descriptor != nullptr)
    {
        this->buffer_descriptor->destroy();
        this->buffer_descriptor = {};
    }

    if (this->descriptor_pool != nullptr)
    {
        this->descriptor_pool->destroy();
        this->descriptor_pool = {};
    }

    if (this->depth_image != nullptr)
    {
        this->depth_image->destroy();
        this->depth_image = {};
    }

    if (this->color_image != nullptr)
    {
        this->color_image->destroy();
        this->color_image = {};
    }

    if (this->normal_image != nullptr)
    {
        this->normal_image->destroy();
        this->normal_image = {};
    }

    if (this->material_image != nullptr)
    {
        this->material_image->destroy();
        this->material_image = {};
    }

    if (this->pipeline != nullptr)
    {
        this->pipeline->destroy();
        this->pipeline = {};
    }

    if (this->pipeline_layout != nullptr)
    {
        this->pipeline_layout->destroy();
        this->pipeline_layout = {};
    }

    if (this->render_pass != nullptr)
    {
        this->render_pass->destroy();
        this->render_pass = {};
    }
    
    //Free the shared references and hope that every vulkan object gets destroyed.
    this->depth_attachment = {};
    this->color_attachment = {};
    this->normal_attachment = {};
    this->material_attachment = {};

    //Don't touch these resources. Only release the shared pointer to them.
    this->scene = {};
    this->shadow_cache = {};
    this->output_image = {};
    this->output_layout = VK_IMAGE_LAYOUT_UNDEFINED;
}

void GeometryBuffer::apply_lighting(VkCommandBuffer command_buffer, lava::index frame, VkDescriptorSet per_frame_descriptor_set)
{
    this->frame = frame;
    this->pipeline_layout->bind(command_buffer, per_frame_descriptor_set, 2);
    
    this->render_pass->process(command_buffer, 0);
}

lava::image::ptr GeometryBuffer::get_depth_image() const
{
    return this->depth_image;
}

lava::image::ptr GeometryBuffer::get_color_image() const
{
    return this->color_image;
}

lava::image::ptr GeometryBuffer::get_normal_image() const
{
    return this->normal_image;
}

lava::image::ptr GeometryBuffer::get_material_image() const
{
    return this->material_image;
}

lava::attachment::ptr GeometryBuffer::get_depth_attachment() const
{
    return this->depth_attachment;
}

lava::attachment::ptr GeometryBuffer::get_color_attachment() const
{
    return this->color_attachment;
}

lava::attachment::ptr GeometryBuffer::get_normal_attachment() const
{
    return this->normal_attachment;
}

lava::attachment::ptr GeometryBuffer::get_material_attachment() const
{
    return this->material_attachment;
}

VkClearValue GeometryBuffer::get_depth_clear_value() const
{
    VkClearValue clear_value;
    clear_value.depthStencil.depth = 1.0f;

    return clear_value;
}

VkClearValue GeometryBuffer::get_color_clear_value() const
{
    VkClearValue clear_value;
    clear_value.color.float32[0] = 0.0f;
    clear_value.color.float32[1] = 0.0f;
    clear_value.color.float32[2] = 0.0f;
    clear_value.color.float32[3] = 0.0f;

    return clear_value;
}

VkClearValue GeometryBuffer::get_normal_clear_value() const
{
    VkClearValue clear_value;
    clear_value.color.float32[0] = 0.0f;
    clear_value.color.float32[1] = 0.0f;
    clear_value.color.float32[2] = 0.0f;
    clear_value.color.float32[3] = 0.0f;

    return clear_value;
}

VkClearValue GeometryBuffer::get_material_clear_value() const
{
    VkClearValue clear_value;
    clear_value.color.float32[0] = 0.0f;
    clear_value.color.float32[1] = 0.0f;
    clear_value.color.float32[2] = 0.0f;
    clear_value.color.float32[3] = 0.0f;

    return clear_value;
}

bool GeometryBuffer::create_images(lava::device_ptr device, const glm::uvec2& size)
{
    this->depth_image = lava::make_image(VK_FORMAT_D32_SFLOAT);
    this->depth_image->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    this->depth_image->set_tiling(VK_IMAGE_TILING_OPTIMAL);

    if (!this->depth_image->create(device, size))
    {
        lava::log()->error("Can't create geometry buffer depth image!");

        return false;
    }

    this->color_image = lava::make_image(VK_FORMAT_R8G8B8A8_UNORM);
    this->color_image->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    this->color_image->set_tiling(VK_IMAGE_TILING_OPTIMAL);

    if (!this->color_image->create(device, size))
    {
        lava::log()->error("Can't create geometry buffer color image!");

        return false;
    }

    this->normal_image = lava::make_image(VK_FORMAT_R16G16B16A16_UNORM);
    this->normal_image->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    this->normal_image->set_tiling(VK_IMAGE_TILING_OPTIMAL);

    if (!this->normal_image->create(device, size))
    {
        lava::log()->error("Can't create geometry buffer normal image!");

        return false;
    }

    this->material_image = lava::make_image(VK_FORMAT_R8G8B8A8_UNORM);
    this->material_image->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    this->material_image->set_tiling(VK_IMAGE_TILING_OPTIMAL);

    if (!this->material_image->create(device, size))
    {
        lava::log()->error("Can't create geometry buffer material image!");

        return false;
    }

    return true;
}

bool GeometryBuffer::create_attachments()
{
    this->depth_attachment = lava::make_attachment(VK_FORMAT_D32_SFLOAT);
    this->depth_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->depth_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->depth_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    this->color_attachment = lava::make_attachment(VK_FORMAT_R8G8B8A8_UNORM);
    this->color_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->color_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->color_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    this->normal_attachment = lava::make_attachment(VK_FORMAT_R16G16B16A16_UNORM);
    this->normal_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->normal_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->normal_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    this->material_attachment = lava::make_attachment(VK_FORMAT_R8G8B8A8_UNORM);
    this->material_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    this->material_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    this->material_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    return true;
}

bool GeometryBuffer::create_sampler(lava::device_ptr device)
{
    VkSamplerCreateInfo sampler_create_info;
    sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_create_info.pNext = nullptr;
    sampler_create_info.flags = 0;
    sampler_create_info.magFilter = VK_FILTER_NEAREST;
    sampler_create_info.minFilter = VK_FILTER_NEAREST;
    sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_create_info.mipLodBias = 0.0f;
    sampler_create_info.anisotropyEnable = VK_FALSE;
    sampler_create_info.maxAnisotropy = 1.0f;
    sampler_create_info.compareEnable = VK_FALSE;
    sampler_create_info.compareOp = VK_COMPARE_OP_LESS;
    sampler_create_info.minLod = 0;
    sampler_create_info.maxLod = VK_LOD_CLAMP_NONE;
    sampler_create_info.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
    sampler_create_info.unnormalizedCoordinates = VK_FALSE;

    if (vkCreateSampler(device->get(), &sampler_create_info, lava::memory::alloc(), &this->sampler) != VK_SUCCESS)
    {
        lava::log()->error("Can't create sampler for deferred local light pass!");

        return false;
    }

    return true;
}

bool GeometryBuffer::create_descriptors(lava::device_ptr device)
{
    lava::VkDescriptorPoolSizes descriptor_type_count =
    {
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4}
    };

    this->descriptor_pool = lava::make_descriptor_pool();

    if (!this->descriptor_pool->create(device, descriptor_type_count, 1))
    {
        lava::log()->error("Can't create descriptor pool for the deferred local light pass!");

        return false;
    }

    this->buffer_descriptor = lava::make_descriptor();
    this->buffer_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 0: depth image
    this->buffer_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 1: color image
    this->buffer_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 2: normal image
    this->buffer_descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 3: material image

    if (!this->buffer_descriptor->create(device))
    {
        lava::log()->error("Can't create buffer descriptor for the deferred local light pass!");

        return false;
    }

    this->buffer_descriptor_set = this->buffer_descriptor->allocate(this->descriptor_pool->get());

    VkDescriptorImageInfo depth_image_info;
    depth_image_info.sampler = this->sampler;
    depth_image_info.imageView = this->depth_image->get_view();
    depth_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo color_image_info;
    color_image_info.sampler = this->sampler;
    color_image_info.imageView = this->color_image->get_view();
    color_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo normal_image_info;
    normal_image_info.sampler = this->sampler;
    normal_image_info.imageView = this->normal_image->get_view();
    normal_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo material_image_info;
    material_image_info.sampler = this->sampler;
    material_image_info.imageView = this->material_image->get_view();
    material_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    std::vector<VkDescriptorImageInfo> image_infos =
    {
        depth_image_info,
        color_image_info,
        normal_image_info,
        material_image_info
    };

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.resize(image_infos.size());

    for (uint32_t index = 0; index < image_infos.size(); index++)
    {
        VkWriteDescriptorSet& descriptor_write = descriptor_writes[index];
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = this->buffer_descriptor_set;
        descriptor_write.dstBinding = index;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptor_write.pImageInfo = &image_infos[index];
        descriptor_write.pBufferInfo = nullptr;
        descriptor_write.pTexelBufferView = nullptr;
    }

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool GeometryBuffer::create_render_pass(lava::device_ptr device, lava::image::ptr output_image, VkImageLayout output_layout)
{
    VkClearValue output_clear_value;
    output_clear_value.color.float32[0] = 0.0f;
    output_clear_value.color.float32[1] = 0.0f;
    output_clear_value.color.float32[2] = 0.0f;
    output_clear_value.color.float32[3] = 1.0f;

    lava::attachment::ptr output_attachment = lava::make_attachment(output_image->get_format());
    output_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    output_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    output_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, output_layout);

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_color_attachment(0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL); //output-image

    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

    this->render_pass = lava::make_render_pass(device);
    this->render_pass->add(subpass);
    this->render_pass->add(subpass_begin_dependency);
    this->render_pass->add(subpass_end_dependency);
    this->render_pass->add(output_attachment); //location 0: output image
    this->render_pass->set_clear_values(
    {
        output_clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        output_image->get_size()
    };

    lava::VkImageViews framebuffer_views =
    {
        output_image->get_view()
    };

    if (!this->render_pass->create({framebuffer_views}, framebuffer_area))
    {
        lava::log()->error("Can't create render pass for the deferred local light pass!");

        return false;
    }

    return true;
}

bool GeometryBuffer::create_pipeline(lava::device_ptr device, lava::descriptor::ptr per_frame_descriptor)
{
    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add(this->buffer_descriptor);                //descriptor-set index 0: geometry-buffer
    this->pipeline_layout->add(this->scene->get_light_descriptor());    //descriptor-set index 1: light-buffer
    this->pipeline_layout->add(per_frame_descriptor);                   //descriptor-set index 2: per frame-buffer

    if (this->shadow_cache != nullptr)
    {
        this->pipeline_layout->add(this->shadow_cache->get_descriptor()); //descriptor-set index 3: shadow-information
    }

    if (this->indirect_cache != nullptr)
    {
        this->pipeline_layout->add(this->indirect_cache->get_descriptor()); //descriptor-set index 3 or 4: shadow-information
    }

    if (!this->pipeline_layout->create(device))
    {
        lava::log()->error("Can't create pipeline layout for deferred local light pass!");

        return false;
    }

    VkPipelineColorBlendAttachmentState output_blend_state;
    output_blend_state.blendEnable = VK_FALSE;
    output_blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    output_blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    output_blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    output_blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    output_blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    output_blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    output_blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    this->pipeline = lava::make_graphics_pipeline(device);
    this->pipeline->set_layout(this->pipeline_layout);
    this->pipeline->set_input_assembly_topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP);
    this->pipeline->add_color_blend_attachment(output_blend_state);

    if (!this->pipeline->add_shader(lava::file_data("dpr/binary/deferred_lighting_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        lava::log()->error("Can't load vertex shader for deferred lighting pass!");

        return false;
    }

    std::string shader_name = "dpr/binary/deferred_lighting";

    if (this->indirect_cache != nullptr)
    {
        shader_name += "_indirect";
    }

    if (this->shadow_cache != nullptr)
    {
        shader_name += "_shadow";
    }

    shader_name += "_fragment.spirv";

    if (!this->pipeline->add_shader(lava::file_data(shader_name.c_str()), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        lava::log()->error("Can't load fragment shader for deferred lighting pass!");

        return false;
    }

    this->pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipline_function(command_buffer);
    };

    if (!this->pipeline->create(this->render_pass->get()))
    {
        lava::log()->error("Can't create graphics pipeline for deferred local light pass!");

        return false;
    }

    this->render_pass->add_front(this->pipeline);

    return true;
}

void GeometryBuffer::pipline_function(VkCommandBuffer command_buffer)
{
    this->pipeline_layout->bind(command_buffer, this->buffer_descriptor_set, 0);
    this->pipeline_layout->bind(command_buffer, this->scene->get_light_descriptor_set(this->frame), 1);

    if (this->shadow_cache != nullptr && this->indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, this->shadow_cache->get_descriptor_set(), 3);
        this->pipeline_layout->bind(command_buffer, this->indirect_cache->get_descriptor_set(), 4);
    }

    else if (this->shadow_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, this->shadow_cache->get_descriptor_set(), 3);
    }

    else if (this->indirect_cache != nullptr)
    {
        this->pipeline_layout->bind(command_buffer, this->indirect_cache->get_descriptor_set(), 3);
    }

    vkCmdDraw(command_buffer, 4, 1, 0, 0); //draw fullscreen quad
}

GeometryBuffer::Ptr make_geometry_buffer()
{
    return std::make_shared<GeometryBuffer>();
}