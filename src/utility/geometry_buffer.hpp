/*
  The geometry buffer is responsible for the allocation of the images of the geometry buffer and allows to compute and write the final lighting to a given output image.
  During the creation of the geometry buffer, an output image has to be specified in which the final lighting will be written.
  After the geometry has been created successfully, the resources provided by the geometry buffer can be used to create a render pass in order to write into the geometry buffer.
  Before calling the function apply_lighting(...) and computing the final lighting, it is neccessary to ensure that every writing process effecting the geometry buffer has been completed.
  Such a synchronisation can be defined by adding a render pass dependency between the last and the extrenal subpass.
  After the completion of apply_lighting(...) the final lighting can be read from the output image in a fragment shader without any additional synchronisations.
  In order to also include shadows when executing apply_lighting(...), a shadow cache object has to be added to the create pareameters.
  For additional indirect lighting, an indirect cache object can be added during the creation of the geometry buffer.
  Example:

    //Optional: Shadow Cache
    ShadowCache::Ptr shadow_cache = make_shadow_cache();
    shadow_cache->create(get_scene(), ShadowCacheSettings()):

    //Optional: Indirect Cache
    IndirectCache::Ptr indirect_cache = make_indirect_cache();
    indirect_cache->create(get_scene(), IndirectCacheSettings());

    //Create render pass
    GeometryBuffer::Ptr geometry_buffer = make_geometry_buffer();
    geometry_buffer->create(output_image, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, get_stereo_transform()->get_descriptor(), get_scene(), Optional: shadow_cache, Optional: indirect_cache);

    lava::subpass_dependency::ptr subpass_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_dependency->set_stage_mask(..., VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    subpass_dependency->set_access_mask(..., VK_ACCESS_SHADER_READ_BIT);

    lava::render_pass::ptr render_pass = lava::make_render_pass(get_device());
    //....
    render_pass->add(subpass_dependency);
    render_pass->add(geometry_buffer->get_depth_attachment());
    render_pass->add(geometry_buffer->get_color_attachment());
    render_pass->add(geometry_buffer->get_normal_attachment());
    render_pass->add(geometry_buffer->get_material_attachment());
    render_pass->set_clear_values(
    {
        geometry_buffer->get_depth_clear_value(),
        geometry_buffer->get_color_clear_value(),
        geometry_buffer->get_normal_clear_value(),
        geometry_buffer->get_material_clear_value()
    });

    VkImageViews image_views =
    {
        geometry_buffer->get_depth_image()->get_view(),
        geometry_buffer->get_color_image()->get_view(),
        geometry_buffer->get_normal_image()->get_view(),
        geometry_buffer->get_material_image()->get_view()
    };

    render_pass->create({image_views}, ...);

    //During rendering
    shadow_cache->compute_shadow(command_buffer, frame); //Optional: Generate shadow maps
    indirect_cache->compute_indirect(command_buffer, frame); //Optional: Compute indirect lighting

    render_pass->process(command_buffer, 0); //Write to geometry buffer
    geometry_buffer->apply_local_lighting(command_buffer, per_frame_descriptor_set); //Compute local lighting and write it to the output image
*/

#pragma once
#include <liblava/lava.hpp>
#include <vector>
#include <memory>
#include <cstdint>

#include "scene.hpp"
#include "shadow_cache.hpp"
#include "indirect_cache.hpp"

class GeometryBuffer
{
public:
    typedef std::shared_ptr<GeometryBuffer> Ptr;

public:
    GeometryBuffer() = default;

    bool create(lava::image::ptr output_image, VkImageLayout output_layout, lava::descriptor::ptr per_frame_descriptor, Scene::Ptr scene, ShadowCache::Ptr shadow_cache = {}, IndirectCache::Ptr indirect_cache = {});
    void destroy();
    
    // Apply lighting based on the perspective defined by the per_frame_descriptor_set and write the results into the output image
    void apply_lighting(VkCommandBuffer command_buffer, lava::index frame, VkDescriptorSet per_frame_descriptor_set);

    // Images of the geometry buffer that can be used to create a render pass
    lava::image::ptr get_depth_image() const;
    lava::image::ptr get_color_image() const;
    lava::image::ptr get_normal_image() const;
    lava::image::ptr get_material_image() const;

    // Attachments defining the load and store operations as well as the layout after the render pass
    lava::attachment::ptr get_depth_attachment() const;
    lava::attachment::ptr get_color_attachment() const;
    lava::attachment::ptr get_normal_attachment() const;
    lava::attachment::ptr get_material_attachment() const;

    // Clear colors that should be used when clearing the images
    VkClearValue get_depth_clear_value() const;
    VkClearValue get_color_clear_value() const;
    VkClearValue get_normal_clear_value() const;
    VkClearValue get_material_clear_value() const;

private:
    bool create_images(lava::device_ptr device, const glm::uvec2& size);
    bool create_attachments();
    bool create_sampler(lava::device_ptr device);
    bool create_descriptors(lava::device_ptr device);
    bool create_render_pass(lava::device_ptr device, lava::image::ptr output_image, VkImageLayout output_layout);
    bool create_pipeline(lava::device_ptr device, lava::descriptor::ptr per_frame_descriptor);
    
    void pipline_function(VkCommandBuffer command_buffer);

private:
    Scene::Ptr scene;
    ShadowCache::Ptr shadow_cache;
    IndirectCache::Ptr indirect_cache;
    uint32_t frame = 0;

    lava::image::ptr output_image;
    VkImageLayout output_layout = VK_IMAGE_LAYOUT_UNDEFINED;

    lava::descriptor::pool::ptr descriptor_pool;
    lava::descriptor::ptr buffer_descriptor;
    VkDescriptorSet buffer_descriptor_set = nullptr;

    lava::pipeline_layout::ptr pipeline_layout;
    lava::graphics_pipeline::ptr pipeline;
    lava::render_pass::ptr render_pass;
    
    VkSampler sampler = nullptr;
    lava::image::ptr depth_image;
    lava::image::ptr color_image;
    lava::image::ptr normal_image;
    lava::image::ptr material_image;

    lava::attachment::ptr depth_attachment;
    lava::attachment::ptr color_attachment;
    lava::attachment::ptr normal_attachment;
    lava::attachment::ptr material_attachment;
};

GeometryBuffer::Ptr make_geometry_buffer();