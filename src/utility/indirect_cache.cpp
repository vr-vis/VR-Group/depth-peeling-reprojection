#include "indirect_cache.hpp"

namespace glsl
{
    using namespace glm;
    using uint = glm::uint32;

#include "res/dpr/data/indirect_data.inc"
}

bool IndirectCache::create(Scene::Ptr scene, const IndirectCacheSettings& settings)
{
    lava::device_ptr device = scene->get_light_descriptor()->get_device();

    this->scene = scene;
    this->settings = settings;

    if (!this->compute_domain(scene, settings))
    {
        return false;
    }

    if (!this->create_command_buffer(device))
    {
        return false;
    }

    if (!this->create_fence(device))
    {
        return false;
    }

    if (!this->create_buffers(device))
    {
        return false;
    }

    if (!this->create_images(device, settings))
    {
        return false;
    }

    if (!this->create_samplers(device))
    {
        return false;
    }

    if (!this->create_descriptors(device))
    {
        return false;
    }

    if (!this->create_layouts(device, scene))
    {
        return false;
    }

    if (!this->create_geometry_pass(device))
    {
        return false;
    }

    if (!this->create_geometry_pipeline(device))
    {
        return false;
    }

    if (!this->create_capture_pass(device, settings))
    {
        return false;
    }

    if (!this->create_capture_pipeline(device))
    {
        return false;
    }

    if (!this->create_injection_pass(device))
    {
        return false;
    }

    if (!this->create_injection_pipeline(device))
    {
        return false;
    }

    if (!this->create_propagation_pipeline(device))
    {
        return false;
    }

    return true;
}

void IndirectCache::destroy()
{
    lava::device_ptr device = this->scene->get_light_descriptor()->get_device();

    if (this->geometry_pipeline != nullptr)
    {
        this->geometry_pipeline->destroy();
    }

    if (this->capture_pipeline != nullptr)
    {
        this->capture_pipeline->destroy();
    }

    if (this->injection_pipeline != nullptr)
    {
        this->capture_pipeline->destroy();
    }

    if (this->propagation_pipeline != nullptr)
    {
        this->propagation_pipeline->destroy();
    }

    if (this->geometry_pass != nullptr)
    {
        this->geometry_pass->destroy();
    }

    if (this->capture_pass != nullptr)
    {
        this->capture_pass->destroy();
    }

    if (this->injection_pass != nullptr)
    {
        this->injection_pass->destroy();
    }

    if (this->geometry_layout != nullptr)
    {
        this->geometry_layout->destroy();
    }

    if (this->capture_layout != nullptr)
    {
        this->capture_layout->destroy();
    }

    if (this->injection_layout != nullptr)
    {
        this->injection_layout->destroy();
    }

    if (this->propagation_layout != nullptr)
    {
        this->propagation_layout->destroy();
    }

    if (this->indirect_descriptor != nullptr)
    {
        this->indirect_descriptor->free(this->indirect_descriptor_set, this->descriptor_pool->get());
        this->indirect_descriptor->destroy();
    }

    if (this->geometry_descriptor != nullptr)
    {
        this->geometry_descriptor->free(this->geometry_descriptor_set, this->descriptor_pool->get());
        this->geometry_descriptor->destroy();
    }

    if (this->injection_descriptor != nullptr)
    {
        this->injection_descriptor->free(this->injection_descriptor_set, this->descriptor_pool->get());
        this->injection_descriptor->destroy();
    }

    if (this->propagation_descriptor != nullptr)
    {
        this->propagation_descriptor->free(this->propagation_descriptor_set[0], this->descriptor_pool->get());
        this->propagation_descriptor->free(this->propagation_descriptor_set[1], this->descriptor_pool->get());
        this->propagation_descriptor->destroy();
    }

    if (this->descriptor_pool != nullptr)
    {
        this->descriptor_pool->destroy();
    }

    if (this->capture_depth_image != nullptr)
    {
        this->capture_depth_image->destroy();
    }

    if (this->capture_flux_image != nullptr)
    {
        this->capture_flux_image->destroy();
    }

    if (this->capture_normal_image != nullptr)
    {
        this->capture_normal_image->destroy();
    }

    if (this->domain_buffer != nullptr)
    {
        this->domain_buffer->destroy();
    }

    if (this->command_buffer != VK_NULL_HANDLE)
    {
        vkFreeCommandBuffers(device->get(), this->command_pool, 1, &this->command_buffer);
    }

    vkDestroyFence(device->get(), this->fence, lava::memory::alloc());
    vkDestroyCommandPool(device->get(), this->command_pool, lava::memory::alloc());

    vkDestroySampler(device->get(), this->indirect_sampler, lava::memory::alloc());
    vkDestroySampler(device->get(), this->geometry_sampler, lava::memory::alloc());
    vkDestroySampler(device->get(), this->injection_sampler, lava::memory::alloc());

    for (uint32_t index = 0; index < this->distribution_red_image.size(); index++)
    {
        vkDestroyImageView(device->get(), this->distribution_red_image_view[index], lava::memory::alloc());
        vkDestroyImageView(device->get(), this->distribution_green_image_view[index], lava::memory::alloc());
        vkDestroyImageView(device->get(), this->distribution_blue_image_view[index], lava::memory::alloc());

        vkDestroyImageView(device->get(), this->distribution_red_image_array_view[index], lava::memory::alloc());
        vkDestroyImageView(device->get(), this->distribution_green_image_array_view[index], lava::memory::alloc());
        vkDestroyImageView(device->get(), this->distribution_blue_image_array_view[index], lava::memory::alloc());

        vkDestroyImage(device->get(), this->distribution_red_image[index], lava::memory::alloc());
        vkDestroyImage(device->get(), this->distribution_green_image[index], lava::memory::alloc());
        vkDestroyImage(device->get(), this->distribution_blue_image[index], lava::memory::alloc());

        vmaFreeMemory(device->get_allocator()->get(), this->distribution_red_image_memory[index]);
        vmaFreeMemory(device->get_allocator()->get(), this->distribution_green_image_memory[index]);
        vmaFreeMemory(device->get_allocator()->get(), this->distribution_blue_image_memory[index]);
    }

    for (uint32_t index = 0; index < this->distribution_geometry_image.size(); index++)
    {
        vkDestroyImageView(device->get(), this->distribution_geometry_image_view[index], lava::memory::alloc());
        vkDestroyImageView(device->get(), this->distribution_geometry_image_integer_view[index], lava::memory::alloc());

        vkDestroyImage(device->get(), this->distribution_geometry_image[index], lava::memory::alloc());

        vmaFreeMemory(device->get_allocator()->get(), this->distribution_geometry_image_memory[index]);
    }
}

bool IndirectCache::compute_indirect(lava::index frame)
{
    this->frame = frame;

    if (!this->submit_setup())
    {
        return false;
    }

    uint32_t batch_size = 100;
    uint32_t batch_count = this->propagation_iterations / batch_size;
    uint32_t batch_rest = this->propagation_iterations % batch_size;

    if (batch_rest % 2 != 0)
    {
        batch_rest += 1;
    }

    lava::log()->info("Computing Indirect Cache: 0%");

    for (uint32_t index = 0; index < batch_count; index++)
    {
        if (!this->submit_propagation(batch_size))
        {
            return false;
        }

        uint32_t progress = (((index + 1) * batch_size * 100) / this->propagation_iterations);
        lava::log()->info("Computing Indirect Cache: {}%", progress);
    }

    if (batch_rest > 0)
    {
        if (!this->submit_propagation(batch_rest))
        {
            return false;
        }

        lava::log()->info("Computing Indirect Cache: 100%");
    }
    
    if (!this->submit_barrier())
    {
        return false;
    }

    return true;
}

lava::descriptor::ptr IndirectCache::get_descriptor() const
{
    return this->indirect_descriptor;
}

VkDescriptorSet IndirectCache::get_descriptor_set() const
{
    return this->indirect_descriptor_set;
}

const IndirectCacheSettings& IndirectCache::get_settings() const
{
    return this->settings;
}

bool IndirectCache::compute_domain(Scene::Ptr scene, const IndirectCacheSettings& settings)
{
    if (settings.voxel_resolution_scale <= 0)
    {
        lava::log()->error("Voxel resolution scale needs to be greater or equal to one!");
    
        return false;
    }

    glm::vec3 scene_min = scene->get_scene_min();
    glm::vec3 scene_max = scene->get_scene_max();

    glm::vec3 scene_center = (scene_min + scene_max) / 2.0f;
    glm::vec3 scene_size = scene_max - scene_min;

    const glm::uvec3 work_group_size = glm::uvec3(16, 16, 2);

    this->domain_cell_size = settings.cell_size;
    this->domain_indirect_resolution = glm::uvec3(glm::max(glm::ceil(scene_size / this->domain_cell_size), glm::vec3(1.0f)));
    this->domain_geometry_resolution = this->domain_indirect_resolution + glm::uvec3(1);
    this->domain_voxel_resolution = this->domain_geometry_resolution * settings.voxel_resolution_scale;

    uint32_t indirect_pixel_count = this->domain_indirect_resolution.x * this->domain_indirect_resolution.y * this->domain_indirect_resolution.z;
    uint32_t geometry_pixel_count = this->domain_geometry_resolution.x * this->domain_geometry_resolution.y * this->domain_geometry_resolution.z;
    uint32_t memory_size = (indirect_pixel_count * 9 * sizeof(glm::vec4) + geometry_pixel_count * 2 * sizeof(glm::vec4)) / (1024 * 1024); // In MiB

    if (memory_size > settings.memory_limit)
    {
        float factor_scene = 11 * (scene_size.x * scene_size.y * scene_size.z) * sizeof(glm::vec4);
        float factor_limit = settings.memory_limit * 1024 * 1024;

        this->domain_cell_size = std::cbrt(factor_scene / factor_limit);

        this->domain_indirect_resolution = glm::uvec3(glm::max(glm::ceil(scene_size / this->domain_cell_size), glm::vec3(1.0f)));
        this->domain_geometry_resolution = this->domain_indirect_resolution + glm::uvec3(1);
        this->domain_voxel_resolution = this->domain_geometry_resolution * settings.voxel_resolution_scale;

        lava::log()->warn("Indirect Cache exceed Memory Limit. Reducing Cell Size to {} Meters!", this->domain_cell_size);
    }

    else
    {
        lava::log()->info("Indirect Cache Size: {} MiB", memory_size);
    }

    this->domain_min = scene_center - 0.5f * this->domain_cell_size * glm::vec3(this->domain_indirect_resolution);
    this->domain_max = scene_center + 0.5f * this->domain_cell_size * glm::vec3(this->domain_indirect_resolution);
    this->domain_work_groups = this->domain_indirect_resolution / work_group_size;
    
    if ((this->domain_indirect_resolution.x % work_group_size.x) > 0)
    {
        this->domain_work_groups.x += 1;
    }

    if ((this->domain_indirect_resolution.y % work_group_size.y) > 0)
    {
        this->domain_work_groups.y += 1;
    }

    if ((this->domain_indirect_resolution.z % work_group_size.z) > 0)
    {
        this->domain_work_groups.z += 1;
    }

    this->propagation_iterations = 2 * glm::max(this->domain_indirect_resolution.x, glm::max(this->domain_indirect_resolution.y, this->domain_indirect_resolution.z));

    return true;
}

bool IndirectCache::create_command_buffer(lava::device_ptr device)
{
    VkCommandPoolCreateInfo pool_info;
    pool_info.sType  = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pool_info.pNext = nullptr;
    pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    pool_info.queueFamilyIndex = device->get_graphics_queue().family;

    if(vkCreateCommandPool(device->get(), &pool_info, lava::memory::alloc(), &this->command_pool) != VK_SUCCESS)
    {
        lava::log()->error("Can't create command pool for indirect cache!");

        return false;
    }

    VkCommandBufferAllocateInfo buffer_info;
    buffer_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    buffer_info.pNext = nullptr;
    buffer_info.commandPool = this->command_pool;
    buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    buffer_info.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(device->get(), &buffer_info, &this->command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't create command buffer for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_fence(lava::device_ptr device)
{
    VkFenceCreateInfo fence_info;
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.pNext = nullptr;
    fence_info.flags = 0;

    if (vkCreateFence(device->get(), &fence_info, lava::memory::alloc(), &this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't create fence for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_buffers(lava::device_ptr device)
{
    glsl::IndirectDomain indirect_domain;
    indirect_domain.indirect_resolution = this->domain_indirect_resolution;
    indirect_domain.cell_size = this->domain_cell_size;
    indirect_domain.geometry_resolution = this->domain_geometry_resolution;
    indirect_domain.padding1 = 0;
    indirect_domain.min = this->domain_min;
    indirect_domain.padding2 = 0;
    indirect_domain.max = this->domain_max;
    indirect_domain.padding3 = 0;

    this->domain_buffer = lava::make_buffer();

    if (!this->domain_buffer->create_mapped(device, &indirect_domain, sizeof(indirect_domain), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT))
    {
        lava::log()->error("Can't create domain buffer for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_images(lava::device_ptr device, const IndirectCacheSettings& settings)
{
    this->capture_depth_image = lava::make_image(VK_FORMAT_D16_UNORM);
    this->capture_depth_image->set_usage(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

    if (!this->capture_depth_image->create(device, glm::uvec2(settings.capture_resolution)))
    {
        lava::log()->error("Can't create depth capture image for indirect cache!");

        return false;
    }

    this->capture_flux_image = lava::make_image(VK_FORMAT_R16G16B16A16_SFLOAT);
    this->capture_flux_image->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

    if (!this->capture_flux_image->create(device, glm::uvec2(settings.capture_resolution)))
    {
        lava::log()->error("Can't create flux capture image for indirect cache!");

        return false;
    }

    this->capture_normal_image = lava::make_image(VK_FORMAT_R16G16_UNORM);
    this->capture_normal_image->set_usage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

    if (!this->capture_normal_image->create(device, glm::uvec2(settings.capture_resolution)))
    {
        lava::log()->error("Can't create normal capture image for indirect cache!");

        return false;
    }

    VmaAllocationCreateInfo allocation_info;
    allocation_info.flags = 0;
    allocation_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    allocation_info.requiredFlags = 0;
    allocation_info.preferredFlags = 0;
    allocation_info.memoryTypeBits = 0;
    allocation_info.pool = nullptr;
    allocation_info.pUserData = nullptr;
    allocation_info.priority = 1.0f;

    VkImageCreateInfo image_create_info;
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.pNext = nullptr;
    image_create_info.flags = VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
    image_create_info.imageType = VK_IMAGE_TYPE_3D;
    image_create_info.format = VK_FORMAT_R16G16B16A16_SFLOAT;
    image_create_info.extent.width = this->domain_indirect_resolution.x;
    image_create_info.extent.height = this->domain_indirect_resolution.y;
    image_create_info.extent.depth = this->domain_indirect_resolution.z;
    image_create_info.mipLevels = 1;
    image_create_info.arrayLayers = 1;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_create_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create_info.queueFamilyIndexCount = 0;
    image_create_info.pQueueFamilyIndices = nullptr;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VkImageViewCreateInfo view_create_info;
    view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view_create_info.pNext = nullptr;
    view_create_info.flags = 0;
    view_create_info.image = VK_NULL_HANDLE;
    view_create_info.viewType = VK_IMAGE_VIEW_TYPE_3D;
    view_create_info.format = VK_FORMAT_R16G16B16A16_SFLOAT;
    view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    view_create_info.subresourceRange.baseMipLevel = 0;
    view_create_info.subresourceRange.levelCount = 1;
    view_create_info.subresourceRange.baseArrayLayer = 0;
    view_create_info.subresourceRange.layerCount = 1;

    VkImageViewCreateInfo array_view_create_info;
    array_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    array_view_create_info.pNext = nullptr;
    array_view_create_info.flags = 0;
    array_view_create_info.image = VK_NULL_HANDLE;
    array_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
    array_view_create_info.format = VK_FORMAT_R16G16B16A16_SFLOAT;
    array_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    array_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    array_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    array_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    array_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    array_view_create_info.subresourceRange.baseMipLevel = 0;
    array_view_create_info.subresourceRange.levelCount = 1;
    array_view_create_info.subresourceRange.baseArrayLayer = 0;
    array_view_create_info.subresourceRange.layerCount = this->domain_indirect_resolution.z;

    for (uint32_t index = 0; index < this->distribution_red_image.size(); index++)
    {
        if (vmaCreateImage(device->get_allocator()->get(), &image_create_info, &allocation_info, &this->distribution_red_image[index], &this->distribution_red_image_memory[index], nullptr) != VK_SUCCESS)
        {
            lava::log()->error("Can't create red distribution image for indirect cache!");

            return false;
        }

        if (vmaCreateImage(device->get_allocator()->get(), &image_create_info, &allocation_info, &this->distribution_green_image[index], &this->distribution_green_image_memory[index], nullptr) != VK_SUCCESS)
        {
            lava::log()->error("Can't create green distribution image for indirect cache!");

            return false;
        }

        if (vmaCreateImage(device->get_allocator()->get(), &image_create_info, &allocation_info, &this->distribution_blue_image[index], &this->distribution_blue_image_memory[index], nullptr) != VK_SUCCESS)
        {
            lava::log()->error("Can't create blue distribution image for indirect cache!");

            return false;
        }

        view_create_info.image = this->distribution_red_image[index];
        array_view_create_info.image = this->distribution_red_image[index];

        if (vkCreateImageView(device->get(), &view_create_info, lava::memory::alloc(), &this->distribution_red_image_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create red distribution image view for indirect cache!");

            return false;
        }

        if (vkCreateImageView(device->get(), &array_view_create_info, lava::memory::alloc(), &this->distribution_red_image_array_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create red distribution image array view for indirect cache!");

            return false;
        }

        view_create_info.image = this->distribution_green_image[index];
        array_view_create_info.image = this->distribution_green_image[index];

        if (vkCreateImageView(device->get(), &view_create_info, lava::memory::alloc(), &this->distribution_green_image_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create green distribution image view for indirect cache!");

            return false;
        }

        if (vkCreateImageView(device->get(), &array_view_create_info, lava::memory::alloc(), &this->distribution_green_image_array_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create green distribution image array view for indirect cache!");

            return false;
        }

        view_create_info.image = this->distribution_blue_image[index];
        array_view_create_info.image = this->distribution_blue_image[index];

        if (vkCreateImageView(device->get(), &view_create_info, lava::memory::alloc(), &this->distribution_blue_image_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create blue distribution image view for indirect cache!");

            return false;
        }

        if (vkCreateImageView(device->get(), &array_view_create_info, lava::memory::alloc(), &this->distribution_blue_image_array_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create blue distribution image array view for indirect cache!");

            return false;
        }
    }

    image_create_info.flags = VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
    image_create_info.format = VK_FORMAT_R16G16_SFLOAT;
    image_create_info.extent.width = this->domain_geometry_resolution.x;
    image_create_info.extent.height = this->domain_geometry_resolution.y;
    image_create_info.extent.depth = this->domain_geometry_resolution.z;
    image_create_info.usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    
    view_create_info.format = VK_FORMAT_R16G16_SFLOAT;

    VkImageViewCreateInfo integer_view_create_info;
    integer_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    integer_view_create_info.pNext = nullptr;
    integer_view_create_info.flags = 0;
    integer_view_create_info.image = VK_NULL_HANDLE;
    integer_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_3D;
    integer_view_create_info.format = VK_FORMAT_R32_UINT;
    integer_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    integer_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    integer_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    integer_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    integer_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    integer_view_create_info.subresourceRange.baseMipLevel = 0;
    integer_view_create_info.subresourceRange.levelCount = 1;
    integer_view_create_info.subresourceRange.baseArrayLayer = 0;
    integer_view_create_info.subresourceRange.layerCount = 1;

    for (uint32_t index = 0; index < this->distribution_geometry_image.size(); index++)
    {
        if (vmaCreateImage(device->get_allocator()->get(), &image_create_info, &allocation_info, &this->distribution_geometry_image[index], &this->distribution_geometry_image_memory[index], nullptr) != VK_SUCCESS)
        {
            lava::log()->error("Can't create geometry distribution image for indirect cache!");

            return false;
        }

        view_create_info.image = this->distribution_geometry_image[index];
        integer_view_create_info.image = this->distribution_geometry_image[index];

        if (vkCreateImageView(device->get(), &view_create_info, lava::memory::alloc(), &this->distribution_geometry_image_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create geometry distribution image view for indirect cache!");

            return false;
        }

        if (vkCreateImageView(device->get(), &integer_view_create_info, lava::memory::alloc(), &this->distribution_geometry_image_integer_view[index]) != VK_SUCCESS)
        {
            lava::log()->error("Can't create geometry distribution image integer view for indirect cache!");

            return false;
        }
    }

    return true;
}

bool IndirectCache::create_samplers(lava::device_ptr device)
{
    VkSamplerCreateInfo sampler_info;
    sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_info.pNext = nullptr;
    sampler_info.flags = 0;
    sampler_info.magFilter = VK_FILTER_LINEAR;
    sampler_info.minFilter = VK_FILTER_LINEAR;
    sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    sampler_info.mipLodBias = 0.0f;
    sampler_info.anisotropyEnable = VK_FALSE;
    sampler_info.maxAnisotropy = 1.0f;
    sampler_info.compareEnable = VK_FALSE;
    sampler_info.compareOp = VK_COMPARE_OP_LESS;
    sampler_info.minLod = 0.0f;
    sampler_info.maxLod = 0.0f;
    sampler_info.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    sampler_info.unnormalizedCoordinates = VK_FALSE;

    if (vkCreateSampler(device->get(), &sampler_info, lava::memory::alloc(), &this->indirect_sampler) != VK_SUCCESS)
    {
        lava::log()->error("Can't create indirect sampler for indirect cache!");

        return false;
    }

    if (vkCreateSampler(device->get(), &sampler_info, lava::memory::alloc(), &this->geometry_sampler) != VK_SUCCESS)
    {
        lava::log()->error("Can't create geometry sampler for indirect cache!");

        return false;
    }
    
    sampler_info.magFilter = VK_FILTER_NEAREST;
    sampler_info.minFilter = VK_FILTER_NEAREST;
    sampler_info.unnormalizedCoordinates = VK_TRUE;

    if (vkCreateSampler(device->get(), &sampler_info, lava::memory::alloc(), &this->injection_sampler) != VK_SUCCESS)
    {
        lava::log()->error("Can't create injection sampler for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_descriptors(lava::device_ptr device)
{
    lava::VkDescriptorPoolSizes descriptor_type_count =
    {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 5 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 10 },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 20 }
    };

    this->descriptor_pool = lava::make_descriptor_pool();

    if (!this->descriptor_pool->create(device, descriptor_type_count, 5))
    {
        lava::log()->error("Can't create descriptor pool for indirect cache!");

        return false;
    }

    this->indirect_descriptor = lava::make_descriptor();
    this->indirect_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 0: indirect red
    this->indirect_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 1: indirect green
    this->indirect_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 2: indirect blue
    this->indirect_descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT);         //descriptor-binding index 3: indirect domain

    if (!this->indirect_descriptor->create(device))
    {
        lava::log()->error("Can't create indirect descriptor set layout for indirect cache!");

        return false;
    }

    this->geometry_descriptor = lava::make_descriptor();
    this->geometry_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 0: indirect domain

    this->geometry_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT);                                 //descriptor-binding index 1: geometry distribution xy
    this->geometry_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT);                                 //descriptor-binding index 2: geometry distribution zw
    
    if (!this->geometry_descriptor->create(device))
    {
        lava::log()->error("Can't create geometry descriptor set layout for indirect cache!");

        return false;
    }

    this->injection_descriptor = lava::make_descriptor();
    this->injection_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT); //descriptor-binding index 0: capture depth
    this->injection_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT); //descriptor-binding index 1: capture flux
    this->injection_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_VERTEX_BIT); //descriptor-binding index 2: capture normal
    this->injection_descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_GEOMETRY_BIT);       //descriptor-binding index 3: indirect domain
    
    if (!this->injection_descriptor->create(device))
    {
        lava::log()->error("Can't create injection descriptor set layout for indirect cache!");

        return false;
    }

    this->propagation_descriptor = lava::make_descriptor();
    this->propagation_descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 0: image red source distribution
    this->propagation_descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 1: image green source distribution
    this->propagation_descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 2: image blue source distribution
        
    this->propagation_descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 3: image red destination distribution
    this->propagation_descriptor->add_binding(4, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 4: image green destination distribution
    this->propagation_descriptor->add_binding(5, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 5: image blue destination distribution
    
    this->propagation_descriptor->add_binding(6, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 6: image red indirect
    this->propagation_descriptor->add_binding(7, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 7: image green indirect
    this->propagation_descriptor->add_binding(8, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);           //descriptor-binding index 8: image blue indirect

    this->propagation_descriptor->add_binding(9, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);  //descriptor-binding index 9: image xz geometrz distribution
    this->propagation_descriptor->add_binding(10, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT); //descriptor-binding index 10: image yw geometrz distribution

    this->propagation_descriptor->add_binding(11, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT);         //descriptor-binding index 11: indirect domain
    
    if (!this->propagation_descriptor->create(device))
    {
        lava::log()->error("Can't create propagation descriptor set layout for indirect cache!");

        return false;
    }

    this->indirect_descriptor_set = this->indirect_descriptor->allocate(this->descriptor_pool->get());
    this->geometry_descriptor_set = this->geometry_descriptor->allocate(this->descriptor_pool->get());
    this->injection_descriptor_set = this->injection_descriptor->allocate(this->descriptor_pool->get());
    this->propagation_descriptor_set[0] = this->propagation_descriptor->allocate(this->descriptor_pool->get());
    this->propagation_descriptor_set[1] = this->propagation_descriptor->allocate(this->descriptor_pool->get());

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    std::vector<VkDescriptorImageInfo> image_infos;

    descriptor_writes.reserve(35);
    image_infos.reserve(30);

    std::vector<VkImageView> indirect_images = 
    {
        this->distribution_red_image_view[2],
        this->distribution_green_image_view[2],
        this->distribution_blue_image_view[2],
    };

    for (uint32_t index = 0; index < indirect_images.size(); index++)
    {
        VkDescriptorImageInfo& image_info = image_infos.emplace_back();
        image_info.sampler = this->indirect_sampler;
        image_info.imageView = indirect_images[index];
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        VkWriteDescriptorSet& descriptor_write = descriptor_writes.emplace_back();
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = this->indirect_descriptor_set;
        descriptor_write.dstBinding = index;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptor_write.pImageInfo = &image_info;
        descriptor_write.pBufferInfo = nullptr;
        descriptor_write.pTexelBufferView = nullptr;
    }

    for (uint32_t index = 0; index < this->distribution_geometry_image_integer_view.size(); index++)
    {
        VkDescriptorImageInfo& image_info = image_infos.emplace_back();
        image_info.sampler = nullptr;
        image_info.imageView = this->distribution_geometry_image_integer_view[index];
        image_info.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

        VkWriteDescriptorSet& descriptor_write = descriptor_writes.emplace_back();
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = this->geometry_descriptor_set;
        descriptor_write.dstBinding = index + 1;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        descriptor_write.pImageInfo = &image_info;
        descriptor_write.pBufferInfo = nullptr;
        descriptor_write.pTexelBufferView = nullptr;
    }

    std::vector<VkImageView> injection_images = 
    {
        this->capture_depth_image->get_view(),
        this->capture_flux_image->get_view(),
        this->capture_normal_image->get_view()
    };

    for (uint32_t index = 0; index < injection_images.size(); index++)
    {
        VkDescriptorImageInfo& image_info = image_infos.emplace_back();
        image_info.sampler = this->injection_sampler;
        image_info.imageView = injection_images[index];
        image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        VkWriteDescriptorSet& descriptor_write = descriptor_writes.emplace_back();
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = this->injection_descriptor_set;
        descriptor_write.dstBinding = index;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptor_write.pImageInfo = &image_info;
        descriptor_write.pBufferInfo = nullptr;
        descriptor_write.pTexelBufferView = nullptr;
    }

    std::vector<VkImageView> propagation_images = 
    {
        this->distribution_red_image_view[0],
        this->distribution_green_image_view[0],
        this->distribution_blue_image_view[0],
        this->distribution_red_image_view[1],
        this->distribution_green_image_view[1],
        this->distribution_blue_image_view[1],
        this->distribution_red_image_view[2],
        this->distribution_green_image_view[2],
        this->distribution_blue_image_view[2]
    };

    for (uint32_t set_index = 0; set_index < this->propagation_descriptor_set.size(); set_index++)
    {
        for (uint32_t image_index = 0; image_index < propagation_images.size(); image_index++)
        {
            uint32_t binding = image_index;

            if (image_index < 6)
            {
                binding = (binding + set_index * 3) % 6;
            }

            VkDescriptorImageInfo& image_info = image_infos.emplace_back();
            image_info.sampler = VK_NULL_HANDLE;
            image_info.imageView = propagation_images[image_index];
            image_info.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

            VkWriteDescriptorSet& descriptor_write = descriptor_writes.emplace_back();
            descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptor_write.pNext = nullptr;
            descriptor_write.dstSet = this->propagation_descriptor_set[set_index];
            descriptor_write.dstBinding = binding;
            descriptor_write.dstArrayElement = 0;
            descriptor_write.descriptorCount = 1;
            descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
            descriptor_write.pImageInfo = &image_info;
            descriptor_write.pBufferInfo = nullptr;
            descriptor_write.pTexelBufferView = nullptr;
        }

        for (uint32_t image_index = 0; image_index < this->distribution_geometry_image_view.size(); image_index++)
        {
            VkDescriptorImageInfo& image_info = image_infos.emplace_back();
            image_info.sampler = this->geometry_sampler;
            image_info.imageView = this->distribution_geometry_image_view[image_index];
            image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

            VkWriteDescriptorSet& descriptor_write = descriptor_writes.emplace_back();
            descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptor_write.pNext = nullptr;
            descriptor_write.dstSet = this->propagation_descriptor_set[set_index];
            descriptor_write.dstBinding = 9 + image_index;
            descriptor_write.dstArrayElement = 0;
            descriptor_write.descriptorCount = 1;
            descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            descriptor_write.pImageInfo = &image_info;
            descriptor_write.pBufferInfo = nullptr;
            descriptor_write.pTexelBufferView = nullptr;
        }

        VkWriteDescriptorSet& propagation_domain_write = descriptor_writes.emplace_back();
        propagation_domain_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        propagation_domain_write.pNext = nullptr;
        propagation_domain_write.dstSet = this->propagation_descriptor_set[set_index];
        propagation_domain_write.dstBinding = 11;
        propagation_domain_write.dstArrayElement = 0;
        propagation_domain_write.descriptorCount = 1;
        propagation_domain_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        propagation_domain_write.pImageInfo = nullptr;
        propagation_domain_write.pBufferInfo = this->domain_buffer->get_descriptor_info();
        propagation_domain_write.pTexelBufferView = nullptr;
    }
    
    VkWriteDescriptorSet& indirect_domain_write = descriptor_writes.emplace_back();
    indirect_domain_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    indirect_domain_write.pNext = nullptr;
    indirect_domain_write.dstSet = this->indirect_descriptor_set;
    indirect_domain_write.dstBinding = 3;
    indirect_domain_write.dstArrayElement = 0;
    indirect_domain_write.descriptorCount = 1;
    indirect_domain_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    indirect_domain_write.pImageInfo = nullptr;
    indirect_domain_write.pBufferInfo = this->domain_buffer->get_descriptor_info();
    indirect_domain_write.pTexelBufferView = nullptr;

    VkWriteDescriptorSet& geometry_domain_write = descriptor_writes.emplace_back();
    geometry_domain_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    geometry_domain_write.pNext = nullptr;
    geometry_domain_write.dstSet = this->geometry_descriptor_set;
    geometry_domain_write.dstBinding = 0;
    geometry_domain_write.dstArrayElement = 0;
    geometry_domain_write.descriptorCount = 1;
    geometry_domain_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    geometry_domain_write.pImageInfo = nullptr;
    geometry_domain_write.pBufferInfo = this->domain_buffer->get_descriptor_info();
    geometry_domain_write.pTexelBufferView = nullptr;

    VkWriteDescriptorSet& injection_domain_write = descriptor_writes.emplace_back();
    injection_domain_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    injection_domain_write.pNext = nullptr;
    injection_domain_write.dstSet = this->injection_descriptor_set;
    injection_domain_write.dstBinding = 3;
    injection_domain_write.dstArrayElement = 0;
    injection_domain_write.descriptorCount = 1;
    injection_domain_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    injection_domain_write.pImageInfo = nullptr;
    injection_domain_write.pBufferInfo = this->domain_buffer->get_descriptor_info();
    injection_domain_write.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool IndirectCache::create_layouts(lava::device_ptr device, Scene::Ptr scene)
{
    VkPushConstantRange geometry_range;
    geometry_range.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    geometry_range.offset = 0;
    geometry_range.size = sizeof(uint32_t) * 3;

    this->geometry_layout = lava::make_pipeline_layout();
    this->geometry_layout->add(scene->get_mesh_descriptor()); //set 0: mesh descriptor
    this->geometry_layout->add(this->geometry_descriptor);    //set 1: geometry descriptor
    this->geometry_layout->add(geometry_range);

    if (!this->geometry_layout->create(device))
    {
        lava::log()->error("Can't create geometry pipeline layout for indirect cache!");

        return false;
    }

    VkPushConstantRange capture_range;
    capture_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    capture_range.offset = 0;
    capture_range.size = sizeof(uint32_t) * 2;

    this->capture_layout = lava::make_pipeline_layout();
    this->capture_layout->add(scene->get_mesh_descriptor());        //set 0: mesh descriptor
    this->capture_layout->add(scene->get_light_descriptor());       //set 1: light descriptor
    this->capture_layout->add(scene->get_material_descriptor());    //set 2: material descriptor
    this->capture_layout->add(capture_range);

    if (!this->capture_layout->create(device))
    {
        lava::log()->error("Can't create capture pipeline layout for indirect cache!");

        return false;
    }

    VkPushConstantRange injection_range;
    injection_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    injection_range.offset = 0;
    injection_range.size = sizeof(uint32_t) * 2 + sizeof(float);

    this->injection_layout = lava::make_pipeline_layout();
    this->injection_layout->add(scene->get_light_descriptor()); //set 0: light descriptor
    this->injection_layout->add(this->injection_descriptor);    //set 1: injection descriptor
    this->injection_layout->add(injection_range);

    if (!this->injection_layout->create(device))
    {
        lava::log()->error("Can't create injection pipeline layout for indirect cache!");

        return false;
    }

    this->propagation_layout = lava::make_pipeline_layout();
    this->propagation_layout->add(this->propagation_descriptor); //set 0: propagation descriptor

    if (!this->propagation_layout->create(device))
    {
        lava::log()->error("Can't create propagation pipeline layout for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_geometry_pass(lava::device_ptr device)
{
    lava::subpass::ptr subpass = lava::make_subpass();

    this->geometry_pass = lava::make_render_pass(device);
    this->geometry_pass->add(subpass);

    uint32_t voxel_max_dimension = glm::max(this->domain_voxel_resolution.x, glm::max(this->domain_voxel_resolution.y, this->domain_voxel_resolution.z));

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        glm::vec2(voxel_max_dimension)
    };

    lava::VkImageViews framebuffer_views = {};

    if (!this->geometry_pass->create({ framebuffer_views }, framebuffer_area))
    {
        lava::log()->error("Can't create geometry pass for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_geometry_pipeline(lava::device_ptr device)
{
    this->geometry_pipeline = lava::make_graphics_pipeline(device);
    this->geometry_pipeline->set_layout(this->geometry_layout);
    this->geometry_pipeline->set_auto_size(false);
    this->geometry_pipeline->set_viewport_count(3);
    this->geometry_pipeline->set_scissor_count(3);

    scene->set_vertex_input(this->geometry_pipeline.get());

    if (!this->geometry_pipeline->add_shader(lava::file_data("dpr/binary/indirect_geometry_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!this->geometry_pipeline->add_shader(lava::file_data("dpr/binary/indirect_geometry_geometry.spirv"), VK_SHADER_STAGE_GEOMETRY_BIT))
    {
        return false;
    }

    if (!this->geometry_pipeline->add_shader(lava::file_data("dpr/binary/indirect_geometry_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        return false;
    }

    this->geometry_pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipeline_geometry(command_buffer);
    };

    if (!this->geometry_pipeline->create(this->geometry_pass->get()))
    {
        return false;
    }

    this->geometry_pass->add_front(this->geometry_pipeline);

    return true;
}

bool IndirectCache::create_capture_pass(lava::device_ptr device, const IndirectCacheSettings& settings)
{
    VkClearValue clear_depth;
    clear_depth.depthStencil.depth = 1.0;
    clear_depth.depthStencil.stencil = 0;

    VkClearValue clear_color;
    clear_color.color.float32[0] = 0.0f;
    clear_color.color.float32[1] = 0.0f;
    clear_color.color.float32[2] = 0.0f;
    clear_color.color.float32[3] = 0.0f;

    lava::attachment::ptr depth_attachment = lava::make_attachment(VK_FORMAT_D16_UNORM);
    depth_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    depth_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    depth_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    lava::attachment::ptr flux_attachment = lava::make_attachment(VK_FORMAT_R16G16B16A16_SFLOAT);
    flux_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    flux_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    flux_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    lava::attachment::ptr normal_attachment = lava::make_attachment(VK_FORMAT_R16G16_UNORM);
    normal_attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    normal_attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    normal_attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    VkAttachmentReference flux_attachment_reference;
    flux_attachment_reference.attachment = 1;
    flux_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference normal_attachment_reference;
    normal_attachment_reference.attachment = 2;
    normal_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_depth_stencil_attachment(0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    subpass->set_color_attachments(
    {
        flux_attachment_reference,
        normal_attachment_reference
    });

    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

    this->capture_pass = lava::make_render_pass(device);
    this->capture_pass->add(subpass);
    this->capture_pass->add(subpass_begin_dependency);
    this->capture_pass->add(subpass_end_dependency);
    this->capture_pass->add(depth_attachment);      //location 0: depth
    this->capture_pass->add(flux_attachment);       //location 1: flux
    this->capture_pass->add(normal_attachment);     //location 2: normal
    this->capture_pass->set_clear_values(
    { 
        clear_depth,
        clear_color,
        clear_color
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        glm::vec2(settings.capture_resolution)
    };

    lava::VkImageViews framebuffer_views =
    {
        this->capture_depth_image->get_view(),
        this->capture_flux_image->get_view(),
        this->capture_normal_image->get_view()
    };

    if (!this->capture_pass->create({ framebuffer_views }, framebuffer_area))
    {
        lava::log()->error("Can't create capture pass for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_capture_pipeline(lava::device_ptr device)
{
    VkPipelineColorBlendAttachmentState blend_state;
    blend_state.blendEnable = VK_FALSE;
    blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    this->capture_pipeline = lava::make_graphics_pipeline(device);
    this->capture_pipeline->set_layout(this->capture_layout);
    this->capture_pipeline->set_rasterization_front_face(VK_FRONT_FACE_CLOCKWISE);
    this->capture_pipeline->set_rasterization_cull_mode(VK_CULL_MODE_BACK_BIT);
    this->capture_pipeline->set_depth_test_and_write(true, true);
    this->capture_pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);
    this->capture_pipeline->add_color_blend_attachment(blend_state);
    this->capture_pipeline->add_color_blend_attachment(blend_state);

    scene->set_vertex_input(this->capture_pipeline.get());

    if (!this->capture_pipeline->add_shader(lava::file_data("dpr/binary/indirect_capture_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!this->capture_pipeline->add_shader(lava::file_data("dpr/binary/indirect_capture_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        return false;
    }

    this->capture_pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipeline_capture(command_buffer);
    };

    if (!this->capture_pipeline->create(this->capture_pass->get()))
    {
        return false;
    }

    this->capture_pass->add_front(this->capture_pipeline);

    return true;
}

bool IndirectCache::create_injection_pass(lava::device_ptr device)
{
    lava::attachment::ptr attachment_red = lava::make_attachment(VK_FORMAT_R16G16B16A16_SFLOAT);
    attachment_red->set_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_STORE);
    attachment_red->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    attachment_red->set_layouts(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    lava::attachment::ptr attachment_green = lava::make_attachment(VK_FORMAT_R16G16B16A16_SFLOAT);
    attachment_green->set_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_STORE);
    attachment_green->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    attachment_green->set_layouts(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    lava::attachment::ptr attachment_blue = lava::make_attachment(VK_FORMAT_R16G16B16A16_SFLOAT);
    attachment_blue->set_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_STORE);
    attachment_blue->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    attachment_blue->set_layouts(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

    VkAttachmentReference attachment_red_reference;
    attachment_red_reference.attachment = 0;
    attachment_red_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachment_green_reference;
    attachment_green_reference.attachment = 1;
    attachment_green_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachment_blue_reference;
    attachment_blue_reference.attachment = 2;
    attachment_blue_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_color_attachments(
    {
        attachment_red_reference,
        attachment_green_reference,
        attachment_blue_reference
    });

    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT);

    this->injection_pass = lava::make_render_pass(device);
    this->injection_pass->add(subpass);
    this->injection_pass->add(subpass_begin_dependency);
    this->injection_pass->add(subpass_end_dependency);
    this->injection_pass->add(attachment_red);      //location 0: distribution red
    this->injection_pass->add(attachment_green);    //location 1: distribution green
    this->injection_pass->add(attachment_blue);     //location 2: distribution blue
    this->injection_pass->set_layers(this->domain_indirect_resolution.z);

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        glm::vec2(this->domain_indirect_resolution.x, this->domain_indirect_resolution.y)
    };

    lava::VkImageViews framebuffer_views =
    {
        this->distribution_red_image_array_view[0],
        this->distribution_green_image_array_view[0],
        this->distribution_blue_image_array_view[0],
    };

    if (!this->injection_pass->create({ framebuffer_views }, framebuffer_area))
    {
        lava::log()->error("Can't create injection pass for indirect cache!");

        return false;
    }

    return true;
}

bool IndirectCache::create_injection_pipeline(lava::device_ptr device)
{
    VkPipelineColorBlendAttachmentState blend_state;
    blend_state.blendEnable = VK_TRUE;
    blend_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_state.dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_state.colorBlendOp = VK_BLEND_OP_ADD;
    blend_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_state.alphaBlendOp = VK_BLEND_OP_ADD;
    blend_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    this->injection_pipeline = lava::make_graphics_pipeline(device);
    this->injection_pipeline->set_layout(this->injection_layout);
    this->injection_pipeline->set_input_assembly_topology(VK_PRIMITIVE_TOPOLOGY_POINT_LIST);
    this->injection_pipeline->add_color_blend_attachment(blend_state);
    this->injection_pipeline->add_color_blend_attachment(blend_state);
    this->injection_pipeline->add_color_blend_attachment(blend_state);

    if (!this->injection_pipeline->add_shader(lava::file_data("dpr/binary/indirect_injection_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!this->injection_pipeline->add_shader(lava::file_data("dpr/binary/indirect_injection_geometry.spirv"), VK_SHADER_STAGE_GEOMETRY_BIT))
    {
        return false;
    }

    if (!this->injection_pipeline->add_shader(lava::file_data("dpr/binary/indirect_injection_fragment.spirv"), VK_SHADER_STAGE_FRAGMENT_BIT))
    {
        return false;
    }

    this->injection_pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipeline_injection(command_buffer);
    };

    if (!this->injection_pipeline->create(this->injection_pass->get()))
    {
        return false;
    }

    this->injection_pass->add_front(this->injection_pipeline);

    return true;
}

bool IndirectCache::create_propagation_pipeline(lava::device_ptr device)
{
    this->propagation_pipeline = lava::make_compute_pipeline(device);
    this->propagation_pipeline->set_layout(this->propagation_layout);
    
    if (!this->propagation_pipeline->set_shader_stage(lava::file_data("dpr/binary/indirect_propagation_compute.spirv"), VK_SHADER_STAGE_COMPUTE_BIT))
    {
        return false;
    }

    if (!this->propagation_pipeline->create())
    {
        return false;
    }

    return true;
}

bool IndirectCache::submit_setup()
{
    VkCommandBufferBeginInfo begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    begin_info.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(this->command_buffer, &begin_info) != VK_SUCCESS)
    {
        lava::log()->error("Can't begin command buffer during setup of indirect cache!");

        return false;
    }

    const std::vector<SceneLight>& lights = this->scene->get_lights();

    std::vector<VkImageMemoryBarrier> clear_begin_barriers = IndirectCache::build_barriers(5, 0, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    clear_begin_barriers[0].image = this->distribution_red_image[0];
    clear_begin_barriers[1].image = this->distribution_green_image[0];
    clear_begin_barriers[2].image = this->distribution_blue_image[0];
    clear_begin_barriers[3].image = this->distribution_geometry_image[0];
    clear_begin_barriers[4].image = this->distribution_geometry_image[1];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, clear_begin_barriers.size(), clear_begin_barriers.data());

    VkClearColorValue clear_color;
    clear_color.float32[0] = 0.0f;
    clear_color.float32[1] = 0.0f;
    clear_color.float32[2] = 0.0f;
    clear_color.float32[3] = 0.0f;

    VkImageSubresourceRange subresource_range;
    subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_range.baseMipLevel = 0;
    subresource_range.levelCount = 1;
    subresource_range.baseArrayLayer = 0;
    subresource_range.layerCount = 1;

    vkCmdClearColorImage(this->command_buffer, this->distribution_red_image[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &subresource_range);
    vkCmdClearColorImage(this->command_buffer, this->distribution_green_image[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &subresource_range);
    vkCmdClearColorImage(this->command_buffer, this->distribution_blue_image[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &subresource_range);
    vkCmdClearColorImage(this->command_buffer, this->distribution_geometry_image[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &subresource_range);
    vkCmdClearColorImage(this->command_buffer, this->distribution_geometry_image[1], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &subresource_range);

    std::vector<VkImageMemoryBarrier> clear_end_barriers = IndirectCache::build_barriers(3, VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_TRANSFER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    clear_end_barriers[0].image = this->distribution_red_image[0];
    clear_end_barriers[1].image = this->distribution_green_image[0];
    clear_end_barriers[2].image = this->distribution_blue_image[0];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, clear_end_barriers.size(), clear_end_barriers.data());

    std::vector<VkImageMemoryBarrier> geometry_begin_barriers = IndirectCache::build_barriers(2, VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
    geometry_begin_barriers[0].image = this->distribution_geometry_image[0];
    geometry_begin_barriers[1].image = this->distribution_geometry_image[1];
    
    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, geometry_begin_barriers.size(), geometry_begin_barriers.data());

    this->geometry_pass->process(this->command_buffer, 0);

    std::vector<VkImageMemoryBarrier> geometry_end_barriers = IndirectCache::build_barriers(2, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    geometry_end_barriers[0].image = this->distribution_geometry_image[0];
    geometry_end_barriers[1].image = this->distribution_geometry_image[1];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, geometry_end_barriers.size(), geometry_end_barriers.data());

    for (uint32_t index = 0; index < lights.size(); index++)
    {
        const SceneLight& light = lights[index];
       
        if (light.data.type != LIGHT_TYPE_DIRECTIONAL)
        {
            continue;
        }
        
        this->light_index = index;
        this->capture_pass->process(this->command_buffer, 0);
        this->injection_pass->process(this->command_buffer, 0);
    }

    //NOTE: Memory barrier for source image provieded by injection render pass
    //      The final image layout of distribution_..._image[0] has to be VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL

    std::vector<VkImageMemoryBarrier> copy_barriers = IndirectCache::build_barriers(3, 0, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copy_barriers[0].image = this->distribution_red_image[2];
    copy_barriers[1].image = this->distribution_green_image[2];
    copy_barriers[2].image = this->distribution_blue_image[2];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, copy_barriers.size(), copy_barriers.data());

    VkImageSubresourceLayers subresource_layers;
    subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource_layers.mipLevel = 0;
    subresource_layers.baseArrayLayer = 0;
    subresource_layers.layerCount = 1;

    VkOffset3D offset;
    offset.x = 0;
    offset.y = 0;
    offset.z = 0;

    VkExtent3D extend;
    extend.width = this->domain_indirect_resolution.x;
    extend.height = this->domain_indirect_resolution.y;
    extend.depth = this->domain_indirect_resolution.z;

    VkImageCopy image_copy;
    image_copy.srcSubresource = subresource_layers;
    image_copy.srcOffset = offset;
    image_copy.dstSubresource = subresource_layers;
    image_copy.dstOffset = offset;
    image_copy.extent = extend;

    vkCmdCopyImage(this->command_buffer, this->distribution_red_image[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->distribution_red_image[2], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);
    vkCmdCopyImage(this->command_buffer, this->distribution_green_image[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->distribution_green_image[2], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);
    vkCmdCopyImage(this->command_buffer, this->distribution_blue_image[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->distribution_blue_image[2], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);

    std::vector<VkImageMemoryBarrier> setup_src_barriers = IndirectCache::build_barriers(3, VK_ACCESS_TRANSFER_READ_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
    setup_src_barriers[0].image = this->distribution_red_image[0];
    setup_src_barriers[1].image = this->distribution_green_image[0];
    setup_src_barriers[2].image = this->distribution_blue_image[0];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, setup_src_barriers.size(), setup_src_barriers.data());

    std::vector<VkImageMemoryBarrier> setup_dst_barriers = IndirectCache::build_barriers(3, 0, VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);
    setup_dst_barriers[0].image = this->distribution_red_image[1];
    setup_dst_barriers[1].image = this->distribution_green_image[1];
    setup_dst_barriers[2].image = this->distribution_blue_image[1];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, setup_dst_barriers.size(), setup_dst_barriers.data());

    std::vector<VkImageMemoryBarrier> setup_indirect_barriers = IndirectCache::build_barriers(3, VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
    setup_indirect_barriers[0].image = this->distribution_red_image[2];
    setup_indirect_barriers[1].image = this->distribution_green_image[2];
    setup_indirect_barriers[2].image = this->distribution_blue_image[2];

    vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, setup_indirect_barriers.size(), setup_indirect_barriers.data());

    if (vkEndCommandBuffer(this->command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't end command buffer during setup of indirect cache!");

        return false;
    }

    lava::device_ptr device = this->scene->get_light_descriptor()->get_device();

    if (vkResetFences(device->get(), 1, &this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't reset fence during setup of indirect cache!");

        return false;
    }

    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &this->command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;

    if (vkQueueSubmit(device->get_graphics_queue().vk_queue, 1, &submit_info, this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't submit command buffer during setup of indirect cache!");

        return false;
    }

    if (vkWaitForFences(device->get(), 1, &this->fence, VK_TRUE, std::numeric_limits<uint64_t>::max()) != VK_SUCCESS)
    {
        lava::log()->error("Can't wait for completion indirect cache setup!");

        return false;
    }

    return true;
}

bool IndirectCache::submit_propagation(uint32_t iterations)
{
    VkCommandBufferBeginInfo begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    begin_info.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(this->command_buffer, &begin_info) != VK_SUCCESS)
    {
        lava::log()->error("Can't begin command buffer during propagation of indirect cache!");

        return false;
    }

    this->propagation_pipeline->bind(this->command_buffer);

    for (uint32_t index = 0; index < iterations; index++)
    {
        uint32_t src_index = index % 2;
        uint32_t dst_index = (index + 1) % 2;

        this->propagation_layout->bind(this->command_buffer, this->propagation_descriptor_set[src_index], 0, {}, VK_PIPELINE_BIND_POINT_COMPUTE);

        vkCmdDispatch(this->command_buffer, this->domain_work_groups.x, this->domain_work_groups.y, this->domain_work_groups.z);
        
        std::vector<VkImageMemoryBarrier> compue_src_barriers = IndirectCache::build_barriers(3, VK_ACCESS_SHADER_READ_BIT, VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
        compue_src_barriers[0].image = this->distribution_red_image[src_index];
        compue_src_barriers[1].image = this->distribution_green_image[src_index];
        compue_src_barriers[2].image = this->distribution_blue_image[src_index];

        vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, compue_src_barriers.size(), compue_src_barriers.data());

        std::vector<VkImageMemoryBarrier> compute_dst_barriers = IndirectCache::build_barriers(3, VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
        compute_dst_barriers[0].image = this->distribution_red_image[dst_index];
        compute_dst_barriers[1].image = this->distribution_green_image[dst_index];
        compute_dst_barriers[2].image = this->distribution_blue_image[dst_index];

        vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, compute_dst_barriers.size(), compute_dst_barriers.data());

        std::vector<VkImageMemoryBarrier> compute_indirect_barriers = IndirectCache::build_barriers(3, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL);
        compute_indirect_barriers[0].image = this->distribution_red_image[2];
        compute_indirect_barriers[1].image = this->distribution_green_image[2];
        compute_indirect_barriers[2].image = this->distribution_blue_image[2];

        vkCmdPipelineBarrier(this->command_buffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr, compute_indirect_barriers.size(), compute_indirect_barriers.data());
    }

    if (vkEndCommandBuffer(this->command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't end command buffer during propagation of indirect cache!");

        return false;
    }

    lava::device_ptr device = this->scene->get_light_descriptor()->get_device();

    if (vkResetFences(device->get(), 1, &this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't reset fence during propagation of indirect cache!");

        return false;
    }

    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &this->command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;

    if (vkQueueSubmit(device->get_graphics_queue().vk_queue, 1, &submit_info, this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't submit command buffer during propagation of indirect cache!");

        return false;
    }

    if (vkWaitForFences(device->get(), 1, &this->fence, VK_TRUE, std::numeric_limits<uint64_t>::max()) != VK_SUCCESS)
    {
        lava::log()->error("Can't wait for completion indirect propagation setup!");

        return false;
    }

    return true;
}

bool IndirectCache::submit_barrier()
{
    VkCommandBufferBeginInfo begin_info;
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    begin_info.pInheritanceInfo = nullptr;

    if (vkBeginCommandBuffer(this->command_buffer, &begin_info) != VK_SUCCESS)
    {
        lava::log()->error("Can't begin command buffer of indirect cache for barrier submission!");

        return false;
    }

    std::vector<VkImageMemoryBarrier> final_barriers = IndirectCache::build_barriers(3, VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    final_barriers[0].image = this->distribution_red_image[2];
    final_barriers[1].image = this->distribution_green_image[2];
    final_barriers[2].image = this->distribution_blue_image[2];

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, final_barriers.size(), final_barriers.data());

    if (vkEndCommandBuffer(this->command_buffer) != VK_SUCCESS)
    {
        lava::log()->error("Can't end command buffer of indirect cache for barrier submission!");

        return false;
    }

    lava::device_ptr device = this->scene->get_light_descriptor()->get_device();

    if (vkResetFences(device->get(), 1, &this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't reset fence of indirect cache for barrier submission!");

        return false;
    }

    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &this->command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;

    if (vkQueueSubmit(device->get_graphics_queue().vk_queue, 1, &submit_info, this->fence) != VK_SUCCESS)
    {
        lava::log()->error("Can't submit command buffer of indirect cache for barrier submission!");

        return false;
    }

    if (vkWaitForFences(device->get(), 1, &this->fence, VK_TRUE, std::numeric_limits<uint64_t>::max()) != VK_SUCCESS)
    {
        lava::log()->error("Can't wait for completion of barrier!");

        return false;
    }

    return true;
}

void IndirectCache::pipeline_geometry(VkCommandBuffer command_buffer)
{
    std::array<uint32_t, 3> push_constants;
    push_constants[0] = this->domain_voxel_resolution.x;
    push_constants[1] = this->domain_voxel_resolution.y;
    push_constants[2] = this->domain_voxel_resolution.z;

    vkCmdPushConstants(command_buffer, this->geometry_layout->get(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(uint32_t) * push_constants.size(), push_constants.data());

    std::array<glm::uvec2, 3> resolutions = 
    {
        glm::uvec2(this->domain_voxel_resolution.y, this->domain_voxel_resolution.z),
        glm::uvec2(this->domain_voxel_resolution.x, this->domain_voxel_resolution.z),
        glm::uvec2(this->domain_voxel_resolution.x, this->domain_voxel_resolution.y)
    };

    std::array<VkViewport, 3> viewports;
    std::array<VkRect2D, 3> scissors;

    for (uint32_t index = 0; index < resolutions.size(); index++)
    {
        viewports[index].x = 0.0f;
        viewports[index].y = 0.0f;
        viewports[index].width = resolutions[index].x;
        viewports[index].height = resolutions[index].y;
        viewports[index].minDepth = 0.0f;
        viewports[index].maxDepth = 1.0f;

        scissors[index].offset.x = 0;
        scissors[index].offset.y = 0;
        scissors[index].extent.width = resolutions[index].x;
        scissors[index].extent.height = resolutions[index].y;
    }

    vkCmdSetViewport(command_buffer, 0, 3, viewports.data());
    vkCmdSetScissor(command_buffer, 0, 3, scissors.data());

    this->geometry_layout->bind(command_buffer, this->geometry_descriptor_set, 1);

    for (const SceneMesh& mesh : this->scene->get_meshes())
    {
        if (!mesh.cast_shadow)
        {
            continue;
        }

        this->geometry_layout->bind(command_buffer, mesh.descriptor_set[this->frame], 0);

        mesh.mesh->bind_draw(command_buffer, this->domain_geometry_resolution.z);
    }
}

void IndirectCache::pipeline_capture(VkCommandBuffer command_buffer)
{
    std::array<uint32_t, 2> push_constants;
    push_constants[0] = this->light_index;
    push_constants[1] = this->settings.capture_resolution;

    vkCmdPushConstants(command_buffer, this->capture_layout->get(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(uint32_t) * push_constants.size(), push_constants.data());

    this->capture_layout->bind(command_buffer, this->scene->get_light_descriptor_set(this->frame), 1);

    const std::vector<SceneMaterial>& materials = this->scene->get_materials();

    for (const SceneMesh& mesh : this->scene->get_meshes())
    {
        if (!mesh.cast_shadow)
        {
            continue;
        }

        const SceneMaterial& material = materials[mesh.material_index];
        this->capture_layout->bind(command_buffer, mesh.descriptor_set[this->frame], 0);
        this->capture_layout->bind(command_buffer, material.descriptor_set, 2);

        mesh.mesh->bind_draw(command_buffer);
    }
}

void IndirectCache::pipeline_injection(VkCommandBuffer command_buffer)
{
    std::array<uint32_t, 2> push_constants;
    push_constants[0] = this->light_index;
    push_constants[1] = this->settings.capture_resolution;

    float cell_size = this->settings.cell_size;

    vkCmdPushConstants(command_buffer, this->injection_layout->get(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(uint32_t) * push_constants.size(), push_constants.data());
    vkCmdPushConstants(command_buffer, this->injection_layout->get(), VK_SHADER_STAGE_VERTEX_BIT, sizeof(uint32_t) * push_constants.size(), sizeof(cell_size), &cell_size);

    this->injection_layout->bind(command_buffer, this->scene->get_light_descriptor_set(this->frame), 0);
    this->injection_layout->bind(command_buffer, this->injection_descriptor_set, 1);

    uint32_t point_count = this->settings.capture_resolution * this->settings.capture_resolution;
    vkCmdDraw(command_buffer, point_count, 1, 0, 0);
}

VkImageMemoryBarrier IndirectCache::build_barrier(VkAccessFlags src_access, VkAccessFlags dst_access, VkImageLayout old_layout, VkImageLayout new_layout)
{
    VkImageMemoryBarrier barrier;
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.pNext = nullptr;
    barrier.srcAccessMask = src_access;
    barrier.dstAccessMask = dst_access;
    barrier.oldLayout = old_layout;
    barrier.newLayout = new_layout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = VK_NULL_HANDLE;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    return barrier;
}

std::vector<VkImageMemoryBarrier> IndirectCache::build_barriers(uint32_t count, VkAccessFlags src_access, VkAccessFlags dst_access, VkImageLayout old_layout, VkImageLayout new_layout)
{
    return std::vector<VkImageMemoryBarrier>(count, IndirectCache::build_barrier(src_access, dst_access, old_layout, new_layout));
}

IndirectCache::Ptr make_indirect_cache()
{
    return std::make_shared<IndirectCache>();
}