/*
  An indirect cache can be used in order to pre-compute indirect light transfer using light propagation volumes.
  During the creation of the indirect cache it is possible to set the cell size that should be used for the propagation volumes with the help of the IndirectCacheSettings struct.
  Before the indirect cache can be used, it is neccessary to call the function compute_indirect(...).
  After that, the indirect cache can be used by ether adding it to a geometry buffer or by attaching the descriptor set of the indirect cache to a pipeline.
  In order to use the indirect cache during forward shading, it is neccessary to bind the descriptor of the cache to the binding point that was specified in the fragment shader by the define INDIRECT_DESCRIPTOR_SET.
  Example (Deferred):

    IndirectCache::Ptr indirect_cache = make_indirect_cache();
    indirect_cache->create(get_scene(), IndirectCacheSettings());

    GeometryBuffer::Ptr geometry_buffer = make_geometry_buffer();
    geometry_buffer->create(output_image, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, get_stereo_transform()->get_descriptor(), get_scene(), {}, indirect_cache); //Add the indirect cache to geometry buffer

    //During Rendering
    indirect_cache->compute_indirect(command_buffer, frame); //Compute indirect lighting

    render_pass->process(command_buffer, 0); //Fill geometry buffer
    geometry_buffer->apply_lighting(command_buffer, per_frame_descriptor_set); //Compute lighting with indirect lighting

  Example (Forward):

    IndirectCache::Ptr indirect_cache = make_indirect_cache();
    indirect_cache->create(get_scene(), IndirectCacheSettings());

    pipeline_layout->add(indirect_cache->get_descriptor()); //Same binding point as INDIRECT_DESCRIPTOR_SET

    //During Rendering
    indirect_cache->compute_indirect(command_buffer, frame); //Compute indirect lighting

    pipeline_layout->bind(command_buffer, indirect_cache->get_descriptor_set(), 42); //Same binding point as INDIRECT_DESCRIPTOR_SET
*/

#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <array>
#include <cstdint>

#include "scene.hpp"

struct IndirectCacheSettings
{
    uint32_t capture_resolution = 1024;
    uint32_t voxel_resolution_scale = 1;    // Needs to be greater or equal to one
    float cell_size = 1.0f;                 // Where cell size is in meters
    uint32_t memory_limit = 512;            // Where memory limit is in MiB
};

class IndirectCache
{
public:
    typedef std::shared_ptr<IndirectCache> Ptr;

public:
    IndirectCache() = default;

    bool create(Scene::Ptr scene, const IndirectCacheSettings& settings);
    void destroy();

    bool compute_indirect(lava::index frame);

    lava::descriptor::ptr get_descriptor() const;
    VkDescriptorSet get_descriptor_set() const;

    const IndirectCacheSettings& get_settings() const;

private:
    bool compute_domain(Scene::Ptr scene, const IndirectCacheSettings& settings);

    bool create_command_buffer(lava::device_ptr device);
    bool create_fence(lava::device_ptr device);
    bool create_buffers(lava::device_ptr device);
    bool create_images(lava::device_ptr device, const IndirectCacheSettings& settings);
    bool create_samplers(lava::device_ptr device);
    bool create_descriptors(lava::device_ptr device);
    bool create_layouts(lava::device_ptr device, Scene::Ptr scene);
    bool create_geometry_pass(lava::device_ptr device);
    bool create_geometry_pipeline(lava::device_ptr device);
    bool create_capture_pass(lava::device_ptr device, const IndirectCacheSettings& settings);
    bool create_capture_pipeline(lava::device_ptr device);
    bool create_injection_pass(lava::device_ptr device);
    bool create_injection_pipeline(lava::device_ptr device);
    bool create_propagation_pipeline(lava::device_ptr device);
   
    bool submit_setup();
    bool submit_propagation(uint32_t iterations); //NOTE: Iterations needs to be multiple of two
    bool submit_barrier();

    void pipeline_geometry(VkCommandBuffer command_buffer);
    void pipeline_capture(VkCommandBuffer command_buffer);
    void pipeline_injection(VkCommandBuffer command_buffer);

    static VkImageMemoryBarrier build_barrier(VkAccessFlags src_access, VkAccessFlags dst_access, VkImageLayout old_layout, VkImageLayout new_layout);
    static std::vector<VkImageMemoryBarrier> build_barriers(uint32_t count, VkAccessFlags src_access, VkAccessFlags dst_access, VkImageLayout old_layout, VkImageLayout new_layout);

private:
    Scene::Ptr scene;
    IndirectCacheSettings settings;

    uint32_t frame = 0;
    uint32_t light_index = 0;

    uint32_t propagation_iterations = 0;

    float domain_cell_size = 1.0f;
    glm::vec3 domain_min = glm::vec3(0.0f);
    glm::vec3 domain_max = glm::vec3(0.0f);
    glm::uvec3 domain_indirect_resolution = glm::uvec3(0);
    glm::uvec3 domain_geometry_resolution = glm::uvec3(0);
    glm::uvec3 domain_voxel_resolution = glm::uvec3(0);
    glm::uvec3 domain_work_groups = glm::uvec3(0);

    VkCommandPool command_pool = VK_NULL_HANDLE;
    VkCommandBuffer command_buffer = VK_NULL_HANDLE;
    VkFence fence = VK_NULL_HANDLE;

    lava::descriptor::pool::ptr descriptor_pool;
    lava::descriptor::ptr indirect_descriptor;
    lava::descriptor::ptr geometry_descriptor;
    lava::descriptor::ptr injection_descriptor;
    lava::descriptor::ptr propagation_descriptor;

    VkDescriptorSet indirect_descriptor_set = VK_NULL_HANDLE;
    VkDescriptorSet geometry_descriptor_set = VK_NULL_HANDLE;
    VkDescriptorSet injection_descriptor_set = VK_NULL_HANDLE;
    std::array<VkDescriptorSet, 2> propagation_descriptor_set;
    
    lava::pipeline_layout::ptr geometry_layout;
    lava::pipeline_layout::ptr capture_layout;
    lava::pipeline_layout::ptr injection_layout;
    lava::pipeline_layout::ptr propagation_layout;

    lava::render_pass::ptr geometry_pass;
    lava::render_pass::ptr capture_pass;
    lava::render_pass::ptr injection_pass;
    
    lava::graphics_pipeline::ptr geometry_pipeline;
    lava::graphics_pipeline::ptr capture_pipeline;
    lava::graphics_pipeline::ptr injection_pipeline;
    lava::compute_pipeline::ptr propagation_pipeline;

    VkSampler indirect_sampler = VK_NULL_HANDLE;
    VkSampler geometry_sampler = VK_NULL_HANDLE;
    VkSampler injection_sampler = VK_NULL_HANDLE;

    lava::image::ptr capture_depth_image;
    lava::image::ptr capture_flux_image;
    lava::image::ptr capture_normal_image;

    //NOTE: Two images for the propagation and one image for the accumulation
    std::array<VmaAllocation, 3> distribution_red_image_memory;
    std::array<VmaAllocation, 3> distribution_green_image_memory;
    std::array<VmaAllocation, 3> distribution_blue_image_memory;
    std::array<VmaAllocation, 2> distribution_geometry_image_memory;

    std::array<VkImage, 3> distribution_red_image;
    std::array<VkImage, 3> distribution_green_image;
    std::array<VkImage, 3> distribution_blue_image;
    std::array<VkImage, 2> distribution_geometry_image;

    std::array<VkImageView, 3> distribution_red_image_view;
    std::array<VkImageView, 3> distribution_green_image_view;
    std::array<VkImageView, 3> distribution_blue_image_view;
    std::array<VkImageView, 2> distribution_geometry_image_view;

    std::array<VkImageView, 3> distribution_red_image_array_view;
    std::array<VkImageView, 3> distribution_green_image_array_view;
    std::array<VkImageView, 3> distribution_blue_image_array_view;
    std::array<VkImageView, 2> distribution_geometry_image_integer_view;
    
    lava::buffer::ptr domain_buffer;
};

IndirectCache::Ptr make_indirect_cache();