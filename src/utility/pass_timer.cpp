#include "pass_timer.hpp"
#include <algorithm>

bool PassTimer::create(lava::device_ptr device, StatisticLog::Ptr log, uint32_t frame_count, uint32_t pass_count)
{
    if (!this->check_timestamp_properties(device))
    {
        return false;
    }

    VkQueryPoolCreateInfo pool_create_info;
    pool_create_info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
    pool_create_info.pNext = nullptr;
    pool_create_info.flags = 0;
    pool_create_info.queryType = VK_QUERY_TYPE_TIMESTAMP;
    pool_create_info.queryCount = pass_count * 2;
    pool_create_info.pipelineStatistics = 0;

    for (uint32_t frame = 0; frame < frame_count; frame++)
    {
        VkQueryPool query_pool = nullptr;

        if (vkCreateQueryPool(device->get(), &pool_create_info, lava::memory::alloc(), &query_pool) != VK_SUCCESS)
        {
            lava::log()->error("can't create query pool for pass timer!");

            return false;
        }

        this->query_pools.push_back(query_pool);
    }

    this->statistic_log = log;

    this->frame_count = frame_count;
    this->timestamp_count = pass_count * 2;

    this->current_frame = 0;
    this->current_timestamp = 0;

    this->timestamp_zero = {};

    return true;
}

void PassTimer::destroy(lava::device_ptr device)
{
    for (const VkQueryPool& pool : this->query_pools)
    {
        vkDestroyQueryPool(device->get(), pool, lava::memory::alloc());
    }

    this->query_pools.clear();

    this->frame_count = 0;
    this->timestamp_count = 0;

    this->current_frame = 0;
    this->current_timestamp = 0;

    this->timestamp_zero = {};
}

bool PassTimer::wait(lava::device_ptr device)
{
    for (uint32_t offset = 0; offset < this->frame_count; offset++)
    {
        uint32_t frame = (this->current_frame + offset + 1) % this->frame_count;

        if (!this->check_submissions(device, frame, true))
        {
            return false;
        }
    }

    return true;
}

void PassTimer::interface()
{
    for (const PassTimerStatistic& pass_statistic : this->statistic_list)
    {
        if (pass_statistic.inactive_frames < PASS_TIMER_STATISTIC_INACTIVE_FRAMES)
        {
            if (!pass_statistic.statistic->is_log_only())
            {
                pass_statistic.statistic->interface(128);    
            }
        }
    }
}

bool PassTimer::next_frame(VkCommandBuffer command_buffer, lava::index frame, lava::device_ptr device)
{
    for (PassTimerStatistic& pass_statistic : this->statistic_list)
    {
        pass_statistic.inactive_frames++;
    }

    if(!this->check_submissions(device, frame, false))
    {
        return false;    
    }

    this->current_frame = frame;
    this->current_timestamp = 0;

    //Rest all timestamps to the unavailable state for the upcomming frame
    VkQueryPool current_pool = this->query_pools[frame];
    vkCmdResetQueryPool(command_buffer, current_pool, 0, this->timestamp_count);

    return true;
}

bool PassTimer::begin_pass(VkCommandBuffer command_buffer, const std::string& pass_name)
{
    if (this->current_timestamp >= this->timestamp_count)
    {
        lava::log()->error("Not enough timestamps available! Please increase the number of passes for the pass timer.");

        return false;
    }

    PassTimerQuerySubmission submission;
    submission.name = pass_name;
    submission.frame_index = this->current_frame;
    submission.timestamp_index = this->current_timestamp;

    this->query_submissions.push_back(submission);

    VkQueryPool current_pool = this->query_pools[this->current_frame];
    vkCmdWriteTimestamp(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, current_pool, this->current_timestamp);

    this->current_timestamp++;

    return true;
}

bool PassTimer::end_pass(VkCommandBuffer command_buffer)
{
    if (this->current_timestamp >= this->timestamp_count)
    {
        lava::log()->error("Not enough timestamps available! Please increase the number of passes for the pass timer.");

        return false;
    }

    VkQueryPool current_pool = this->query_pools[this->current_frame];
    vkCmdWriteTimestamp(command_buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, current_pool, this->current_timestamp);

    this->current_timestamp++;

    return true;
}

bool PassTimer::check_timestamp_properties(lava::device_ptr device)
{
    uint32_t property_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device->get_vk_physical_device(), &property_count, nullptr);

    std::vector<VkQueueFamilyProperties> property_list(property_count);
    vkGetPhysicalDeviceQueueFamilyProperties(device->get_vk_physical_device(), &property_count, property_list.data());

    const VkPhysicalDeviceProperties& device_properties = device->get_properties();
    lava::queue::ref graphics_queue = device->get_graphics_queue();
    const VkQueueFamilyProperties& graphics_queue_properties = property_list[graphics_queue.family];
    
    if (graphics_queue_properties.timestampValidBits == 0)
    {
        lava::log()->error("the graphics queue does not support timestamps");

        return false;
    }

    this->timestamp_bits = graphics_queue_properties.timestampValidBits;
    this->timestamp_period = (double)device_properties.limits.timestampPeriod;

    return true;
}

bool PassTimer::check_submissions(lava::device_ptr device, lava::index frame, bool wait)
{
    VkQueryPool current_pool = this->query_pools[frame];

    for (const PassTimerQuerySubmission& submission : this->query_submissions)
    {
        if (submission.frame_index == frame)
        {
            uint32_t flags = VK_QUERY_RESULT_64_BIT;

            if (wait)
            {
                flags |= VK_QUERY_RESULT_WAIT_BIT;
            }

            std::array<uint64_t, 2> timestamps;

            if (vkGetQueryPoolResults(device->get(), current_pool, submission.timestamp_index, 1, sizeof(uint64_t), &timestamps[0], 0, flags) != VK_SUCCESS)
            {
                lava::log()->error("can't get the timestamp results for the pass timer");

                return false;
            }

            if (vkGetQueryPoolResults(device->get(), current_pool, submission.timestamp_index + 1, 1, sizeof(uint64_t), &timestamps[1], 0, flags) != VK_SUCCESS)
            {
                lava::log()->error("can't get the timestamp results for the pass timer");

                return false;
            }

            if (!this->timestamp_zero.has_value())
            {
                this->timestamp_zero = timestamps[0];
            }

            //Subtract the end timestamp form the start timestamp and multiply it with timestamp_period.
            //The result of this computation gives the pass time in nano seconds
            //In order to get milli seconds divide by 10^6
            double start_time = ((double)(timestamps[0] - this->timestamp_zero.value()) * this->timestamp_period) / 1000000.0;
            double end_time = ((double)(timestamps[1] - this->timestamp_zero.value()) * this->timestamp_period) / 1000000.0;
            double delta_time = ((double)(timestamps[1] - timestamps[0]) * this->timestamp_period) / 1000000.0;

            this->add_sample("timestamp_" + submission.name + "_start", true, start_time);
            this->add_sample("timestamp_" + submission.name + "_end", true, end_time);
            this->add_sample(submission.name, false, delta_time);
        }
    }

    this->query_submissions.erase(std::remove_if(this->query_submissions.begin(), this->query_submissions.end(), [=](const PassTimerQuerySubmission& submission)
    {
        return submission.frame_index == frame;
    }), this->query_submissions.end());

    return true;
}

void PassTimer::add_sample(const std::string& name, bool log_only, double value)
{
    for (PassTimerStatistic& pass_statistic : this->statistic_list)
    {
        if (pass_statistic.statistic->get_name() == name)
        {
            pass_statistic.inactive_frames = 0;
            pass_statistic.statistic->add_sample(value);

            return;
        }
    }

    PassTimerStatistic pass_statistic;
    pass_statistic.inactive_frames = 0;
    pass_statistic.statistic = make_statistic(name, UNIT_MILLISECONDS, log_only);
    pass_statistic.statistic->add_sample(value);

    this->statistic_log->add_statistic(pass_statistic.statistic);
    this->statistic_list.push_back(pass_statistic);
}

PassTimer::Ptr make_pass_timer()
{
    return std::make_shared<PassTimer>();
}