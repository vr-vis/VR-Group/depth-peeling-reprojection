/*
  With the pass timer it is possible to measure the time that a pass needs to be completed on the gpu.
  The pass timer can be accessed inside of a stereo strategy by calling the function get_pass_timer().
  To measure the time of a pass call begin_pass(...) before and end_pass(...) after the pass.
  Example:

   get_pass_timer().begin_pass(command_buffer, "pass_name");
    //messured commands 
   get_pass_timer().end_pass(command_buffer, "pass_name");
*/

#pragma once
#include <liblava/lava.hpp>
#include <vector>
#include <string>
#include <optional>
#include <memory>
#include <cstdint>

#include "utility/statistic.hpp"

#define PASS_TIMER_STATISTIC_INACTIVE_FRAMES 128

struct PassTimerStatistic
{
    Statistic::Ptr statistic;
    uint32_t inactive_frames = 0;
};

struct PassTimerQuerySubmission
{
    std::string name;
    uint32_t frame_index = 0;
    uint32_t timestamp_index = 0;
};

class PassTimer
{
public:
    typedef std::shared_ptr<PassTimer> Ptr;

public:
    PassTimer() = default;

    bool create(lava::device_ptr device, StatisticLog::Ptr log, uint32_t frame_count, uint32_t pass_count);
    void destroy(lava::device_ptr device);
    bool wait(lava::device_ptr device);
    void interface();

    bool next_frame(VkCommandBuffer command_buffer, lava::index frame, lava::device_ptr device);
    bool begin_pass(VkCommandBuffer command_buffer, const std::string& pass_name);
    bool end_pass(VkCommandBuffer command_buffer);

private:
    bool check_timestamp_properties(lava::device_ptr device);
    bool check_submissions(lava::device_ptr device, lava::index frame, bool wait);
    void add_sample(const std::string& name, bool log_only, double value);

private:
    StatisticLog::Ptr statistic_log;
    std::vector<PassTimerStatistic> statistic_list;

    std::vector<VkQueryPool> query_pools;
    std::vector<PassTimerQuerySubmission> query_submissions;

    uint32_t frame_count = 0;
    uint32_t timestamp_count = 0;

    uint32_t current_frame = 0;
    uint32_t current_timestamp = 0;

    uint32_t timestamp_bits = 0; //number of bits used for a timestamp in case of a graphics queue
    double timestamp_period = 0; //one increment of the timestamp in nanoseconds

    std::optional<uint64_t> timestamp_zero; //the timestamp that defines the zero time-point
};

PassTimer::Ptr make_pass_timer();