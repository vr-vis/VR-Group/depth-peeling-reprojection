#include "ring_buffer.hpp"
#include <glm/glm.hpp>

bool RingBuffer::peak(uint8_t* buffer, uint32_t size)
{
    if (this->get_read_bytes() < size)
    {
        return false;
    }

    uint32_t copy_offset = this->read_offset;

    while (size > 0)
    {
        uint32_t copy_size = glm::min(size, (uint32_t)this->ring_buffer.size() - copy_offset);
        memcpy(buffer, this->ring_buffer.data() + copy_offset, copy_size);

        copy_offset = (copy_offset + copy_size) % this->ring_buffer.size();
        buffer += copy_size;
        size -= copy_size;
    }

    return true;
}

bool RingBuffer::read(uint8_t* buffer, uint32_t size)
{
    if (!this->peak(buffer, size))
    {
        return false;
    }

    this->read_offset = (this->read_offset + size) % this->ring_buffer.size();
    this->read_count -= size;

    return true;
}

bool RingBuffer::write(WriteFunction function)
{
    if (this->get_write_bytes() < 0)
    {
        return false;
    }

    uint32_t offset = (this->read_offset + this->read_count) % this->ring_buffer.size();
    uint32_t max_size = glm::min(this->get_write_bytes(), (uint32_t)this->ring_buffer.size() - offset);
    uint32_t size = function(this->ring_buffer.data() + offset, max_size);

    this->read_count += size;

    return true;
}

uint32_t RingBuffer::get_read_bytes() const
{
    return this->read_count;
}

uint32_t RingBuffer::get_write_bytes() const
{
    return this->ring_buffer.size() - this->read_count;
}