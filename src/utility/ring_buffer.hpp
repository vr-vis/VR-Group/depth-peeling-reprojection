#pragma once
#include <functional>
#include <array>
#include <cstdint>

#define RING_BUFFER_SIZE 1024

class RingBuffer
{
public:
    typedef std::function<uint32_t(uint8_t* buffer, uint32_t max_size)> WriteFunction;

private:
    std::array<uint8_t, RING_BUFFER_SIZE> ring_buffer;
    uint32_t read_offset = 0;
    uint32_t read_count = 0;

public:
    RingBuffer() = default;

    bool peak(uint8_t* buffer, uint32_t size);
    bool read(uint8_t* buffer, uint32_t size);
    bool write(WriteFunction function);

    uint32_t get_read_bytes() const;
    uint32_t get_write_bytes() const;
};