#include "shadow_cache.hpp"

namespace glsl
{
    using namespace glm;
    using uint = glm::uint32;

#include "res/dpr/data/shadow_data.inc"
}

bool ShadowCache::create(Scene::Ptr scene, const ShadowCacheSettings& settings)
{
    lava::device_ptr device = scene->get_light_descriptor()->get_device();

    this->scene = scene;
    this->settings = settings;

    if (!this->create_images(device, scene, settings))
    {
        return false;
    }

    if (!this->create_buffers(device, settings))
    {
        return false;
    }

    if (!this->create_descriptors(device))
    {
        return false;
    }

    if (!this->create_render_pass(device, settings))
    {
        return false;
    }

    if (!this->create_pipeline(device, scene))
    {
        return false;
    }

    return true;
}

void ShadowCache::destroy()
{
    if (this->scene == nullptr)
    {
        return;
    }

    lava::device_ptr device = this->scene->get_light_descriptor()->get_device();
    
    if (this->pipeline != nullptr)
    {
        this->pipeline->destroy();
        this->pipeline = nullptr;
    }

    if (this->pipeline_layout != nullptr)
    {
        this->pipeline_layout->destroy();
        this->pipeline_layout = nullptr;
    }

    if (this->render_pass != nullptr)
    {
        this->render_pass->destroy();
        this->render_pass = nullptr;
    }

    if (this->descriptor_set != nullptr)
    {
        this->descriptor->free(this->descriptor_set, this->descriptor_pool->get());
        this->descriptor_set = nullptr;
    }

    if (this->descriptor != nullptr)
    {
        this->descriptor->destroy();
        this->descriptor = nullptr;
    }

    if (this->descriptor_pool != nullptr)
    {
        this->descriptor_pool->destroy();
        this->descriptor_pool = nullptr;
    }

    if (this->image_view_directional != nullptr)
    {
        vkDestroyImageView(device->get(), this->image_view_directional, lava::memory::alloc());
        this->image_view_directional = nullptr;
    }

    if (this->image_view_spot != nullptr)
    {
        vkDestroyImageView(device->get(), this->image_view_spot, lava::memory::alloc());
        this->image_view_spot = nullptr;
    }

    if (this->image_view_point != nullptr)
    {
        vkDestroyImageView(device->get(), this->image_view_point, lava::memory::alloc());
        this->image_view_point = nullptr;
    }

    if (this->image_array != nullptr)
    {
        this->image_array->destroy();
        this->image_array = nullptr;
    }

    if (this->image_default_plane != nullptr)
    {
        this->image_default_plane->destroy();
        this->image_default_plane = nullptr;
    }

    if (this->image_default_cube != nullptr)
    {
        this->image_default_cube->destroy();
        this->image_default_cube = nullptr;
    }

    if (this->sampler_default != nullptr)
    {
        vkDestroySampler(device->get(), this->sampler_default, lava::memory::alloc());
        this->sampler_default = nullptr;
    }

    if (this->sampler_directional != nullptr)
    {
        vkDestroySampler(device->get(), this->sampler_directional, lava::memory::alloc());
        this->sampler_directional = nullptr;
    }

    if (this->sampler_spot != nullptr)
    {
        vkDestroySampler(device->get(), this->sampler_spot, lava::memory::alloc());
        this->sampler_spot = nullptr;
    }

    if (this->sampler_point != nullptr)
    {
        vkDestroySampler(device->get(), this->sampler_point, lava::memory::alloc());
        this->sampler_point = nullptr;
    }

    if (this->shadow_buffer != nullptr)
    {
        this->shadow_buffer->destroy();
        this->shadow_buffer = nullptr;
    }

    //Don't touch these resources. Only release the shared pointer to them.
    this->scene = {};
}

void ShadowCache::compute_shadow(VkCommandBuffer command_buffer, lava::index index)
{
    if (!this->image_default_cleared)
    {
        lava::device_ptr device = scene->get_light_descriptor()->get_device();

        lava::insert_image_memory_barrier(device, command_buffer, this->image_default_plane->get(), 0, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, this->image_default_plane->get_subresource_range());
        lava::insert_image_memory_barrier(device, command_buffer, this->image_default_cube->get(), 0, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, this->image_default_cube->get_subresource_range());

        VkClearDepthStencilValue clear_value;
        clear_value.depth = 1.0f;
        clear_value.stencil = 0;

        vkCmdClearDepthStencilImage(command_buffer, this->image_default_plane->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_value, 1, &this->image_default_plane->get_subresource_range());
        vkCmdClearDepthStencilImage(command_buffer, this->image_default_cube->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_value, 1, &this->image_default_cube->get_subresource_range());

        lava::insert_image_memory_barrier(device, command_buffer, this->image_default_plane->get(), VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, this->image_default_plane->get_subresource_range());
        lava::insert_image_memory_barrier(device, command_buffer, this->image_default_cube->get(), VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, this->image_default_cube->get_subresource_range());

        this->image_default_cleared = true;
    }

    this->frame = index;
    this->render_pass->process(command_buffer, 0);
}

lava::descriptor::ptr ShadowCache::get_descriptor() const
{
    return this->descriptor;
}

VkDescriptorSet ShadowCache::get_descriptor_set() const
{
    return this->descriptor_set;
}

const ShadowCacheSettings& ShadowCache::get_settings() const
{
    return this->settings;
}

bool ShadowCache::create_image_view(lava::device_ptr device, lava::image::ptr image_array, uint32_t layer_offset, uint32_t layer_count, VkImageViewType view_type, VkImageView& view)
{
    VkComponentMapping components;
    components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    VkImageSubresourceRange subresource_range;
    subresource_range.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    subresource_range.baseMipLevel  = 0;
    subresource_range.levelCount = 1;
    subresource_range.baseArrayLayer = layer_offset;
    subresource_range.layerCount = layer_count;

    VkImageViewCreateInfo create_info;
    create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = 0;
    create_info.image = image_array->get();
    create_info.viewType = view_type;
    create_info.format = image_array->get_format();
    create_info.components = components;
    create_info.subresourceRange = subresource_range;

    if (vkCreateImageView(device->get(), &create_info, lava::memory::alloc(), &view) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ShadowCache::create_sampler(lava::device_ptr device, VkSampler& sampler)
{
    VkSamplerCreateInfo create_info;
    create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = 0;
    create_info.magFilter = VK_FILTER_LINEAR;
    create_info.minFilter = VK_FILTER_LINEAR;
    create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
    create_info.mipLodBias = 0.0f;
    create_info.anisotropyEnable = VK_FALSE;
    create_info.maxAnisotropy = 1.0f;
    create_info.compareEnable = VK_TRUE;
    create_info.compareOp = VK_COMPARE_OP_LESS;
    create_info.minLod = 0;
    create_info.maxLod = VK_LOD_CLAMP_NONE;
    create_info.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
    create_info.unnormalizedCoordinates = VK_FALSE;

    if (vkCreateSampler(device->get(), &create_info, lava::memory::alloc(), &sampler) != VK_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ShadowCache::create_images(lava::device_ptr device, Scene::Ptr scene, const ShadowCacheSettings& settings)
{
    this->image_default_plane = std::make_shared<lava::image>(VK_FORMAT_D16_UNORM);
    this->image_default_plane->set_layer_count(1);
    this->image_default_plane->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);

    if (!this->image_default_plane->create(device, lava::uv2(1, 1)))
    {
        return false;
    }

    this->image_default_cube = std::make_shared<lava::image>(VK_FORMAT_D16_UNORM);
    this->image_default_cube->set_layer_count(6);
    this->image_default_cube->set_view_type(VK_IMAGE_VIEW_TYPE_CUBE_ARRAY);
    this->image_default_cube->set_flags(VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);

    if (!this->image_default_cube->create(device, lava::uv2(1, 1)))
    {
        return false;
    }

    if (!this->create_sampler(device, this->sampler_default))
    {
        return false;
    }

    uint32_t directional_count = 0;
    uint32_t spot_count = 0;
    uint32_t point_count = 0;

    for (const SceneLight& light : scene->get_lights())
    {
        switch (light.data.type)
        {
        case LIGHT_TYPE_DIRECTIONAL:
            directional_count++;
            break;
        case LIGHT_TYPE_SPOT:
            spot_count++;
            break;
        case LIGHT_TYPE_POINT:
            point_count++;
            break;
        default:
            break;
        }
    }

    uint32_t layer_count = directional_count + spot_count + 6 * point_count;
    uint32_t layer_offset = 0;

    this->image_array = std::make_shared<lava::image>(VK_FORMAT_D16_UNORM);
    this->image_array->set_layer_count(layer_count);
    this->image_array->set_view_type(VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    this->image_array->set_usage(VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);

    if (point_count > 0)
    {
        this->image_array->set_flags(VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);
    }

    if (!this->image_array->create(device, lava::uv2(settings.resolution)))
    {
        return false;
    }

    if (directional_count > 0)
    {
        if (!this->create_image_view(device, this->image_array, layer_offset, directional_count, VK_IMAGE_VIEW_TYPE_2D_ARRAY, this->image_view_directional))
        {
            return false;
        }

        if (!this->create_sampler(device, this->sampler_directional))
        {
            return false;
        }

        this->directional_offset = layer_offset;
        layer_offset += directional_count;
    }

    if (spot_count > 0)
    {
        if (!this->create_image_view(device, this->image_array, layer_offset, spot_count, VK_IMAGE_VIEW_TYPE_2D_ARRAY, this->image_view_spot))
        {
            return false;
        }

        if (!this->create_sampler(device, this->sampler_spot))
        {
            return false;
        }

        this->spot_offset = layer_offset;
        layer_offset += spot_count;
    }

    if (point_count > 0)
    {
        if (!this->create_image_view(device, this->image_array, layer_offset, point_count * 6, VK_IMAGE_VIEW_TYPE_CUBE_ARRAY, this->image_view_point))
        {
            return false;
        }

        if (!this->create_sampler(device, this->sampler_point))
        {
            return false;
        }

        this->point_offset = layer_offset;
        layer_offset += point_count * 6;
    }

    return true;
}

bool ShadowCache::create_buffers(lava::device_ptr device, const ShadowCacheSettings& settings)
{
    glsl::ShadowParameter shadow_parameter;
    shadow_parameter.use_directional = settings.use_directional ? 1 : 0;
    shadow_parameter.use_spot = settings.use_spot ? 1 : 0;
    shadow_parameter.use_point = settings.use_point ? 1 : 0;

    this->shadow_buffer = lava::make_buffer();

    if (!this->shadow_buffer->create_mapped(device, &shadow_parameter, sizeof(glsl::ShadowParameter), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT))
    {
        return false;
    }

    return true;
}

bool ShadowCache::create_descriptors(lava::device_ptr device)
{
    lava::VkDescriptorPoolSizes descriptor_type_count =
    {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 3 }
    };

    this->descriptor_pool = lava::make_descriptor_pool();

    if (!this->descriptor_pool->create(device, descriptor_type_count, 1))
    {
        return false;
    }

    this->descriptor = lava::make_descriptor();
    this->descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 0: directional image array
    this->descriptor->add_binding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 1: spot image array
    this->descriptor->add_binding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT); //descriptor-binding index 2: point image array
    this->descriptor->add_binding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT);         //descriptor-binding index 3: shadow buffer

    if (!this->descriptor->create(device))
    {
        return false;
    }

    this->descriptor_set = this->descriptor->allocate(this->descriptor_pool->get());

    std::array<VkDescriptorImageInfo, 3> image_infos;

    VkDescriptorImageInfo& directional_image_info = image_infos[0];
    directional_image_info.sampler = this->sampler_default;
    directional_image_info.imageView = this->image_default_plane->get_view();
    directional_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo& spot_image_info = image_infos[1];
    spot_image_info.sampler = this->sampler_default;
    spot_image_info.imageView = this->image_default_plane->get_view();
    spot_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorImageInfo& point_image_info = image_infos[2];
    point_image_info.sampler = this->sampler_default;
    point_image_info.imageView = this->image_default_cube->get_view();
    point_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    if (this->image_view_directional != nullptr)
    {
        directional_image_info.sampler = this->sampler_directional;
        directional_image_info.imageView = this->image_view_directional;
    }

    if (this->image_view_spot != nullptr)
    {
        spot_image_info.sampler = this->sampler_spot;
        spot_image_info.imageView = this->image_view_spot;
    }

    if (this->image_view_point != nullptr)
    {
        point_image_info.sampler = this->sampler_point;
        point_image_info.imageView = this->image_view_point;
    }

    std::vector<VkWriteDescriptorSet> descriptor_writes;
    descriptor_writes.resize(image_infos.size() + 1);

    for (uint32_t index = 0; index < image_infos.size(); index++)
    {
        VkWriteDescriptorSet& descriptor_write = descriptor_writes[index];
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = this->descriptor_set;
        descriptor_write.dstBinding = index;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptor_write.pImageInfo = &image_infos[index];
        descriptor_write.pBufferInfo = nullptr;
        descriptor_write.pTexelBufferView = nullptr;
    }

    VkDescriptorBufferInfo buffer_info;
    buffer_info.buffer = this->shadow_buffer->get();
    buffer_info.offset = 0;
    buffer_info.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet& descriptor_write = descriptor_writes.back();
    descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_write.pNext = nullptr;
    descriptor_write.dstSet = this->descriptor_set;
    descriptor_write.dstBinding = 3;
    descriptor_write.dstArrayElement = 0;
    descriptor_write.descriptorCount = 1;
    descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_write.pImageInfo = nullptr;
    descriptor_write.pBufferInfo = &buffer_info;
    descriptor_write.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

bool ShadowCache::create_render_pass(lava::device_ptr device, const ShadowCacheSettings& settings)
{
    VkClearValue clear_value;
    clear_value.depthStencil.depth = 1.0;
    clear_value.depthStencil.stencil = 0;
    
    lava::attachment::ptr attachment = lava::make_attachment(this->image_array->get_format());
    attachment->set_op(VK_ATTACHMENT_LOAD_OP_CLEAR, VK_ATTACHMENT_STORE_OP_STORE);
    attachment->set_stencil_op(VK_ATTACHMENT_LOAD_OP_DONT_CARE, VK_ATTACHMENT_STORE_OP_DONT_CARE);
    attachment->set_layouts(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    lava::subpass::ptr subpass = lava::make_subpass();
    subpass->set_depth_stencil_attachment(0, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    lava::subpass_dependency::ptr subpass_begin_dependency = lava::make_subpass_dependency(VK_SUBPASS_EXTERNAL, 0);
    subpass_begin_dependency->set_stage_mask(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
    subpass_begin_dependency->set_access_mask(0, VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

    lava::subpass_dependency::ptr subpass_end_dependency = lava::make_subpass_dependency(0, VK_SUBPASS_EXTERNAL);
    subpass_end_dependency->set_stage_mask(VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
    subpass_end_dependency->set_access_mask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT);

    this->render_pass = lava::make_render_pass(device);
    this->render_pass->add(subpass);
    this->render_pass->add(subpass_begin_dependency);
    this->render_pass->add(subpass_end_dependency);
    this->render_pass->add(attachment); //location 0: image array
    this->render_pass->set_layers(this->image_array->get_info().arrayLayers);
    this->render_pass->set_clear_values(
    { 
        clear_value
    });

    lava::rect framebuffer_area =
    {
        glm::vec2(0.0f),
        glm::vec2(settings.resolution)
    };

    lava::VkImageViews framebuffer_views =
    {
        this->image_array->get_view()
    };

    if (!this->render_pass->create({ framebuffer_views }, framebuffer_area))
    {
        return false;
    }

    return true;
}

bool ShadowCache::create_pipeline(lava::device_ptr device, Scene::Ptr scene)
{
    VkPushConstantRange push_range;
    push_range.offset = 0;
    push_range.size = sizeof(uint32_t) * 3; //layer-index, light-index, frustum-index
    push_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT;

    this->pipeline_layout = lava::make_pipeline_layout();
    this->pipeline_layout->add_push_constant_range(push_range);
    this->pipeline_layout->add(scene->get_light_descriptor()); //descriptor-set index 0: light-buffer
    this->pipeline_layout->add(scene->get_mesh_descriptor());  //descriptor-set index 1: mesh-buffer

    if (!this->pipeline_layout->create(device))
    {
        return false;
    }

    this->pipeline = lava::make_graphics_pipeline(device);
    this->pipeline->set_layout(this->pipeline_layout);
    this->pipeline->set_rasterization_front_face(VK_FRONT_FACE_CLOCKWISE);
    this->pipeline->set_rasterization_cull_mode(VK_CULL_MODE_BACK_BIT);
    this->pipeline->set_depth_test_and_write(true, true);
    this->pipeline->set_depth_compare_op(VK_COMPARE_OP_LESS);

    scene->set_vertex_input_only_position(this->pipeline.get());

    if (!this->pipeline->add_shader(lava::file_data("dpr/binary/shadow_vertex.spirv"), VK_SHADER_STAGE_VERTEX_BIT))
    {
        return false;
    }

    if (!this->pipeline->add_shader(lava::file_data("dpr/binary/shadow_geometry.spirv"), VK_SHADER_STAGE_GEOMETRY_BIT))
    {
        return false;
    }

    this->pipeline->on_process = [this](VkCommandBuffer command_buffer)
    {
        this->pipline_function(command_buffer);
    };

    if (!this->pipeline->create(this->render_pass->get()))
    {
        return false;
    }

    this->render_pass->add_front(this->pipeline);

    return true;
}

void ShadowCache::pipline_function(VkCommandBuffer command_buffer)
{
    this->pipeline_layout->bind(command_buffer, this->scene->get_light_descriptor_set(this->frame), 0);

    const std::vector<SceneLight>& lights = this->scene->get_lights();

    for (uint32_t light_index = 0; light_index < lights.size(); light_index++)
    {
        const SceneLight& light = lights[light_index];

        if (light.data.type == LIGHT_TYPE_DIRECTIONAL)
        {
            uint32_t layer_index = this->directional_offset + light.data.type_index;

            std::array<uint32_t, 3> constants;
            constants[0] = layer_index;
            constants[1] = light_index;
            constants[2] = 0;

            vkCmdPushConstants(command_buffer, this->pipeline_layout->get(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, 0, sizeof(uint32_t) * constants.size(), constants.data());

            this->pipline_scene(command_buffer);
        }
    }

    for (uint32_t light_index = 0; light_index < lights.size(); light_index++)
    {
        const SceneLight& light = lights[light_index];

        if (light.data.type == LIGHT_TYPE_SPOT)
        {
            uint32_t layer_index = this->spot_offset + light.data.type_index;

            std::array<uint32_t, 3> constants;
            constants[0] = layer_index;
            constants[1] = light_index;
            constants[2] = 0;

            vkCmdPushConstants(command_buffer, this->pipeline_layout->get(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, 0, sizeof(uint32_t) * constants.size(), constants.data());

            this->pipline_scene(command_buffer);
        }
    }

    for (uint32_t light_index = 0; light_index < lights.size(); light_index++)
    {
        const SceneLight& light = lights[light_index];

        if (light.data.type == LIGHT_TYPE_POINT)
        {
            for (uint32_t frustum_index = 0; frustum_index < 6; frustum_index++)
            {
                uint32_t layer_index = this->point_offset + light.data.type_index * 6 + frustum_index;

                std::array<uint32_t, 3> constants;
                constants[0] = layer_index;
                constants[1] = light_index;
                constants[2] = frustum_index;

                vkCmdPushConstants(command_buffer, this->pipeline_layout->get(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT, 0, sizeof(uint32_t) * constants.size(), constants.data());

                this->pipline_scene(command_buffer);
            }
        }
    }
}

void ShadowCache::pipline_scene(VkCommandBuffer command_buffer)
{
    for (const SceneMesh& mesh : this->scene->get_meshes())
    {
        if (!mesh.cast_shadow)
        {
            continue;
        }

        this->pipeline_layout->bind(command_buffer, mesh.descriptor_set[this->frame], 1);

        mesh.mesh->bind_draw(command_buffer);
    }
}

ShadowCache::Ptr make_shadow_cache()
{
    return std::make_shared<ShadowCache>();
}