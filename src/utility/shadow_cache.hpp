/*
  The shadow cache is responsible for the allocation and generation of the shadow maps.
  During the creation for the shadow cache, it is possible to set the resolution of shadow maps and the types of light for which a shadow map should be generated with the help of a ShadowCacheSettings struct.
  Before the shadow map can be used, the function compute_shadow(...) has to be called during rendering.
  After that, the shadow cache can be used by ether by adding it to a geometry buffer or by attaching the descriptor set of the shadow cache to a pipeline.
  In order to use the shadow cache during forward shading, it is neccessary to bind the descriptor of the cache to the binding point that is specified in the fragment shader by the define SHADOW_DESCRIPTOR_SET.
  Example (Deferred):

    ShadowCache::Ptr shadow_cache = make_shadow_cache();
    shadow_cache->create(get_scene(), ShadowCacheSettings());

    GeometryBuffer::Ptr geometry_buffer = make_geometry_buffer();
    geometry_buffer->create(output_image, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, get_stereo_transform()->get_descriptor(), get_scene(), shadow_cache); //Add the shadow cache to geometry buffer

    //During Rendering
    shadow_cache->compute_shadow(command_buffer, frame); //Generate shadow maps

    render_pass->process(command_buffer, 0); //Fill geometry buffer
    geometry_buffer->apply_lighting(command_buffer, per_frame_descriptor_set); //Compute lighting with shadows

  Example (Forward):

    ShadowCache::Ptr shadow_cache = make_shadow_cache();
    shadow_cache->create(get_scene(), ShadowCacheSettings());

    pipeline_layout->add(shadow_cache->get_descriptor()); //Same binding point as SHADOW_DESCRIPTOR_SET

    //During Rendering
    shadow_cache->compute_shadow(command_buffer, frame); //Generate shadow maps

    pipeline_layout->bind(command_buffer, shadow_cache->get_descriptor_set(), 42); //Same binding point as SHADOW_DESCRIPTOR_SET
*/

#pragma once
#include <liblava/lava.hpp>
#include <memory>
#include <cstdint>

#include "scene.hpp"

struct ShadowCacheSettings
{
    uint32_t resolution = 1024;

    bool use_directional = true;
    bool use_spot = true;
    bool use_point = true;
};

class ShadowCache
{
public:
    typedef std::shared_ptr<ShadowCache> Ptr; 

public:
    ShadowCache() = default;

    bool create(Scene::Ptr scene, const ShadowCacheSettings& settings);
    void destroy();
    
    void compute_shadow(VkCommandBuffer command_buffer, lava::index frame);

    lava::descriptor::ptr get_descriptor() const;
    VkDescriptorSet get_descriptor_set() const;
    
    const ShadowCacheSettings& get_settings() const;

private:
    bool create_image_view(lava::device_ptr device, lava::image::ptr image_array, uint32_t layer_offset, uint32_t layer_count, VkImageViewType view_type, VkImageView& view);
    bool create_sampler(lava::device_ptr device, VkSampler& sampler);
    bool create_images(lava::device_ptr device, Scene::Ptr scene, const ShadowCacheSettings& settings);
    bool create_buffers(lava::device_ptr device, const ShadowCacheSettings& settings);
    bool create_descriptors(lava::device_ptr device);

    bool create_render_pass(lava::device_ptr device, const ShadowCacheSettings& settings);
    bool create_pipeline(lava::device_ptr device, Scene::Ptr scene);
    
    void pipline_function(VkCommandBuffer command_buffer);
    void pipline_scene(VkCommandBuffer command_buffer);

private:
    Scene::Ptr scene;
    ShadowCacheSettings settings;
    uint32_t frame = 0;

    uint32_t directional_offset = 0;
    uint32_t spot_offset = 0;
    uint32_t point_offset = 0;

    lava::image::ptr image_default_plane;
    lava::image::ptr image_default_cube;
    bool image_default_cleared = false;

    lava::image::ptr image_array;
    VkImageView image_view_directional = nullptr;
    VkImageView image_view_spot = nullptr;
    VkImageView image_view_point = nullptr;

    VkSampler sampler_default = nullptr;
    VkSampler sampler_directional = nullptr;
    VkSampler sampler_spot = nullptr;
    VkSampler sampler_point = nullptr;

    lava::buffer::ptr shadow_buffer;

    lava::descriptor::pool::ptr descriptor_pool;
    lava::descriptor::ptr descriptor;
    VkDescriptorSet descriptor_set = nullptr;
    
    lava::pipeline_layout::ptr pipeline_layout;
    lava::graphics_pipeline::ptr pipeline;
    lava::render_pass::ptr render_pass;
};

ShadowCache::Ptr make_shadow_cache();