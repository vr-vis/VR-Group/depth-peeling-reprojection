#include "statistic.hpp"
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <imgui.h>

#include <filesystem>
#include <fstream>

bool StatisticKey::operator<(const StatisticKey& key) const
{
    if (this->frame_number == key.frame_number)
    {
        return this->transform_id < key.transform_id;
    }

    return this->frame_number < key.frame_number;
}

Statistic::Statistic(const std::string& name, Unit unit, bool log_only) : name(name), unit(unit), log_only(log_only)
{
    
}

void Statistic::interface(uint32_t sample_count)
{
    std::string plot_name = this->get_label_name();

    uint32_t plot_length = glm::min(sample_count, (uint32_t)this->samples.size());
    uint32_t plot_src_offset = this->samples.size() - plot_length;
    uint32_t plot_dst_offset = sample_count - plot_length;

    std::vector<float> plot_samples;
    plot_samples.resize(sample_count, 0.0f);

    for (uint32_t index = 0; index < plot_length; index++)
    {
        plot_samples[plot_dst_offset + index] = this->samples[plot_src_offset + index].value;
    }

    ImGui::PlotLines(plot_name.c_str(), plot_samples.data(), plot_samples.size(), 0, nullptr, 0.0f, this->get_max(sample_count), ImVec2(0, 64));

    ImVec2 cursor = ImGui::GetCursorPos();
    ImGui::SetWindowFontScale(0.7f);

    ImGui::SetCursorPosX(cursor.x + 5);
    ImGui::SetCursorPosY(cursor.y - 22.0f);
    ImGui::Text("Avg: %8.4f", this->get_average(sample_count));
    ImGui::SetCursorPosX(cursor.x + 90);
    ImGui::SetCursorPosY(cursor.y - 22.0f);
    ImGui::Text("Min: %8.4f", this->get_min(sample_count));
    ImGui::SetCursorPosX(cursor.x + 175);
    ImGui::SetCursorPosY(cursor.y - 22.0f);
    ImGui::Text("Max: %8.4f", this->get_max(sample_count));

    ImGui::SetCursorPos(cursor);
    ImGui::SetWindowFontScale(1.0);
}

void Statistic::add_sample(float value)
{
    StatisticSample sample;
    sample.value = value;
    sample.time_point = std::chrono::high_resolution_clock::now();

    this->samples.push_back(sample);
}

void Statistic::add_sample(const StatisticSample& sample)
{
    this->samples.push_back(sample);
}

double Statistic::get_average() const
{
    return this->get_average(this->samples.size());
}

double Statistic::get_average(uint32_t sample_count) const
{
    uint32_t count = glm::min(sample_count, (uint32_t)this->samples.size());

    if (count <= 0)
    {
        return 0.0f;
    }

    uint32_t offset = this->samples.size() - count;
    double sum = 0.0f;

    for (uint32_t index = offset; index < this->samples.size(); index++)
    {
        sum += this->samples[index].value;
    }

    return sum / (double)count;
}

double Statistic::get_min() const
{
    return this->get_min(this->samples.size());
}

double Statistic::get_min(uint32_t sample_count) const
{
    uint32_t count = glm::min(sample_count, (uint32_t)this->samples.size());

    if (count <= 0)
    {
        return 0.0f;
    }

    uint32_t offset = this->samples.size() - count;
    double min = this->samples[offset].value;

    for (uint32_t index = offset + 1; index < this->samples.size(); index++)
    {
        min = glm::min(min, this->samples[index].value);
    }

    return min;
}

double Statistic::get_max() const
{
    return this->get_max(this->samples.size());
}

double Statistic::get_max(uint32_t sample_count) const
{
    uint32_t count = glm::min(sample_count, (uint32_t)this->samples.size());

    if (count <= 0)
    {
        return 0.0f;
    }

    uint32_t offset = this->samples.size() - count;
    double max = this->samples[offset].value;

    for (uint32_t index = offset + 1; index < this->samples.size(); index++)
    {
        max = glm::max(max, this->samples[index].value);
    }

    return max;
}

std::vector<float> Statistic::get_sample_values(uint32_t sample_count) const
{
    std::vector<float> sample_values;
    sample_values.reserve(sample_count);

    uint32_t count = glm::min(sample_count, (uint32_t) this->samples.size());
    uint32_t offset = this->samples.size() - count;

    for (uint32_t index = offset; index < this->samples.size(); index++)
    {
        sample_values.push_back(this->samples[index].value);
    }

    return sample_values;
}

std::string Statistic::get_label_name() const
{
    return this->get_display_name() + " [" + this->get_unit_name() + "]";
}

std::string Statistic::get_display_name() const
{
    std::string display_name = this->name;

    for (uint32_t index = 0; index < display_name.size(); index++)
    {
        if (index == 0)
        {
            display_name[index] = std::toupper(display_name[index]);
        }

        else if (display_name[index - 1] == '_')
        {
            display_name[index - 1] = ' ';
            display_name[index] = std::toupper(display_name[index]);
        }
    }

    return display_name;
}

std::string Statistic::get_unit_name() const
{
    switch (this->unit)
    {
    case UNIT_UNDEFINED:
        return "undef";
    case UNIT_HOURS:
        return "h";
    case UNIT_MINUTES:
        return "min";
    case UNIT_SECONDS:
        return "s";
    case UNIT_MILLISECONDS:
        return "ms";
    case UNIT_MIRCOSECONDS:
        return "us";
    case UNIT_BITS:
        return "bit";
    case UNIT_KBITS:
        return "kbit";
    case UNIT_MBITS:
        return "Mbit";
    case UNIT_KIBITS:
        return "Kibit";
    case UNIT_MIBITS:
        return "Mibit";
    case UNIT_BYTES:
        return "B";
    case UNIT_KBYTES:
        return "kB";
    case UNIT_MBYTES:
        return "MB";
    case UNIT_KIBYTES:
        return "KiB";
    case UNIT_MIBYTES:
        return "MiB";
    case UNIT_UNDEFINED_PER_SECOND:
        return "undef/s";
    case UNIT_BITS_PER_SECOND:
        return "bit/s";
    case UNIT_KBITS_PER_SECOND:
        return "kbit/s";
    case UNIT_MBITS_PER_SECOND:
        return "Mbit/s";
    default:
        lava::log()->error("Unkown unit!");
        break;
    }

    return "undef";
}

const std::vector<StatisticSample>& Statistic::get_samples() const
{
    return this->samples;
}

const std::string& Statistic::get_name() const
{
    return this->name;
}

Unit Statistic::get_unit() const
{
    return this->unit;
}

bool Statistic::is_log_only() const
{
    return this->log_only;
}

StatisticLog::StatisticLog()
{
    this->time_origin = std::chrono::high_resolution_clock::now();
}

bool StatisticLog::write(const std::string& directory)
{
    if (!std::filesystem::exists(directory))
    {
        std::filesystem::create_directory(directory);
    }

    std::string file_name = this->build_file_name();

    std::fstream file;
    file.open(directory + "/" + file_name, std::ios::out);

    if (!file.good())
    {
        lava::log()->error("Can't write to file '" + file_name + "' for statistic!");

        return false;
    }

    std::vector<std::string> ordered_sample_labels;
    std::map<StatisticKey, std::vector<std::optional<double>>> ordered_samples;

    std::vector<std::string> unordered_sample_labels;
    std::vector<std::vector<double>> unordered_samples;

    for (const Statistic::Ptr& statistic : this->statistic_list)
    {
        for (const StatisticSample& sample : statistic->get_samples())
        {
            std::string label_value;
            std::string label_time;

            if (sample.frame_id.has_value())
            {
                label_value = statistic->get_name() + "_" + std::to_string(sample.frame_id.value()) + " [" + statistic->get_unit_name() + "]";
                label_time = statistic->get_name() + "_" + std::to_string(sample.frame_id.value()) + "_time [ms]";
            }

            else
            {
                label_value = statistic->get_name() + " [" + statistic->get_unit_name() + "]";
                label_time = statistic->get_name() + "_time [ms]";
            }

            double sample_time = std::chrono::duration_cast<std::chrono::duration<double, std::chrono::milliseconds::period>>(sample.time_point - this->time_origin).count();
            bool ordered = sample.frame_number.has_value() || sample.transform_id.has_value();
            
            if (ordered)
            {
                StatisticKey key;
                key.frame_number = sample.frame_number.value_or(0);
                key.transform_id = sample.transform_id.value_or(0);
                
                uint32_t column_index = 0;
                bool column_found = false;

                for (uint32_t index = 0; index < ordered_sample_labels.size(); index++)
                {
                    if (ordered_sample_labels[index] == label_value)
                    {
                        column_index = index;
                        column_found = true;

                        break;
                    }
                }

                if (!column_found)
                {
                    column_index = ordered_sample_labels.size();
                    ordered_sample_labels.push_back(label_value);
                    ordered_sample_labels.push_back(label_time);
                }

                std::vector<std::optional<double>>& row = ordered_samples[key];

                if (row.size() <= column_index)
                {
                    row.resize(column_index + 2);
                }

                row[column_index] = sample.value;
                row[column_index + 1] = sample_time;
            }

            else
            {
                uint32_t column_index = 0;
                bool column_found = false;

                for (uint32_t index = 0; index < unordered_sample_labels.size(); index++)
                {
                    if (unordered_sample_labels[index] == label_value)
                    {
                        column_index = index;
                        column_found = true;

                        break;
                    }
                }

                if (!column_found)
                {
                    column_index = unordered_sample_labels.size();
                    unordered_sample_labels.push_back(label_value);
                    unordered_sample_labels.push_back(label_time);
                }

                if (unordered_samples.size() <= column_index)
                {
                    unordered_samples.resize(column_index + 2);
                }

                unordered_samples[column_index].push_back(sample.value);
                unordered_samples[column_index + 1].push_back(sample_time);
            }
        }
    }

    std::vector<std::string> row_cells;

    if (!ordered_sample_labels.empty())
    {
        row_cells.push_back("frame_number");
        row_cells.push_back("transform_id");

        for (const std::string& label : ordered_sample_labels)
        {
            row_cells.push_back(label);
        }

        if (!unordered_sample_labels.empty())
        {
            row_cells.push_back("");
        }
    }

    for (const std::string& label : unordered_sample_labels)
    {
        row_cells.push_back(label);
    }

    for (uint32_t index = 0; index < row_cells.size(); index++)
    {
        if (index > 0)
        {
            file << ";";
        }

        file << row_cells[index];
    }

    file << std::endl;

    std::map<StatisticKey, std::vector<std::optional<double>>>::iterator row_iter = ordered_samples.begin();
    uint32_t row_index = 0;
    bool row_added = true;

    while (row_added)
    {
        row_cells.clear();
        row_added = false;

        if (row_iter != ordered_samples.end())
        {
            const StatisticKey& key = row_iter->first;
            const std::vector<std::optional<double>>& row = row_iter->second;

            row_cells.push_back(std::to_string(key.frame_number));
            row_cells.push_back(std::to_string(key.transform_id));

            for (uint32_t index = 0; index < row.size(); index++)
            {
                if (row[index].has_value())
                {
                    row_cells.push_back(std::to_string(row[index].value()));
                }

                else
                {
                    row_cells.push_back("");
                }
            }

            for (uint32_t index = 0; index < (ordered_sample_labels.size() - row.size()); index++)
            {
                row_cells.push_back("");
            }

            row_added = true;
            row_iter++;
        }
        
        else if (!ordered_sample_labels.empty())
        {
            row_cells.push_back("");
            row_cells.push_back("");

            for (uint32_t index = 0; index < ordered_sample_labels.size(); index++)
            {
                row_cells.push_back("");
            }
        }

        if (!ordered_sample_labels.empty() && !unordered_sample_labels.empty())
        {
            row_cells.push_back("");
        }

        for (const std::vector<double>& column : unordered_samples)
        {
            if (row_index < column.size())
            {
                row_cells.push_back(std::to_string(column[row_index]));
                row_added = true;
            }

            else
            {
                row_cells.push_back("");
            }
        }

        for (uint32_t index = 0; index < row_cells.size(); index++)
        {
            if (index > 0)
            {
                file << ";";
            }

            file << row_cells[index];
        }

        file << std::endl;
        row_index++;
    }

    file.close();

    return true;
}

void StatisticLog::add_statistic(Statistic::Ptr statistic)
{
    this->statistic_list.push_back(statistic);
}

std::string StatisticLog::build_file_name()
{
    time_t system_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    tm local_time = *localtime(&system_time);

    std::string name = "statistic";
    name += "_" + std::to_string(local_time.tm_mday);
    name += "-" + std::to_string(local_time.tm_mon + 1);
    name += "-" + std::to_string(local_time.tm_year + 1900);
    name += "_" + std::to_string(local_time.tm_hour);
    name += "-" + std::to_string(local_time.tm_min);
    name += "-" + std::to_string(local_time.tm_sec);
    name += ".csv";

    return name;
}

Statistic::Ptr make_statistic(const std::string& name, Unit unit, bool log_only)
{
    return std::make_shared<Statistic>(name, unit, log_only);
}

StatisticLog::Ptr make_statistic_log()
{
    return std::make_shared<StatisticLog>();
}