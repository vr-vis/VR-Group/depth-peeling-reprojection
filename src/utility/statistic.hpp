/*
  With an statistic object it is possible to measure a value over time and calculate average, min and max.
  Besides that it is possible to draw a plot of the measurements by calling the function interface(...).
  With the help of an statistic log object it is possible to create single a .csv file for a list of statistic objects.
  Example:

   Statistic::Ptr statistic = make_statistic("statistic_name", UNIT_UNDEFINED);
   statistic->add_sample(42.0f);
   statistic->interface();

   StatisticLog::Ptr statistic_log = make_statistic_log();
   statistic_log->add_statistic(statistic);
   statistic_log->write("directory");
*/

#pragma once
#include <chrono>
#include <memory>
#include <string>
#include <vector>
#include <optional>
#include <cstdint>

#include "types.hpp"

struct StatisticSample
{
    double value;
    std::chrono::high_resolution_clock::time_point time_point;

    std::optional<FrameNumber> frame_number;
    std::optional<FrameId> frame_id;
    std::optional<TransformId> transform_id;
};

struct StatisticKey
{
public:
    FrameNumber frame_number;
    TransformId transform_id;

public:
    StatisticKey() = default;

    bool operator<(const StatisticKey& key) const;
};

class Statistic
{
public:
    typedef std::shared_ptr<Statistic> Ptr;

private:
    std::string name;
    Unit unit;
    bool log_only;

    std::vector<StatisticSample> samples;

public:
    Statistic(const std::string& name, Unit unit, bool log_only);

    void interface(uint32_t sample_count);

    void add_sample(float value);
    void add_sample(const StatisticSample& sample);

    double get_average() const;
    double get_average(uint32_t sample_count) const;
    double get_min() const;
    double get_min(uint32_t sample_count) const;
    double get_max() const;
    double get_max(uint32_t sample_count) const;

    std::vector<float> get_sample_values(uint32_t sample_count) const;

    std::string get_label_name() const;
    std::string get_display_name() const;
    std::string get_unit_name() const;

    const std::vector<StatisticSample>& get_samples() const;
    const std::string& get_name() const;
    Unit get_unit() const;

    bool is_log_only() const;
};

class StatisticLog
{
public:
    typedef std::shared_ptr<StatisticLog> Ptr;

public:
    StatisticLog();

    bool write(const std::string& directory);

    void add_statistic(Statistic::Ptr statistic);

private:
    std::string build_file_name();

private:
    std::vector<Statistic::Ptr> statistic_list;
    std::chrono::high_resolution_clock::time_point time_origin;
};

Statistic::Ptr make_statistic(const std::string& name, Unit unit, bool log_only = false);
StatisticLog::Ptr make_statistic_log();