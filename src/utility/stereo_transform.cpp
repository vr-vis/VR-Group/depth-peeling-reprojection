#include "stereo_transform.hpp"
#include "vr_application.hpp"

StereoTransform::StereoTransform()
{
    for (glsl::PerFrameData& data : this->per_frame_data)
    {
        data.projection = glm::mat4(1.0f);
        data.headToEye = glm::mat4(1.0f);
        data.view = glm::mat4(1.0f);
        data.viewProjection = glm::mat4(1.0f);
        data.invView = glm::mat4(1.0f);
        data.invProjection = glm::mat4(1.0f);
        data.eyePosition = glm::vec3(0.0f);
    }
}

bool StereoTransform::create(lava::device_ptr device, uint32_t frame_count)
{
    uint32_t descriptor_count = frame_count * 2;
    lava::VkDescriptorPoolSizesRef descriptor_types =
    {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, descriptor_count }
    };

    this->descriptor_pool = lava::make_descriptor_pool();
    
    if (!this->descriptor_pool->create(device, descriptor_types, descriptor_count))
    {
        lava::log()->error("Can't create descriptor pool for stereo transform!");

        return false;
    }

    this->descriptor = lava::make_descriptor();
    this->descriptor->add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_ALL_GRAPHICS);

    if (!this->descriptor->create(device))
    {
        lava::log()->error("Can't create descriptor for stereo transform!");

        return false;
    }

    if (!this->create_buffers(EYE_LEFT, device, frame_count))
    {
        return false;
    }

    if (!this->create_buffers(EYE_RIGHT, device, frame_count))
    {
        return false;
    }

    return true;
}

void StereoTransform::destroy()
{
    this->destroy_buffers(EYE_LEFT);
    this->destroy_buffers(EYE_RIGHT);

    this->descriptor->destroy();
    this->descriptor_pool->destroy();
}

void StereoTransform::write(lava::index frame)
{
    std::array<VkBufferMemoryBarrier, 2> buffer_barriers;

    for (uint32_t index = 0; index < this->buffers.size(); index++)
    {
        lava::buffer::ptr buffer = this->buffers[index][frame];

        std::memcpy(buffer->get_mapped_data(), &this->per_frame_data[index], sizeof(glsl::PerFrameData));
        buffer->flush();
    }
}

void StereoTransform::set_view_matrix(const glm::mat4& matrix)
{
    this->view_matrix = matrix;

    this->compute_matrices(EYE_LEFT);
    this->compute_matrices(EYE_RIGHT);
}

void StereoTransform::set_head_to_eye_matrix(Eye eye, const glm::mat4& matrix)
{
    this->head_to_eye_matrices[eye] = matrix;

    this->compute_matrices(eye);
}

void StereoTransform::set_projection_matrix(Eye eye, const glm::mat4& matrix)
{
    this->projection_matrices[eye] = matrix;

    this->compute_matrices(eye);
}

const glm::mat4& StereoTransform::get_view_matrix() const
{
    return this->view_matrix;
}

const glm::mat4& StereoTransform::get_head_to_eye_matrix(Eye eye) const
{
    return this->head_to_eye_matrices[eye];
}

const glm::mat4& StereoTransform::get_projection_matrix(Eye eye) const
{
    return this->projection_matrices[eye];
}

const glm::mat4& StereoTransform::get_view_eye_projection_matrix(Eye eye) const
{
    return this->per_frame_data[eye].viewProjection;
}

const glm::mat4& StereoTransform::get_inverse_view_eye_matrix(Eye eye) const
{
    return this->per_frame_data[eye].invView;
}

const glm::mat4& StereoTransform::get_inverse_projection_matrix(Eye eye) const
{
    return this->per_frame_data[eye].invProjection;
}

const glm::vec3& StereoTransform::get_eye_position(Eye eye) const
{
    return this->per_frame_data[eye].eyePosition;
}

lava::descriptor::ptr StereoTransform::get_descriptor() const
{
    return this->descriptor;
}

VkDescriptorSet StereoTransform::get_descriptor_set(Eye eye, lava::index frame) const
{
    return this->descriptor_sets[eye][frame];
}

bool StereoTransform::create_buffers(Eye eye, lava::device_ptr device, uint32_t frame_count)
{
    std::vector<VkWriteDescriptorSet> descriptor_writes(frame_count);

    for (uint32_t index = 0; index < frame_count; index++)
    {
        lava::buffer::ptr buffer = lava::make_buffer();

        if (!buffer->create_mapped(device, nullptr, sizeof(glsl::PerFrameData), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU))
        {
            lava::log()->error("Can't create buffer for stereo transform!");

            return false;
        }

        VkDescriptorSet descriptor_set = this->descriptor->allocate(this->descriptor_pool->get());

        this->descriptor_sets[eye].push_back(descriptor_set);
        this->buffers[eye].push_back(buffer);

        VkWriteDescriptorSet& descriptor_write = descriptor_writes[index];
        descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptor_write.pNext = nullptr;
        descriptor_write.dstSet = descriptor_set;
        descriptor_write.dstBinding = 0;
        descriptor_write.dstArrayElement = 0;
        descriptor_write.descriptorCount = 1;
        descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_write.pImageInfo = nullptr;
        descriptor_write.pBufferInfo = buffer->get_descriptor_info();
        descriptor_write.pTexelBufferView = nullptr;
    }

    vkUpdateDescriptorSets(device->get(), descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return true;
}

void StereoTransform::destroy_buffers(Eye eye)
{
    for (VkDescriptorSet descriptor_set : this->descriptor_sets[eye])
    {
        this->descriptor->free_set(descriptor_set, this->descriptor_pool->get());
    }

    this->buffers[eye].clear();
    this->descriptor_sets[eye].clear();
}

void StereoTransform::compute_matrices(Eye eye)
{
    glsl::PerFrameData& data = this->per_frame_data[eye];

    glm::mat4 view_eye_matrix = this->head_to_eye_matrices[eye] * this->view_matrix;
    glm::mat4 view_eye_projection_matrix = this->projection_matrices[eye] * view_eye_matrix;
    glm::mat4 inv_view_eye_matrix = glm::inverse(view_eye_matrix);
    glm::mat4 inv_projection_matrix = glm::inverse(this->projection_matrices[eye]);
    glm::vec3 eye_position = glm::vec3(inv_view_eye_matrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

    data.projection = this->projection_matrices[eye];
    data.headToEye = this->head_to_eye_matrices[eye];
    data.view = this->view_matrix;
    data.viewProjection = view_eye_projection_matrix;
    data.invView = inv_view_eye_matrix;
    data.invProjection = inv_projection_matrix;
    data.eyePosition = eye_position;
}

StereoTransform::Ptr make_stereo_transform()
{
    return std::make_shared<StereoTransform>();
}