/*
  The stereo transform defines the per frame transforms that should be used for rendering.
  It can be accessed inside of a stereo strategy by calling the function get_stereo_transform(...).
  The per frame descriptor set can be determined by calling the function get_descriptor_set(...).
  Example:

   get_stereo_transform()->get_descriptor_set(EYE_LEFT, get_application()->get_frame_index());
*/

#pragma once
#include <liblava/lava.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <array>
#include <cstdint>

#include "headset/headset.hpp"

class VRApplication;

namespace glsl
{
using namespace glm;
#include "res/dpr/data/per_frame.inc"
}

class StereoTransform
{
public:
    typedef std::shared_ptr<StereoTransform> Ptr;

public:
    StereoTransform();

    bool create(lava::device_ptr device, uint32_t frame_count);
    void destroy();
    void write(lava::index frame);

    void set_view_matrix(const glm::mat4& matrix);
    void set_head_to_eye_matrix(Eye eye, const glm::mat4& matrix);
    void set_projection_matrix(Eye eye, const glm::mat4& matrix);

    const glm::mat4& get_view_matrix() const;
    const glm::mat4& get_head_to_eye_matrix(Eye eye) const;
    const glm::mat4& get_projection_matrix(Eye eye) const;

    const glm::mat4& get_view_eye_projection_matrix(Eye eye) const;
    const glm::mat4& get_inverse_view_eye_matrix(Eye eye) const;
    const glm::mat4& get_inverse_projection_matrix(Eye eye) const;
    const glm::vec3& get_eye_position(Eye eye) const;

    lava::descriptor::ptr get_descriptor() const;
    VkDescriptorSet get_descriptor_set(Eye eye, lava::index frame) const;

private:
    bool create_buffers(Eye eye, lava::device_ptr device, uint32_t frame_count);
    void destroy_buffers(Eye eye);

    void compute_matrices(Eye eye);

private:
    lava::descriptor::pool::ptr descriptor_pool;
    lava::descriptor::ptr descriptor;
    std::array<std::vector<VkDescriptorSet>, 2> descriptor_sets;
    std::array<std::vector<lava::buffer::ptr>, 2> buffers;

    glm::mat4 view_matrix;
    std::array<glm::mat4, 2> head_to_eye_matrices;
    std::array<glm::mat4, 2> projection_matrices;

    std::array<glsl::PerFrameData, 2> per_frame_data;
};

StereoTransform::Ptr make_stereo_transform();