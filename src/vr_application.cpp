#include "vr_application.hpp"
#include <glm/gtx/matrix_operation.hpp>
#include <iostream>
#include <imgui.h>

bool VRApplication::setup(lava::name name, argh::parser cmd_line)
{
    if (!this->command_parser.parse_command(cmd_line))
    {
        return false;
    }

    this->headset = make_headset(this, this->command_parser.get_headset());

    if (this->headset == nullptr)
    {
        return false;
    }

    this->strategies = make_all_stereo_strategies(this);
    this->active_strategy = this->command_parser.get_stereo_strategy();

	lava::frame_config config(name, std::forward<argh::parser>(cmd_line));
    config.log.debug = true;
    config.debug.validation = false;

    if (!this->headset->on_setup_instance(config))
    {
        std::cout << "Error during headset setup instance!" << std::endl;

        return false;    
    }

    for (StereoStrategy::Ptr strategy : this->strategies)
    {
        if (!strategy->on_setup_instance(config))
        {
            std::cout << "Error during strategy setup instance!" << std::endl;

            return false;        
        }
    }

	this->app.emplace(config);
    this->app->shading.set_clear_enabled(false);
   
	this->app->manager.on_create_param = [this](lava::device::create_param& parameters)
	{
        if (!this->headset->on_setup_device(parameters))
        {
            lava::log()->error("Error during headset setup device!");

            this->app->shut_down();
        }

        for (StereoStrategy::Ptr strategy : this->strategies)
        {
            if (!strategy->on_setup_device(parameters))
            {
                lava::log()->error("Error during strategy setup device!");

                this->app->shut_down();
            }
        }

        parameters.features.samplerAnisotropy = true;
        parameters.features.fillModeNonSolid = true;
        parameters.features.multiViewport = true;
        parameters.features.geometryShader = true;
        parameters.features.tessellationShader = true;
        parameters.features.imageCubeArray = true;
        parameters.features.fragmentStoresAndAtomics = true;
    };

	this->app->on_create = [this]()
	{
        if (!this->created)
        {
            this->created = true;

            if (!this->on_create())
            {
                this->on_destroy();

                return false;
            }
        }

        return true;
	};

	this->app->on_destroy = [this]()
	{
        this->on_destroy();
    };

    this->app->window.on_resize = [this](uint32_t width, uint32_t height)
    {
        return this->on_resize();
    };

	this->app->on_update = [this](lava::delta time_delta)
	{
        return this->on_update(time_delta);
    };

	this->app->imgui.on_draw = [this]()
	{
        if (!this->on_interface())
		{
            this->app->shut_down();
		}
    };

	this->app->on_process = [this](VkCommandBuffer command_buffer, lava::index frame)
	{
		if (!this->on_render(command_buffer, frame))
		{
            this->app->shut_down();
		}
    };

	return this->app->setup();
}

void VRApplication::run()
{
    this->app->run();
}

Scene::Ptr VRApplication::get_scene() const
{
    return this->scene;
}

Headset::Ptr VRApplication::get_headset() const
{
    return this->headset;
}

StereoStrategy::Ptr VRApplication::get_stereo_strategy() const
{
    return this->strategies[this->active_strategy];
}

StereoStrategyType VRApplication::get_stereo_strategy_type() const
{
    return this->active_strategy;
}

CompanionWindow::Ptr VRApplication::get_companion_window() const
{
    return this->companion_window;
}

PassTimer::Ptr VRApplication::get_pass_timer() const
{
    return this->pass_timer;
}

FrameCapture::Ptr VRApplication::get_frame_capture() const
{
    return this->frame_capture;
}

StereoTransform::Ptr VRApplication::get_stereo_transform() const
{
    return this->stereo_transform;
}

ShadowCache::Ptr VRApplication::get_shadow_cache() const
{
    return this->shadow_cache;
}

IndirectCache::Ptr VRApplication::get_indirect_cache() const
{
    return this->indirect_cache;
}

StatisticLog::Ptr VRApplication::get_statistic_log() const
{
    return this->statistic_log;
}

const CommandParser& VRApplication::get_command_parser() const
{
    return this->command_parser;
}

lava::device_ptr VRApplication::get_device() const
{
    return this->app->device;
}

lava::render_target::ptr VRApplication::get_target() const
{
    return this->app->target;
}

lava::instance& VRApplication::get_instance()
{
    return lava::instance::singleton();
}

lava::window& VRApplication::get_window()
{
    return this->app->window;
}

lava::renderer& VRApplication::get_renderer()
{
    return this->app->renderer;
}

lava::input& VRApplication::get_input()
{
    return this->app->input;
}

lava::camera& VRApplication::get_camera()
{
    return this->app->camera;
}

lava::index VRApplication::get_frame_index() const
{
    return this->app->renderer.get_frame();
}

uint32_t VRApplication::get_frame_count() const
{
    return this->app->target->get_frame_count();
}

bool VRApplication::create_config()
{
    if (!this->headset->on_create())
    {
        lava::log()->error("Can't create headset!");

        return false;
    }

    StereoStrategy::Ptr strategy = this->get_stereo_strategy();

    if (!strategy->on_create())
    {
        lava::log()->error("Can't create strategy!");

        return false;
    }

    return true;
}

bool VRApplication::change_config(StereoStrategyType strategy)
{
    this->app->device->wait_for_idle();

    this->destroy_config();

    this->active_strategy = strategy;

    return this->create_config();
}

void VRApplication::destroy_config()
{
    StereoStrategy::Ptr strategy = this->get_stereo_strategy();
   
    if (strategy != nullptr)
    {
        strategy->on_destroy();
    }

    if (this->headset != nullptr)
    {
        this->headset->on_destroy();
    }
}

void VRApplication::begin_render(VkCommandBuffer command_buffer, lava::index frame)
{
    lava::image::ptr window_image = this->app->target->get_backbuffer(frame);

    VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(window_image->get(), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    frame_barrier.srcAccessMask = 0;
    frame_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    frame_barrier.subresourceRange = window_image->get_subresource_range();

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &frame_barrier);

    VkClearColorValue clear_value;
    clear_value.float32[0] = 0.086f;
    clear_value.float32[1] = 0.086f;
    clear_value.float32[2] = 0.094f;
    clear_value.float32[3] = 1.0f;

    vkCmdClearColorImage(command_buffer, window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_value, 1, &window_image->get_subresource_range());
}

void VRApplication::end_render(VkCommandBuffer command_buffer, lava::index frame)
{
    lava::image::ptr window_image = this->app->target->get_backbuffer(frame);

    VkImageMemoryBarrier frame_barrier = lava::image_memory_barrier(window_image->get(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
    frame_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    frame_barrier.dstAccessMask = 0;
    frame_barrier.subresourceRange = window_image->get_subresource_range();

    vkCmdPipelineBarrier(command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &frame_barrier);
}

bool VRApplication::on_create()
{
    lava::file_system::mount(std::string(RESOURCE_FOLDER)); //NOTE: Set absolut path for the resource folder so that also in release all resources can be found.

    SceneConfig config;
    config.scene_file = this->command_parser.get_scene_path();
    config.scene_unit = this->command_parser.get_scene_unit();
    config.scene_orientation = this->command_parser.get_scene_orientation();
    config.scene_uvs = this->command_parser.get_scene_uvs();
    config.scene_exposure = this->command_parser.get_scene_exposure();

    if (this->command_parser.get_sky_sphere_path().has_value())
    {
        config.sky_sphere_file = this->command_parser.get_sky_sphere_path().value();
        config.sky_sphere_intensity = this->command_parser.get_sky_sphere_intensity();
    }

    if (!this->command_parser.should_benchmark())
    {
        std::string left_controller_file = "/dpr-controller/ObjModelViveFocus3ControllerLeft.fbx";
        std::string right_controller_file = "/dpr-controller/ObjModelViveFocus3ControllerRight.fbx";

        config.controller_left_file = lava::file_system::get_real_dir(right_controller_file.c_str()) + left_controller_file;
        config.controller_left_unit = SCENE_UNIT_CENTIMETERS;
        
        config.controller_right_file = lava::file_system::get_real_dir(right_controller_file.c_str()) + right_controller_file;
        config.controller_right_unit = SCENE_UNIT_CENTIMETERS;
    }

    this->scene = make_scene();

    if (!this->scene->create(config, this->app->device, this->get_frame_count()))
    {
        return false;
    }

    if (this->command_parser.should_benchmark())
    {
        this->scene->set_animation_active(true);
        this->scene->set_animation_loop(false);
        
        if (this->command_parser.get_animation_name().has_value())
        {
            if (!this->scene->set_animation(this->command_parser.get_animation_name().value()))
            {
                lava::log()->error("Can't find animation with name '{}' !", this->command_parser.get_animation_name().value());

                return false;
            }
        }

        else if (this->command_parser.get_animation_index().has_value())
        {
            if (!this->scene->set_animation(this->command_parser.get_animation_index().value()))
            {
                lava::log()->error("Can't find animation with index '{}' !", this->command_parser.get_animation_index().value());

                return false;
            }
        }

        else
        {
            lava::log()->error("No animation selected for benchmark!");

            return false;
        }

        if (this->command_parser.get_camera_name().has_value())
        {
            if (!this->scene->set_camera(this->command_parser.get_camera_name().value()))
            {
                lava::log()->error("Can't find camera with name '{}' !", this->command_parser.get_camera_name().value());

                return false;
            }
        }

        else if (this->command_parser.get_camera_index().has_value())
        {
            if (!this->scene->set_camera(this->command_parser.get_camera_index().value()))
            {
                lava::log()->error("Can't find camera with index '{}' !", this->command_parser.get_camera_index().value());

                return false;
            }
        }

        else
        {
            lava::log()->error("No camera selected for benchmark!");

            return false;
        }
    }

    this->statistic_log = make_statistic_log();
    this->frame_time = make_statistic("frame_time", UNIT_MILLISECONDS);
    this->statistic_log->add_statistic(this->frame_time);

    this->companion_window = make_companion_window();
    this->companion_window->set_enabled(this->command_parser.should_show_companion_window());

    if (!this->companion_window->create(this->app->device, this->app->target))
    {
        lava::log()->error("Can't create companion window!");

        return false;
    }

    this->frame_capture = make_frame_capture("frame_captures");
    this->frame_capture->set_enabled(this->command_parser.should_write_frames());

    if (!this->frame_capture->create(this->get_frame_count()))
    {
        lava::log()->error("Can't create frame capture!");

        return false;
    }

    this->pass_timer = make_pass_timer();

    if (!this->pass_timer->create(this->app->device, this->statistic_log, this->get_frame_count(), 10))
    {
        lava::log()->error("Can't create pass timer!");

        return false;
    }

    this->stereo_transform = make_stereo_transform();

    if (!this->stereo_transform->create(this->app->device, this->get_frame_count()))
    {
        lava::log()->error("Can't create stereo transform!");

        return false;
    }

    if (!this->command_parser.should_disbale_shadows())
    {
        this->shadow_cache = make_shadow_cache();

        if (!this->shadow_cache->create(this->scene, ShadowCacheSettings()))
        {
            lava::log()->error("Can't create shadow cache!");

            return false;
        }
    }

    if (!this->command_parser.should_disable_indirect_lighting())
    {
        this->indirect_cache = make_indirect_cache();

        if (!this->indirect_cache->create(this->scene, IndirectCacheSettings()))
        {
            lava::log()->error("Can't create indirect cache!");

            return false;
        }

        this->scene->write(0);

        if (!this->indirect_cache->compute_indirect(0))
        {
            lava::log()->error("Can't compute indirect cache!");

            return false;
        }
    }

    return this->create_config();
}

void VRApplication::on_destroy()
{  
    this->destroy_config();

    if (this->headset != nullptr)
    {
        this->headset->on_shutdown();
    }

    if (this->pass_timer != nullptr)
    {
        this->pass_timer->wait(this->app->device);
        this->pass_timer->destroy(this->app->device);
    }

    if (this->indirect_cache != nullptr)
    {
        this->indirect_cache->destroy();
    }

    if (this->shadow_cache != nullptr)
    {
        this->shadow_cache->destroy();
    }

    if (this->stereo_transform != nullptr)
    {
        this->stereo_transform->destroy();
    }

    if (this->frame_capture != nullptr)
    {
        this->frame_capture->destroy();
    }

    if (this->companion_window != nullptr)
    {
        this->companion_window->destroy();
    }

    if (this->scene != nullptr)
    {
        this->scene->destroy();
    }

    if (this->statistic_log != nullptr)
    {
        if (!this->statistic_log->write("statistics"))
        {
            lava::log()->error("Can't write statistic!");
        }
    }
}

bool VRApplication::on_resize()
{
    this->companion_window->destroy();

    if (!this->companion_window->create(this->app->device, this->app->target))
    {
        lava::log()->error("Can't resize companion window!");

        return false;
    }

    return true;
}

bool VRApplication::on_interface()
{
    if (this->command_parser.should_benchmark())
    {
        return true;
    }

    ImGui::SetNextWindowPos(ImVec2(30, 30), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(193, 90), ImGuiCond_FirstUseEver);

    ImGui::Begin("Application");

    if (ImGui::CollapsingHeader("Scene", ImGuiTreeNodeFlags_DefaultOpen))
    {
        ImGui::PushID("SceneInterface");

        if (!this->scene->interface())
        {
            lava::log()->error("Error during scene interface!");
            ImGui::End();

            return false;
        }

        ImGui::PopID();
    }

    if (ImGui::CollapsingHeader("Headset", ImGuiTreeNodeFlags_DefaultOpen))
    {        
        ImGui::PushID("HeadsetInterface");

        if (!this->headset->on_interface())
        {
            lava::log()->error("Error during headset interface!");
            ImGui::End();

            return false;
        }

        ImGui::PopID();
    }

    if (ImGui::CollapsingHeader("Strategy", ImGuiTreeNodeFlags_DefaultOpen))
    {
        ImGui::PushID("StrategyInterface");

        std::vector<const char*> strategy_names;

        for (StereoStrategy::Ptr strategy : this->strategies)
        {
            strategy_names.push_back(strategy->get_name());
        }

        int32_t strategy = this->active_strategy;

        if (ImGui::Combo("Stereo Strategy", &strategy, strategy_names.data(), strategy_names.size()))
        {
            this->selected_strategy = (StereoStrategyType) strategy;
        }

        if (!this->get_stereo_strategy()->on_interface())
        {
            lava::log()->error("Error during strategy interface!");
            ImGui::End();

            return false;
        }

        ImGui::PopID();
    }

    if (ImGui::CollapsingHeader("Debug"))
    {
        ImGui::PushID("DebugInterface");

        bool companion_enabled = this->companion_window->is_enabled();
        ImGui::Checkbox("Companion Window", &companion_enabled);
        this->companion_window->set_enabled(companion_enabled);

        bool capture_enabled = this->frame_capture->is_enabled();
        ImGui::Checkbox("Frame Capture", &capture_enabled);
        this->frame_capture->set_enabled(capture_enabled);

        this->frame_time->interface(128);
        this->pass_timer->interface();

        this->app->draw_about(true);

        ImGui::PopID();
    }

    ImGui::End();

    return true;
}

bool VRApplication::on_update(lava::delta delta_time)
{
    this->frame_time->add_sample(delta_time * 1000.0);

    if (this->selected_strategy.has_value())
    {
        if (!this->change_config(this->selected_strategy.value()))
        {
            return false;
        }

        this->selected_strategy.reset();
    }

    // TODO: Place the headset update before the scene update for lower latency. 
    // However this would cause the controller transformation to be delayed by one frame.
    if (!this->headset->on_update(delta_time))
    {
        lava::log()->error("Error during headset update!");

        return false;
    }

    if (this->command_parser.should_benchmark())
    {
        float time_step = 1.0f / this->command_parser.get_update_rate().value();
        
        if (!this->scene->update(time_step, this->headset))
        {
            lava::log()->error("Error during scene update!");

            return false;
        }

        if (this->scene->is_animation_complete())
        {
            return false;
        }
    }

    else
    {
        if (!this->scene->update(delta_time, this->headset))
        {
            lava::log()->error("Error during scene update!");

            return false;
        }
    }

    this->stereo_transform->set_head_to_eye_matrix(EYE_LEFT, this->headset->get_head_to_eye_matrix(EYE_LEFT));
    this->stereo_transform->set_head_to_eye_matrix(EYE_RIGHT, this->headset->get_head_to_eye_matrix(EYE_RIGHT));

    this->stereo_transform->set_projection_matrix(EYE_LEFT, this->headset->get_projection_matrix(EYE_LEFT));
    this->stereo_transform->set_projection_matrix(EYE_RIGHT, this->headset->get_projection_matrix(EYE_RIGHT));

    if (this->scene->is_animation_active() && this->scene->is_camera_active())
    {
        this->stereo_transform->set_view_matrix(this->scene->get_animation_view_matrix());
    }

    else
    {
        this->stereo_transform->set_view_matrix(this->headset->get_view_matrix());
    }

    StereoStrategy::Ptr strategy = this->get_stereo_strategy();

    if (!strategy->on_update(delta_time))
    {
        lava::log()->error("Error during strategy update!");

        return false;
    }

    return true;
}

bool VRApplication::on_render(VkCommandBuffer command_buffer, lava::index frame)
{
    this->frame_capture->next_frame(frame);
    this->pass_timer->next_frame(command_buffer, frame, this->app->device);

    this->stereo_transform->write(frame);
    this->scene->write(frame);

    if (!this->cache_filled)
    {
        if (this->shadow_cache != nullptr)
        {
            this->shadow_cache->compute_shadow(command_buffer, frame);
        }
        
        this->cache_filled = true;
    }

    this->begin_render(command_buffer, frame);

    StereoStrategy::Ptr strategy = this->get_stereo_strategy();

    if (!strategy->on_render(command_buffer, frame))
    {
        lava::log()->error("Error during strategy render!");

        return false;
    }

    this->companion_window->render(command_buffer, this->app->target, this->get_frame_index());

    this->end_render(command_buffer, frame);

    return true;
}