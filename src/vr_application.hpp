#pragma once

#include <memory>
#include <vector>
#include <array>
#include <optional>

#include <liblava/lava.hpp>
#include <glm/glm.hpp>

#include "scene.hpp"
#include "headset/headset.hpp"
#include "strategy/stereo_strategy.hpp"

#include "utility/companion_window.hpp"
#include "utility/command_parser.hpp"
#include "utility/frame_capture.hpp"
#include "utility/pass_timer.hpp"
#include "utility/stereo_transform.hpp"
#include "utility/statistic.hpp"
#include "utility/shadow_cache.hpp"
#include "utility/indirect_cache.hpp"

class VRApplication
{
public:
    typedef std::shared_ptr<VRApplication> Ptr;

public:
    VRApplication() = default;

    bool setup(lava::name name, argh::parser cmd_line);
    void run();

    Scene::Ptr get_scene() const;

    Headset::Ptr get_headset() const;
    StereoStrategy::Ptr get_stereo_strategy() const;
    StereoStrategyType get_stereo_strategy_type() const;
    
    CompanionWindow::Ptr get_companion_window() const;
    PassTimer::Ptr get_pass_timer() const;
    FrameCapture::Ptr get_frame_capture() const;
    StereoTransform::Ptr get_stereo_transform() const;

    ShadowCache::Ptr get_shadow_cache() const;          //NOTE: If shadow is disabled, nullptr is returned
    IndirectCache::Ptr get_indirect_cache() const;      //NOTE: If indirect lighting is disabled, nullptr is returned

    StatisticLog::Ptr get_statistic_log() const;

    const CommandParser& get_command_parser() const;

    lava::device_ptr get_device() const;
    lava::render_target::ptr get_target() const;

    lava::instance& get_instance();
    lava::window& get_window();
    lava::renderer& get_renderer();
    lava::input& get_input();
    lava::camera& get_camera();

    lava::index get_frame_index() const;
    uint32_t get_frame_count() const;

private:
    bool create_config();
    bool change_config(StereoStrategyType strategy);
    void destroy_config();

    void begin_render(VkCommandBuffer command_buffer, lava::index frame);
    void end_render(VkCommandBuffer command_buffer, lava::index frame);

    bool on_create();
    void on_destroy();
    bool on_resize();
    bool on_interface();
    bool on_update(lava::delta delta_time);
    bool on_render(VkCommandBuffer command_buffer, lava::index frame);

private:
    std::optional<lava::app> app;

    Scene::Ptr scene;

    Headset::Ptr headset;
    std::vector<StereoStrategy::Ptr> strategies;
    StereoStrategyType active_strategy = STEREO_STRATEGY_TYPE_NATIVE_FORWARD;
    std::optional<StereoStrategyType> selected_strategy;

    CompanionWindow::Ptr companion_window;
    PassTimer::Ptr pass_timer;
    FrameCapture::Ptr frame_capture;
    StereoTransform::Ptr stereo_transform;

    ShadowCache::Ptr shadow_cache;
    IndirectCache::Ptr indirect_cache;
    bool cache_filled = false;

    CommandParser command_parser;
    bool created = false;

    StatisticLog::Ptr statistic_log;
    Statistic::Ptr frame_time;
};